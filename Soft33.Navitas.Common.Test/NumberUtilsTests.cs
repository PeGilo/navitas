﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Soft33.Navitas.Common;

namespace Soft33.Navitas.Common.Test
{
    [TestClass]
    public class NumberUtilsTests
    {
        [TestMethod]
        public void NotNumber()
        {
            Assert.IsFalse(NumberUtils.ExtractDouble(null).HasValue);
            Assert.IsFalse(NumberUtils.ExtractDouble("").HasValue);
            Assert.IsFalse(NumberUtils.ExtractDouble(" ").HasValue);
            Assert.IsFalse(NumberUtils.ExtractDouble("xxx").HasValue);
            Assert.IsFalse(NumberUtils.ExtractDouble(",").HasValue);
            Assert.IsFalse(NumberUtils.ExtractDouble(".").HasValue);
            Assert.IsFalse(NumberUtils.ExtractDouble(",.").HasValue);
            Assert.IsFalse(NumberUtils.ExtractDouble(" , ").HasValue);
        }

        [TestMethod]
        public void Number()
        {
            Assert.AreEqual(0D, NumberUtils.ExtractDouble("0").Value);
            Assert.AreEqual(1D, NumberUtils.ExtractDouble("1").Value);
            Assert.AreEqual(123D, NumberUtils.ExtractDouble("123").Value);
            Assert.AreEqual(0.5, NumberUtils.ExtractDouble("0,5").Value);
            Assert.AreEqual(0.5, NumberUtils.ExtractDouble("0.5").Value);
            Assert.AreEqual(0.5, NumberUtils.ExtractDouble(".5").Value);
            Assert.AreEqual(0.5, NumberUtils.ExtractDouble("0,5 S").Value);
            Assert.AreEqual(0.5, NumberUtils.ExtractDouble("S 0,5").Value);
        }
    }
}
