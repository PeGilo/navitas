﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Soft33.Navitas.Common;

namespace Soft33.Navitas.Common.Test
{
    [TestClass]
    public class TextUtilsTests
    {
        [TestMethod]
        public void Empty()
        {
            Assert.AreEqual("", TextUtils.ConcatenateText());
            Assert.AreEqual("", TextUtils.ConcatenateText(""));
        }

        [TestMethod]
        public void One()
        {
            Assert.AreEqual("один", TextUtils.ConcatenateText("один"));
        }

        [TestMethod]
        public void Two()
        {
            Assert.AreEqual("один и два", TextUtils.ConcatenateText("один", "два"));
        }

        [TestMethod]
        public void Three()
        {
            Assert.AreEqual("один, два и три", TextUtils.ConcatenateText("один", "два", "три"));
        }

        [TestMethod]
        public void Four()
        {
            Assert.AreEqual("один, два, три и четыре", TextUtils.ConcatenateText("один", "два", "три", "четыре"));
        }

        [TestMethod]
        public void FourWithEmpty()
        {
            Assert.AreEqual("один, два, три и четыре", TextUtils.ConcatenateText("один", "", "два", "три", "", "четыре"));
        }
    }
}
