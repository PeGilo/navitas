﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Soft33.Navitas.Common.Collections;

namespace Soft33.Navitas.Common.Test.Collections
{
    [TestClass]
    public class DictionaryWithDefaultTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Проверить, что возвращает default value вместо не существующего
            DictionaryWithDefault<string, int> dict = new DictionaryWithDefault<string, int>(() => 100);

            dict.Add("1", 1);

            Assert.AreEqual(1, dict["1"]);
            Assert.AreEqual(100, dict["not exist"]);
        }


        [TestMethod]
        public void TestMethod2()
        {
            // Проверить, что работает со сложными объектами
            DictionaryWithDefault<string, List<string>> dict = new DictionaryWithDefault<string, List<string>>(() => new List<string>() { "a", "b", "c" });

            dict.Add("1", new List<string>() { "1" });

            Assert.AreEqual("1", dict["1"][0]);
            Assert.AreEqual("a", dict["not exist"][0]);
        }

        [TestMethod]
        public void TestMethod3()
        {
            // Проверить, что возвращает один и тот же объект, когда обращаемся несколько раз подряд.
            DictionaryWithDefault<string, List<string>> dict = new DictionaryWithDefault<string, List<string>>(() => new List<string>());

            dict["not exist"].Add("a");
            dict["not exist"].Add("b");
            dict["not exist"].Add("c");

            Assert.AreEqual(3, dict["not exist"].Count);
        }

        [TestMethod]
        public void TestMethod4()
        {
            // Обычное использование

            DictionaryWithDefault<string, List<int>> dict = new DictionaryWithDefault<string, List<int>>(() => new List<int>());

            dict.Add("123", new List<int>() { 1, 2, 3 });
            dict.Add("4567", new List<int>() { 4, 5, 6, 7 });
            dict.Add("78901", new List<int>() { 7, 8, 9, 0, 1 });

            Assert.AreEqual(3, dict["123"].Count);
            Assert.AreEqual(4, dict["4567"].Count);
            Assert.AreEqual(5, dict["78901"].Count);

            for (int i = 0; i < dict["123"].Count; i++)
            {
                Assert.AreEqual(i+1, dict["123"][i]);
            }
            
        }

        [TestMethod]
        public void TestMethod5()
        {
            // Для каждого из значений должнен быть свой объект
            DictionaryWithDefault<string, List<string>> dict = new DictionaryWithDefault<string, List<string>>(() => new List<string>());

            dict["not exist1"].Add("a");
            dict["not exist2"].Add("b");
            dict["not exist3"].Add("c");

            Assert.AreEqual(1, dict["not exist1"].Count);
            Assert.AreEqual(1, dict["not exist2"].Count);
            Assert.AreEqual(1, dict["not exist3"].Count);
        }
    }
}
