﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace Soft33.Navitas.Common
{
    public static class ReflectionHelper
    {
        public static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;

            if (memberExpression == null)
                return null;

            return memberExpression.Member.Name;
        }


        /// <summary>
        /// Ищет в объекте свойство с указанным именем типа коллекции и возвращает количество элементов, содержащихся в нем.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="collectionPropertyName"></param>
        /// <returns></returns>
        public static int GetCollectionItemsCount<T>(T obj, string collectionPropertyName)
        {
            Type t = obj.GetType();

            PropertyInfo prop = t.GetProperty(collectionPropertyName);
            if (prop != null)
            {
                object propValue = prop.GetValue(obj, null);
                PropertyInfo countProperty = propValue.GetType().GetProperty("Count", typeof(int));
                int count = (int)countProperty.GetValue(propValue, null);
                return count;
            }
            else
            {
                throw new InvalidOperationException("Объект не имеет свойства " + collectionPropertyName);
            }
        }
    }

}
