﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.Navitas.Common.Collections
{
    /// <summary>
    /// Вместо отсутствующих значений возвращает объект созданный при помощи
    /// указанного делегата.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class DictionaryWithDefault<TKey, TValue> : Dictionary<TKey, TValue>
    {
        private Func<TValue> _defaultValueFactory;
        
        //public TValue DefaultValue
        //{
        //    get { return _default; }
        //    set { _default = value; }
        //}

        //public DictionaryWithDefault() : base() { }

        //public DictionaryWithDefault(TValue defaultValue)
        //    : base()
        //{
        //    _default = defaultValue;
        //}

        public DictionaryWithDefault(Func<TValue> defaultValueFactory)
            : base()
        {
            _defaultValueFactory = defaultValueFactory;
        }

        public new TValue this[TKey key]
        {
            get {
                TValue value;
                if (base.TryGetValue(key, out value))
                {
                    return value;
                }
                else
                {
                    // В случае отсутствия сгенерировать объект и сохранит его
                    value = _defaultValueFactory();
                    base[key] = value;
                    return value;
                }
            }
            set { base[key] = value; }
        }
    }
}
