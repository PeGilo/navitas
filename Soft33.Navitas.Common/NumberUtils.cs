﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Soft33.Navitas.Common
{
    public static class NumberUtils
    {
        /// <summary>
        /// Из строки вырезает число
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static double? ExtractDouble(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return null;
            }

            Match m = Regex.Match(text, @"(\d+[\.,]\d+|\d+|[\.,]{1}\d+)");
            if (m.Success)
            {
                string t = Regex.Replace(m.Value, @"[\.|,]", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                double res;
                if (Double.TryParse(t, out res))
                {
                    return res;
                }
            }
            return null;
        }

        public static string FormatDouble(double d)
        {
            return d.ToString("0.00");
        }

        public static string FormatDouble(double d, int digits)
        {
            if (!Double.IsNaN(d) && !Double.IsInfinity(d))
            {
                return d.ToString("0." + new String('#', digits));
            }
            else
            {
                return "-";
            }
        }

        public static string FormatPercents(double d)
        {
            return d.ToString("0");
        }

        public static double Percents(double chunk, double total)
        {
            if (total != 0D)
            {
                double res = (chunk / total) * 100;
                return Double.IsInfinity(res) || Double.IsNaN(res) ? 0D : res;
            }
            return 0D;
        }
    }
}
