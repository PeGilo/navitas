﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.Navitas.Common.Enums
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class DisplayNameAttribute : System.Attribute
    {
        public string DisplayName { get; set; }
    }
}
