﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.Navitas.Common
{
    public static class TextUtils
    {
        /// <summary>
        /// Соединяет отдельные строки в предложение через запятые
        /// </summary>
        /// <param name="chunks"></param>
        /// <returns></returns>
        public static string ConcatenateText(params string[] chunks)
        {
            return ConcatenateText(chunks.AsEnumerable());
        }

        public static string ConcatenateText(IEnumerable<string> chunks)
        {
            IEnumerable<string> nonEmptyList = chunks.Where(s => !String.IsNullOrEmpty(s));

            if (nonEmptyList.Count() == 0)
            {
                return String.Empty;
            }

            string text = nonEmptyList.Aggregate((s1, s2) => s1 + ", " + s2);

            // Удалить последнюю запятую
            text = text.TrimEnd(',', ' ');

            // Заменить предпоследнюю запятую на 'и'
            int index = text.LastIndexOf(", ");
            if (index >= 0)
            {
                text = text.Remove(index, 1).Insert(index, " и");
            }

            return text;
        }
    }
}
