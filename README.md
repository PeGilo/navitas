# Data Registration app #
This web-application allows to register technical data in centralized storage. User completes several technical forms and submits data to the server.

This is an APS.NET MVC App.

Presentation layer, business logic layer and data access layer are separated and they are dependent only on interfaces. Dependency injection pattern is used to define specific usages.
Database schema is defined in Visual Studio Database project.

Developer environment: **C# (.NET Framework 3.5), Visual Studio**.

Used:

 * **ASP.NET MVC 3**;
 * **LING to SQL** - for object-relation mapping;
 * **JavaScript Minifier** - for javascript minification.