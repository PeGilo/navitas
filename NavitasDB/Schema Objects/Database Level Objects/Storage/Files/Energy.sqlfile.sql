﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILE (NAME = [Energy], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', FILEGROWTH = 1024 KB) TO FILEGROUP [PRIMARY];

