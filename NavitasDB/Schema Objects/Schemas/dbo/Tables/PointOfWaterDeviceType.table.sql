﻿CREATE TABLE [dbo].[PointOfWaterDeviceType] (
    [PointOfWaterDeviceTypeID]  INT              IDENTITY (1, 1) NOT NULL,
    [Caption]                   NVARCHAR (440)   NOT NULL,
    [PointOfWaterDeviceTypeUID] UNIQUEIDENTIFIER NOT NULL
);

