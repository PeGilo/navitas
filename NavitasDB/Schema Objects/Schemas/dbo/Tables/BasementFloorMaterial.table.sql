﻿CREATE TABLE [dbo].[BasementFloorMaterial] (
    [BasementFloorMaterialID]  INT              IDENTITY (1, 1) NOT NULL,
    [Caption]                  NVARCHAR (440)   NOT NULL,
    [ThermalResistance]        FLOAT            NOT NULL,
    [BasementFloorMaterialUID] UNIQUEIDENTIFIER NOT NULL
);

