﻿CREATE TABLE [dbo].[InspectionType] (
    [InspectionTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [Caption]          NVARCHAR (1024) NOT NULL
);

