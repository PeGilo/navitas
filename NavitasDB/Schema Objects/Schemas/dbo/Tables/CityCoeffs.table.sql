﻿CREATE TABLE [dbo].[CityCoeffs]
(
	[CityCoeffsID] int NOT NULL, 
	[CityName] nvarchar(80) NOT NULL,
	[Coeffs] XML NULL
)
