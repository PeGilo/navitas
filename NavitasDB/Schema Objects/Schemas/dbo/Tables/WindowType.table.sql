﻿CREATE TABLE [dbo].[WindowType] (
    [WindowTypeID]      INT              IDENTITY (1, 1) NOT NULL,
    [Caption]           NVARCHAR (440)   NOT NULL,
    [ThermalResistance] FLOAT            NOT NULL,
    [WindowTypeUID]     UNIQUEIDENTIFIER NOT NULL
);

