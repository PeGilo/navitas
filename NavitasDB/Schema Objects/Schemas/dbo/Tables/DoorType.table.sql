﻿CREATE TABLE [dbo].[DoorType] (
    [DoorTypeID]        INT              IDENTITY (1, 1) NOT NULL,
    [Caption]           NVARCHAR (440)   NOT NULL,
    [ThermalResistance] FLOAT            NOT NULL,
    [DoorTypeUID]       UNIQUEIDENTIFIER NOT NULL
);

