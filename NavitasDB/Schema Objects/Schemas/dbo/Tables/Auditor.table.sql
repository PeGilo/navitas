﻿CREATE TABLE [dbo].[Auditor] (
    [AuditorID]   INT            IDENTITY (1, 1) NOT NULL,
    [FullName]    NVARCHAR (1024) NOT NULL,
    [AuditorData] XML            NULL
);

