﻿CREATE TABLE [dbo].[WallMaterial] (
    [WallMaterialID]    INT              IDENTITY (1, 1) NOT NULL,
    [Caption]           NVARCHAR (440)   NOT NULL,
    [ThermalResistance] FLOAT            NOT NULL,
    [WallMaterialUID]   UNIQUEIDENTIFIER NOT NULL
);

