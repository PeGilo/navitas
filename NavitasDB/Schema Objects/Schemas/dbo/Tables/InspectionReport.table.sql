﻿CREATE TABLE [dbo].[InspectionReport]
(
	[InspectionID] int NOT NULL,
	[SectionID] int identity(1,1) NOT NULL,
	[SequenceNumber] int NOT NULL,
	[Content] XML NULL,
	[TimeStamp] timestamp
)
