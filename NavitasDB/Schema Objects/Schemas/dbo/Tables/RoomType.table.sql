﻿CREATE TABLE [dbo].[RoomType] (
    [RoomTypeID]  INT              IDENTITY (1, 1) NOT NULL,
    [Caption]     NVARCHAR (440)   NOT NULL,
    [RoomTypeUID] UNIQUEIDENTIFIER NOT NULL
);

