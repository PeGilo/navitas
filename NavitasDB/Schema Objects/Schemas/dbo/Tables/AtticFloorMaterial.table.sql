﻿CREATE TABLE [dbo].[AtticFloorMaterial] (
    [AtticFloorMaterialID]  INT              IDENTITY (1, 1) NOT NULL,
    [Caption]               NVARCHAR (440)   NOT NULL,
    [ThermalResistance]     FLOAT            NOT NULL,
    [AtticFloorMaterialUID] UNIQUEIDENTIFIER NOT NULL
);

