﻿CREATE TABLE [dbo].[PointOfWaterType] (
    [PointOfWaterTypeID]  INT              IDENTITY (1, 1) NOT NULL,
    [Caption]             NVARCHAR (440)   NOT NULL,
    [PointOfWaterTypeUID] UNIQUEIDENTIFIER NOT NULL
);

