﻿CREATE TABLE [dbo].[CaptionsRegister] (
    [CaptionsRegisterID]  INT              IDENTITY (1, 1) NOT NULL,
    [CaptionsRegisterUID] UNIQUEIDENTIFIER NOT NULL,
    [TypeID]              INT              NOT NULL,
    [Caption]             NVARCHAR (440)    NOT NULL
);

