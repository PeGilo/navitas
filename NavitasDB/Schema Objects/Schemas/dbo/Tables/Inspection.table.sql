﻿CREATE TABLE [dbo].[Inspection] (
    [InspectionID]     INT              IDENTITY (1, 1) NOT NULL,
    [InspectionGUID]   UNIQUEIDENTIFIER NOT NULL,
    [ClientCaption]    NVARCHAR (2048)   NOT NULL,
    [Client]           XML              NULL,
    [AuditorID]        INT              NOT NULL,
    [ExpertID]         INT              NULL,
    [InspectionTypeID] INT              NULL,
    [Production]       XML              NULL,
    [Intake]           XML              NULL,
    [Transport]        XML              NULL,
    [BaseYear]         INT              NULL,
    [Staff]            XML              NULL,
    [CalculationCoeffs] XML              NULL
);

