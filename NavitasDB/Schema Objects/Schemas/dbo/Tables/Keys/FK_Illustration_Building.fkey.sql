﻿ALTER TABLE [dbo].[Illustration]
    ADD CONSTRAINT [FK_Illustration_Building] FOREIGN KEY ([BuildingID]) REFERENCES [dbo].[Building] ([BuildingID]) ON DELETE SET NULL ON UPDATE NO ACTION;	