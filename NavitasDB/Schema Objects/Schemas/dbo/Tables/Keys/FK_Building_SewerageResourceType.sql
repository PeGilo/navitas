﻿
ALTER TABLE [dbo].[Building]
    ADD CONSTRAINT [FK_Building_SewerageResourceType] FOREIGN KEY ([SewerageResourceTypeID]) REFERENCES [dbo].[ResourceSupplyType] ([ResourceSupplyTypeID]) ON DELETE NO ACTION ON UPDATE NO ACTION;