﻿ALTER TABLE [dbo].[Building]
    ADD CONSTRAINT [FK_Building_WaterResourceType] FOREIGN KEY ([WaterResourceTypeID]) REFERENCES [dbo].[ResourceSupplyType] ([ResourceSupplyTypeID]) ON DELETE NO ACTION ON UPDATE NO ACTION;
