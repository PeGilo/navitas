﻿ALTER TABLE [dbo].[InpectionState]
    ADD CONSTRAINT [PK_InpectionState] PRIMARY KEY CLUSTERED ([InspectionStateID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

