﻿ALTER TABLE [dbo].[WallMaterial]
    ADD CONSTRAINT [UN_Caption_WallMaterialUID] UNIQUE NONCLUSTERED ([WallMaterialUID] ASC, [Caption] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY];

