﻿ALTER TABLE [dbo].[Building]
    ADD CONSTRAINT [FK_Building_Inspection] FOREIGN KEY ([InspectionID]) REFERENCES [dbo].[Inspection] ([InspectionID]) ON DELETE CASCADE ON UPDATE NO ACTION;

