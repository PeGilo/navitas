﻿ALTER TABLE [dbo].[MasterReportTemplate]
    ADD CONSTRAINT [FK_MasterReportTemplate_ReportTemplate] FOREIGN KEY ([MasterReportTemplateID]) REFERENCES [dbo].[ReportTemplate] ([ReportTemplateID]) ON DELETE NO ACTION ON UPDATE NO ACTION;
