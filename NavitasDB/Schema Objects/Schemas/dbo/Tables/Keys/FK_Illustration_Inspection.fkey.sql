﻿ALTER TABLE [dbo].[Illustration]
    ADD CONSTRAINT [FK_Illustration_Inspection] FOREIGN KEY ([InspectionID]) REFERENCES [dbo].[Inspection] ([InspectionID]) ON DELETE NO ACTION ON UPDATE NO ACTION;