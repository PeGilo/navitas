﻿ALTER TABLE [dbo].[Inspection]
    ADD CONSTRAINT [FK_Inspection_InspectionType] FOREIGN KEY ([InspectionTypeID]) REFERENCES [dbo].[InspectionType] ([InspectionTypeID]) ON DELETE SET NULL ON UPDATE NO ACTION;

