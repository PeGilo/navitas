﻿ALTER TABLE [dbo].[Inspection]
    ADD CONSTRAINT [FK_Inspection_Auditor] FOREIGN KEY ([AuditorID]) REFERENCES [dbo].[Auditor] ([AuditorID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

