﻿ALTER TABLE [dbo].[InspectionReport]
    ADD CONSTRAINT [FK_InspectionReport_Inspection] FOREIGN KEY ([InspectionID]) REFERENCES [dbo].[Inspection] ([InspectionID]) ON DELETE CASCADE ON UPDATE CASCADE;

