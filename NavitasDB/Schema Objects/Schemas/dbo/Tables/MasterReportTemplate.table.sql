﻿CREATE TABLE [dbo].[MasterReportTemplate]
(
	MasterReportTemplateID int NOT NULL, 
	SectionID int identity(1,1) NOT NULL,
	SequenceNumber int NOT NULL,
	RepeatOn varchar(80) NULL,
	Content XML NULL
)
