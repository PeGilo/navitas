﻿CREATE TABLE [dbo].[Illustration]
(
	IllustrationID int IDENTITY (1, 1) NOT NULL, 
	InspectionID int NOT NULL,
	BuildingID int NULL,

	DivisionName nvarchar(512) NOT NULL,
	UserTitle nvarchar(512) NOT NULL,
	[FileName] nvarchar(512) NOT NULL
)
