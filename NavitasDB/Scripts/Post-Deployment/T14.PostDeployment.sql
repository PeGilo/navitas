﻿
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1550, NULL, N'
<Section>
  <Elements>

	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Сведения об эксплуатации транспортных средств </Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Парк транспортных средств (далее -ТС) предприятия @Model.Name состоит из @Model.TransportCount единиц автомобилей, используемых для пассажироперевозок сотрудников. &lt;font color="aqua"&gt;Система учета топлива ведется через путевые листы, где указывается маршрут, количество км и тип израсходованного топлива&lt;/font&gt;. 				
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Характеристика транспортных средств представлена в таблице 9.1.
			&lt;/p&gt;
			&lt;p&gt;
				При проведении обследования были получены данные о пробеге и количестве отпущенного и израсходованного топлива.
			&lt;/p&gt;
			&lt;p&gt;
				При осмотре транспортных средств было выявлено:
				&lt;ul&gt;
					&lt;li&gt;ТС находятся в технически  исправном состоянии;&lt;/li&gt;
					&lt;li&gt;эксплуатируются в нормальных условиях;&lt;/li&gt;
					&lt;li&gt;&lt;font color="aqua"&gt;в не рабочее время находятся в теплом боксе.&lt;/font&gt;;&lt;/li&gt;
				&lt;/ul&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
					Поскольку ТС в зимнее время года находятся в теплом боксе, это сокращает расход топлива на  прогрев двигателя и составляет &lt;font color="aqua"&gt;???&lt;/font&gt; л. Среднегодовой расход  топлива составляет &lt;font color="aqua"&gt;???&lt;/font&gt; л, а затраты &lt;font color="aqua"&gt;???&lt;/font&gt; руб.
			&lt;/p&gt;
			&lt;p&gt;
					Снизить затраты на топливо  можно с помощью следующих затратных мероприятий:
				&lt;ul&gt;
					&lt;li&gt;сокращения времени прогрева и остывания двигателя в холодный период года путем утепления двигателя специальным одеялом (экономия топлива ? 10%);&lt;/li&gt;
					&lt;li&gt;контроль за передвижением ТС через навигационную систему ГЛОНАС или GPS (экономия топлива? 15%).&lt;/li&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
					Так же для снижения расхода топлива можно предложить организационные мероприятия, такие как своевременное проведение технического осмотра ТС (замены масел, фильтров, свечей зажигания и т.д.) регламентированного заводом изготовителем, а так же выполнять периодическую проверку и выставление требуемого давления в баллонах шин. Не рекомендуется  заменять узлы и агрегаты не соответствующие  заводским параметрам (например, увеличение размера шин приведет к  увеличению сопротивления воздуха и увеличению расхода топлива).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Характеристика транспортных средств</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CT.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[5])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[6])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[7])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[8])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[9])&lt;/td&gt;							
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

  </Elements>
</Section>')