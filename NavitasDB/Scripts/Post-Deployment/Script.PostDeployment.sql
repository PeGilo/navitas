﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
-- Создание аудитора
INSERT INTO dbo.[Auditor] ([FullName]) VALUES(N'Тестовый аудитор')


IF NOT EXISTS (SELECT * FROM ResourceSupplyType WHERE ResourceSupplyTypeID = 1)
BEGIN
	INSERT INTO ResourceSupplyType (ResourceSupplyTypeID, Name) VALUES (1, N'Централизованное')
END

IF NOT EXISTS (SELECT * FROM ResourceSupplyType WHERE ResourceSupplyTypeID = 2)
BEGIN
	INSERT INTO ResourceSupplyType (ResourceSupplyTypeID, Name) VALUES (2, N'Отсутствует')
END

IF NOT EXISTS (SELECT * FROM ResourceSupplyType WHERE ResourceSupplyTypeID = 3)
BEGIN
	INSERT INTO ResourceSupplyType (ResourceSupplyTypeID, Name) VALUES (3, N'Автономное')
END

DELETE WallMaterial
GO

INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('61D8951F-3093-4C44-A7CF-AE95E5A73340', N'Глиняный обыкновенный кирпич', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('29F1B6BC-F368-4B80-9CC1-9A5E09E1C22A', N'Силикатный кирпич', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('74DA9B18-0B74-4D97-B807-B5A8A6F5D032', N'Керамический пустотный кирпич', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('D6ED2D9A-7BF5-4B45-87EE-DB4CB7F7AAF8', N'Силикатный пустотный кирпич', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('DE0A672F-6F4A-4B56-83D8-0DBA7D7E0A7E', N'Шлакоблок', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('5E419FEB-61FB-43A2-A07F-7AF2517F3691', N'Железобетон', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('03EBAFDE-4C52-41E2-8D4E-AD5385B0070F', N'Бетон на гравии или щебне из природного камня', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('4B4C8EB9-2A9A-46EA-B527-2E392F47762D', N'Керамзитобетон', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('A401D770-290F-4F5E-8DAD-9E17F3FFF1F1', N'Деревянный брус', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('D300D9DF-8868-4EB5-AC10-28C815C9542D', N'Плиты древеснно-волокнистые и древеснно-стружечные (сэндвич панели)', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('15A7810B-4BD4-46DB-BFE0-37A8B2B62359', N'Al стеклопакет (тройной)', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('CFBB5855-0188-40BC-9D21-DB3CB61BA4A4', N'Аллюминий', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('62D2B7B8-39A2-46FF-9DF4-5B59FBDF0376', N'Асбестоцементные листы', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('DC738521-6C41-4A40-85B9-1061C8373CBF', N'Дерево неокрашенное', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('BC1E502E-02CD-433F-98F1-44DD44792F8B', N'Гранит, гнейс, базальт', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('274A77B6-1D7A-4CF8-ABE6-0509CD86137C', N'Мрамор', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('A36138A6-0A46-4777-B797-FE8ACEB2999E', N'Известняк', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('E4C8AF96-12E1-4257-977D-679626390559', N'Плитка облицовочная керамическая', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('FEA34D89-8C7E-440E-B5F4-D2D543243FD2', N'Сталь листовая', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('D7321D87-3FB2-4B22-9E71-1AB21A1DBF81', N'Стекло облицовочное', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('ABB92DFD-424B-44E5-A3F0-B46568F8ACCC', N'Штукатурка известковая', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('971C447B-8E95-4CFD-8DC8-F071074B7E45', N'Штукатурка цементная', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('3FE742D5-4A05-4098-A08F-15365D7F98FA', N'Маты минераловатные прошивные', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('4C87A1DE-F468-4106-9A4B-1B4E4ED1E2F7', N'Пенополистирол', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('F019392C-8389-4438-95DE-F3D50D9D7FA0', N'Листы гипсовые обшивочные', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('E3E192E6-D7B3-4772-90A6-DB98FD713360', N'Штукатурка', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('6F09BB99-5310-4E16-8CF5-E63D7CE2EEEE', N'Краска', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('35DEAEF5-B33A-4D86-84C5-3ADB6E515645', N'Обои', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('65BAAF54-5CEF-40B7-AFED-131ACA3C393C', N'Рейка деревянная', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('D07C341E-B430-4BE3-91FB-51B0DA1A4761', N'Кафель', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('E36AE337-2763-45FC-AC99-BDA18D596E08', N'Панели ПВХ', 0)
INSERT INTO WallMaterial ([WallMaterialUID], Caption, ThermalResistance) VALUES ('EF26FD5C-A8C6-47BD-8A3C-0860F8A11736', N'другое:', 0)
GO

DELETE AtticFloorMaterial
GO

INSERT INTO AtticFloorMaterial ([AtticFloorMaterialUID], Caption, ThermalResistance) VALUES ('863C1E8B-4D48-496A-84D0-E0E46D782DFB', N'Ж/Б плита пустотная', 0)
INSERT INTO AtticFloorMaterial ([AtticFloorMaterialUID], Caption, ThermalResistance) VALUES ('B33B5F29-3A9A-441C-ACDC-871C4DECF026', N'Деревянный брус', 0)
INSERT INTO AtticFloorMaterial ([AtticFloorMaterialUID], Caption, ThermalResistance) VALUES ('A0810A09-054D-4251-8D80-67B8429E00FE', N'Маты минераловатные прошивные', 0)
INSERT INTO AtticFloorMaterial ([AtticFloorMaterialUID], Caption, ThermalResistance) VALUES ('631C4530-DD7D-467E-8D22-8EDA9FD9200F', N'Пенополистирол', 0)
INSERT INTO AtticFloorMaterial ([AtticFloorMaterialUID], Caption, ThermalResistance) VALUES ('BE1990A6-9AD5-4389-9D1C-F33EAB5FF4E1', N'Гравий керамзитный', 0)
INSERT INTO AtticFloorMaterial ([AtticFloorMaterialUID], Caption, ThermalResistance) VALUES ('579DF0DF-2845-43AC-9ED4-8A8AB0A1E2A8', N'Шлак', 0)
INSERT INTO AtticFloorMaterial ([AtticFloorMaterialUID], Caption, ThermalResistance) VALUES ('ACF54824-CD0C-41DB-B972-052A6673A909', N'другое:', 0)
GO

DELETE BasementFloorMaterial
GO

INSERT INTO BasementFloorMaterial ([BasementFloorMaterialUID], Caption, ThermalResistance) VALUES ('22EC2942-274B-40D3-A868-65582DF7FFAF', N'Ж/Б плита пустотная', 0) 
INSERT INTO BasementFloorMaterial ([BasementFloorMaterialUID], Caption, ThermalResistance) VALUES ('38C77F70-67C7-469F-8CFD-F9C7DFFD2506', N'Деревянный брус', 0)
INSERT INTO BasementFloorMaterial ([BasementFloorMaterialUID], Caption, ThermalResistance) VALUES ('715A12BA-E077-4493-A6E5-7933AE707F64', N'Линолеум', 0.35)
INSERT INTO BasementFloorMaterial ([BasementFloorMaterialUID], Caption, ThermalResistance) VALUES ('D68EF5F6-BB6C-49AD-B1E2-B750C0D10B6A', N'Керамическая плитка', 0.3)
GO

DELETE PointOfWaterDeviceType
GO

INSERT INTO PointOfWaterDeviceType ([PointOfWaterDeviceTypeUID], Caption) VALUES ('781DE3CC-F6A2-4963-9173-F58661BCB3CB', N'Смеситель') 
INSERT INTO PointOfWaterDeviceType ([PointOfWaterDeviceTypeUID], Caption) VALUES ('ED1D725E-706C-4015-81A6-A35548615065', N'Смывной бачок')
GO

DELETE PointOfWaterType
GO

INSERT INTO PointOfWaterType ([PointOfWaterTypeUID], Caption) VALUES ('21176CA5-B1B7-40FF-91D4-12E48A4CDD60', N'Ванная') 
INSERT INTO PointOfWaterType ([PointOfWaterTypeUID], Caption) VALUES ('1FA86F4D-AF12-48D1-A398-75941D845F26', N'Душевая')
INSERT INTO PointOfWaterType ([PointOfWaterTypeUID], Caption) VALUES ('2585B291-DCC2-46E4-8819-3996542B9ABA', N'Кухня')
INSERT INTO PointOfWaterType ([PointOfWaterTypeUID], Caption) VALUES ('CC8AC5F3-5D88-4A1B-910B-B642D265B538', N'Столовая')
INSERT INTO PointOfWaterType ([PointOfWaterTypeUID], Caption) VALUES ('4A70F80E-581C-43E9-8295-8C64A612ABFB', N'Туалет')
GO

DELETE RoomType
GO

INSERT INTO RoomType ([RoomTypeUID], Caption) VALUES ('BD518CD6-D5C4-4F74-A948-507BC1B9822E', N'Кабинет')
INSERT INTO RoomType ([RoomTypeUID], Caption) VALUES ('503E16C7-49FB-477B-B031-4B155576D7B1', N'Коридор')
INSERT INTO RoomType ([RoomTypeUID], Caption) VALUES ('057BA8C8-6C01-4788-82BB-F3F23ABC0FBB', N'Лестница')
INSERT INTO RoomType ([RoomTypeUID], Caption) VALUES ('9665BE4C-FEFA-43B5-B808-BCE62197630C', N'Туалет')
INSERT INTO RoomType ([RoomTypeUID], Caption) VALUES ('1BD2AEE8-7280-4CC2-9F3F-E09EAFD56945', N'ИТП')
INSERT INTO RoomType ([RoomTypeUID], Caption) VALUES ('158F05C3-D5F2-42D3-A808-8A394E5CCE2B', N'Щитовая')
GO

DELETE WindowType
GO

INSERT INTO WindowType (Caption, ThermalResistance, WindowTypeUID) VALUES (N'Деревянные с раздельным двойным остекленением', 0, '2D2B4F85-BDB2-4CE9-B1C8-830D1738DB53')
INSERT INTO WindowType (Caption, ThermalResistance, WindowTypeUID) VALUES (N'Деревянные с одинарным остекленинием', 0, '60502525-25B5-4186-A293-10E11A89F1EF')
INSERT INTO WindowType (Caption, ThermalResistance, WindowTypeUID) VALUES (N'Трехкамерный стеклопакет (ПВХ-профиль)', 0, '663BCC3E-D120-4DD9-AB21-6F8921F97524')
INSERT INTO WindowType (Caption, ThermalResistance, WindowTypeUID) VALUES (N'Ленточное остекление по металлическому профилю', 0, 'DA2FDAA3-8739-4293-BF19-5D54D94939C8')
INSERT INTO WindowType (Caption, ThermalResistance, WindowTypeUID) VALUES (N'другое:', 0, '63FAE4CC-B081-4C0D-8441-C5213BFCFB35')
GO

DELETE DoorType
GO

INSERT INTO DoorType (Caption, ThermalResistance, DoorTypeUID) VALUES (N'Деревянные двери', 0, '2147638C-003D-49BA-8F28-454F8B397991')
INSERT INTO DoorType (Caption, ThermalResistance, DoorTypeUID) VALUES (N'Металлические двери', 0, 'D1F8E6B3-8D1B-46FE-A011-C7584F599898')
INSERT INTO DoorType (Caption, ThermalResistance, DoorTypeUID) VALUES (N'Двери из ПВХ профиля', 0, 'BABEE16E-B0CA-418C-AB89-0BF21EA778A7')
GO

--INSERT INTO FoundationType (Caption, ThermalResistance, FoundationTypeUID) VALUES ('Столбчатый фундамент', 0, '7C83C3E1-CC1B-498F-8A78-263B5A2189BA')
--INSERT INTO FoundationType (Caption, ThermalResistance, FoundationTypeUID) VALUES ('Плитный фундамент', 0, '2D3283AD-E6AD-4130-9237-98DFA0DC9573')
--INSERT INTO FoundationType (Caption, ThermalResistance, FoundationTypeUID) VALUES ('Ленточный фундамент', 0, '3BEA177F-CC7B-4AFA-AE3C-C9169C7F63C1')
--INSERT INTO FoundationType (Caption, ThermalResistance, FoundationTypeUID) VALUES ('Блочный фундамент', 0, '27C88E35-BBA9-4D1B-8239-795D35F31CC1')
--INSERT INTO FoundationType (Caption, ThermalResistance, FoundationTypeUID) VALUES ('нет', 0, '14142800-E6C1-4B42-AED0-119D8B708AF2')
--INSERT INTO FoundationType (Caption, ThermalResistance, FoundationTypeUID) VALUES ('другое:', 0, 'BBC4A6BB-1C04-4A46-8E34-06B41EDAC1C5')
--GO

DELETE CaptionsRegister
GO

INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('EE8DF956-4858-499C-9AAF-41A13A6FE259', 1, N'Энергомера ЦЭ6803 В')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('313FBF1C-5DB2-4266-A95F-D168BE99E3CF', 1, N'СА4У-И672')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('21B98677-7F00-4E17-84FF-1AA12C5EDA07', 1, N'ТРИО')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('C71FE8F3-ADB8-4A1C-B17F-0344042D26E6', 1, N'СЭТ3а')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('7E7E6253-267C-44ED-904E-0607270FC40F', 1, N'Меркурий 230 АМ-02')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('D38A0DFC-0A38-4337-8A57-5CE52B011C5B', 1, N'СА4У-И678М')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('10E9F67A-C9CB-4A37-B9FD-7058ACBF0138', 1, N'СА4-И678')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('23743665-9810-4AD6-BEC4-45A6FB8486C9', 1, N'ЦЭ6803ВМ')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('66114130-725F-476E-BF5F-0BB5B579DAD5', 1, N'СЭТ3а-02-34-03/1П')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('BB150930-11A5-492E-8265-C6C51AB480C1', 1, N'ЦЭ6807БК')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('63882B8B-48E5-4934-9E24-54EE27A36EC4', 6, N'ВРУ')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('D7D58A84-67C3-4CFC-8AC3-A7363F41C388', 6, N'ТП')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('D2D76D39-9999-4891-941C-A27586AEF125', 11, N'0,5 S')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('316505E7-78E1-49DB-BF11-DEA69BB98844', 11, N'0,5')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('8D3CE0E2-A8AA-4A34-BB87-3FAE2B468481', 11, N'1')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('B1C8CD28-E919-40A5-851E-FC38A0129663', 11, N'2')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)

VALUES ('EF134988-BA78-42E5-8CEB-8AA5321E692E', 2, N'ОСВ-25')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('24C9971B-BFCD-416F-A26C-052072260BF2', 2, N'ОСВ-40')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('EBD76839-6AFC-45C3-86AD-2C3740F33428', 2, N'СКБ-40')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('034D90E4-A965-4B7C-8F6A-831D341C797A', 2, N'СГВ-15')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('F8505A51-3FE7-4908-A52F-332C89FFC4F2', 2, N'СГВ-20')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('FEB7BAED-E019-405B-9E09-4A05D9389E18', 2, N'СХВ-15')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('CB222BF4-4D80-4337-B33D-8A9387625889', 2, N'СВМ-25')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('A74E5495-039C-49FA-9904-C361E02A371D', 2, N'ВСХ-15')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('43130028-EFA1-48CB-953A-05FF730DCD96', 2, N'ВСХ-20')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('539DFA83-17EF-4FF3-BCE1-3DA71D5CA11C', 2, N'Бетар')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('79BC0C4E-BDDF-4D70-AE3E-A21E4E2AF3F5', 2, N'Minol')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('ACE49389-94B8-4B58-8C0B-A18834802643', 2, N'ITELMA')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('8CA39B1E-D63A-4108-AB72-3EBF43DA7E37', 2, N'ZR')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('56E2B2ED-90CC-4789-9A4A-9736FBC18387', 12, N'A1')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('726074AF-4013-4F9E-81F4-B3C836AEA447', 12, N'B2')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('92029DC4-FB80-4D21-A32A-315A70C69FE6', 12, N'C3')

INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('EF134988-BA78-42E5-8CEB-8AA5321E692E', 3, N'ОСВ-25')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('24C9971B-BFCD-416F-A26C-052072260BF2', 3, N'ОСВ-40')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('EBD76839-6AFC-45C3-86AD-2C3740F33428', 3, N'СКБ-40')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('034D90E4-A965-4B7C-8F6A-831D341C797A', 3, N'СГВ-15')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('F8505A51-3FE7-4908-A52F-332C89FFC4F2', 3, N'СГВ-20')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('FEB7BAED-E019-405B-9E09-4A05D9389E18', 3, N'СХВ-15')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('CB222BF4-4D80-4337-B33D-8A9387625889', 3, N'СВМ-25')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('A74E5495-039C-49FA-9904-C361E02A371D', 3, N'ВСХ-15')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('43130028-EFA1-48CB-953A-05FF730DCD96', 3, N'ВСХ-20')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('539DFA83-17EF-4FF3-BCE1-3DA71D5CA11C', 3, N'Бетар')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('79BC0C4E-BDDF-4D70-AE3E-A21E4E2AF3F5', 3, N'Minol')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('ACE49389-94B8-4B58-8C0B-A18834802643', 3, N'ITELMA')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('8CA39B1E-D63A-4108-AB72-3EBF43DA7E37', 3, N'ZR')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('56E2B2ED-90CC-4789-9A4A-9736FBC18387', 13, N'A1')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('726074AF-4013-4F9E-81F4-B3C836AEA447', 13, N'B2')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('92029DC4-FB80-4D21-A32A-315A70C69FE6', 13, N'C3')

INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('510136B0-EFD9-417B-B924-99066C75875C', 4, N'Теплоизмеритель 1')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('498320D1-564A-43F0-B9B6-00ED5591AC4D', 4, N'Теплоизмеритель 2')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('F10F3CEC-0ABB-4820-AC91-3EECEBEFBA7B', 14, N'Класс теплоизмерителя 1')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('9CC9C0E0-4B79-48AB-9FA3-5EAB0B5E354A', 14, N'Класс теплоизмерителя 2')

INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('B8B9BAF6-8A7A-47BC-8C05-C80502C2B26A', 5, N'Газоизмеритель 1')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('F6A08D4F-B9A5-4139-8871-09DF67065763', 5, N'Газоизмеритель 2')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('9E681766-355D-4A83-8BB1-7FF3CF8941BE', 15, N'Класс газоизмерителя 1')
INSERT INTO dbo.CaptionsRegister (CaptionsRegisterUID, TypeID, Caption)
VALUES ('441CD008-725F-46D9-BD21-F816B758E69F', 15, N'Класс газоизмерителя 2')
GO

DELETE [CityCoeffs]
GO

INSERT INTO [CityCoeffs] ([CityCoeffsID], [CityName], [Coeffs]) VALUES (1, N'Красноярск', 
	N'<CityCoeffsValues>
  <a>0.9</a>
  <t0>-40.0</t0>
  <w0>3.8</w0>
  <Tout_ventin>-40.0</Tout_ventin>
  <Tout_ventincalc>-7.1</Tout_ventincalc>
  <Zh_default>240</Zh_default>
  <Tint_default>20</Tint_default>
  <Tout_default>-7.1</Tout_default>
  <Tout_cp>
    <double>-18.2</double>
    <double>-16.8</double>
    <double>-7.8</double>
    <double>2.6</double>
    <double>9.4</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>9.4</double>
    <double>1.5</double>
    <double>-8.8</double>
    <double>-16.3</double>
  </Tout_cp>
	<HeatingDaysByMonth>
		<int>31</int>
		<int>28</int>
		<int>31</int>
		<int>30</int>
		<int>11</int>
		<int>0</int>
		<int>0</int>
		<int>0</int>
		<int>11</int>
		<int>31</int>
		<int>30</int>
		<int>31</int>
	</HeatingDaysByMonth>
	  </CityCoeffsValues>')
INSERT INTO [CityCoeffs] ([CityCoeffsID], [CityName], [Coeffs]) VALUES (2, N'Абакан', 
	N'<CityCoeffsValues>
  <a>0.9</a>
  <t0>-40.0</t0>
  <w0>2.8</w0>
  <Tout_ventin>-40.0</Tout_ventin>
  <Tout_ventincalc>-9.7</Tout_ventincalc>
  <Zh_default>225</Zh_default>
  <Tint_default>20</Tint_default>
  <Tout_default>-9.7</Tout_default>
  <Tout_cp>
    <double>-25.2</double>
    <double>-18.5</double>
    <double>-8.5</double>
    <double>2.9</double>
    <double>10.5</double>
    <double>17.3</double>
    <double>19.5</double>
    <double>16.4</double>
    <double>9.9</double>
    <double>1.6</double>
    <double>-9.5</double>
    <double>-17.9</double>
  </Tout_cp>
	<HeatingDaysByMonth>
		<int>31</int>
		<int>28</int>
		<int>31</int>
		<int>30</int>
		<int>11</int>
		<int>0</int>
		<int>0</int>
		<int>0</int>
		<int>11</int>
		<int>31</int>
		<int>30</int>
		<int>31</int>
	</HeatingDaysByMonth>
	  </CityCoeffsValues>')
INSERT INTO [CityCoeffs] ([CityCoeffsID], [CityName], [Coeffs]) VALUES (3, N'Ачинск', 
	N'<CityCoeffsValues>
  <a>0.9</a>
  <t0>-41.0</t0>
  <w0>4.7</w0>
  <Tout_ventin>-40.0</Tout_ventin>
  <Tout_ventincalc>-7.6</Tout_ventincalc>
  <Zh_default>237</Zh_default>
  <Tint_default>20</Tint_default>
  <Tout_default>-7.6</Tout_default>
  <Tout_cp>
    <double>-17.7</double>
    <double>-15.6</double>
    <double>-9.1</double>
    <double>0.4</double>
    <double>8.6</double>
    <double>15.6</double>
    <double>17.9</double>
    <double>15.0</double>
    <double>9.0</double>
    <double>0.6</double>
    <double>-9.3</double>
    <double>-16.3</double>
  </Tout_cp>
	<HeatingDaysByMonth>
		<int>31</int>
		<int>28</int>
		<int>31</int>
		<int>30</int>
		<int>11</int>
		<int>0</int>
		<int>0</int>
		<int>0</int>
		<int>11</int>
		<int>31</int>
		<int>30</int>
		<int>31</int>
	</HeatingDaysByMonth>
	  </CityCoeffsValues>')
GO

DELETE [CalculationCoeffs]
GO

INSERT INTO [CalculationCoeffs] ([CalculationCoeffsID], [Name], [Coeffs]) VALUES (1, N'Теплоснабжение', 
	N'<CalculationThermoCoeffs>
		<tox>5</tox>
		<toxl>15</toxl>
		<tDHW>55</tDHW>
		<p>1</p>
		<cw>4.2</cw>
		<Khi>0.3</Khi>
		<Knm>1.05</Knm>
		<HoursPerMonth>
			<int>744</int>
			<int>672</int>
			<int>744</int>
			<int>720</int>
			<int>744</int>
			<int>720</int>
			<int>744</int>
			<int>744</int>
			<int>720</int>
			<int>744</int>
			<int>720</int>
			<int>744</int>
		</HoursPerMonth>
	  </CalculationThermoCoeffs>')

INSERT INTO [CalculationCoeffs] ([CalculationCoeffsID], [Name], [Coeffs]) VALUES (2, N'Финансы', 
	N'<CalculationFinanceCoeffs>
		<HeatReflectingLayerCost>280.0</HeatReflectingLayerCost>
	  </CalculationFinanceCoeffs>')
GO


IF NOT EXISTS (SELECT * FROM [dbo].[ReportTemplate] WHERE [ReportTemplateID] = 1)
BEGIN
	INSERT INTO [dbo].[ReportTemplate] ([ReportTemplateID], [Name]) VALUES (1, N'Главный шаблон отчета')
END
GO

DELETE [dbo].[MasterReportTemplate] 
GO

DELETE [dbo].[InspectionReport] 
GO


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1, NULL, N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;Некоммерческое партнерство «Содействие регламентации в области энергосбережения и энергоэффективности топливно-энергетических ресурсов Сибири»&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
		&lt;hr align="center" width="80%" /&gt;
	  </Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;&lt;font size="2"&gt;(наименование саморегулируемой организации)&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;ООО «СибЭнергоСбережение 2030»&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
		&lt;hr align="center" width="80%" /&gt;
	  </Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;&lt;font size="2"&gt;(наименование организации (лица), проводившего энергетическое обследование)&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;&lt;font size="5"&gt;ОТЧЕТ № __________________&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;&lt;font size="4"&gt;о проведении энергетического обследования зданий и сооружений @Model.FullName, расположенного по адресу: @Model.JuridicalAddress&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
		&lt;hr align="center" width="80%" /&gt;
	  </Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;&lt;font size="2"&gt;(наименование обследованной организации (объекта))&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;&lt;font size="3"&gt;Составлен по результатам обязательного энергетического обследования&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>



	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="right"&gt;&lt;font size="3"&gt;Веретенников А. А.&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
		&lt;hr align="right" width="40%" /&gt;
	  </Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="right"&gt;&lt;font size="2"&gt;(Директор ООО «СибЭнергоСбережение 2030»)&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>


	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="right"&gt;&lt;font size="3"&gt;@Model.Head.FullName&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
		&lt;hr align="right" width="40%" /&gt;
	  </Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="right"&gt;&lt;font size="2"&gt;@Model.Head.Position @Model.Name&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;&lt;font size="3"&gt;@Model.Month, @Model.Year г. &lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 4, NULL, N'<Section  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<Elements>
	<SectionTitleElement>
      <Id>333</Id>
      <Hidden>false</Hidden>
      <Numbered>false</Numbered>
      <Text>Нормативные ссылки</Text>
    </SectionTitleElement>
	<EditableTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
		&lt;ol&gt;
			&lt;li&gt;Федеральный закон №261-ФЗ «Об энергосбережении и о повышении энергетической эффективности и о внесении изменений в отдельные законодательные акты Российской Федерации» от 23 ноября 2009 года.&lt;/li&gt;
			&lt;li&gt;Приказ Министерства регионального развития РФ от 28 мая 2010 г. № 262 «О требованиях энергетической эффективности зданий, строений, сооружений».&lt;/li&gt;
			&lt;li&gt;ГОСТ 30494-96 «Здания жилые и общественные. Параметры микроклимата в помещениях».&lt;/li&gt;
			&lt;li&gt;ГОСТ 26629-85 «Метод тепловизионного контроля качества теплоизоляции ограждающих конструкции».&lt;/li&gt;
			&lt;li&gt;ГОСТ 26254-84 «Здания и сооружения. Методы определения сопротивления теплопередаче ограждающих конструкций».&lt;/li&gt;
			&lt;li&gt;ГОСТ 26629-85 «Метод тепловизионного контроля качества теплоизоляции ограждающих конструкции».&lt;/li&gt;
			&lt;li&gt;СНиП 2-04-01-85 «Внутренний водопровод и канализация зданий».&lt;/li&gt;
			&lt;li&gt;СНиП 41-01-2003 «Отопление, вентиляция и кондиционирование».&lt;/li&gt;
			&lt;li&gt;СНиП 31-06-2009 «Общественные здания и сооружения».&lt;/li&gt;
			&lt;li&gt;СНиП 23-01-99* «Строительная климатология».&lt;/li&gt;
			&lt;li&gt;СНиП 23-02-2003 «Тепловая защита зданий».&lt;/li&gt;
			&lt;li&gt;СП 23-101-2004 «Проектирование тепловой защиты зданий».&lt;/li&gt;
			&lt;li&gt;СП 52.13330.2011 «Естественное и искусственное освещение».&lt;/li&gt;
			&lt;li&gt;СанПиН 2.1.2.2645-10 «Санитарно-эпидимиологические требования к условиям проживания в жилых зданиях и помещениях».&lt;/li&gt;
			&lt;li&gt;СанПиН 2.2.4.548-96 «Гигиенические требования к микроклимату производственных помещений».&lt;/li&gt;
			&lt;li&gt;РД 153-34.0-20.363-99 «Методика инфракрасного контроля электрооборудования и ВЛ».&lt;/li&gt;
			&lt;li&gt;ВСН-53-86(Р) «Ведомственные строительные нормы».&lt;/li&gt;
			&lt;li&gt;Правила устройства электроустановок.&lt;/li&gt;
			&lt;li&gt;МДС 41-2.2000* «Методика определения количества тепловой энергии и теплоносителя в водяных системах коммунального теплоснабжения».&lt;/li&gt;
			&lt;li&gt;МДС 13-20.2004 «Комплексная методика по обследованию и энергоаудиту реконструируемых зданий. Пособие по проектированию».&lt;/li&gt;
		&lt;/ol&gt;
	  </Content>
	</EditableTextElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 7, NULL, N'<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>false</Numbered>
      <Text>Техническое задание на проведение работ по обязательным энергетическим обследованиям</Text>
    </SectionTitleElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
		    &lt;p&gt;&lt;strong&gt;1. Цель работы:&lt;/strong&gt;&lt;/p&gt;
						&lt;p&gt;1.1 Оценка эффективности функционирования систем энергоснабжения организаций.&lt;/p&gt;
						&lt;p&gt;1.2 Разработка энергетического паспорта и отчета на основе проведенного энергетического обследования. Разработка рекомендаций и технических решений по рациональному использованию энергии с оценкой предполагаемых затрат, необходимых для   реализации намечаемых мероприятий, и возможных сроков окупаемости.&lt;/p&gt;
			&lt;p&gt;&lt;strong&gt;2. Содержание основных работ:&lt;/strong&gt;&lt;/p&gt;
						&lt;p&gt;&lt;u&gt;2.1 Система  электроснабжения&lt;/u&gt;&lt;&lt;/p&gt;
							&lt;p&gt;2.1.1 Анализ договорных условий на электроснабжение.&lt;/p&gt;
							&lt;p&gt;2.1.2 Анализ объема потребления электроэнергии за последние 5 лет и динамики его  изменения.&lt;/p&gt;
							&lt;p&gt;2.1.3 Анализ схемы электроснабжения организации, технического состояния электрооборудования и внутренних электрических сетей, освещения.&lt;/p&gt;
							&lt;p&gt;2.1.4 Анализ загрузки и режима работы оборудования элекктропотребляющего оборудования.&lt;/p&gt;
							&lt;p&gt;2.1.5 Оценка состояния приборов учета расхода электроэнергии.&lt;/p&gt;
							&lt;p&gt;2.1.6 Проведение контрольных замеров параметров качества электроэнергии.&lt;/p&gt;
							&lt;p&gt;2.1.7 Выполнение тепловизионной съемки распределительных устройств системы электроснабжения.&lt;/p&gt;
							&lt;p&gt;2.1.8 Анализ фактического и нормативного удельного расхода электроэнергии.&lt;/p&gt;
							&lt;p&gt;2.1.9 Составление баланса прихода-расхода электроэнергии.&lt;/p&gt;
							&lt;p&gt;2.1.10 Разработка мероприятий по рациональному использованию электрической энергии с оценкой их эффективности, объема затрат на их внедрение и расчетом срока окупаемости.&lt;/p&gt;
						&lt;p&gt;&lt;u&gt;2.2 Система теплоснабжения&lt;/u&gt;&lt;/p&gt;
							&lt;p&gt;2.2.1 Анализ договорных условий на теплоснабжение.&lt;/p&gt;
							&lt;p&gt;2.2.2 Оценка состояния приборов учета расхода тепловой энергии.&lt;/p&gt;
							&lt;p&gt;2.2.3 Анализ схемы теплоснабжения и технического состояния оборудования теплоснабжения (отопления, ГВС, приточной вентиляции).&lt;/p&gt;
							&lt;p&gt;2.2.4 Оценка состояния и эффективности работы теплопотребляющего оборудования.&lt;/p&gt;
							&lt;p&gt;2.2.5 Тепловизионная съемка элементов системы теплоснабжения.&lt;/p&gt;
							&lt;p&gt;2.2.6 Оценка фактической и нормативной тепловой нагрузки (на отопление, ГВС, приточную вентиляцию) и их сравнение.&lt;/p&gt;
							&lt;p&gt;2.2.7 Анализ удельной тепловой характеристики.&lt;/p&gt;
							&lt;p&gt;2.2.8 Разработка мероприятий по рациональному использованию тепловой энергии с оценкой их эффективности, объема затрат на их внедрение и расчетом срока окупаемости.&lt;/p&gt;
						&lt;p&gt;&lt;u&gt;2.3  Система водоснабжения и водоотведения&lt;/u&gt;&lt;/p&gt;
							&lt;p&gt;2.3.1 Анализ договорных условий на водоснабжение и водоотведение.&lt;/p&gt;
							&lt;p&gt;2.3.2 Перечень и характеристика оборудования водоснабжения и водоотведения.&lt;/p&gt; 
							&lt;p&gt;2.3.3 Анализ системы учета и контроля водоснабжения и водоотведения.&lt;/p&gt;
							&lt;p&gt;2.3.4 Анализ фактических и нормативных удельных расходов воды.&lt;/p&gt;
							&lt;p&gt;2.3.5 Разработка мероприятий по рациональному использованию холодного водоснабжения с оценкой их эффективности и объема затрат на их внедрение.&lt;/p&gt;
						&lt;p&gt;&lt;u&gt;2.4 Ограждающие конструкции &lt;/u&gt;&lt;/p&gt;
							&lt;p&gt;2.4.1 Визуальное обследование здания и ограждающих конструкций.&lt;/p&gt;
							&lt;p&gt;2.4.2 Тепловизионная съемка ограждающих конструкций.&lt;/p&gt;
							&lt;p&gt;2.4.3 Определение теплотехнических характеристик здания.&lt;/p&gt;
							&lt;p&gt;2.4.4 Разработка мероприятий по повышению энергоэффективности,  оценка их стоимости и срока окупаемости.&lt;/p&gt;
						&lt;p&gt;&lt;u&gt;2.5  Транспортный парк &lt;/u&gt;&lt;/p&gt;
							&lt;p&gt;2.5.1 Анализ работы транспортных средств.&lt;/p&gt;
							&lt;p&gt;2.5.2 Оценка и анализ расхода топлива на работу транспортных средств.&lt;/p&gt;
							&lt;p&gt;2.5.3 Разработка мероприятий по рациональному использованию топлива.&lt;/p&gt;
						&lt;p&gt;&lt;u&gt;2.6  Составление энергетического паспорта.&lt;/u&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
&lt;table class="simple"&gt;
&lt;tr&gt;
&lt;td&gt;№ этапа&lt;/td&gt;&lt;td&gt;Содержание этапов&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;1&lt;/td&gt;
&lt;td&gt;Сбор необходимой технической  и прочей документации (технические условия, копии договоров с РСО, схемы энергоснабжения). Сбор необходимых экономических данных (фактическое потребление ТЭР, платежные документы). Составление схем энергоснабжения и учета энергоресурсов.
&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;2&lt;/td&gt;
&lt;td&gt;Анализ потребления энергоресурсов по направлениям (теплоснабжение, горячее водоснабжение, электроснабжение, холодное водоснабжение).&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;3&lt;/td&gt;&lt;td&gt;Визуальное и инструментальное обследование объекта.&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;4&lt;/td&gt;&lt;td&gt;Анализ данных, полученных при визуальном и инструментальном обследовании. Определение показателей нормативных расходов ТЭР. Определение удельных показателей фактических и нормативных расходов ТЭР. Расчет потенциала энергосбережения и повышения энергетической эффективности объекта.&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;5&lt;/td&gt;&lt;td&gt;Составление плана рекомендуемых мероприятий по энергосбережению и повышению энергетической эффективности объекта, расчет окупаемости.&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;6&lt;/td&gt;&lt;td&gt;Обобщение результатов обследования. Оформление результатов энергетического обследования и сдача Заказчику.&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
	  </Content>
    </ReadonlyTextElement>

	<EditableTextElement>
		<Id>2</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;strong&gt;3. Перечень и комплектность результатов работы&lt;/strong&gt;&lt;/p&gt;
						&lt;p&gt;3.1. Отчет о проведенном энергетическом обследовании, включающий в себя:&lt;/p&gt;
							&lt;p&gt;- исходные статистические и технические данные по обследуемому объекту и результаты их анализа;&lt;/p&gt;
							&lt;p&gt;- результаты тепловизионного обследования ограждающих конструкций;&lt;/p&gt;
							&lt;p&gt;- заверенные ответственным лицом объекта обследования справки о потреблении топливно-энергетических ресурсов (ТЭР) за последние пять лет (2011-2007гг);&lt;/p&gt;
							&lt;p&gt;- результаты тепловизионного обследования энергетических коммуникаций здания;&lt;/p&gt;
							&lt;p&gt;- результаты  теплотехнического расчета ограждающих конструкций;&lt;/p&gt;
							&lt;p&gt;- фотографии имеющихся в наличие у объекта узлов коммерческого учета ТЭР и водоснабжения;&lt;/p&gt;
							&lt;p&gt;- результаты расчета потенциала энергосбережения ТЭР и водоснабжения;&lt;/p&gt;
							&lt;p&gt;- перечень организационно-технических мероприятий по повышению эффективности потребления ТЭР и водоснабжения на объекте;&lt;/p&gt;
							&lt;p&gt;- оценку экономии ТЭР и водоснабжения  по всем  рекомендованным энергосберегающим мероприятиям;&lt;/p&gt;
							&lt;p&gt;- оценку стоимости реализации предложенных мероприятий и период их окупаемости.&lt;/p&gt;
						&lt;p&gt;3.2. Энергетический паспорт, оформленный в соответствии с Приказом Минэнерго РФ от 19.04.2010 № 182 «Об утверждении требований к энергетическому паспорту, составленному по результатам обязательного энергетического обследования, и энергетическому паспорту, составленному на основании проектной документации, и правил направления копии энергетического паспорта, составленного по результатам обязательного энергетического обследования». &lt;/p&gt;
	  	</Content>
	</EditableTextElement>
<EditableTextElement>
		<Id>3</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;Отчет и энергетический паспорт, разработанные по результатам обязательного энергетического обследования, передаются Государственному Заказчику в двух экземплярах на бумажном носителе и в одном экземпляре на электронном носителе.&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 10, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>false</Numbered>
      <Text>Введение</Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;Энергетическое обследование здания 
			@Model.FullName, @Model.JuridicalAddress
			&lt;/p&gt;выполнено на основании:&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;ul&gt;
				&lt;li&gt;Федерального закона от 23.11.2009 № 261-ФЗ «Об энергосбережении и повышении энергетической эффективности и о внесении изменений в отдельные законодательные акты РФ»;&lt;/li&gt;
				&lt;li&gt;договора № ___________ от __________ между @Model.FullName (далее по тексту - Заказчик) и ООО "СибЭнергоСбережение 2030". &lt;/li&gt;
			&lt;/ul&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
		&lt;p&gt;Энергетическое обследование (энергоаудит) Объекта проводилось в _______________ 2012 г. аттестованными специалистами - энергоаудиторами ООО "СибЭнергоСбережение 2030".&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;В соответствии с Техническим заданием на проведение работ и нормативными документами по энергоаудиту цели энергетического обследования зданий Объекта состояли в:&lt;/p&gt;
			&lt;ul&gt;
				&lt;li&gt;получение объективных данных об объеме используемых топливных и энергетических ресурсов на объектах обследования;&lt;/li&gt;
				&lt;li&gt;определение показателей энергетической эффективности;&lt;/li&gt;
				&lt;li&gt;определение потенциала энергосбережения и повышения энергетической эффективности;&lt;/li&gt;
				&lt;li&gt;разработка перечня типовых, общедоступных мероприятий по энергосбережению и повышению энергетической эффективности и проведение их стоимостной оценки;&lt;/li&gt;
				&lt;li&gt;разработка энергетического паспорта.&lt;/li&gt;
			&lt;/ul&gt;
			&lt;p&gt;Энергоаудит включал в себя документальное и инструментальное обследование, а также аналитический этап по обработке полученной информации, в завершение которых оформлены отчет с программой энергосбережения и энергетический паспорт Объекта.&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
		&lt;p&gt;Объектом обследования являлась инженерная система энергообеспечения здания Объекта, а также потребители энергоресурсов. В соответствии с методологией энергоаудита и техническим заданием к договору рассматривались следующие системы видов ресурсов и конструкции:&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;ul&gt;
				&lt;li&gt;система электроснабжения;&lt;/li&gt;
				&lt;li&gt;система теплоснабжения;&lt;/li&gt;
				&lt;li&gt;система водоснабжения и водоотведения;&lt;/li&gt;
				@if(Model.HasVentilation) {
				&lt;li&gt;система вентиляции;&lt;/li&gt;
				}
				&lt;li&gt;ограждающие конструкции.&lt;/li&gt;
			&lt;/ul&gt;
		</Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;Работа выполнена при информационном и организационном содействии руководства, специалистов и @Model.FullName, которым выражается признательность.&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 13, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Общие сведения об объекте энергетического обследования</Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;@Model.FullName, расположенное по адресу: @Model.JuridicalAddress&lt;/p&gt;
			&lt;p align="center"&gt;(полное наименование организации)&lt;/p&gt;
			
				- Организационно-правовая форма @Model.OPF&lt;br/&gt;
				- Юридический адрес @Model.JuridicalAddress&lt;br/&gt; 
				- Фактический адрес @Model.PhysicalAddress&lt;br/&gt;
				- Форма собственности ___________________________&lt;br/&gt;
				- Наименование основного общества (для дочерних (зависимых) обществ) @Model.NameOfParentCompany&lt;br/&gt;
				- Доля государственной (муниципальной) собственности, % (для акционерных обществ) @Model.ShareOfOwnership&lt;br/&gt;
				- Банковские реквизиты, ИНН/КПП: @Model.CurrentAccount, @Model.INN, @Model.KPP&lt;br/&gt;
				- Код по ОКВЭД (с расшифровкой) @Model.OKVED @Model.OKVEDDescription&lt;br&gt;
				- @Model.Head.FullName, @Model.Head.Position
				&lt;p align="center"&gt;(Ф.И.О., должность руководителя)&lt;/p&gt;
			
			
				- @Model.TechnicalPerson.FullName, @Model.TechnicalPerson.Position, @Model.TechnicalPerson.Phone, @Model.TechnicalPerson.Fax&lt;br/&gt;
				&lt;p align="center"&gt;(Ф.И.О., должность, телефон, факс должностного лица, ответственного за техническое состояние оборудования)&lt;/p&gt;
				- @Model.EnergyPerson.FullName, @Model.EnergyPerson.Position, @Model.EnergyPerson.Phone, @Model.EnergyPerson.Fax&lt;br/&gt;
				&lt;p align="center"&gt;(Ф.И.О., должность, телефон, факс должностного лица, ответственного за энергетическое хозяйство)&lt;/p&gt;
			
			&lt;p&gt;
				В состав Объекта, расположенного по адресу: @Model.JuridicalAddress, входит @Model.BuildingsCount здания(-е):&lt;br/&gt;	
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;ul&gt;
				@for (int i = 0; i &lt; @Model.Buildings.Count; i++)
				{
					if (i == @Model.Buildings.Count - 1)
					{
						&lt;li&gt;@Model.Buildings[i].Name, @Model.Buildings[i].PhysicalAddress, по назначению – @Model.Buildings[i].Purpose.&lt;/li&gt;
					}
					else
					{
						&lt;li&gt;@Model.Buildings[i].Name, @Model.Buildings[i].PhysicalAddress, по назначению – @Model.Buildings[i].Purpose;&lt;/li&gt;
					}
				}
			&lt;/ul&gt;	
		</Content>
	</ReadonlyTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 16, 'Buildings', N'
<Section>
  <Elements>
	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["общий вид"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["общий вид"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;Общий вид @Model.Buildings[%INDEX%].Name&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует иллюстрация общего вида здания @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>
  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 19, NULL, N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Основные характеристики зданий, входящего в объект энергетического обследования приведены в таблице 1.1-1.&lt;font color="aqua"&gt;2&lt;/font&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 22, 'Buildings', N'
<Section>
  <Elements>
	<TableElement>
		<Id>1</Id>
		<TableName>Основные характеристики здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].GeneralInfoTable.GeneralInfoTable;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt;
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 25, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Объем и структура потребляемых энергоресурсов</Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Здание потребляет из систем централизованного ресурсоснабжения:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;ul&gt;
					&lt;li&gt;
					электроэнергию – на освещение, силовую нагрузку (@Model.ElectricEnergyConsumers);
					&lt;/li&gt;
				@if (Model.UsingHeatEnergy)
				{
					&lt;li&gt;
					тепловую энергию – @Model.HeatEnergyConsumers;
					&lt;/li&gt;
				}
				@if (@Model.UsingWater)
				{
					&lt;li&gt;
					воду хозяйственно-питьевого назначения – для сантехнического использования – а также услуги по водоотведению (канализации).
					&lt;/li&gt;
				}
			&lt;/ul&gt;	
		</Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Для осуществления основной деятельности объектом обследования использованы энергоресурсы в объеме, приведенном в таблице 2.1. Здесь же указаны суммарные платежи за ресурсы. 
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Объёмы энергопотребления и суммы платежей</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CECAP.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0)
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
						}
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Из представленных видов энергетических и материальных ресурсов все являются покупными, собственная генерация отсутствует.
			&lt;/p&gt;
			&lt;p&gt;
				Для анализа структуры потребления энергоресурсов выполнен их перевод в условное топливо (у. т.) с использованием коэффициентов (значения коэффициентов приняты по диаграмме пересчета энергетических величин, представленной в Приложении № 1):
				&lt;ul&gt;
					&lt;li&gt;для электроэнергии – 0,123 т у. т./ тыс. кВт*ч;&lt;/li&gt;
					&lt;li&gt;для тепловой энергии – 0,143 т у. т./Гкал.&lt;/li&gt;
				&lt;/ul&gt;
			&lt;/p&gt;
			&lt;p&gt;
				Структура платежей (%) за потребленные в @Model.BaseYear г. объектом ресурсы, показана на рисунке 2.1
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>Структура платежей в @Model.BaseYear г.</ChartTitle>
	  <DataContent>Model.ChartPaymentStructure.Chart</DataContent>
	</ChartElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Предприятием активно используется электроэнергия, тепловая энергия и вода; как видно из диаграммы  и  в  
				стоимостном  выражении - электроэнергия составляет ___%, тепловая энергия – ___%, ХВС – ___%, ГВС – ___% и водоотведение – ___% соответственно. Эти энергоносители являются основными для объекта и 
				требуют соответствующего повышенного внимания к эффективности их расходования. 
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Динамика энергопотребления</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				В таблице 2.2 приведена динамика энергопотребления за последние 5 лет (@Model.BaseYear_4 - @Model.BaseYear гг.)
				и на рисунке 2.2 приведен график динамики потребления энергетических ресурсов. 
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Динамика энергопотребления</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CECD.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td rowspan="2"&gt;Энергоресурс&lt;/td&gt;
						&lt;td rowspan="2"&gt;Единица измерения&lt;/td&gt;
						&lt;td colspan="5"&gt;Годовое потребление энергоресурсов&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;@Model.BaseYear_4&lt;/td&gt;
						&lt;td&gt;@Model.BaseYear_3&lt;/td&gt;
						&lt;td&gt;@Model.BaseYear_2&lt;/td&gt;
						&lt;td&gt;@Model.BaseYear_1&lt;/td&gt;
						&lt;td&gt;@Model.BaseYear&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 0; i &lt; table.Rows.Count - 1; i += 2) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td rowspan="2"&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[6])&lt;/td&gt;
						&lt;/tr&gt;

						&lt;tr class="@RowTypeClass(table.Rows[i+1].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i+1].Cells[1]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i+1].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i+1].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i+1].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i+1].Cells[5])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i+1].Cells[6])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>График динамики потребления энергетических ресурсов</ChartTitle>
	  <DataContent>Model.ChartEnergyConsumptionDynamic.Chart</DataContent>
	</ChartElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Диаграмма иллюстрирует неравномерное потребление всех энергоресурсов. В 20__ и 20__ гг. наблюдалось наибольший объем потребления ХВС по сравнению с остальными годами. В 20__ г наблюдается наименьшее суммарное потребление энергоресурсов. 
				В 20__ г потребление электроэнергии в 2 раза превышает потребление электроэнергии в 20__ г.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>


  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 28, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Numbered>true</Numbered>
		<Text>Характеристика системы электроснабжения</Text>
    </SectionTitleElement>
    
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Анализ договорных условий</Text>
    </SubsectionTitleElement>
	
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt; 
				ВСТАВЬТЕ АНАЛИЗ ДОГОВОРНЫХ ОТНОШЕНИЙ
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt; 
				ПРИ НАЛИЧИИ АКТА РАЗДЕЛА ГРАНИЦ ВСТАВЬТЕ ИЛЛЮСТРАЦИЮ ВМЕСТО ЭТОГО ТЕКСТА.
			&lt;/p&gt;
		</Content>
	</ReadonlyTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 34, NULL, N'
<Section>
  <Elements>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Характеристика приборов учета электроэнергии</Text>
    </SubsectionTitleElement>
  </Elements>
</Section>')
		
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 37, 'Buildings', N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Коммерческий учет потребляемой электроэнергии зданием @Model.Buildings[%INDEX%].Name осуществляется по показаниям следующих электросчетчиков:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Список приборов электроучета здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CEEML.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["Приборы учета электроэнергии"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["Приборы учета электроэнергии"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;@ill.UserTitle&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует иллюстрация прибора учета электроэнергии @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 40, NULL, N'
<Section>
  <Elements>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Анализ режима потребления электроэнергии</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Данные по фактическому потреблению электроэнергии на обследуемом объекте, за период с 
				@Model.BaseYear_4 по @Model.BaseYear гг., представлены в таблице 3.3.1. 
				Все данные соответствуют опросным листам, заполненным работниками бухгалтерии обследуемого объекта.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Потребление электроэнергии на объекте за @Model.BaseYear_4 - @Model.BaseYear гг.</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CEEC.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0)
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
						}
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

   <EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;В таблице 3.3.2 приведены сведения о потреблении электроэнергии по месяцам за @Model.BaseYear год.&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Сведения о помесячном электропотреблении на объекте в @Model.BaseYear г.</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CEECBM.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0)
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
						}
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;Данные таблицы 3.3.2 позволяют проследить динамику потребления электроэнергии на объекте за @Model.BaseYear г., которая изображена на рисунке 3.3.1.&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>Динамика электропотребления на объекте за @Model.BaseYear г.</ChartTitle>
	  <DataContent>Model.ChartElectricEnergyConsumptionDynamic.Chart</DataContent>
	</ChartElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;На графике наблюдается неравномерное потребление электроэнергии. В зимние месяцы наблюдается увеличение потребления.&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Перечень и  характеристика электрооборудования</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				В обследуемых зданиях @Model.BuildingsList потребителями электроэнергии являются: @Model.ElectricEnergyConsumers.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				В связи с тем, что на объекте нет фактических данных по годовому потреблению элек-троэнергии каждым электроприемником, Wn, кВт*ч\год, выполняем расчет данного потребления, по следующей формуле:
			&lt;/p&gt;
			&lt;center&gt;Wn= NH*Tp,&lt;/center&gt;&lt;br/&gt;
			где &lt;br/&gt;
			NH - номинальная мощность, кВт (по паспорту электроприемника);&lt;br/&gt;
			Тр – количество часов работы электроприемника в год, ч (согласно  данным Заказчика).&lt;br/&gt;
			&lt;p&gt;
				Результаты расчета, перечень и характеристика электроприемников зданий, представлены в таблице(-ах).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 43, 'Buildings', N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Перечень и характеристика электроприемников здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CEEMD.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[5])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Суммарная расчетная мощность электропотребления в здании @Model.Buildings[%INDEX%].Name за @Model.BaseYear г., составила @Model.Buildings[%INDEX%].TotalElectricEnergyConsumption кВт*ч.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 46, NULL, N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>18</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Для снижения  потребления электрической энергии системой освещения объекта, рекомендуется выполнить замену ламп накаливания на энергосберегающие лампы (подробнее см. раздел 8.1 «Мероприятия по экономии электроэнергии»).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Характеристика системы освещения</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Несмотря на то, что осветительное оборудование является не самым энергоемким потребителем электроэнергии, на обследуемом Объекте расчетный годовой расход электроэнергии системой освещения составляет @Model.LightsEnergyConsumptionByYear кВт•ч, при установленной мощности @Model.LightsPower кВт.
			&lt;/p&gt;
			&lt;p&gt;
				&lt;font color="aqua"&gt;Управление осветительными приборами на объекте осуществляется вручную.&lt;/font&gt;
			&lt;/p&gt;
			&lt;p&gt;
				Характеристика системы освещения обследуемого объекта представлена в таблице 3.5.1.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<TableElement>
		<Id>1</Id>
		<TableName>Характеристика системы освещения</TableName>
		<Content>@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CLSC.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;Наименование здания&lt;/td&gt;
						&lt;td&gt;Площадь здания, м&lt;sup&gt;2&lt;/sup&gt;&lt;/td&gt;
						&lt;td&gt;Лампы накаливания, шт.&lt;/td&gt;
						&lt;td&gt;Люминесцентные лампы, шт.&lt;/td&gt;
						&lt;td&gt;Энергосберегающие лампы, шт&lt;/td&gt;
						&lt;td&gt;Точечные светильники, шт.&lt;/td&gt;
						&lt;td&gt;Общая мощность световых приборов, кВт&lt;/td&gt;
						&lt;td&gt;Удельный расход электроэнергии на освещение, Вт/м&lt;sup&gt;2&lt;/sup&gt;&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt;
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[5])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[6])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[7])&lt;/td&gt;
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>	
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Для снижения  потребления электрической энергии системой освещения объекта, рекомендуется выполнить замену ламп накаливания на энергосберегающие лампы (подробнее см. раздел 8.1 «Мероприятия по экономии электроэнергии»).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Баланс электрической энергии</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Полученные данные от Заказчика и результаты собственного анализа позволяют составить баланс прихода - расхода электроэнергии  на объекте.
			&lt;/p&gt;
			&lt;p&gt;
				Баланс электроэнергии, с группировкой потребителей по направлениям использования, приведен в таблице 3.6.1 и на рисунке 3.6.1.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Баланс электроэнергии</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CEEB.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>Структура расходной части баланса электроэнергии </ChartTitle>
	  <DataContent>Model.ChartElectricEnergyConsumptionStructure.Chart</DataContent>
	</ChartElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				В производственном расходе, самое большое потребление электроэнергии приходится на @Model.MostConsumingElEnergyEquipment (@Model.MostConsumingElEnergyPercent от объема электроэнергии, поступающей в его электросеть).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 49, NULL, N'
<Section>
  <Elements>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Анализ фактических и нормативных расходов электроэнергии</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Согласно, Методическому указанию по расчету расхода ТЭР для зданий жилищно-гражданского назначения, норму удельного расхода электроэнергии на единицу площади здания вычисляют путем деления соответствующего суммарного расхода электроэнергии за определенный период на полезную площадь здания.
			&lt;/p&gt;
			&lt;p&gt;
				Фактический удельный расход электроэнергии вычисляется путем деления фактического потребления электроэнергии (по отчетным данным) за определенный период на полезную площадь здания.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 52, 'Buildings', N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Полезная площадь здания @Model.Buildings[%INDEX%].Name, согласно паспорта БТИ, составляет @Model.Buildings[%INDEX%].BuildingYardage м2.
			&lt;/p&gt;
			&lt;p&gt;
				Суммарный месячный расход электроэнергии, согласно проведенным расчетам в подразделе 3.4, составляет @Model.Buildings[%INDEX%].TotalElectricEnergyConsumption кВт*ч.
			&lt;/p&gt;
			&lt;p&gt;
				Значения фактического и нормативного удельных расходов электроэнергии приведены в таблице 3.7..
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Фактический и нормативный удельные расходы электроэнергии зданием @Model.Buildings[%INDEX%].Name за @Model.BaseYear г.</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CSEC.SpecificEnergyConsumptionTable;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;							
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				На рисунке 3.7.1 представлена диаграмма сравнения фактического помесячного удельного расхода электроэнергии зданием @Model.Buildings[%INDEX%].Name с нормативным  удельным расходом электроэнергии.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>Фактический и нормативный удельные расходы электроэнергии зданием @Model.Buildings[%INDEX%].Name</ChartTitle>
	  <DataContent>Model.Buildings[%INDEX%].ChartSpecificEnergyConsumption.Chart</DataContent>
	</ChartElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Из выше представленного графика видно, что расход электроэнергии в @Model.BaseYear г. @Model.Buildings[%INDEX%].MoreOrLessConsumption
				нормативные значения удельного расхода электроэнергии в среднем на @Model.Buildings[%INDEX%].AverageSpreadBetweenNormAndFactConsumption кВт*ч\м2. 
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 55, NULL, N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Одним из резервов снижения удельного расхода электроэнергии на 1 м2 полезной площади является оптимизация работы электропотребляющего оборудования и системы освещения Объекта. Мероприятия по экономии электроэнергии представлены в разделе 8.1.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Инструментальное и тепловизионное обследование</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				Так как электроснабжения зданий объекта осуществляется по 3-й категории надежности, то техническое состояние оборудования выходит на первое место в обеспечении надежности и бесперебойности электроснабжения. Этими же факторами во многом определяются технологические потери электроэнергии в системе электроснабжения.
			&lt;/p&gt;
			&lt;p&gt;    
				В ходе оценки качества электрической энергии в рамках энергетического обследования были получены следующие графики:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 58, 'Buildings', N'
<Section>
  <Elements>
  
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p align="center"&gt;    
				Здание @Model.Buildings[%INDEX%].Name
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ChartStackElement>
	  <Id>1</Id>
	  <ChartTitle>Качество электроэнергии в здании @Model.Buildings[%INDEX%].Name</ChartTitle>
	  <DataContent>Model.Buildings[%INDEX%].Charts</DataContent>
	</ChartStackElement>


  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 59, 'Buildings', N'
<Section>
  <Elements>
	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["ВРУ"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["ВРУ"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;@ill.UserTitle&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует иллюстрация ВРУ @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 61, NULL, N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;    
				По графику напряжения видно отклонение значения напряжения от Uном (220 Вольт). 
				Максимальное отклонение принимает значение @Model.MaxDeviationUnom Вольт, 
				что составляет @Model.MaxDeviationUnomInPercents % от Uном. Это @Model.MaxDeviationUnomComplies величина, так как критическое отклонение не может 
				превышать величину ±10% от Uном. Из графика также видно, что напряжение симметрично. В целом, система электроснабже-ния находится в удовлетворительном состоянии.
			&lt;/p&gt;
			&lt;p&gt;
				Данные показатели соответствуют норме качества электрической энергии в системах электроснабжения общего назначения.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				На рисунках 3.8.2 – 3.8… представлены результаты фото и тепловые съемки вводно-распределительного устройства (ВРУ).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 64, 'Buildings', N'
<Section>
  <Elements>
  
	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["Термограммы"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["Термограммы"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;@ill.UserTitle&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует термограмма здания @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 67, NULL, N'
<Section>
  <Elements>
	
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Развит дефект. Значение избыточной температуры составляет 00,00С.  Нагрев провода обусловлен повышенным переходным сопротивлением в местах соединения провода и клеммы рубильника, что является ненормальным режимом работы. Необходим учащенный контроль.
			&lt;/p&gt;
			&lt;p&gt;
				Анализ состояния электрооборудования по результатам тепловизионного обследования показывает, что часть элементов обследуемого оборудования имеет характерные дефекты, вы-званные аномальным нагревом токоведущих частей. Дефекты такого типа приводят к потерям электроэнергии, обусловленным нагревом токоведущих частей обследуемого электрооборудо-вания. В дальнейшем эти дефекты могут вызвать нарушения в работе основного оборудования организации, а также к перерывам в электроснабжении, а в критической ситуации к пожару.
			&lt;/p&gt;
			&lt;p&gt;
				Рекомендации по устранению дефектов:
				&lt;ul&gt;
				&lt;li&gt;перераспределить равномерно нагрузку по фазам;&lt;/li&gt;
				&lt;li&gt;проверка наличия цепи и восстановление качества контактных соединений устройства выравнивания потенциалов.&lt;/li&gt;
				&lt;/ul&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 70, NULL, N'
<Section>
  <Elements>

	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Характеристика системы теплоснабжения</Text>
    </SectionTitleElement>

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Анализ договорных условий на теплоснабжение</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Теплоснабжение ________________________________ (абонента) - централизованное и осуществляется по договору номер __________ от __________ (дата) с ___________________________________________ (поставщик).
			&lt;/p&gt;
			&lt;p&gt;
				Согласно данного договора, расчетное количество потребляемой тепловой энергии абонентом составляет _________ Гкал. 
				Максимальная потребляемая тепловая мощность составляет ___________ Гкал/час, в том числе: на отопление _______________ Гкал\ч; на ГВС ____________ Гкал\ч; на вентиляцию ____________ Гкал/час.
			&lt;/p&gt;


			&lt;p&gt;
				Границей обслуживания между _______________________________________ (название поставщика) и потребителем _______________________________________ (название абонента) по ул. _______________________________ является наружная стена тепловой камеры ТК ____________________________ (номер камеры).
			&lt;/p&gt;
			&lt;p&gt;
				ОПИСАНИЕ балансовой принадлежности, если есть в договоре.
			&lt;/p&gt;
			&lt;p&gt;
				Схема раздела границ ответственности сторон по балансовой принадлежности представлена на рисунке 4.1.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<ReadonlyTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt; 
				ПРИ НАЛИЧИИ АКТА РАЗДЕЛА ГРАНИЦ ВСТАВЬТЕ ИЛЛЮСТРАЦИЮ ВМЕСТО ЭТОГО ТЕКСТА.
			&lt;/p&gt;
		</Content>
	</ReadonlyTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Расчет стоимости полученной тепловой энергии абонентом производится по тарифу  в размере ________ руб. за 1 Гкал (по договору). Определение объема потребленной тепловой энергии абонентом за расчетный период выполняется расчетным методом.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>



  </Elements>
</Section>')

-- Закомментировано согласно Work Item ID=64

--<SubsectionTitleElement>
--	<Id>1</Id>
--	<Hidden>false</Hidden>
--	<Text>Характеристика приборов учета тепловой энергии</Text>
--</SubsectionTitleElement>

-- Закомментировано согласно Work Item ID=64

--INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
--VALUES (1, 70, 'Buildings', N'
--<Section>
--  <Elements>
  
--	<EditableTextElement>
--		<Id>1</Id>
--		<Hidden>false</Hidden>
--		<Content>
--			&lt;p align="center"&gt;Здание @Model.Buildings[%INDEX%].Name &lt;/p&gt;
--		</Content>
--	</EditableTextElement>


--	<EditableTextElement>
--		<Id>1</Id>
--		<Hidden>false</Hidden>
--		<Content>
--			&lt;p&gt;
--				Границей обслуживания между _______________________________________ (название поставщика) и потребителем _______________________________________ (название абонента) по ул. _______________________________ является наружная стена тепловой камеры ТК ____________________________ (номер камеры).
--			&lt;/p&gt;
--			&lt;p&gt;
--				ОПИСАНИЕ балансовой принадлежности, если есть в договоре.
--			&lt;/p&gt;
--			&lt;p&gt;
--				Схема раздела границ ответственности сторон по балансовой принадлежности представлена на рисунке 4.1.
--			&lt;/p&gt;
--		</Content>
--	</EditableTextElement>

--	<ReadonlyTextElement>
--		<Id>1</Id>
--		<Hidden>false</Hidden>
--		<Content>
--			&lt;p&gt; 
--				ПРИ НАЛИЧИИ АКТА РАЗДЕЛА ГРАНИЦ ДЛЯ ЗДАНИЯ @Model.Buildings[%INDEX%].Name ВСТАВЬТЕ ИЛЛЮСТРАЦИЮ ВМЕСТО ЭТОГО ТЕКСТА.
--			&lt;/p&gt;
--		</Content>
--	</ReadonlyTextElement>
--  </Elements>
--</Section>')

-- Закомментировано согласно Work Item ID=64

	--<EditableTextElement>
	--	<Id>1</Id>
	--	<Hidden>false</Hidden>
	--	<Content>
	--		&lt;p&gt;
	--			Расчет стоимости полученной тепловой энергии абонентом производится по тарифу  в размере ________ руб. за 1 Гкал (по договору). Определение объема потребленной тепловой энергии абонентом за расчетный период выполняется расчетным методом.
	--		&lt;/p&gt;
	--	</Content>
	--</EditableTextElement>

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 73, NULL, N'
<Section>
  <Elements>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Характеристика приборов учета тепловой энергии</Text>
    </SubsectionTitleElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 76, 'Buildings', N'
<Section>
  <Elements>
  
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Характеристики прибора учета тепловой энергии и его комплектующих, установленных в здании @Model.Buildings[%INDEX%].Name, указаны в таблице 4.1.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Характеристики прибора учета тепловой энергии и его комплектующих здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CHeatML.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;							
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[5])&lt;/td&gt;							
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[6])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[7])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[8])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				На рисунках 4.2 - 4.3 представлены фотографии приборов.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["Приборы учета тепла"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["Приборы учета тепла"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;@ill.UserTitle&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует фотография прибора учета тепла здания @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 79, NULL, N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				ВЫВОДЫ ПО ПРИБОРАМ УЧЕТА ТЕПЛА
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 82, 'Buildings', N'
<Section>
  <Elements>
  
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Характеристики приборов учета ГВС, установленных в здании @Model.Buildings[%INDEX%].Name, указаны в таблице 4.2.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Характеристики приборов учета ГВС здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CHotWaterML.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;							
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[5])&lt;/td&gt;							
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[6])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[7])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[8])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[9])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				На рисунках 4.2 - 4.3 представлены фотографии приборов.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["Приборы учета горячей воды"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["Приборы учета горячей воды"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;@ill.UserTitle&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует фотография прибора учета горячей воды здания @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 85, NULL, N'
<Section>
  <Elements>

  	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Характеристика системы теплоснабжения</Text>
    </SubsectionTitleElement>

	<ReadonlyTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="blue"&gt;
				Здесь необходимо описать тепловые вводы, схемы присоединения отопления, ГВС, приточной вентяляции.
				&lt;/font&gt;
			&lt;/p&gt;
		</Content>
	</ReadonlyTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 88, 'Buildings', N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="blue"&gt;
				Система отопления в здании @Model.Buildings[%INDEX%].Name &lt;br/&gt;
				исполнение: двухтрубное\однотрубное&lt;br/&gt;
				разводка: нижняя\верхняя\стояковая&lt;br/&gt;
				&lt;/font&gt;			
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 91, NULL, N'
<Section>
  <Elements>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Характеристика оборудования системы теплоснабжения</Text>
    </SubsectionTitleElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 94, 'Buildings', N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				В ИТП здания @Model.Buildings[%INDEX%].Name предусмотрено следующее оборудование и приборы КИП:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

	<TableElement>
		<Id>1</Id>
		<TableName>Характеристики прибора учета тепловой энергии и его комплектующих здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CHeatML.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;							
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[5])&lt;/td&gt;							
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[6])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[7])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[8])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				На рисунке 4.4 представлена фотография и тепловизионная съемка ИТП.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["ИТП"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["ИТП"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;@ill.UserTitle&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует иллюстрация ИТП здания @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>

	<RequiredImageElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["Тепловизионная съемка ИТП"].Count &gt; 0) {
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["Тепловизионная съемка ИТП"]) {
					&lt;div class="required-illustration"&gt;
					&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
					&lt;div class="required-illustration-caption"&gt;@ill.UserTitle&lt;/div&gt;
					&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
					&lt;/div&gt;
				}
			}
			else{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует иллюстрация тепловизионной съемки ИТП здания @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="blue"&gt;
				Здесь вставить описание утстановленного оборудования.
				&lt;/font&gt;			
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="blue"&gt;
				Здесь вставить описание результатов тепловизионной съёмки.
				&lt;/font&gt;			
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="blue"&gt;
				Здесь вставить дополнительные фотографии и описание к ним.
				&lt;/font&gt;			
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="blue"&gt;
				Здесь описать состояние трубопроводов.
				&lt;/font&gt;			
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="blue"&gt;
ЕСЛИ ТЕПЛОВОЙ ПУНКТ В ПЛОХОМ СОСТОЯНИИ (УСТАРЕВШИЙ):&lt;br/&gt;
РЕКОМЕНДОВАТЬ- автоматику, изоляцию, независимую схему отопления, насосное смешение, насосы, ЧРП, регуляторы расхода.
				&lt;/font&gt;			
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 100, NULL, N'
<Section>
  <Elements>

  	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Анализ фактического и нормируемого потребления тепловой энергии</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Данные по фактическому потреблению тепловой энергии на обследуемом объекте, за период с
				@Model.BaseYear_4 по @Model.BaseYear гг., представлены в таблицах 4.3,4.4. 
				Все данные соответствуют опросным листам, заполненным работниками бухгалтерии обследуемого объекта.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

	<TableElement>
		<Id>1</Id>
		<TableName>Потребление тепловой энергии на объекте за @Model.BaseYear_4 – @Model.BaseYear гг.</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CHEC.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0)
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
						}
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
	</TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				В таблице 4.4 приведены сведения о потреблении тепловой энергии по месяцам и по видам, за @Model.BaseYear год. Эти сведения позволяют проследить динамику потребления, которая изображена на рисунке 4.12.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Динамика энергопотребления</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CHECBM.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td rowspan="2"&gt;Месяц&lt;/td&gt;
						&lt;td colspan="2"&gt;Отопление&lt;/td&gt;
						&lt;td colspan="2"&gt;ГВС&lt;/td&gt;
						&lt;td colspan="2"&gt;Вентиляция&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;@table.Rows[1].Cells[1]&lt;/td&gt;
						&lt;td&gt;@table.Rows[1].Cells[2]&lt;/td&gt;
						&lt;td&gt;@table.Rows[1].Cells[3]&lt;/td&gt;
						&lt;td&gt;@table.Rows[1].Cells[4]&lt;/td&gt;
						&lt;td&gt;@table.Rows[1].Cells[5]&lt;/td&gt;
						&lt;td&gt;@table.Rows[1].Cells[6]&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 2; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[6])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>Динамика потребления тепловой энергии за @Model.BaseYear г.</ChartTitle>
	  <DataContent>Model.ChartHeatEnergyConsumptionDynamic.Chart</DataContent>
	</ChartElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				ВЫВОД ПО ПОТРЕБЛЕНИЮ ТЕПЛОВОЙ ЭНЕРГИИ
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Для анализа фактических данных потребления тепловой энергии объектом за @Model.BaseYear г. и сравнения их с нормативными тепловыми нагрузками, выполним расчет часовых нагрузок на отопление, на ГВС и на вентиляцию.
			&lt;/p&gt;
		</Content>
	</EditableTextElement> 

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Расчет нормативной тепловой нагрузки на отопление ведется согласно МДС 41-2.2000 «Методика определения количества тепловой энергии и теплоносителя в водяных системах коммунального теплоснабжения».
			&lt;/p&gt;

			&lt;p&gt;
				Расчетная часовая  тепловая нагрузка на отопление здания, по укрупненным показателям Q&lt;sub&gt;omax&lt;/sub&gt;, Гкал/ч, рассчитывается по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_Qomax.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где α - поправочный коэффициент, учитывающий отличие расчетной температуры наружного воздуха для проектирования отопления, от t&lt;sub&gt;o&lt;/sub&gt; = -30°С, принимается по МДС 41-2.2000 (прил.1, табл.2);
			&lt;p&gt;
				V - объем здания по наружному обмеру, м&lt;sup&gt;3&lt;/sup&gt;, принимается по паспорту БТИ здания;
			&lt;/p&gt;
			&lt;p&gt;
				q&lt;sub&gt;o&lt;/sub&gt; - удельная отопительная характеристика здания при от t&lt;sub&gt;o&lt;/sub&gt; = -30°С, ккал/м&lt;sup&gt;3&lt;/sup&gt;ч°С, принимается по МДС 41-2.2000 (табл.3);
			&lt;/p&gt;
			&lt;p&gt;
				t&lt;sub&gt;j&lt;/sub&gt; - усредненная расчетная температура внутреннего воздуха в отапливаемых помещениях здания,°С. Принимается по ГОСТ 30494-96 (табл.1,2) или СанПиН 2.2.4.548-96  (табл.2);
			&lt;/p&gt;
			&lt;p&gt;
				t&lt;sub&gt;o&lt;/sub&gt; - расчетная температура наружного воздуха для проектирования отопления, °С, принимается по СНиП 23-01-99.
			&lt;/p&gt;
			&lt;p&gt;
				K&lt;sub&gt;н.р.&lt;/sub&gt; - расчетный коэффициент инфильтрации, обусловленной тепловым и ветровым напором, т.е. соотношение тепловых потерь зданием с инфильтрацией и теплопередачей через наружные ограждения при температуре наружного воздуха. Коэффициент инфильтрации рассчитывается по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_KHP.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где g – ускорение свободного падения, м/с&lt;sup&gt;2&lt;/sup&gt;, g=9,81 м/с&lt;sup&gt;2&lt;/sup&gt;;
			&lt;p&gt;
				L - высота здания, м, принимается по паспорту БТИ здания;
			&lt;/p&gt;
			&lt;p&gt;
				t&lt;sub&gt;o&lt;/sub&gt; – расчетная температура наружного воздуха для проектирования отопления, °С, принимается по СНиП 23-01-99;
			&lt;/p&gt;
			&lt;p&gt;
				t&lt;sub&gt;вн&lt;/sub&gt; - усредненная расчетная температура внутреннего воздуха в отапливаемых помещениях здания,°С. Принимается по ГОСТ 30494-96 (табл.1, 2) или СанПиН 2.2.4.548-96 (табл.2);
			&lt;/p&gt;
			&lt;p&gt;
				w&lt;sub&gt;o&lt;/sub&gt; – скорость ветра, м/с, принимается по СНиП 23-01-99;
			&lt;/p&gt;
			&lt;p&gt;
				Средняя часовая тепловая нагрузка на отопление здания Q&lt;sub&gt;ср.о.&lt;/sub&gt;, Гкал/ч,  рассчитывается по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_Qcpo.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где Q&lt;sub&gt;оmax&lt;/sub&gt; – расчетная часовая  тепловая нагрузка на отопление здания, по укрупненным показателям, Гкал/ч, рассчитанная по формуле (1);
			&lt;p&gt;
				t&lt;sub&gt;в.р.&lt;/sub&gt;- расчетная температура внутреннего воздуха в отапливаемых помещениях здания,°С. Принимается по ГОСТ 30494-96 (табл.1,2) или СанПиН 2.2.4.548-96 (табл.2 );
			&lt;/p&gt;
			&lt;p&gt;
				t&lt;sub&gt;н.ср&lt;/sub&gt; – средняя месячная расчетная температура наружного воздуха, °С, принимается по СНиП 23-01-99.
			&lt;/p&gt;
			&lt;p&gt;
				t&lt;sub&gt;o&lt;/sub&gt; – расчетная температура наружного воздуха для проектирования отопления, °С, принимается по СНиП 23-01-99;
			&lt;/p&gt;
			&lt;p&gt;
				Итоговые расчетные значения средней часовой тепловой нагрузка на отопление здания по месяцам представлены в таблице 4.5.
			&lt;/p&gt;
			&lt;p&gt;
				Для определения значений помесячной нормативной тепловой нагрузки, значения средней часовой тепловой нагрузки на отопление умножаются на количество отопительных часов в соответствующем месяце (указанных в таблице 4.5).
			&lt;/p&gt;
			&lt;p&gt;
				Фактическая и нормативная тепловые нагрузки на отопление обследуемого здания представлены в таблице 4.5.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Значения расчетных величин для зданий</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CBHC.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;Здание&lt;/td&gt;
						&lt;td&gt;α&lt;/td&gt;
						&lt;td&gt;V, м&lt;sup&gt;3&lt;/sup&gt;&lt;/td&gt;
						&lt;td&gt;L, м&lt;/td&gt;
						&lt;td&gt;q&lt;sub&gt;o&lt;/sub&gt;, ккал/мм&lt;sup&gt;3&lt;/sup&gt;ч°С&lt;/td&gt;
						&lt;td&gt;t&lt;sub&gt;o&lt;/sub&gt;, °С&lt;/td&gt;
						&lt;td&gt;w&lt;sub&gt;o&lt;/sub&gt;, м/сек&lt;/td&gt;
						&lt;td&gt;К&lt;sub&gt;н.р.&lt;/sub&gt;&lt;/td&gt;
						&lt;td&gt;Q&lt;sub&gt;omax&lt;/sub&gt;, Гкал/ч&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[6])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[7])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[8])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 105, 'Buildings', N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Фактическая и нормативная тепловые нагрузки на отопление за @Model.BaseYear г. для здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CHLFAN.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;Месяц&lt;/td&gt;
						&lt;td&gt;Среднемесячная температура наружного воздуха, t&lt;sub&gt;н.ср.&lt;/sub&gt;, °С&lt;/td&gt;
						&lt;td&gt;Средняя часовая тепловая нагрузка на отопление, Q&lt;sub&gt;ср.о.&lt;/sub&gt;, Гкал/ч&lt;/td&gt;
						&lt;td&gt;Количество отопительныхдней в месяце&lt;/td&gt;
						&lt;td&gt;Нормативное потребление тепловой энергии на отопление, Гкал&lt;/td&gt;
						&lt;td&gt;Фактическое потребление тепловой энергии на отопление, Гкал&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>График фактического и нормативного потребления тепловой энергии на отопление за @Model.BaseYear г.</ChartTitle>
	  <DataContent>Model.Buildings[%INDEX%].ChartNormAndFactHeatingConsumption.Chart</DataContent>
	</ChartElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 108, NULL, N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="red"&gt;
				ВЫВОД ПО ГРАФИКУ:  Сравнивая нормативные и фактические тепловые нагрузки  на отопление объекта, можно сказать, что в @Model.BaseYear г. был недотоп/ перетоп здания. РЕКОМЕНДАЦИИ: поставить автоматику/ счетчики и т.п.
				ИЛИ - В целом потребление тепловой энергии на отопление здания, в @Model.BaseYear г., почти соответствует нормативным значениям.
			&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Средний расчетный суточный объем потребления горячей воды V&lt;sub&gt;hw&lt;/sub&gt;, м&lt;sup&gt;3&lt;/sup&gt;/сут, в здании в течение года, определяется по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_Vhw.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где	g&lt;sub&gt;г&lt;/sub&gt; – средний суточный расход горячей воды л/сутки одним пользователем, принимается согласно СНиП 2.04.01-85* прил.3; 
			&lt;p&gt;
				m – количество пользователей, по данным Заказчика.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Среднечасовой за отопительный период расход тепловой энергии на горячее водоснабжение, Q&lt;sub&gt;о.чhw&lt;/sub&gt;, Гкал/ч, определяется по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_Qohw.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где V&lt;sub&gt;hw&lt;/sub&gt; – среднесуточный расчетный расход горячей воды всеми видами потребителей данного здания, определенный по формуле (4.4);
			&lt;p&gt;
				t&lt;sub&gt;оx&lt;/sub&gt; – температура холодной водопроводной воды в отопительный период +5°С;
			&lt;/p&gt;
			&lt;p&gt;
				55 – нормативная температура горячей воды, °С;
			&lt;/p&gt;
			&lt;p&gt;
				ρ – плотность воды, 1 кг/л;
			&lt;/p&gt;
			&lt;p&gt;
				c&lt;sub&gt;w&lt;/sub&gt; – удельная теплоемкость, 4,2 кДж/кг*°С;
			&lt;/p&gt;
			&lt;p&gt;
				K&lt;sub&gt;hi&lt;/sub&gt; – коэффициент, учитывающий потери теплоты трубопроводами системы горячего водоснабжения. С неизолированными стояками и полотенцесушителем принимается равным 0,3.
			&lt;/p&gt;
			&lt;p&gt;
				Среднечасовой за летний период расход тепловой энергии на горячее водоснабжение, Q&lt;sub&gt;л.чhw&lt;/sub&gt;, Гкал/ч, определяется по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_Qlhw.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где	t&lt;sub&gt;лx&lt;/sub&gt; – температура холодной водопроводной воды в летний период +15°С;
			&lt;p&gt;
				Остальные параметры те же, что и в формуле (4.5).
			&lt;/p&gt;
			&lt;p&gt;
				Далее значения средней часовой тепловой нагрузки на ГВС умножаются на продолжительность работы системы в часах, в соответствующем месяце. Фактическая и нормативная тепловые нагрузки на ГВС представлены в таблице 4.6.
			&lt;/p&gt;	
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 111, NULL, N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Значения расчетных величин для зданий</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CBDHWC.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;Здание&lt;/td&gt;
						&lt;td&gt;g&lt;sub&gt;г&lt;/sub&gt;, л/сутки&lt;/td&gt;
						&lt;td&gt;m, чел&lt;/td&gt;
						&lt;td&gt;V&lt;sub&gt;hw&lt;/sub&gt;, м&lt;sup&gt;3&lt;/sup&gt;/сут&lt;/td&gt;
						&lt;td&gt;Q&lt;sub&gt;о.чhw&lt;/sub&gt;, Гкал/ч&lt;/td&gt;
						&lt;td&gt;Q&lt;sub&gt;л.чhw&lt;/sub&gt;, Гкал/ч&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 115, 'Buildings', N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Фактическая и нормативная тепловые нагрузки на ГВС за @Model.BaseYear г. для здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CDHWLFAN.Table;
				&lt;table&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@FormatDouble(table.Rows[i].Cells[1], 6)&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>График фактического и нормативного потребления тепловой энергии на ГВС здания @Model.Buildings[%INDEX%].Name за @Model.BaseYear г.</ChartTitle>
	  <DataContent>Model.Buildings[%INDEX%].ChartNormAndFactDHWConsumption.Chart</DataContent>
	</ChartElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 120, NULL, N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="red"&gt;
				ВЫВОД ПО ГРАФИКУ:  Сравнивая нормативные и фактические тепловые нагрузки на ГВС обследуемого объекта, можно сказать, что в @Model.BaseYear г. было превышение потребления тепла на нужды ГВС.&lt;br/&gt;
				РЕКОМЕНДАЦИИ: поставить автоматику/ счетчик и т.п.&lt;br/&gt;
				ИЛИ - В целом потребление тепловой энергии на нужды ГВС здания, в @Model.BaseYear г., соответствует нормативным значениям или не превышает их.
			&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Расчетная часовая тепловая нагрузка на приточную вентиляцию, Q&lt;sub&gt;в.р&lt;/sub&gt;, Гкал/ч, рассчитывается по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_Qvr.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где Q&lt;sub&gt;в.пр&lt;/sub&gt; - расчетная часовая тепловая нагрузка приточной вентиляции по проекту, Гкал/ч. При отсутствии проектных данных, рассчитывается по укрупненным показателям по формуле (4.8);
			&lt;p&gt;
				t&lt;sub&gt;н.пр.в&lt;/sub&gt; – расчетная температура наружного воздуха, при которой определена тепловая нагрузка приточной вентиляции в проекте, °С, принимается по СНиП 23-01-99;
			&lt;/p&gt;
			&lt;p&gt;
				t&lt;sub&gt;н.р.в&lt;/sub&gt; - средняя расчетная температура наружного воздуха для проектирования приточной вентиляции в местности, где расположено здание, °С, принимается по СНиП 23-01-99;
			&lt;/p&gt;
			&lt;p&gt;
				Расчетная часовая тепловая нагрузка приточной вентиляции по укрупненным показателям, Q&lt;sub&gt;в.р&lt;/sub&gt;, Гкал/ч, рассчитывается формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_Qvr2.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где q&lt;sub&gt;в&lt;/sub&gt; - удельная тепловая вентиляционная характеристика здания, зависящая от назначения и строительного объема вентилируемого здания, ккал/м&lt;sup&gt;3&lt;/sup&gt;ч°С, принимается по МДС 41-2.2000 (табл.3);
			&lt;p&gt;
				t&lt;sub&gt;н.р.в&lt;/sub&gt; - средняя расчетная температура наружного воздуха для проектирования приточной вентиляции в местности, где расположено здание,°С, принимается по СНиП 23-01-99;
			&lt;/p&gt;
			&lt;p&gt;
				Остальные параметры те же, что и в формуле (4.1).
			&lt;/p&gt;
			&lt;p&gt;
				Далее значения средней часовой тепловой нагрузки на приточную вентиляцию умножаются на продолжительность работы системы в часах, в соответствующем месяце.
			&lt;/p&gt;
			&lt;p&gt;
				Фактическая и нормативная тепловые нагрузки на приточную вентиляцию представлены в таблице 4.7.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>


  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 125, NULL, N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Значения расчетных величин для зданий</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CBVIC.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;Здание&lt;/td&gt;

						&lt;td&gt;t&lt;sub&gt;н.пр.в&lt;/sub&gt;, °С&lt;/td&gt;
						&lt;td&gt;t&lt;sub&gt;н.р.в&lt;/sub&gt;, °С&lt;/td&gt;
						&lt;td&gt;q&lt;sub&gt;в&lt;/sub&gt;, ккал/м&lt;sup&gt;3&lt;/sup&gt;ч°С&lt;/td&gt;

						&lt;td&gt;Q&lt;sub&gt;в.р&lt;/sub&gt;, Гкал/ч&lt;/td&gt;
						&lt;td&gt;Q&lt;sub&gt;в.р&lt;/sub&gt;, Гкал/ч&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 130, 'Buildings', N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Фактическая и нормативная тепловые нагрузки на приточную вентиляцию за @Model.BaseYear г. для здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CVILFAN.Table;
				&lt;table&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[0])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@CommonFormat(table.Rows[i].Cells[4])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>График фактического и нормативного потребления тепловой энергии на приточную вентиляцию за @Model.BaseYear г.</ChartTitle>
	  <DataContent>Model.Buildings[%INDEX%].ChartNormAndFactVentinConsumption.Chart</DataContent>
	</ChartElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 135, NULL, N'
<Section>
  <Elements>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Анализ удельной тепловой характеристики объекта</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Фактическую удельную тепловую характеристику на отопление здания, q&lt;sub&gt;факт&lt;/sub&gt;, Вт/м&lt;sup&gt;3&lt;/sup&gt;°С, рассчитываем по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_q_fact.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>


	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где	Q&lt;sub&gt;год факт&lt;/sub&gt; - фактический расход тепловой энергии на отопление здания в @Model.BaseYear году, Гкал;
			&lt;p&gt;
				Z&lt;sub&gt;h факт&lt;/sub&gt; - фактическая продолжительность отопительного периода в отчетном (базовом) году, сут (при отсутствии данных СНиП 23-01-99);
			&lt;/p&gt;
			&lt;p&gt;
				Z&lt;sub&gt;int факт&lt;/sub&gt; - фактическая средняя температура внутреннего воздуха за отопительный период в отчетном (базовом) году, °С (при отсутствии данных может приниматься в соответствии с СНиП 23-02-2003 с учетом Г°СТ 30494-96);
			&lt;/p&gt;
			&lt;p&gt;
				Z&lt;sub&gt;ext факт&lt;/sub&gt; - фактическая средняя температура наружного воздуха за отопительный период в @Model.BaseYear году (при отсутствии данных СНиП 23-01-99);
			&lt;/p&gt;
			&lt;p&gt;
				K&lt;sub&gt;nm&lt;/sub&gt; - коэффициент для учета потерь теплоты теплопроводами, проложенными в не отапливаемых помещениях, принимается в соответствии со СНиП 2.04.05-91*, равным 1,05;
			&lt;/p&gt;
			&lt;p&gt;
				V&lt;sub&gt;h&lt;/sub&gt; - отапливаемый объем помещений здания, м&lt;sup&gt;3&lt;/sup&gt;, принимаем по паспорту БТИ;
			&lt;/p&gt;
			&lt;p&gt;
				Итоговое расчетное значение фактической удельной тепловой  характеристики здания  представлено в таблице 4.8.
			&lt;/p&gt;
			&lt;p&gt;
				Значение нормативной удельной тепловой характеристики обследуемого здания принимаем в соответствии с МДС 41-4.2000 (прил.1, табл.3 или 4). Данные значения  сведены в таблицу 4.10.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 140, NULL, N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Значения расчетных величин для зданий</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CBSTC.Table;
				&lt;table&gt;

					&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
						&lt;td&gt;Здание&lt;/td&gt;
						&lt;td&gt;Z&lt;sub&gt;h факт&lt;/sub&gt;, сут&lt;/td&gt;
						&lt;td&gt;Z&lt;sub&gt;int факт&lt;/sub&gt;, °С&lt;/td&gt;
						&lt;td&gt;Z&lt;sub&gt;ext факт&lt;/sub&gt;, °С&lt;/td&gt;
						&lt;td&gt;Фактическая удельная тепловая характеристика здания, Вт/м&lt;sup&gt;3&lt;/sup&gt; °С&lt;/td&gt;
						&lt;td&gt;Нормативная удельная тепловая характеристика здания, Вт/м&lt;sup&gt;3&lt;/sup&gt; °С&lt;/td&gt;
					&lt;/tr&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble(table.Rows[i].Cells[4], 4)&lt;/td&gt;
							&lt;td&gt;@FormatDouble(table.Rows[i].Cells[5], 4)&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 145, NULL, N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>47</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="red"&gt;
				Как видно из таблицы 4.8, фактическая удельная тепловая характеристика здания выше/ ниже значения нормативной удельной тепловой характеристики здания, что говорит о превышении/занижении температуры внутреннего воздуха в здании по сравнению с требуемыми значениями. Рекомендуется установка систем автоматического регулирования систем теплоснабжения.
			&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Оценка потенциала экономии тепловой энергии</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Потенциал экономии тепловой энергии ΔЭ&lt;sub&gt;пот&lt;/sub&gt; представляет собой разность между фактическим годовым потреблением тепловой энергии обследуемым объектом, Э&lt;sub&gt;факт&lt;/sub&gt;. и потреблением при нормативных условиях работы, Э&lt;sub&gt;нор&lt;/sub&gt;.
			&lt;/p&gt;
			&lt;p&gt;
				Согласно данным, представленным в разделе 4.5 данного отчета, фактическое потребление тепловой энергии объектом за @Model.BaseYear г. составило Э&lt;sub&gt;факт&lt;/sub&gt; = @Model.ThermalEnergyConsumptionFact Гкал, а нормативное значение данного потребления составило @Model.ThermalEnergyConsumptionNorm Гкал, соответственно потенциал экономии тепловой энергии составляет:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_En_pot.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 150, NULL, N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Значения расчетных величин для зданий</TableName>
		<Content>
		@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CBTEEC.Table;
				&lt;table&gt;

					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[1])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[2])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
							&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
						&lt;/tr&gt;
					}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Потенциал обследуемого объекта по потреблению тепловой энергии составляет @Model.ThermalEnergyConsumptionPotentialPercents;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	

  </Elements>
</Section>')




























INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 3000, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>false</Numbered>
      <Text>Термины и определения</Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Показатель энергоэффективности&lt;/i&gt;&lt;/b&gt; - абсолютная или удельная величина потребления или потерь энергетических ресурсов для продукции любого на значения или технологического процесса.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Система теплоснабжения&lt;/i&gt;&lt;/b&gt; – совокупность гидравлически связанных трубопроводов, установок и устройств для производства, передачи, распределения и использования тепловой энергии.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Тепловая сеть&lt;/i&gt;&lt;/b&gt; – совокупность трубопроводов и установок, предназначенных для передачи тепловой энергии от источников ее потребителям.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Технологическое оборудование&lt;/i&gt;&lt;/b&gt; - это оборудование, предназначенное для осуществления основных или вспомогательных технологических процессов и эффективной работы учреждения.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Тепловизор&lt;/i&gt;&lt;/b&gt; – это прибор, который получает тепловое изображение в инфракрасной области спектра без непосредственного контакта с оборудованием.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Топливно-энергетические ресурсы (ТЭР)&lt;/i&gt;&lt;/b&gt; – совокупность природных и производственных энергоносителей, запасенная энергия которых при существующем уровне развития техники и технологии доступна для использования в хозяйственной деятельности.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Энергетическое обследование&lt;/i&gt;&lt;/b&gt; – обследование потребителей топливноэнергетических ресурсов с целью установления эффективности использования ими топливно-энергетических ресурсов и выработки экономически обоснованных мер по снижению затрат на топливо- и энергообеспечение.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Энергоаудит&lt;/i&gt;&lt;/b&gt; – добровольное энергетическое обследование, проводимое по инициативе потребителя топливно-энергетических ресурсов.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Энергосбережение&lt;/i&gt;&lt;/b&gt; - реализация правовых, организационных, научных, производственных, технических и экономических мер, направленных на эффективное (рациональное) использование (и экономное расходование) ТЭР и на вовлечение в хозяйственный оборот возобновляемых источников энергии.&lt;/p&gt;
			&lt;p&gt;&lt;b&gt;&lt;i&gt;Эффективное использование энергетических ресурсов&lt;/i&gt;&lt;/b&gt; – достижение экономически оправданной эффективности использования энергетических ресурсов при существующем уровне развития техники и технологии и соблюдении требований к охране окружающей природной среды.&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 3010, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>false</Numbered>
      <Text>Обозначения и сокращения</Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;ХВС - холодное водоснабжение;&lt;/p&gt;
			&lt;p&gt;ГВС - горячее водоснабжение;&lt;/p&gt;
			&lt;p&gt;ИТП – Индивидуальный тепловой пункт;&lt;/p&gt;
			&lt;p&gt;РЭС - районные электрические сети;&lt;/p&gt;
			&lt;p&gt;МРСК - межрегиональная распределительная сетевая компания;&lt;/p&gt;
			&lt;p&gt;ПС – подстанция;&lt;/p&gt;
			&lt;p&gt;ЩС – щит силовой;&lt;/p&gt;
			&lt;p&gt;ЩО - щит освещения;&lt;/p&gt;
			&lt;p&gt;РЩ - распределительный щит;&lt;/p&gt;
			&lt;p&gt;ЩУ - щит учета;&lt;/p&gt;
			&lt;p&gt;АСКУЭ - автоматизированная система контроля и учета энергоресурсов;&lt;/p&gt;
			&lt;p&gt;ТП - трансформаторная подстанция;&lt;/p&gt;
			&lt;p&gt;ЛЛ - люминесцентная лампа;&lt;/p&gt;
			&lt;p&gt;ЛН - лампа накаливания;&lt;/p&gt;
			&lt;p&gt;ЛС – светодиодная лампа;&lt;/p&gt;
			&lt;p&gt;ЛЭ – энергосберегающая лампа;&lt;/p&gt;
			&lt;p&gt;Т У.Т.- тонна условного топлива;&lt;/p&gt;
			&lt;p&gt;ГСОП – градусо-сутки отопительного периода.&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 3020, NULL, N'<Section  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>false</Numbered>
      <Text>Список литературы</Text>
    </SectionTitleElement>

	<EditableTextElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Content>
		&lt;ol&gt;
			&lt;li&gt;ФЗ №261 от 23.11.2009 «об энергосбережении и повышении энергетической эффективности и о внесении в отдельные законодательные акты РФ».&lt;/li&gt;
			&lt;li&gt;Приказ Министерства регионального развития РФ от 28 мая 2010 г. № 262 «О требованиях энергетической эффективности зданий, строений, сооружений».&lt;/li&gt;
			&lt;li&gt;Приказ министерства экономического развития РФ от 17.02.2010г №61 «Об утверждении примерного перечня мероприятий в области энергосбережения и повышения энергетической эффективности, который может быть использован в целях разработки региональных, муниципальных программ в области энергосбережения и повышения энергетической эффективности».&lt;/li&gt;
			&lt;li&gt;ГОСТ 31168-2003 «Здания жилые. Метод определения удельного потребления тепловой энергии на отопление».&lt;/li&gt;
			&lt;li&gt;ГОСТ 2.105-95 «Общие требования к текстовым документам».&lt;/li&gt;
			&lt;li&gt;ГОСТ	30494-96  «Здания  общественные  и  жилые.  Параметры  микроклимата  в Помещениях».&lt;/li&gt;
			&lt;li&gt;ГОСТ 13109-97 «Электрическая энергия. Требования к качеству электрической энергии в электрических сетях общего назначения»&lt;/li&gt;
			&lt;li&gt;ГОСТ 26629-85 «Метод тепловизионного контроля качества теплоизоляции ограждающих конструкции».&lt;/li&gt;
			&lt;li&gt;ГОСТ 2.105-95 «Общие требования к текстовым документам».&lt;/li&gt;
			&lt;li&gt;СНиП 23-01-99* «Строительная климатология».&lt;/li&gt;
			&lt;li&gt;СНиП 23-02-2003 «Тепловая защита зданий».&lt;/li&gt;
			&lt;li&gt;СНиП 02.04.01-85 «Внутренний водопровод и канализация зданий».&lt;/li&gt;
			&lt;li&gt;СНиП 41-01-2003 «Отопление, вентиляция и кондиционирование».&lt;/li&gt;
			&lt;li&gt;СП 41-101-95 «Проектирование тепловых пунктов».&lt;/li&gt;
			&lt;li&gt;СП 23-101-2004 «Проектирование тепловой защиты зданий».&lt;/li&gt;
			&lt;li&gt;СП 52.13330.2011«Естественное и искусственное освещение».&lt;/li&gt;
			&lt;li&gt;МДС  13-7.2000  «Рекомендации  по  первоочередным  малозатратным  мероприятиям, обеспечивающим энергоресурсосбережение в ЖКХ города».&lt;/li&gt;
			&lt;li&gt;МДС 41-2.2000* «Методика определения количества тепловой энергии и теплоносителя в водяных системах коммунального теплоснабжения».&lt;/li&gt;
			&lt;li&gt;МДС 13-20.2004 «Комплексная методика по обследованию и энергоаудиту реконструируемых зданий. Пособие по проектированию».&lt;/li&gt;
			&lt;li&gt;РД 153-34.0-20.363-99 «Методика инфракрасного контроля электрооборудования и ВЛ».&lt;/li&gt;
			&lt;li&gt;ВСН 53-86р «Правила оценки физического износа жилых зданий».&lt;/li&gt;
			&lt;li&gt;СанПиН 2.1.2.2645-10 «Санитарно-эпидимиологические требования к условиям проживания в жилых зданиях и помещениях».&lt;/li&gt;
		&lt;/ol&gt;
	  </Content>
	</EditableTextElement>

	<EditableTextElement>
      <Id>2</Id>
      <Hidden>false</Hidden>
      <Content>
	  &lt;p&gt;Все стандарты приведенные выше являются действующими.&lt;/p&gt;
	  </Content>
	</EditableTextElement>
  </Elements>
</Section>')


--INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
--VALUES (1, 1, NULL, N'
--<Section>
--  <Elements>

--  </Elements>
--</Section>')


--
-- Дополнительные скрипты
--
:r .\Script1.PostDeployment.sql

:r .\T13.PostDeployment.sql
:r .\T14.PostDeployment.sql
:r .\T15.PostDeployment.sql