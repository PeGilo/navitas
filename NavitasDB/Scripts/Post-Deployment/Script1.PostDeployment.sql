﻿-- =============================================
-- Script Template
-- =============================================
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1000, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Характеристика системы снабжения холодной водой</Text>
    </SectionTitleElement>
	
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Договор на снабжение холодной воды</Text>
    </SubsectionTitleElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1005, 'Buildings', N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Водоснабжение @Model.Buildings[%INDEX%].WaterResource &lt;font color="aqua"&gt;здания @Model.Buildings[%INDEX%].Name &lt;/font&gt; осуществляется по &lt;font color="aqua"&gt;УКАЗАТЬ НАЗВАНИЕ, НОМЕР, ДАТУ ДОГОВОРА&lt;/font&gt;.
			&lt;p&gt;
				Водоотведение @Model.Buildings[%INDEX%].SewerageResource &lt;font color="aqua"&gt;здания @Model.Buildings[%INDEX%].Name &lt;/font&gt; осуществляется по &lt;font color="aqua"&gt;УКАЗАТЬ НАЗВАНИЕ, НОМЕР, ДАТУ ДОГОВОРА&lt;/font&gt;.
			&lt;br/&gt;
				Коммерческий учет потребляемой зданиями Объекта воды осуществляется по показаниям счетчиков указанных в таблице:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<TableElement>
		<Id>2</Id>
		<TableName>Характеристика водосчетчика(ов)</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].ColdWaterMetersTable;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; @if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[6]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[6]&lt;/td&gt;
						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

	<RequiredImageElement>
		<Id>3</Id>
		<Hidden>false</Hidden>
		<Content>
			@if(Model.Buildings[%INDEX%].Illustration["Приборы учета холодной воды"].Count &gt; 0) 
			{
				foreach(var ill in Model.Buildings[%INDEX%].Illustration["Приборы учета холодной воды"]) 
					{
						&lt;div class="required-illustration"&gt;
						&lt;img src="@IllustrationUrl(ill.FileName)" alt="@ill.UserTitle"/&gt;
						&lt;div class="required-illustration-caption"&gt;Водосчетчики @Model.Buildings[%INDEX%].Name&lt;/div&gt;
						&lt;input type="hidden" value="@ill.IllustrationID" /&gt;
						&lt;/div&gt;
					}
			}
			else
			{
				&lt;div class="missing-illustration"&gt;
				&lt;span&gt;Отсутствует иллюстрация водосчетчиков @Model.Buildings[%INDEX%].Name.&lt;/span&gt;
				&lt;/div&gt;
			}
		</Content>
	</RequiredImageElement>
  </Elements>
</Section>')

--Рисунки
/*INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1010, NULL, N'
<Section>
</Section>')*/

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1015, NULL, N'
<Section>
  <Elements>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Характеристика системы снабжения холодной воды</Text>
    </SubsectionTitleElement>

	<EditableTextElement>
		<Id>5</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Источником водоснабжения служит &lt;font color="aqua"&gt;городская сеть&lt;/font&gt;. Внутри Объекта вода &lt;font color="aqua"&gt;направляется  по внутренним трубопроводам к конечным потребителям&lt;/font&gt;.  Основные направления использования воды – &lt;font color="aqua"&gt;ХВС, хозяйственно-питьевые нужды&lt;/font&gt;.				
			&lt;br/&gt;
				Система водоснабжения водоотведения соответствует проектным решениям, система передачи распределения воды к конечным потребителям в целом поддерживается в технически исправленном состоянии. Конечные потребители воды – водоразборные приборы находятся в технически исправном состоянии. Оценка системы водоснабжения и распределения является удовлетворительной. Однако их технический уровень не отвечает современным стандартам рационального водопотребления. &lt;font color="aqua"&gt;Смывные бачки имеют однопозиционный режим смыва&lt;/font&gt;. Современные бачки выпускаются с меньшим объемом воды, а также на 2 режима смыва, что позволяет экономить до 50 % воды при обеспечении гигиенических требований. Кроме того, смесители раковин также имеют устаревшую конструкцию с ручным управлением вентилями, отсутствием аэрации потока и ограничения струи. Современные водосберегающие смесители управляются автоматически по присутствию рук человека в зоне струи, а также используют эффект аэрации и другие средства для снижения расхода воды. 
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>5</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Точки водоразбора по зданиям:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1020, 'Buildings', N'
<Section>
  <Elements>
	<TableElement>
		<Id>2</Id>
		<TableName>Точки водоразбора @Model.Buildings[%INDEX%].Name</TableName>
		<Content>
		@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].PointOfWaterTable;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; @if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1025, Null, N'
<Section>
  <Elements>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Анализ нормативного и фактического водопотребления</Text>
    </SubsectionTitleElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				При анализе режима потребления воды Объектом за период @Model.BaseYear_4 - @Model.BaseYear гг. исследованы данные бухгалтерских документов и показания приборов учета воды. 				
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<TableElement>
		<Id>1</Id>
		<TableName>Анализ расходов на водопотребление за @Model.BaseYear_4 – @Model.BaseYear гг.</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.AnalysisCostsOfWaterUse;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;
						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>
	<EditableTextElement>
		<Id>8</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="aqua"&gt;  
				В 2007 и 2008 гг. наблюдался наибольший объем потребления ХВС по сравнению с остальными годами, наименьшее потребление – в 2011г. В 2010 г наблюдаются наибольшие затраты за потребление  ХВС. 
			&lt;/font&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1030, Null, N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Данные помесячного  нормативного  и фактического потребления воды организацией, приведены в таблице &lt;font color="aqua"&gt;5.3.2.&lt;/font&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				&lt;b&gt;Нормативный расход воды&lt;/b&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>NormalWaterFlowRegulation.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
			N – норма расхода воды в час наибольшего водопотребления на человека из СНиП 02.04.01-85 «Внутренний водопровод и канализация зданий»;			
			&lt;br/&gt;
			k – количество рабочих дней в год;
			&lt;br/&gt;
			Т – количество рабочих часов в день;
			&lt;br/&gt;
			n – количество человек (персонал, проживающие).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<TableElement>
		<Id>1</Id>
		<TableName>Помесячное нормативное и фактическое потребление воды</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.WaterFlowRegulation;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Для сопоставления построен график нормативного  и  фактического  водопотребления в @Model.BaseYear г.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<ChartElement>
	  <Id>1</Id>
	  <ChartTitle>Нормативное и фактическое потребление холодной воды</ChartTitle>
	  <DataContent>Model.WFR.Chart</DataContent>
	</ChartElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="aqua"&gt; 
					Водопользование Объекта можно оценить с точки зрения энергоэффективности как неудовлетворительное. Из графика следует, что фактический расход холодной воды превышает нормативный расход. Перерасход воды может быть вызван нерациональным использованием, утечками.
			&lt;/font&gt;
		</Content>
	</EditableTextElement>	
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1035, 'Buildings', N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Нормативное и фактическое постребление холодной воды в здании @Model.Buildings[%INDEX%].Name
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
	<TableElement>
		<Id>1</Id>
		<TableName>Помесячное нормативное и фактическое потребление воды</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].WaterFlowRegulation;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="aqua"&gt; 
				Краткие выводы по водопотреблению зданием @Model.Buildings[%INDEX%].Name
			&lt;/font&gt;&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

--Характеристика системы вентиляции
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1200, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Характеристика системы вентиляции</Text>
    </SectionTitleElement>
	
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Описание систем вентиляции</Text>
    </SubsectionTitleElement>
  </Elements>
</Section>')
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1205, 'Buildings', N'
<Section>
  <Elements>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="aqua"&gt; 
				В здании @Model.Buildings[%INDEX%].Name @Model.Buildings[%INDEX%].HavingVentilationCombinedExtractInput.
			&lt;/font&gt;&lt;br/&gt;
				Характеристики систем  механической вентиляции представлены в таблицах 6.1, 6.2.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
	<TableElement>
		<Id>1</Id>
		<TableName>Характеристика систем приточной вентиляции</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].VentilationInputTable;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[5]&lt;/td&gt;

						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;&lt;font color="aqua"&gt; 
				Дополнения к таблице
			&lt;/font&gt;
		</Content>
	</EditableTextElement>	
	<TableElement>
		<Id>1</Id>
		<TableName>Характеристика систем вытяжной вентиляции</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].VentilationExtractTable;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>	
	<EditableTextElement>
		<Id>4</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Регулирование и управление вентагрегатами выполняется системой регулирования и контроля &lt;font color="aqua"&gt;Satchwell BAS 2000 на основе DDC&lt;/font&gt;.  
			&lt;/p&gt;
			&lt;p&gt;
				&lt;font color="aqua"&gt;Предусмотрена \ не предусмотрена рекуперация \рециркуляция&lt;/font&gt; отходящего тепла.
			&lt;br/&gt;
				Сеть вентиляционных каналов по большей части предусмотрена &lt;font color="aqua"&gt;круглыми \ прямоугольными воздуховодами&lt;/font&gt;. Воздуховоды системы приточной вентиляции (с функцией охлаждения) &lt;font color="aqua"&gt;покрыты \ не покрыты&lt;/font&gt; тепловой изоляцией.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

--Характеристика ограждающих конструкций
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1300, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Сведения об ограждающих конструкциях</Text>
    </SectionTitleElement>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Характеристика ограждающих конструкций</Text>
    </SubsectionTitleElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1305, 'Buildings', N'
<Section>
  <Elements>
  	<TableElement>
		<Id>1</Id>
		<TableName>Описание конструктивных элементов здания @Model.Buildings[%INDEX%].Name и их износа</TableName>
		<Content>@{	
			{Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].BuildingComponentsTable;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
						@if(i == 0) 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
						}
						else 
						{
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[2]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[3]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[4]&lt;/td&gt;
						}
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>	
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Физический износ конструкций здания определяется &lt;font color="aqua"&gt;по паспорту БТИ \ или по ВСН-53-86(Р)&lt;/font&gt;.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

--Характеристика ограждающих конструкций
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1310, NULL, N'
<Section>
  <Elements>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Теплотехнический расчет ограждающих конструкций</Text>
    </SubsectionTitleElement>
	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Теплотехнический расчет ограждающих конструкций выполняется согласно СНиП 23-02-2003  и СП 23-101-2004.&lt;br/&gt;
				Температурные условия для выполнения теплотехнического расчета принимаются в соответствии с требованиями &lt;font color="aqua"&gt;ГОСТ 30494-96 \ или СанПиН 2.2.4.548-96 "&lt;/font&gt;, СНиП 23-01-99*.
			&lt;/p&gt;
			&lt;p&gt;
				Градусо-сутки отопительного периода (ГСОП), &lt;font color="aqua"&gt;°С&lt;/font&gt; * сут,  определяется по формуле: 
			&lt;/p&gt;
			&lt;p align="center"&gt;
				ГСОП = (tв - tот. пер)*Zот.пер
			&lt;/p&gt;
			где  tв -средняя температура внутреннего воздуха в отапливаемых помещениях здания, принимается по &lt;font color="aqua"&gt;ГОСТ 30494-96 \  или СанПиН 2.2.4.548-96&lt;/font&gt;.
			&lt;p&gt;
				tот. пер - средняя температура воздуха в отопительный период по СНиП 23-01-99*. 
				Zот.пер.- продолжительность отопительного периода по СНиП 23-01-99*. 
			&lt;/p&gt;


			&lt;p&gt;
				Расчетный коэффициент сопротивления теплопередаче (R0) ограждающих конструкций, принимается в соответствии с требованиями п.5.3 СНиП 23-02-2003, должно быть не менее нормативного значения (Rтро.э), рассчитанного из условий энергосбережения.
			&lt;/p&gt;
			&lt;p&gt;
				Расчетный коэффициент сопротивления теплопередаче ограждающих конструкций (стен, чердачного перекрытия, полов над холодным подвалом), R0, (м2 °С) /Вт,  определяется по формуле:
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
	<FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>tr_01.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>
	<FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>tr_03.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>
	<EditableTextElement>
		<Id>4</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Коэффициент сопротивления теплопередаче для остеклений определяется по сертификату установленных окон.   
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1311, 'Buildings', N'
<Section>
  <Elements>
	<SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Расчет коэффициентов сопротивления теплопередаче ограждающих конструкций для здания @Model.Buildings[%INDEX%].Name</Text>
    </SubsectionTitleElement>
  	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Для обследуемого здания 
				&lt;br/&gt;
				t&lt;sub&gt;в&lt;/sub&gt; = @Model.Buildings[%INDEX%].Tin °С;
				&lt;br/&gt;
				t&lt;sub&gt;от.пер&lt;/sub&gt; = @Model.Buildings[%INDEX%].Tout_fact °С;
				&lt;br/&gt;
				Z&lt;sub&gt;от.пер&lt;/sub&gt; = @Model.Buildings[%INDEX%].Zh_fact;
				&lt;br/&gt;
				ГСОП = @Model.Buildings[%INDEX%].GSOP;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
  	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Согласно СНиП 23-02-2003, для рассчитанного значения ГСОП нормативные сопротивления теплопередаче ограждающих конструкций &lt;font color="red"&gt;(R&lt;sup&gt;тр&lt;/sup&gt;&lt;sub&gt;о.э&lt;/sub&gt;)&lt;/font&gt; составляют:
				&lt;br/&gt;
				Для стен, R&lt;sup&gt;тр&lt;/sup&gt;&lt;sub&gt;о.э&lt;/sub&gt; = @Model.Buildings[%INDEX%].ThermoResistanceWalls м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт;
				&lt;br/&gt;
				Для перекрытия, R&lt;sup&gt;тр&lt;/sup&gt;&lt;sub&gt;о.э&lt;/sub&gt; = @Model.Buildings[%INDEX%].ThermoResistanceAtticFloor м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт;
				&lt;br/&gt;
				Для полов над техподпольем, R&lt;sup&gt;тр&lt;/sup&gt;&lt;sub&gt;о.э&lt;/sub&gt; = @Model.Buildings[%INDEX%].ThermoResistanceAtticFloor м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт;
				&lt;br/&gt;
				Для окон, R&lt;sup&gt;тр&lt;/sup&gt;&lt;sub&gt;о.э&lt;/sub&gt; = @Model.Buildings[%INDEX%].ThermoResistanceDoorsWindows м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
  	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Определяем расчетный коэффициент сопротивления теплопередаче для стен:
				&lt;br/&gt;
				R&lt;sub&gt;0стен&lt;/sub&gt; = @Model.Buildings[%INDEX%].R0sten м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт,   
				&lt;br/&gt;
				Определяем термическое сопротивление стен
				&lt;br/&gt;
				R&lt;sub&gt;Кстен&lt;/sub&gt; = @Model.Buildings[%INDEX%].RKsten м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт
				&lt;br/&gt;
				Определяем расчетный коэффициент сопротивления теплопередаче для стен:
				&lt;br/&gt;
				R&lt;sub&gt;0черд&lt;/sub&gt; = @Model.Buildings[%INDEX%].R0cherd м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт,   
				&lt;br/&gt;
				R&lt;sub&gt;kчерд&lt;/sub&gt; = @Model.Buildings[%INDEX%].RKcherd м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт,   
				&lt;br/&gt;
				R&lt;sub&gt;0пол&lt;/sub&gt; = @Model.Buildings[%INDEX%].R0pol м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт
				&lt;br/&gt;
				R&lt;sub&gt;kпол&lt;/sub&gt; = @Model.Buildings[%INDEX%].RKpol м&lt;sup&gt;2&lt;/sup&gt;•°С/Вт
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
  	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				&lt;font color="red"&gt;ЕСЛИ ПОЛ НАД ХОЛОДНЫМ ПОДВАЛОМ, ТО РАСЧЕТ ВЕДЕМ ПО:&lt;/font&gt;&lt;br/&gt;
				Расчетный коэффициент сопротивления теплопередаче для &lt;b&gt;пола над холодным подвалом&lt;/b&gt; определяем по формуле  (7.2.2):  &lt;br/&gt;
				R0пол= 1/8,7 + 1/6 + &lt;font color="aqua"&gt; ... &lt;/font&gt; = &lt;font color="aqua"&gt; ... &lt;/font&gt; м2 • °С /Вт,   &lt;br/&gt;
				Термическое сопротивление пола определяем по формуле (7.2.3): &lt;br/&gt;
				RKпол = &lt;font color="aqua"&gt;  &lt;/font&gt; +  &lt;font color="aqua"&gt; +... &lt;/font&gt;   =  &lt;font color="aqua"&gt; ... &lt;/font&gt; м2 • °С /Вт.
				font color="red"&gt;ЕСЛИ ПОЛ НАД ТЕПЛЫМ ПОДВАЛОМ, ТО РАСЧЕТ ВЕДЕМ ПО::&lt;/font&gt;&lt;br/&gt;
				Расчетный коэффициент сопротивления  теплопередаче &lt;b&gt;для пола над теплым подвалом&lt;/b&gt;, Rпол,  (м2 °С) /Вт, определяется по формуле:
			&lt;/p&gt;
			&lt;p align="center"&gt;&lt;font color="red"&gt;
				Форумла,                                                                   (7.2.4)
			&lt;/font&gt;&lt;/p&gt;
			где n - коэффициент, учитывающий зависимость положения наружной поверхности ограждающих конструкций по отношению к наружному воздуху, по табл. 6 СНиП  23-03-2003. Для обследуемого здания n = &lt;font color="aqua"&gt; ... &lt;/font&gt;;
			&lt;p&gt;
				(спец символ) - нормируемый температурный перепад между температурой внутреннего воздуха (спец символ) и температурой внутренней поверхности (спец символ) ограждающей конструкции, °С , по табл. 5 СНиП  23-03-2003. Для обследуемого здания  =  &lt;font color="aqua"&gt; ... &lt;/font&gt;°С;&lt;br/&gt;
				(спец символ) - коэффициент теплоотдачи внутренней поверхности ограждающих конструкций, Вт/(м •°С), по табл. 7 СНиП  23-03-2003. Для обследуемого здания (спец символ) = &lt;font color="aqua"&gt; ... &lt;/font&gt;Вт/(м •°С);&lt;br/&gt;
				(спец символ) - расчетная средняя температура внутреннего воздуха здания, °С, &lt;font color="aqua"&gt;  по ГОСТ 30494 \ или СанПиН 2.2.4.548-96.&lt;/font&gt; Для обследуемого здания (спец символ) =  &lt;font color="aqua"&gt; ... &lt;/font&gt;°С;&lt;br/&gt;
				(спец символ) - расчетная температура наружного воздуха в холодный период года, °С, для полов над теплым подвалом принимается 5 С.&lt;br/&gt;
				Итоги расчета коэффициентов сопротивления теплопередаче ограждающих конструкций и характеристика ограждающих конструкций представлены в таблице 7.2.1
			&lt;/p&gt;
		</Content>
	</EditableTextElement>	
  </Elements>
</Section>')


--				Согласно СНиП 23-02-2003, для рассчитанного значения ГСОП нормативные сопротивления теплопередаче ограждающих конструкций &lt;font color="red"&gt;(R&lt;sup&gt;тр&lt;/sup&gt;&lt;sub&gt;о.э&lt;/sub&gt;)&lt;/font&gt; составляют:
--			&lt;ul&gt;
--				&lt;li&gt;Для стен, Rтро.э = &lt;font color="aqua"&gt;...&lt;/font&gt; м2 • °С /Вт;&lt;/li&gt;
--				&lt;li&gt;Для перекрытия, Rтро.э = &lt;font color="aqua"&gt;...&lt;/font&gt; м2 • °С /Вт;&lt;/li&gt;
--				&lt;li&gt;Для полов над техподпольем, Rтро.э = &lt;font color="aqua"&gt;...&lt;/font&gt; м2 • °С /Вт.&lt;/li&gt;
--				&lt;li&gt;Для окон, Rтро.э = &lt;font color="aqua"&gt;...&lt;/font&gt; м2 • °С /Вт.&lt;/li&gt;
-- 			&lt;/ul&gt;
--			&lt;/p&gt;
--Коэффициент сопротивления теплопередаче для остеклений определяется по сертификату установленных окон и составляет R0 окон= @Model.Building[%(м2 °С) /Вт.   