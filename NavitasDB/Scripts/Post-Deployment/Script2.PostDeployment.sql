﻿-- =============================================
-- Script Template
-- =============================================
INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 2000, NULL, N'
<Section>
  <Elements>
	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>true</Hidden>
      <Numbered>true</Numbered>
      <Text>ТЕСТ2</Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>true</Hidden>
		<Content>
			ТЕСТ2
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')