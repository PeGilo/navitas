﻿-- =============================================
-- Script Template
-- =============================================

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 2000, NULL, N'
<Section>
  <Elements>

	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Энергосберегающие мероприятия</Text>
    </SectionTitleElement>

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Мероприятия по экономии электроэнергии</Text>
    </SubsectionTitleElement>

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Level>3</Level>
		<Text>Повышение энергетической эффективности систем освещения зданий, строений, сооружений</Text>
    </SubsectionTitleElement>

  </Elements>
</Section>')


INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 2100, NULL, N'
<Section>
  <Elements>

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Level>3</Level>
		<Text>Закупка энергопотребляющего оборудования высоких классов энергетической эффективности</Text>
    </SubsectionTitleElement>	

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Вся бытовая и оргтехника имеет маркировку класса энергоэффективности. По инициативе ЕС бытовым приборам присваивается один из семи классов (от А до G) в зависимости от степени электропотребления. Оборудование с самой высокой степенью энергоэффективности имеет класс А, с самой низкой – класс G. Заменившая с 18 июня 2010 года новая Директива № 2010/30/ЕС по маркировке этикеткой энергетической эффективности расширила сферу регулирования на промышленные и торговые приборы и оборудование. Ввелись три новых класса энергоэффективности: А+, А++ и А+++, которые в свою очередь еще более экономичнее расходуют электрическую энергию. Если продукция имеет высший класс энергоэффективности (А+++), низшие классы (E – G) из этикетки для такого продукта исключаются. (На маркировке также указывается: торговая марка оборудования, модель, уровень звукового давления, хладо- и теплопроизводительность, что позволяет сравнивать эффективность различной продукции).
			&lt;/p&gt;
			&lt;p&gt;
				При замене бытовой и оргтехники рекомендуется приобретать оборудование с классом энергоэффективности «А». По сравнению с оборудованием более низкого класса, например «G», потребление электрической энергии сокращается до 40%, что приводит к существенной экономии. Однако стоимость оборудования с высоким классом энергоэффективности на данный момент достаточно высока, а соответственно имеет большой срок окупаемости.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 2110, NULL, N'
<Section>
  <Elements>

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Мероприятия по экономии тепловой энергии</Text>
    </SubsectionTitleElement>	

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Level>3</Level>
		<Text>Установка теплоотражателей за отопительными приборами</Text>
    </SubsectionTitleElement>	

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Для повышения теплоотдачи от отопительных приборов и уменьшения теплопотребления рекомендуется установить теплоотражатели тепловые зеркала) за отопительными радиаторами.
			&lt;/p&gt;
			&lt;p&gt;
				Теплоотражатели представляют собой теплоизоляционные прокладки с отражающим слоем, устанавливаемые за отопительным радиатором на стене с помощью двустороннего скотча.
			&lt;/p&gt;
			&lt;p&gt;
				Для конкретности экономических расчетов рассматривается вариант применения термоотражающей пленки &lt;font color="aqua"&gt;«Соларекс»&lt;/font&gt;. Сократив потери тепла с помощью установки теплоотражающего экрана, экономия энергии может составить до 3%, следовательно, снизится потребление тепловой энергии на цели отопления.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Экономия тепловой энергии на отопление зданий: 
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

    <FormulaImageElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <FileName>formula_En.png</FileName>
      <UserTitle>Формула</UserTitle>
    </FormulaImageElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			где Wa = @Model.Wa Гкал/год – годовое потребление тепла на отопление;
			&lt;p&gt;
				k = 0,03 – коэффициент отражающий экономию (3%).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Таким образом экономия тепловой энергии на отопление зданий составит @Model.EnEco Гкал/год.
				При  тарифе  на тепловую энергию, равном @Model.ThermoEnergyTariff руб./Гкал, ежегодная экономия составит: 
			&lt;/p&gt;
			&lt;p align="center"&gt;
				Э = @Model.EnEco • @Model.ThermoEnergyTariff = @Model.ThermoEconomyByYear тыс. руб./год.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Затраты на приобретение теплоотражающей пленки оцениваются величиной: 
			&lt;/p&gt;	
			&lt;p align="center"&gt;
				З = @Model.HeatReflectingLayerCost • @Model.RegistersYardage / 1000 = @Model.HeatReflectingCost тыс. руб.,
			&lt;/p&gt;
			где З – стоимость теплоотражающей пленки &lt;font color="aqua"&gt;«Соларекс»&lt;/font&gt; (@Model.HeatReflectingLayerCost руб./м&lt;sup&gt;2&lt;/sup&gt;); 
			&lt;p&gt;
				@Model.RegistersYardage – общая площадь регистров (м2).
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Установка теплоотражающей пленки может осуществляться силами персонала Объекта.
			&lt;/p&gt;
			&lt;p&gt;				 
				Простой срок окупаемости мероприятия составит: 
			&lt;/p&gt;
			&lt;p align="center"&gt;
				Ток = @Model.HeatReflectingCost / @Model.ThermoEconomyByYear = @Model.Tok года.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 2200, NULL, N'
<Section>
  <Elements>

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Text>Мероприятия по экономии воды</Text>
    </SubsectionTitleElement>	

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Level>3</Level>
		<Text>Замена водоразборных приборов на водосберегающие</Text>
    </SubsectionTitleElement>	

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Конечные потребители воды - водоразборные приборы в целом находятся в технически исправном состоянии. Однако их технический уровень не отвечает современным стандартам рационального водопотребления. Смывные бачки имеют однопозиционный режим смыва. Современные бачки выпускаются на меньший объем воды, а также на 2 режима смыва, что позволяет экономить до 50% воды при обеспечении гигиенических требований.
			&lt;/p&gt;
			&lt;p&gt;
				Кроме того, смесители раковин также имеют устаревшую конструкцию с ручным управлением вентилями, отсутствием аэрации потока и ограничения струи. Современные водосберегающие смесители управляются автоматически по присутствии рук человека в зоне струи, а также используют эффект аэрации и другие средства для снижения расхода воды.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
			Годовой расход воды за @Model.BaseYear г. @Model.WaterConsumptionPerYear м&lt;sup&gt;3&lt;/sup&gt;. 
			При  тарифе @Model.BaseYear г. @Model.ColdWaterTariff руб/м&lt;sup&gt;3&lt;/sup&gt; на холодную воду и @Model.HotWaterTariff руб/м&lt;sup&gt;3&lt;/sup&gt; 
			на горячую воду, годовом  потреблении воды в смесителях @Model.WaterConsumptionPerYear м&lt;sup&gt;3&lt;/sup&gt;
			(@Model.HotWaterConsumptionPerYear м&lt;sup&gt;3&lt;/sup&gt; – в системе ГВС, @Model.ColdWaterConsumptionPerYear м&lt;sup&gt;3&lt;/sup&gt; – в системе ХВС)
			и  расчетной экономии воды 30% 
			(@Model.HotWaterEconomy м&lt;sup&gt;3&lt;/sup&gt; горячей воды стоимостью @Model.HotWaterEconomyCost руб. 
			и @Model.ColdWaterEconomy м&lt;sup&gt;3&lt;/sup&gt; холодной воды стоимостью @Model.ColdWaterEconomyCost руб.)
			окупаемость в течение нормативных 5-ти лет обеспечивают только затраты ____________ руб. 
			Стоимость одного бесконтактного смесителя составляет 18 000 руб.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Затраты на замену &lt;font color="aqua"&gt;63&lt;/font&gt; неавтоматизированных приборов на автоматизированные:
			&lt;/p&gt;
			&lt;p&gt;
				&lt;font color="aqua"&gt;Зм = 63 * 18000 = 1134000 руб.&lt;/font&gt;
			&lt;/p&gt;
			&lt;p&gt;
				Простой срок окупаемости мероприятия составит: 
			&lt;/p&gt;
			&lt;p&gt;
				&lt;font color="aqua"&gt;Ток = 1134000 / 206730,181=5,485лет.&lt;/font&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				&lt;font color="aqua"&gt;Если срок окупаемости больше 15 лет, то пишем вывод:&lt;/font&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Тотальная замена водоразборных приборов в связи с относительно малым водопотреблением зданиями не окупится. 
				Кроме того, есть необходимость в отборе из системы только холодной воды (например, для питья и технических нужд), 
				когда автоматизированный смеситель, настроенный на подачу теплой воды для мытья рук, не подходит. 
				Здесь требуется использование более сложных конструктивно и дорогих  смесителей, либо сохранение части неавтоматизированных приборов.
				Окончательное решение необходимо принять исходя из финансовых возможностей.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 2210, NULL, N'
<Section>
  <Elements>	

    <SubsectionTitleElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Level>3</Level>
		<Text>Установка водосчетчиков</Text>
    </SubsectionTitleElement>	

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				САНТЕХМОНТАЖ, тел. +7 (391) 288-97-47
			&lt;/p&gt;
			&lt;p&gt;
				Водосчетчики позволяют контролировать объем расходуемой воды, что естественным образом сказывается на экономии бюджета. Ведь платить приходится только за тот объем воды, который тратится реально, а не предположительно.
			&lt;/p&gt;
			&lt;p&gt;
				По принципу работы счетчики можно разделить на три типа: механические, ультразвуковые и электромагнитные. В воде с большим количеством примесей лучше использовать ультразвуковые и электромагнитные счетчики. Там, где вода чистая, применяют механические приборы. Они проще в эксплуатации и информация на них представляется нагляднее.
			&lt;/p&gt;
			&lt;p&gt;
				В системе водоснабжения со счетчиком необходимо установить сетчатый фильтр. Он поможет продлить срок службы прибора. При установке счетчика необходимо учесть и другие условия.
			&lt;/p&gt;
			&lt;p&gt;
				Нередко счетчики устанавливают в местах подвода воды в дом: в подвалах и цокольных этажах, которые могут иметь повышенную влажность. Для электронных приборов это неблагоприятная среда. Лучше вынести электронную часть прибора в сухое помещение.
			&lt;/p&gt;
			&lt;p&gt;
				Счетчик воды можно установить где угодно, хоть на самом смесителе в кухне или ванной.
			&lt;/p&gt;
			&lt;p&gt;
				При выборе счетчика ориентируются на параметры трубы и расход воды. Расход надо выяснить хотя бы приблизительно. Если он небольшой и составляет до 1,5 кубов в час, можно обойтись недорогими счетчиками. Они рассчитаны на дюймовый или полуторадюймовый посадочные размеры. При небольшом напоре воды большой диаметр трубы смущать не должен, он не помеха, можно становить переходник, а на него счетчик. При покупке счетчика необходимо сверится с его техническими данными. Они должны совпадать с соответствующими параметрами расхода воды в квартире или в доме.
			&lt;/p&gt;
			&lt;p&gt;
				На рынке в широком ассортименте представлены механические счетчики воды.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

	<TableElement>
		<Id>1</Id>
		<TableName>Затраты на установку водосчетчика</TableName>
		<Content>@{	
			{
				Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.CIWMC.Table;
				&lt;table&gt;
					@for(int i = 0; i &lt; table.Rows.Count; i++) 
					{
						&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
							&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
							&lt;td&gt;@table.Rows[i].Cells[1]&lt;/td&gt;
						&lt;/tr&gt;
					} 
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>	

  </Elements>
</Section>')