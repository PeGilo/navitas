﻿INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1500, NULL, N'
<Section>
  <Elements>

	<SectionTitleElement>
      <Id>1</Id>
      <Hidden>false</Hidden>
      <Numbered>true</Numbered>
      <Text>Характеристика эксплуатационной среды</Text>
    </SectionTitleElement>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				Измерения параметров микроклимата в помещениях обследуемых зданий проводились согласно ГОСТ 26629-85, ГОСТ 30494-96, СНиП 23-01-99, МДС 13-20.2004, СанПиН 2.1.2.2645-10.
			&lt;/p&gt;
			&lt;p&gt;
				Измерения проводились следующими приборами: тепловизор testo-875; люксометр цифровой testo-540; термоанемометр testo 410-1/-2.
			&lt;/p&gt;
			&lt;p&gt;
				Сведения об эксплуатационной среде в помещениях обследуемых зданий представлены в таблице 8.1.
			&lt;/p&gt;
		</Content>
	</EditableTextElement>
  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1505, 'Buildings', N'
<Section>
  <Elements>

	<TableElement>
		<Id>1</Id>
		<TableName>Сведения об эксплуатационной среде здания @Model.Buildings[%INDEX%].Name</TableName>
		<Content>@{	
			{ Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[%INDEX%].CE.Table;
				&lt;table&gt;
						&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
							&lt;td rowspan="2"&gt;Наименование помещения&lt;/td&gt;
							&lt;td rowspan="2"&gt;Площадь помещения, м&lt;sup&gt;2&lt;/sup&gt;&lt;/td&gt;
							&lt;td rowspan="2"&gt;Время измерений&lt;/td&gt;
							&lt;td colspan="2"&gt;Температура, °С&lt;/td&gt;
							&lt;td colspan="2"&gt;Освещенность, Лк&lt;/td&gt;
							&lt;td colspan="2"&gt;Влажность воздуха, %&lt;/td&gt;
							&lt;td colspan="2"&gt;Движение воздуха, м/с&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr class="@RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType.Header)"&gt;
							&lt;td&gt;Фактичес кая&lt;/td&gt;
							&lt;td&gt;Норматив ная&lt;/td&gt;
							&lt;td&gt;Фактичес кая&lt;/td&gt;
							&lt;td&gt;Норматив ная&lt;/td&gt;
							&lt;td&gt;Фактичес кая&lt;/td&gt;
							&lt;td&gt;Норматив ная&lt;/td&gt;
							&lt;td&gt;Фактичес кое&lt;/td&gt;
							&lt;td&gt;Норматив ное&lt;/td&gt;
						&lt;/tr&gt;

						@for(int i = 0; i &lt; table.Rows.Count; i++) 
						{
							&lt;tr class="@RowTypeClass(table.Rows[i].RowType)"&gt; 
								&lt;td&gt;@table.Rows[i].Cells[0]&lt;/td&gt;
								&lt;td&gt;@CommonFormat(table.Rows[i].Cells[1])&lt;/td&gt;
								&lt;td&gt;@CommonFormat(table.Rows[i].Cells[2])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[3])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[4])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[5])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[6])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[7])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[8])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[9])&lt;/td&gt;
								&lt;td&gt;@FormatDouble2(table.Rows[i].Cells[10])&lt;/td&gt;
							&lt;/tr&gt;
						}
				&lt;/table&gt;
			}
		}
		</Content>
    </TableElement>

  </Elements>
</Section>')

INSERT INTO [dbo].[MasterReportTemplate] ([MasterReportTemplateID], [SequenceNumber], [RepeatOn], [Content])
VALUES (1, 1510, NULL, N'
<Section>
  <Elements>

	<EditableTextElement>
		<Id>1</Id>
		<Hidden>false</Hidden>
		<Content>
			&lt;p&gt;
				&lt;i&gt;Выводы по состоянию эксплуатационной среды:&lt;/i&gt;
				&lt;font color="aqua"&gt;  
					&lt;ul type="disc"&gt;
						&lt;li&gt;температура воздуха внутри помещения выше нормы, требуется регулировка системы отопления;&lt;/li&gt;
						&lt;li&gt;фактическая освещенность согласно замерам находится в пределах нормируемых значений;&lt;/li&gt;
						&lt;li&gt;инструментальные замеры параметров микроклимата показали несоответствие ГОСТ 30494-96.&lt;/li&gt;	
					&lt;/ul&gt;
				&lt;/font&gt;
			&lt;/p&gt;
		</Content>
	</EditableTextElement>

  </Elements>
</Section>')

