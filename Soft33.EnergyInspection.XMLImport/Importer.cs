﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyInspection.XMLImport
{
    public class Importer
    {
        private Inspection inspection;

        public Importer(Inspection inspection)
        {
            this.inspection = inspection;
        }

        public bool ImportFromClient()
        {
            return false;
        }

        public bool ImportFromAndroid()
        {
            return false;
        }

    }
}
