﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Models
{
    /// <summary>
    /// Спецификатор содержимого таблицы. Уточняет к каким данным
    /// происходит обращение в таблице CaptionsRegister.
    /// </summary>
    public enum CaptionsTypeID
    {
        MeterCaptionsElectricEnergy = 1,
        MeterCaptionsColdWater = 2,
        MeterCaptionsHotWater = 3,
        MeterCaptionsHeating = 4,
        MeterCaptionsGas = 5,
        MeterPlacesElectricEnergy = 6,
        MeterPlacesColdWater = 7,
        MeterPlacesHotWater = 8,
        MeterPlacesHeating = 9,
        MeterPlacesGas = 10,
        MeterClassesElectricEnergy = 11,
        MeterClassesColdWater = 12,
        MeterClassesHotWater = 13,
        MeterClassesHeating = 14,
        MeterClassesGas = 15
    }

    /// <summary>
    /// Класс для методов доступа к таблицам-справочникам,
    /// для которых не определены методы изменения данных (только чтение).
    /// </summary>
    public class DirectoryModels
    {
        private EnergyInspectionDataContext _dataContext;

        public DirectoryModels(EnergyInspectionDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        /// <summary>
        /// Получить все Типы русорсов
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourceSupplyType> GetResourceSupplyTypes()
        {
            return (from rst in _dataContext.ResourceSupplyType
                   select rst);
        }

        /// <summary>
        /// Выбрать Тип ресурса по его идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResourceSupplyType GetResourceSupplyTypeById(int id)
        {
            return (from rst in _dataContext.ResourceSupplyType
                    where rst.ResourceSupplyTypeID == id
                    select rst).FirstOrDefault();
        }

        /// <summary>
        /// Выбрать коэф-ты по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CityCoeff GetCityCoeffsById(int id)
        {
            return (from ccfs in _dataContext.CityCoeffs
                    where ccfs.CityCoeffsID == id
                    select ccfs).FirstOrDefault();
        }

        /// <summary>
        /// Выбрать все материалы
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WallMaterial> GetWallMaterials()
        {
            return (from wm in _dataContext.WallMaterial
                    select wm);
        }

        public IEnumerable<AtticFloorMaterial> GetAtticFloorMaterials()
        {
            return (from atf in _dataContext.AtticFloorMaterial
                    select atf);
        }

        public IEnumerable<BasementFloorMaterial> GetBasementFloorMaterials()
        {
            return (from bsf in _dataContext.BasementFloorMaterial
                    select bsf);
        }

        /// <summary>
        /// Выбрать материал по идентификатору
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public WallMaterial GetWallMaterialByUid(System.Guid uid)
        {
            return (from wm in _dataContext.WallMaterial
                    where wm.WallMaterialUID.Equals(uid)
                    select wm).FirstOrDefault();
        }

        /// <summary>
        /// Выбрать материал по идентификатору
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public AtticFloorMaterial GetAtticFloorMaterialByUid(System.Guid uid)
        {
            return (from atf in _dataContext.AtticFloorMaterial
                    where atf.AtticFloorMaterialUID.Equals(uid)
                    select atf).FirstOrDefault();
        }

        /// <summary>
        /// Выбрать материал по идентификатору
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public BasementFloorMaterial GetBasementFloorMaterialByUid(System.Guid uid)
        {
            return (from bsf in _dataContext.BasementFloorMaterial
                    where bsf.BasementFloorMaterialUID.Equals(uid)
                    select bsf).FirstOrDefault();
        }

        public IEnumerable<PointOfWaterDeviceType> GetPointOfWaterDeviceTypes()
        {
            return (from device in _dataContext.PointOfWaterDeviceType
                    select device);
        }

        public PointOfWaterDeviceType GetPointOfWaterDeviceTypeByUid(Guid uid)
        {
            return (from device in _dataContext.PointOfWaterDeviceType
                    where device.PointOfWaterDeviceTypeUID.Equals(uid)
                    select device).FirstOrDefault();
        }

        public IEnumerable<PointOfWaterType> GetPointOfWaterTypes()
        {
            return (from powt in _dataContext.PointOfWaterType
                    select powt);
        }

        public PointOfWaterType GetPointOfWaterTypeByUid(Guid uid)
        {
            return (from powt in _dataContext.PointOfWaterType
                    where powt.PointOfWaterTypeUID.Equals(uid)
                    select powt).FirstOrDefault();
        }

        public IEnumerable<RoomType> GetRoomTypes()
        {
            return (from roomType in _dataContext.RoomType
                    select roomType);
        }

        public RoomType GetRoomTypeByUid(Guid uid)
        {
            return (from roomType in _dataContext.RoomType
                    where roomType.RoomTypeUID.Equals(uid)
                    select roomType).FirstOrDefault();
        }

        public IEnumerable<WindowType> GetWindowTypes()
        {
            return (from windowType in _dataContext.WindowType
                    select windowType);
        }

        public WindowType GetWindowTypeByUid(Guid uid)
        {
            return (from windowType in _dataContext.WindowType
                    where windowType.WindowTypeUID.Equals(uid)
                    select windowType).FirstOrDefault();
        }

        public IEnumerable<DoorType> GetDoorTypes()
        {
            return (from doorType in _dataContext.DoorType
                    select doorType);
        }

        public DoorType GetDoorTypeByUid(Guid uid)
        {
            return (from doorType in _dataContext.DoorType
                    where doorType.DoorTypeUID.Equals(uid)
                    select doorType).FirstOrDefault();
        }

        //public IEnumerable<FoundationType> GetFoundationTypes()
        //{
        //    return (from foundationType in _dataContext.FoundationType
        //            select foundationType);
        //}

        //public FoundationType GetFoundationTypeByUid(Guid uid)
        //{
        //    return (from foundationType in _dataContext.FoundationType
        //            where foundationType.FoundationTypeUID.Equals(uid)
        //            select foundationType).FirstOrDefault();
        //}

        /// <summary>
        /// Производит поиск записи по точному совпадению с указанной строкой. Если
        /// такая запись отсутствует, добавляет ее.
        /// </summary>
        /// <param name="captionText">Заголовок, по которому производится поиск.</param>
        /// <param name="type">Уточняет какие записи необходимо искать.</param>
        /// <returns>Добавленная, либо найденная запись. Может вернуть null.</returns>
        public CaptionsRegister GetExistingOrAddCaption(string captionText, CaptionsTypeID type)
        {
            CaptionsRegister reg =  (from item in _dataContext.CaptionsRegisters
                                    where item.TypeID == (int)type
                                        && item.Caption == captionText
                                    select item).FirstOrDefault();

            if (reg == null)
            {
                // Попробовать вставить
                try
                {
                    CaptionsRegister newReg = new CaptionsRegister();
                    newReg.Caption = captionText;
                    newReg.TypeID = (int)type;
                    newReg.CaptionsRegisterUID = Guid.NewGuid();

                    _dataContext.CaptionsRegisters.InsertOnSubmit(newReg);
                    _dataContext.SubmitChanges();

                    return newReg;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    // При нарушении уникальности, попробовать выбрать снова
                    return (from item in _dataContext.CaptionsRegisters
                            where item.TypeID == (int)type
                                && item.Caption == captionText
                            select item).FirstOrDefault();
                }
            }
            else
            {
                return reg;
            }
        }

        /// <summary>
        /// Производит поиск записей, заголовки которых содержат указанный текст.
        /// </summary>
        /// <param name="captionText">Текст, который должен содержаться в заголовке (Caption).</param>
        /// <param name="maxCount">Максимально количество возвращаемых значений.</param>
        /// <param name="type">Уточняет какие записи необходимо искать.</param>
        /// <returns>Найденный Captions, которые содержат указанный текст.</returns>
        private IEnumerable<string> SelectCaptionsByContainingText(string captionText, int maxCount, CaptionsTypeID type)
        {
            return (from item in _dataContext.CaptionsRegisters
                    where item.TypeID == (int)type
                          && item.Caption.Contains(captionText)
                    select item.Caption).Take(maxCount);
        }

        public IEnumerable<string> GetMeterCaptionsElectricEnergy(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterCaptionsElectricEnergy);
        }

        public IEnumerable<string> GetMeterCaptionsColdWater(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterCaptionsColdWater);
        }

        public IEnumerable<string> GetMeterCaptionsHotWater(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterCaptionsHotWater);
        }

        public IEnumerable<string> GetMeterCaptionsHeating(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterCaptionsHeating);
        }

        public IEnumerable<string> GetMeterCaptionsGas(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterCaptionsGas);
        }

        public IEnumerable<string> GetMeterPlacesElectricEnergy(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterPlacesElectricEnergy);
        }

        public IEnumerable<string> GetMeterPlacesColdWater(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterPlacesColdWater);
        }

        public IEnumerable<string> GetMeterPlacesHotWater(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterPlacesHotWater);
        }

        public IEnumerable<string> GetMeterPlacesHeating(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterPlacesHeating);
        }

        public IEnumerable<string> GetMeterPlacesGas(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterPlacesGas);
        }

        public IEnumerable<string> GetMeterClassesElectricEnergy(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterClassesElectricEnergy);
        }

        public IEnumerable<string> GetMeterClassesColdWater(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterClassesColdWater);
        }

        public IEnumerable<string> GetMeterClassesHotWater(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterClassesHotWater);
        }

        public IEnumerable<string> GetMeterClassesHeating(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterClassesHeating);
        }

        public IEnumerable<string> GetMeterClassesGas(string captionText, int maxCount)
        {
            return SelectCaptionsByContainingText(captionText, maxCount, CaptionsTypeID.MeterClassesGas);
        }

        public IEnumerable<CityCoeff> GetCityCoeffs()
        {
            return (from cc in _dataContext.CityCoeffs
                    select cc);
        }
    }
}