﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Models
{
    //[Flags]
    //public enum BuildingOptions
    //{
    //    None = 0,
    //    IncludeResourcesSupplyTypes = 1,
    //    IncludeIllustrations = 2
    //}

    public class BuildingModels : BaseModels<Models.Building>
    {
        private EnergyInspectionDataContext _dataContext;

        public BuildingModels(EnergyInspectionDataContext dataContext)
            : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public Building GetBuildingById(int id)
        {
            return (from b in _dataContext.Building
                    where b.BuildingID == id
                    select b)
                        .FirstOrDefault<Building>();
        }

        public void Remove(Models.Building building)
        {
            _dataContext.Building.DeleteOnSubmit(building);
        }
    }
}