﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

using Navitas.DataAccess.Exceptions;
using Soft33.EnergyPassport.V2.BL;

namespace Navitas.Models
{
    public class InspectionReportList
    {
        public IList<Models.InspectionReport> Items { get; set; }

        public InspectionReportList()
        {
            Items = new List<Models.InspectionReport>();
        }

        public InspectionReportList(IList<Models.InspectionReport> list)
        {
            Items = new List<Models.InspectionReport>(list);
        }
    }

    public class InspectionReportModels : BaseModels<Models.InspectionReport>
    {
        private EnergyInspectionDataContext _dataContext;

        public InspectionReportModels(EnergyInspectionDataContext dataContext)
            : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public bool ReportExists(int inspectionId)
        {
            return _dataContext.InspectionReport.Any(ir => ir.InspectionID == inspectionId);
        }

        public IList<InspectionReport> SelectReportSections(int inspectionId)
        {
            return (from ir in _dataContext.InspectionReport
                    where ir.InspectionID == inspectionId
                    select ir).ToList();
        }

        public InspectionReport GetById(int sectionId)
        {
            return (from ir in _dataContext.InspectionReport
                    where ir.SectionID == sectionId
                    select ir).FirstOrDefault();
        }

        public void DeleteReport(int inspectionId)
        {
            _dataContext.InspectionReport.DeleteAllOnSubmit(SelectReportSections(inspectionId));
        }

        //public virtual void Add(Models.InspectionReport section)
        //{
        //    try
        //    {
        //        _dataContext.InspectionReports.InsertOnSubmit(section);
        //        _dataContext.SubmitChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new DataAccess.Exceptions.ReadWriteException("Ошибка при вставке нового обследования.", ex);
        //    }
        //}
    }
}