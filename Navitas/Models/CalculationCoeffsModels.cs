﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Models
{
    public class CalculationCoeffsModels : BaseModels<Models.CalculationCoeff>
    {
        private EnergyInspectionDataContext _dataContext;

        public CalculationCoeffsModels(EnergyInspectionDataContext dataContext)
            : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public CalculationCoeff GetCalculationCoeffByName(string name)
        {
            return (from i in _dataContext.CalculationCoeffs
                    where i.Name == name
                    select i).FirstOrDefault<CalculationCoeff>();
        }
    }
}