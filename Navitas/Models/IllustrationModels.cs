﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Models
{
    public class IllustrationModels : BaseModels<Models.Illustration>
    {
        private EnergyInspectionDataContext _dataContext;

        public IllustrationModels(EnergyInspectionDataContext dataContext)
            : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<Illustration> GetIllustrationsByInspectionId(int inspectionId)
        {
            return (from i in _dataContext.Illustration
                    where i.InspectionID == inspectionId
                    select i).ToList();
        }

        public Illustration GetIllustrationById(int id)
        {
            return (from i in _dataContext.Illustration
                    where i.IllustrationID == id
                    select i)
                        .FirstOrDefault<Illustration>();
        }

        public void Remove(Illustration illustration)
        {
            _dataContext.Illustration.DeleteOnSubmit(illustration);
        }
    }
}