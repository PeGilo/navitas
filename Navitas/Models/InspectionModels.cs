﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

using Navitas.DataAccess.Exceptions;
using Soft33.EnergyPassport.V2.BL;

namespace Navitas.Models
{
    //[Flags]
    //public enum InspectionOptions
    //{
    //    None = 0,
    //    IncludeBuildings = 1
    //}

    [Flags]
    public enum LoadOptions
    {
        None,
        IncludeBuildings = 1,
        IncludeResourcesSupplyTypes = 2,
        IncludeIllustrations = 4,
        IncludeAuditor = 8,
        IncludeCityCoeffs = 16
    }

    public class InspectionModels : BaseModels<Models.Inspection>
    {
        private EnergyInspectionDataContext _dataContext;

        //private InspectionOptions _options = InspectionOptions.None;
        private LoadOptions _options = LoadOptions.None;

        public LoadOptions Options
        {
            get { return _options; }
            set { _options = value; }
        }

        public InspectionModels(EnergyInspectionDataContext dataContext)
            : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public void Remove(Models.Inspection inspection)
        {
            _dataContext.Inspection.DeleteOnSubmit(inspection);
        }

        public IEnumerable<Inspection> GetInspectionOfAuditor(int auditorId)
        {
            return _dataContext.Inspection.Where(i => i.AuditorID == auditorId);
        }

        public IEnumerable<Inspection> GetInspectionsByAuditor(int auditorId, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            if(pageSize < 0){
                pageSize = 0;
            }
            if(pageNumber < 1){
                pageNumber = 1;
            }

            SetLoadOptions(); 
            var q = _dataContext.Inspection.Where(i => i.AuditorID == auditorId);
            totalRecords = q.Count();

            if (!String.IsNullOrEmpty(sortField))
            {
                string orderClause = sortField + " " + (asc.HasValue ? (asc.Value ? "ASC" : "DESC") : "ASC");
                q = q.OrderBy(orderClause);
            }
            return q.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public Inspection GetInspectionById(int inspectionId)
        {
            SetLoadOptions();
            return (from ins in _dataContext.Inspection
                            where ins.InspectionID == inspectionId
                            select ins)
                        .FirstOrDefault<Inspection>();
        }

        private void SetLoadOptions()
        {
            if (_options != LoadOptions.None)
            {
                DataLoadOptions dlo = new DataLoadOptions();

                if ((_options & LoadOptions.IncludeBuildings) != 0)
                {
                    dlo.LoadWith<Inspection>(insp => insp.Building);
                }

                if ((_options & LoadOptions.IncludeIllustrations) != 0)
                {
                    dlo.LoadWith<Building>(b => b.Illustration);
                }

                if ((_options & LoadOptions.IncludeAuditor) != 0)
                {
                    dlo.LoadWith<Inspection>(insp => insp.Auditor);
                }

                _dataContext.LoadOptions = dlo;
            }
        }
    }
}