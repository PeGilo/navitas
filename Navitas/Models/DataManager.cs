﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Models
{
    public class DataManager
    {
        private EnergyInspectionDataContext _dataContext;

        public DataManager(string connectionString)
        {
            _dataContext = new EnergyInspectionDataContext(connectionString);
        }

        private InspectionModels _inspection;

        public InspectionModels Inspection
        {
            get
            {
                if (_inspection == null)
                    _inspection = new InspectionModels(_dataContext);
                return _inspection;
            }
        }

        private BuildingModels _building;

        public BuildingModels Building
        {
            get
            {
                if (_building == null)
                    _building = new BuildingModels(_dataContext);
                return _building;
            }
        }

        private DirectoryModels _directory;

        public DirectoryModels Directory
        {
            get
            {
                if (_directory == null)
                    _directory = new DirectoryModels(_dataContext);
                return _directory;
            }
        }

        private InspectionReportModels _inspectionReport;

        public InspectionReportModels InspectionReport
        {
            get
            {
                if (_inspectionReport == null)
                    _inspectionReport = new InspectionReportModels(_dataContext);
                return _inspectionReport;
            }
        }

        private MasterReportModels _masterReportModels;

        public MasterReportModels MasterReportModels
        {
            get
            {
                if (_masterReportModels == null)
                    _masterReportModels = new MasterReportModels(_dataContext);
                return _masterReportModels;
            }
        }

        private IllustrationModels _illustrationModels;

        public IllustrationModels IllustrationModels
        {
            get
            {
                if (_illustrationModels == null)
                    _illustrationModels = new IllustrationModels(_dataContext);
                return _illustrationModels;
            }
        }

        private CalculationCoeffsModels _calculationCoeffsModels;

        public CalculationCoeffsModels CalculationCoeffsModels
        {
            get
            {
                if (_calculationCoeffsModels == null)
                    _calculationCoeffsModels = new CalculationCoeffsModels(_dataContext);
                return _calculationCoeffsModels;
            }
        }

        public void Save()
        {
            _dataContext.SubmitChanges();
        }
    }
}