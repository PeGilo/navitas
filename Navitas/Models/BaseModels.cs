﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

using Navitas.DataAccess.Exceptions;

namespace Navitas.Models
{
    public class BaseModels<E> where E : class
    {
        private EnergyInspectionDataContext _dataContext;

        public BaseModels(EnergyInspectionDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        /// <summary>
        /// Добавляет новый объект в таблицу.
        /// </summary>
        /// <typeparam name="T">Тип аттрибута-идентификатора</typeparam>
        /// <param name="entity">Добавляемый объект</param>
        /// <param name="IdPropertyName">Имя аттрибута-идентификатор</param>
        /// <returns>Идентификатор добавленного объекта</returns>
        public virtual T Add<T>(E entity, string IdPropertyName)
        {
            try
            {
                _dataContext.GetTable<E>().InsertOnSubmit(entity);
                _dataContext.SubmitChanges();

                return (T)(entity.GetType().GetProperty(IdPropertyName).GetValue(entity, null));
            }
            catch (System.Data.Linq.DuplicateKeyException ex)
            {
                throw new DataAccess.Exceptions.ConcurrencyException("Запись с таким ключем уже существует", ex);
            }
            catch (Exception ex)
            {
                throw new DataAccess.Exceptions.ReadWriteException("Ошибка при вставке нового объекта.", ex);
            }
        }

        public virtual void AddRange(IEnumerable<E> entities)
        {
            try
            {
                foreach (E entity in entities)
                {
                    _dataContext.GetTable<E>().InsertOnSubmit(entity);
                }
                _dataContext.SubmitChanges();
            }
            catch (System.Data.Linq.DuplicateKeyException ex)
            {
                throw new DataAccess.Exceptions.ConcurrencyException("Запись с таким ключем уже существует", ex);
            }
            catch (Exception ex)
            {
                throw new DataAccess.Exceptions.ReadWriteException("Ошибка при вставке нового объекта.", ex);
            }
        }
    }
}