﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

using Navitas.DataAccess.Exceptions;
using Soft33.EnergyPassport.V2.BL;

namespace Navitas.Models
{
    public class MasterReportModels
    {
        private EnergyInspectionDataContext _dataContext;

        public  MasterReportModels(EnergyInspectionDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public List<MasterReportTemplate> GetSections(int masterReportTemplateId)
        {
            return (from e in _dataContext.MasterReportTemplate
                    where e.MasterReportTemplateID == masterReportTemplateId
                    select e).ToList();
        }
    }
}