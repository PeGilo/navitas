﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Web.Routing;

namespace Navitas
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // localhost/file?fileName=123.JPG
            // localhost/file/Index/123.JPG
            // localhost/file/thumb?fileName=123.jpg&w=100&h=100
            routes.MapRoute(
                "Files",                                         // Route name
                "File/{action}/{fileName}",                           // URL with params
                new { controller = "File", action = "Index" } // Param defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ControllerBuilder.Current.SetControllerFactory(new Core.EnergyInspectionControllerFactory());

            ModelBinders.Binders.DefaultBinder = new Infrastructure.MvcHelpers.CustomModelBinder();

            DefaultModelBinder.ResourceClassKey = "Resources";

            var existingProvider = ModelValidatorProviders.Providers
                    .Single(x => x is ClientDataTypeModelValidatorProvider);
            ModelValidatorProviders.Providers.Remove(existingProvider);
            ModelValidatorProviders.Providers.Add(new Navitas.Infrastructure.MvcHelpers.ClientNumberValidatorProvider());

            // Для Razor'а
            //HostingEnvironment.RegisterVirtualPathProvider(new RazorEngine.Web.RazorVirtualPathProvider());
        }
    }
}