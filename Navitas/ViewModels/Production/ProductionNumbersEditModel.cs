﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Production
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>Пришлось сделать значения по отдельности, а не коллекцией, чтобы навесить валидирующие атрибуты.</remarks>
    public class ProductionNumbersEditModel : IDataAnnotationsSwitch
    {
        private bool _isReadOnly = false;

        public ProductionNumbersEditModel()
        {

        }

        /// <summary>
        /// Определяет предназначен ли объект для записи или только для чтения.
        /// Влияет на валидацию - для объектов, предназначенных только для чтения, отключаются __custom'ные__
        /// валидирующие аттрибуты.
        /// </summary>
        public bool IsReadOnly { get { return _isReadOnly; } set { _isReadOnly = value; } }

        [HyphenNumber(ErrorMessageResourceName="NotFloatingPointNumber", ErrorMessageResourceType=typeof(Navitas.Resources.ValidationStrings))]
        public string BaseMinus4 { get; set; }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string BaseMinus3 { get; set; }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string BaseMinus2 { get; set; }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string BaseMinus1 { get; set; }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string BaseMinus0 { get; set; }

        public string this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return BaseMinus4;
                    case 1:
                        return BaseMinus3;
                    case 2:
                        return BaseMinus2;
                    case 3:
                        return BaseMinus1;
                    case 4:
                        return BaseMinus0;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        BaseMinus4 = value;
                        break;
                    case 1:
                        BaseMinus3 = value;
                        break;
                    case 2:
                        BaseMinus2 = value;
                        break;
                    case 3:
                        BaseMinus1 = value;
                        break;
                    case 4:
                        BaseMinus0 = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }
        }

        public bool CheckValidationAttributes
        {
            get { return !IsReadOnly; }
        }

        //public IEnumerable<string> GetChangedProperties(ProductionNumbersEditModel oldModel)
        //{
        //    if (BaseMinus0 != oldModel.BaseMinus0)
        //    {
        //        yield return ReflectionHelper.GetPropertyName<string>(() => oldModel.BaseMinus0); //"BaseMinus0";
        //    }
        //    if (BaseMinus1 != oldModel.BaseMinus1)
        //    {
        //        yield return ReflectionHelper.GetPropertyName<string>(() => oldModel.BaseMinus1); //"BaseMinus1";
        //    }
        //    if (BaseMinus2 != oldModel.BaseMinus2)
        //    {
        //        yield return ReflectionHelper.GetPropertyName<string>(() => oldModel.BaseMinus2); // "BaseMinus2";
        //    }
        //    if (BaseMinus3 != oldModel.BaseMinus3)
        //    {
        //        yield return ReflectionHelper.GetPropertyName<string>(() => oldModel.BaseMinus3); // "BaseMinus3";
        //    }
        //    if (BaseMinus4 != oldModel.BaseMinus4)
        //    {
        //        yield return ReflectionHelper.GetPropertyName<string>(() => oldModel.BaseMinus4); // "BaseMinus4";
        //    }
        //}
    }
}