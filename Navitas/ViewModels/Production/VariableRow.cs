﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Production
{
    public class VariableRow<T>
    {
        public VariableRow()
        {
            Items = new List<T>();
        }

        public VariableRow(int itemsCount)
        {
            Items = new List<T>(itemsCount);
            for (int i = 0; i < itemsCount; i++)
            {
                Items.Add(default(T));
            }
        }

        public IList<T> Items { get; set; }
    }
}