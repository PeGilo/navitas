﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Production
{
    public class VariableStringRow : VariableRow<String>
    {
        public VariableStringRow(int itemsCount)
            : base(itemsCount)
        {
        }
    }
}