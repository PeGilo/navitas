﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Production
{
    public class ProductionEditModel
    {
        private const int ItemsCount = 5;

        public int BaseYear { get; set; }

        [LocalizedDisplayName("ProductionRow15", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow15Description", NameResourceType = typeof(Resources.Names))]
        public VariableStringRow Row1 { get; private set; }

        [LocalizedDisplayName("ProductionRow16", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow16Description", NameResourceType = typeof(Resources.Names))]
        public VariableStringRow Row2 { get; private set; }

        [LocalizedDisplayName("ProductionRow17", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow17Description", NameResourceType = typeof(Resources.Names))]
        public ProductionNumbersEditModel Row3 { get; private set; }

        [LocalizedDisplayName("ProductionRow18", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow18Description", NameResourceType = typeof(Resources.Names))]
        public ProductionNumbersEditModel Row4 { get; private set; }

        [LocalizedDisplayName("ProductionRow19", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow19Description", NameResourceType = typeof(Resources.Names))]
        public ProductionNumbersEditModel Row5 { get; private set; }

        [LocalizedDisplayName("ProductionRow20", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow20Description", NameResourceType = typeof(Resources.Names))]
        public ProductionNumbersEditModel Row6 { get; private set; }

        [LocalizedDisplayName("ProductionRow21", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow21Description", NameResourceType = typeof(Resources.Names))]
        public ProductionNumbersEditModel Row7 { get; private set; }

        [LocalizedDisplayName("ProductionRow22", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ProductionRow22Description", NameResourceType = typeof(Resources.Names))]
        public ProductionNumbersEditModel Row8 { get; private set; }

        public string PhysicalTerms { get; set; }
        public string MainProductionPhysicalTerms { get; set; }

        public ProductionEditModel()
        {
            PhysicalTerms = String.Empty;
            MainProductionPhysicalTerms = String.Empty;
            Row1 = new VariableStringRow(ItemsCount);
            Row2 = new VariableStringRow(ItemsCount);
            Row3 = new ProductionNumbersEditModel();
            Row4 = new ProductionNumbersEditModel();
            Row5 = new ProductionNumbersEditModel();
            Row6 = new ProductionNumbersEditModel();
            Row7 = new ProductionNumbersEditModel();
            Row8 = new ProductionNumbersEditModel();
        }

        public ProductionEditModel(Soft33.EnergyPassport.V2.BL.Production entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.Production entity)
        {
            entity.PhysicalTerms = PhysicalTerms;
            entity.MainProductionPhysicalTerms = MainProductionPhysicalTerms;

            #region "Копирование данных из строк в столбцы"
            entity.ProductionOfYearBaseMinus4.NomenclatureOfMainProducts = Row1.Items[0];
            entity.ProductionOfYearBaseMinus3.NomenclatureOfMainProducts = Row1.Items[1]; 
            entity.ProductionOfYearBaseMinus2.NomenclatureOfMainProducts = Row1.Items[2]; 
            entity.ProductionOfYearBaseMinus1.NomenclatureOfMainProducts = Row1.Items[3]; 
            entity.ProductionOfYearBaseMinus0.NomenclatureOfMainProducts = Row1.Items[4]; 

            entity.ProductionOfYearBaseMinus4.CodeOfMainProducts = Row2.Items[0];
            entity.ProductionOfYearBaseMinus3.CodeOfMainProducts = Row2.Items[1]; 
            entity.ProductionOfYearBaseMinus2.CodeOfMainProducts = Row2.Items[2]; 
            entity.ProductionOfYearBaseMinus1.CodeOfMainProducts = Row2.Items[3]; 
            entity.ProductionOfYearBaseMinus0.CodeOfMainProducts = Row2.Items[4]; 

            entity.ProductionOfYearBaseMinus4.VolumeOfProduction = ConvertHelper.ParseFloat(Row3[0]); 
            entity.ProductionOfYearBaseMinus3.VolumeOfProduction = ConvertHelper.ParseFloat(Row3[1]); 
            entity.ProductionOfYearBaseMinus2.VolumeOfProduction = ConvertHelper.ParseFloat(Row3[2]); 
            entity.ProductionOfYearBaseMinus1.VolumeOfProduction = ConvertHelper.ParseFloat(Row3[3]);
            entity.ProductionOfYearBaseMinus0.VolumeOfProduction = ConvertHelper.ParseFloat(Row3[4]);

            entity.ProductionOfYearBaseMinus4.VolumeOfMainProduction = ConvertHelper.ParseFloat(Row4[0]);
            entity.ProductionOfYearBaseMinus3.VolumeOfMainProduction = ConvertHelper.ParseFloat(Row4[1]);
            entity.ProductionOfYearBaseMinus2.VolumeOfMainProduction = ConvertHelper.ParseFloat(Row4[2]);
            entity.ProductionOfYearBaseMinus1.VolumeOfMainProduction = ConvertHelper.ParseFloat(Row4[3]);
            entity.ProductionOfYearBaseMinus0.VolumeOfMainProduction = ConvertHelper.ParseFloat(Row4[4]);

            entity.ProductionOfYearBaseMinus4.VolumeOfAdditionalProduction = ConvertHelper.ParseFloat(Row5[0]);
            entity.ProductionOfYearBaseMinus3.VolumeOfAdditionalProduction = ConvertHelper.ParseFloat(Row5[1]);
            entity.ProductionOfYearBaseMinus2.VolumeOfAdditionalProduction = ConvertHelper.ParseFloat(Row5[2]);
            entity.ProductionOfYearBaseMinus1.VolumeOfAdditionalProduction = ConvertHelper.ParseFloat(Row5[3]);
            entity.ProductionOfYearBaseMinus0.VolumeOfAdditionalProduction = ConvertHelper.ParseFloat(Row5[4]);

            entity.ProductionOfYearBaseMinus4.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts = ConvertHelper.ParseFloat(Row6[0]);
            entity.ProductionOfYearBaseMinus3.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts = ConvertHelper.ParseFloat(Row6[1]);
            entity.ProductionOfYearBaseMinus2.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts = ConvertHelper.ParseFloat(Row6[2]);
            entity.ProductionOfYearBaseMinus1.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts = ConvertHelper.ParseFloat(Row6[3]);
            entity.ProductionOfYearBaseMinus0.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts = ConvertHelper.ParseFloat(Row6[4]);

            entity.ProductionOfYearBaseMinus4.ProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row7[0]);
            entity.ProductionOfYearBaseMinus3.ProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row7[1]);
            entity.ProductionOfYearBaseMinus2.ProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row7[2]);
            entity.ProductionOfYearBaseMinus1.ProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row7[3]);
            entity.ProductionOfYearBaseMinus0.ProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row7[4]);

            entity.ProductionOfYearBaseMinus4.MainProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row8[0]);
            entity.ProductionOfYearBaseMinus3.MainProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row8[1]);
            entity.ProductionOfYearBaseMinus2.MainProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row8[2]);
            entity.ProductionOfYearBaseMinus1.MainProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row8[3]);
            entity.ProductionOfYearBaseMinus0.MainProductionInPhysicalTerms = ConvertHelper.ParseFloat(Row8[4]);
            #endregion
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.Production entity)
        {
            PhysicalTerms = entity.PhysicalTerms;
            MainProductionPhysicalTerms = entity.MainProductionPhysicalTerms;

            #region "Копирование данных из столбцов в строки"
            Row1.Items[0] = entity.ProductionOfYearBaseMinus4.NomenclatureOfMainProducts;
            Row1.Items[1] = entity.ProductionOfYearBaseMinus3.NomenclatureOfMainProducts;
            Row1.Items[2] = entity.ProductionOfYearBaseMinus2.NomenclatureOfMainProducts;
            Row1.Items[3] = entity.ProductionOfYearBaseMinus1.NomenclatureOfMainProducts;
            Row1.Items[4] = entity.ProductionOfYearBaseMinus0.NomenclatureOfMainProducts;

            Row2.Items[0] = entity.ProductionOfYearBaseMinus4.CodeOfMainProducts;
            Row2.Items[1] = entity.ProductionOfYearBaseMinus3.CodeOfMainProducts;
            Row2.Items[2] = entity.ProductionOfYearBaseMinus2.CodeOfMainProducts;
            Row2.Items[3] = entity.ProductionOfYearBaseMinus1.CodeOfMainProducts;
            Row2.Items[4] = entity.ProductionOfYearBaseMinus0.CodeOfMainProducts;

            Row3[0] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus4.VolumeOfProduction);
            Row3[1] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus3.VolumeOfProduction);
            Row3[2] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus2.VolumeOfProduction);
            Row3[3] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus1.VolumeOfProduction);
            Row3[4] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus0.VolumeOfProduction);

            Row4[0] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus4.VolumeOfMainProduction);
            Row4[1] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus3.VolumeOfMainProduction);
            Row4[2] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus2.VolumeOfMainProduction);
            Row4[3] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus1.VolumeOfMainProduction);
            Row4[4] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus0.VolumeOfMainProduction);

            Row5[0] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus4.VolumeOfAdditionalProduction);
            Row5[1] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus3.VolumeOfAdditionalProduction);
            Row5[2] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus2.VolumeOfAdditionalProduction);
            Row5[3] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus1.VolumeOfAdditionalProduction);
            Row5[4] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus0.VolumeOfAdditionalProduction);

            Row6[0] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus4.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts);
            Row6[1] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus3.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts);
            Row6[2] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus2.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts);
            Row6[3] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus1.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts);
            Row6[4] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus0.VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts);

            Row7[0] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus4.ProductionInPhysicalTerms);
            Row7[1] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus3.ProductionInPhysicalTerms);
            Row7[2] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus2.ProductionInPhysicalTerms);
            Row7[3] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus1.ProductionInPhysicalTerms);
            Row7[4] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus0.ProductionInPhysicalTerms);

            Row8[0] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus4.MainProductionInPhysicalTerms);
            Row8[1] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus3.MainProductionInPhysicalTerms);
            Row8[2] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus2.MainProductionInPhysicalTerms);
            Row8[3] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus1.MainProductionInPhysicalTerms);
            Row8[4] = ConvertHelper.Convert(entity.ProductionOfYearBaseMinus0.MainProductionInPhysicalTerms);
            #endregion
        }
    }
}