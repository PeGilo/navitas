﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Services;

namespace Navitas.ViewModels.Vehicle
{
    public class VehicleListEditModel
    {
        public IList<VehicleUnitEditModel> Vehicles { get; private set; }

        /// <summary>
        /// Список видов топлива.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList FuelTypeList
        {
            get;
            set;
        }

        /// <summary>
        /// Список видов одометров.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList OdometerTypeList
        {
            get;
            set;
        }

        public VehicleListEditModel()
        {
            Vehicles = new List<VehicleUnitEditModel>();
        }

        public VehicleListEditModel(Soft33.EnergyPassport.V2.BL.VehicleList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.VehicleList entity)
        {
            entity.Vehicles.Clear();
            foreach (var unit in Vehicles)
            {
                var entityUnit = new Soft33.EnergyPassport.V2.BL.VehicleUnit();
                unit.UpdateEntity(entityUnit);
                entity.Vehicles.Add(entityUnit);
            }
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.VehicleList entity)
        {
            Vehicles = new List<VehicleUnitEditModel>();

            foreach (var entityUnit in entity.Vehicles)
            {
                var unit = new ViewModels.Vehicle.VehicleUnitEditModel(entityUnit);
                Vehicles.Add(unit);
            }
        }
    }
}