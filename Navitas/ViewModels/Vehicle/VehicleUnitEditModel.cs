﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Vehicle
{
    public class VehicleUnitEditModel
    {
        /* Атрибут Required определяется для целочисленных, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("Vehicle_Brand", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_Brand_Description", NameResourceType = typeof(Resources.Names))]
        public string Brand { get; set; }

        [LocalizedDisplayName("Vehicle_Model", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_Model_Description", NameResourceType = typeof(Resources.Names))]
        public string Model { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType=typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Vehicle_Year", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_Year_Description", NameResourceType = typeof(Resources.Names))]
        public int Year { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Vehicle_Count", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_Count_Description", NameResourceType = typeof(Resources.Names))]
        public int Count { get; set; }

        [LocalizedDisplayName("Vehicle_Capacity", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_Capacity_Description", NameResourceType = typeof(Resources.Names))]
        public string Capacity { get; set; }

        [LocalizedDisplayName("Vehicle_TypeOfFuel", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_TypeOfFuel_Description", NameResourceType = typeof(Resources.Names))]
        public string TypeOfFuel { get; set; }

        [LocalizedDisplayName("Vehicle_FuelConsumption", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_FuelConsumption_Description", NameResourceType = typeof(Resources.Names))]
        public string FuelConsumption { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Vehicle_MileageLastYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_MileageLastYear_Description", NameResourceType = typeof(Resources.Names))]
        public int MileageLastYear { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Vehicle_OperatingTimeLastYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_OperatingTimeLastYear_Description", NameResourceType = typeof(Resources.Names))]
        public int OperatingTimeLastYear { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Vehicle_VolumeOfCargo", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_VolumeOfCargo_Description", NameResourceType = typeof(Resources.Names))]
        public int VolumeOfCargo { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Vehicle_VolumeOfPassengerTraffic", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_VolumeOfPassengerTraffic_Description", NameResourceType = typeof(Resources.Names))]
        public int VolumeOfPassengerTraffic { get; set; }

        [LocalizedDisplayName("Vehicle_TypeOfOdometer", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_TypeOfOdometer_Description", NameResourceType = typeof(Resources.Names))]
        public string TypeOfOdometer { get; set; }

        [LocalizedDisplayName("Vehicle_OdometerIsOK", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_OdometerIsOK_Description", NameResourceType = typeof(Resources.Names))]
        public bool OdometerIsOK { get; set; }

        [LocalizedDisplayName("Vehicle_OdometerSealed", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Vehicle_OdometerSealed_Description", NameResourceType = typeof(Resources.Names))]
        public bool OdometerSealed { get; set; }
        
        public VehicleUnitEditModel()
        {
            Brand = String.Empty;
            Model = String.Empty;
            Capacity = String.Empty;
            TypeOfFuel = String.Empty;
            FuelConsumption = String.Empty;
            TypeOfOdometer = String.Empty;
            OdometerIsOK = true;
            OdometerSealed = true;
        }

        public VehicleUnitEditModel(Soft33.EnergyPassport.V2.BL.VehicleUnit entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.VehicleUnit entity)
        {
            entity.Brand = Brand;
            entity.Model = Model;
            entity.Year = Year;
            entity.Count = Count;
            entity.Capacity = Capacity;
            entity.TypeOfFuel = TypeOfFuel;
            entity.FuelConsumption = FuelConsumption;
            entity.MileageLastYear = MileageLastYear;
            entity.OperatingTimeLastYear = OperatingTimeLastYear;
            entity.VolumeOfCargo = VolumeOfCargo;
            entity.VolumeOfPassengerTraffic = VolumeOfPassengerTraffic;
            entity.TypeOfOdometer = TypeOfOdometer;
            entity.OdometerIsOK = OdometerIsOK;
            entity.OdometerSealed = OdometerSealed;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.VehicleUnit entity)
        {
            Brand = entity.Brand;
            Model = entity.Model;
            Year = entity.Year;
            Count = entity.Count;
            Capacity = entity.Capacity;
            TypeOfFuel = entity.TypeOfFuel;
            FuelConsumption = entity.FuelConsumption;
            MileageLastYear = entity.MileageLastYear;
            OperatingTimeLastYear = entity.OperatingTimeLastYear;
            VolumeOfCargo = entity.VolumeOfCargo;
            VolumeOfPassengerTraffic = entity.VolumeOfPassengerTraffic;
            TypeOfOdometer = entity.TypeOfOdometer;
            OdometerIsOK = entity.OdometerIsOK;
            OdometerSealed = entity.OdometerSealed;
        }
    }
}