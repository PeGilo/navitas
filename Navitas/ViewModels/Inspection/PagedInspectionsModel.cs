﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Inspection
{
    public class PagedInspectionsModel
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalRows { get; set; }
        public IEnumerable<InspectionBriefModel> Inspections { get; set; }

        public PagedInspectionsModel()
        {
            PageSize = 0;
            PageNumber = 0;
            TotalRows = 0;
            Inspections = new List<InspectionBriefModel>();
        }
    }
}