﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Inspection
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class CalculationCoeffsEditModel
    {
        /// <summary>
        /// Тариф на тепловую энергию
        /// </summary>
        [LocalizedDisplayName("Technical_ThermoEnergyTariff", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ThermoEnergyTariff_Description", NameResourceType = typeof(Resources.Names))]        
        public double? ThermoEnergyTariff { get; set; }

        /// <summary>
        /// Тариф на хол. воду
        /// </summary>
        [LocalizedDisplayName("Technical_ColdWaterTariff", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ColdWaterTariff_Description", NameResourceType = typeof(Resources.Names))]
        public double? ColdWaterTariff { get; set; }

        /// <summary>
        /// Тариф на гор. воду
        /// </summary>
        [LocalizedDisplayName("Technical_HotWaterTariff", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_HotWaterTariff_Description", NameResourceType = typeof(Resources.Names))]
        public double? HotWaterTariff { get; set; }

        public CalculationCoeffsEditModel()
        {
        }

        public CalculationCoeffsEditModel(BL.CalculationCoeffsOfInspection entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.CalculationCoeffsOfInspection entity)
        {
            entity.ThermoEnergyTariff = ThermoEnergyTariff;
            entity.HotWaterTariff = HotWaterTariff;
            entity.ColdWaterTariff = ColdWaterTariff;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.CalculationCoeffsOfInspection entity)
        {
            ThermoEnergyTariff = entity.ThermoEnergyTariff;
            HotWaterTariff = entity.HotWaterTariff;
            ColdWaterTariff = entity.ColdWaterTariff;
        }
    }
}