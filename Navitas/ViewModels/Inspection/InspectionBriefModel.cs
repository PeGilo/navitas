﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Inspection
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class InspectionBriefModel
    {
        public int InspectionID { get; set; }
        public string State { get; set; }
        public string ClientCaption { get; set; }
        public string AuditorFullName { get; set; }
        public string Accountant { get; set; }

        public InspectionBriefModel()
        {
            InspectionID = 1;
            State = "Неизвестно";
            Accountant = "Неизвестно";
            AuditorFullName = "Аудитор тест";
            ClientCaption = "Название клиента";
        }

        public InspectionBriefModel(BL.InspectionBrief entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.InspectionBrief entity)
        {
            InspectionID = entity.InspectionID;
            ClientCaption = entity.ClientCaption;
            AuditorFullName = entity.AuditorFullName;
        }
    }
}