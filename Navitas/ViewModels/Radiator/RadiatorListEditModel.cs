﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Radiator
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class RadiatorListEditModel
    {
        public IList<RadiatorItemEditModel> Items { get; private set; }

        public RadiatorListEditModel()
        {
            Items = new List<RadiatorItemEditModel>();
        }

        public RadiatorListEditModel(BL.RadiatorList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.RadiatorList entity)
        {
            List<BL.Radiator> newList = new List<BL.Radiator>();
            foreach (var item in Items)
            {
                BL.Radiator entityItem = new BL.Radiator();

                item.UpdateEntity(entityItem);
                newList.Add(entityItem);
            }
            entity.Radiators = newList;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.RadiatorList entity)
        {
            Items = new List<RadiatorItemEditModel>();

            foreach (var entityItem in entity.Radiators)
            {
                Items.Add(new RadiatorItemEditModel(entityItem));
            }
        }
    }
}