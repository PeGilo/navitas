﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Radiator
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class RadiatorItemEditModel
    {
        [LocalizedDisplayName("Radiator_Location", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Radiator_Location_Description", NameResourceType = typeof(Resources.Names))]
        public string Location { get; set; }

        [LocalizedDisplayName("Radiator_Name", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Radiator_Name_Description", NameResourceType = typeof(Resources.Names))]
        public string Name { get; set; }

        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Radiator_Count", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Radiator_Count_Description", NameResourceType = typeof(Resources.Names))]
        public string Count { get; set; } // int

        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Radiator_CountThermostats", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Radiator_CountThermostats_Description", NameResourceType = typeof(Resources.Names))]
        public string CountThermostats { get; set; } // int

        public RadiatorItemEditModel()
        {
        }

        public RadiatorItemEditModel(BL.Radiator entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Radiator entity)
        {
            entity.Name = Name;
            entity.Location = Location;
            entity.Count = ConvertHelper.ParseInt32(Count);
            entity.CountThermostats = ConvertHelper.ParseInt32(CountThermostats);
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Radiator entity)
        {
            Name = entity.Name;
            Location = entity.Location;
            Count = ConvertHelper.Convert(entity.Count);
            CountThermostats = ConvertHelper.Convert(entity.CountThermostats);
        }
    }
}