﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Client
{
    public class ClientEditModel
    {
        [DisplayName("01. Краткое наименование юридического лица")]
        [Description("Например, Тагильская средняя школа №1")]
        public string Name { get; set; }

        [DisplayName("03. Полное наименование юридического лица")]
        [Description("Например, МБУЗ Тагильская средняя школа №1")]
        public string FullName { get; set; }

        [DisplayName("02. Организационно правовая форма")]
        [Description("Например, Общество с ограниченной ответственностью или Краевое государственное учреждение")]
        public string OPF { get; set; }

        [DisplayName("04. Фактический адрес")]
        [Description("Фактическое местонахождение юридического лица")]
        public string PhysicalAddress { get; set; }

        [DisplayName("05. Юридический адрес")]
        [Description("Юридический адрес")]
        public string JuridicalAddress { get; set; }

        [DisplayName("06. Наименование основного общества")]
        [Description("Для зависимых обществ")]
        public string NameOfParentCompany { get; set; }

        [DisplayName("07. Доля муниципальной, государственной обственности")]
        [Description("с указанием единицы, например, 75%")]
        public string ShareOfOwnership { get; set; }

        [DisplayName("08. ИНН")]
        [Description("ИНН")]
        public string INN { get; set; }

        [DisplayName("09. КПП")]
        [Description("КПП")]
        public string KPP { get; set; }

        [DisplayName("10. Расчётный счёт")]
        [Description("Расчётный счёт, полная формулировка, например, 407883738684585 в Красноярском филиале ОАО \"ВТБ24\" корр.счёт 301234234223, БИК 040456768 г.Красноярск")]
        public string CurrentAccount { get; set; }

        [DisplayName("11. ОКВЭД")]
        [Description("ОКВЭД")]
        public ViewModels.OKVED.OKVEDEditModel Okved { get; set; }

        [DisplayName("12. Руководитель")]
        [Description("Руководитель юридического лица")]
        public ViewModels.Person.PersonEditModel Head { get; set; }

        [DisplayName("13. Ответственный за энергообследование")]
        [Description("Ответственный за проведение энергетического обследования")]
        public ViewModels.Person.PersonEditModel EnergyPerson { get; set; }

        [DisplayName("14. Энергетик")]
        [Description("Энергетик")]
        public ViewModels.Person.PersonEditModel TechnicalPerson { get; set; }

        public ClientEditModel()
        {
            Name = String.Empty;
            FullName = String.Empty;
            OPF = String.Empty;
            PhysicalAddress = String.Empty;
            JuridicalAddress = String.Empty;
            NameOfParentCompany = String.Empty;
            ShareOfOwnership = String.Empty;
            INN = String.Empty;
            KPP = String.Empty;
            CurrentAccount = String.Empty;
            Okved = new ViewModels.OKVED.OKVEDEditModel();
            Head = new ViewModels.Person.PersonEditModel();
            EnergyPerson = new ViewModels.Person.PersonEditModel();
            TechnicalPerson = new ViewModels.Person.PersonEditModel();
        }

        public ClientEditModel(Soft33.EnergyPassport.V2.BL.Client entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.Client entity)
        {
            entity.Name = Name;
            entity.FullName = FullName;
            entity.OPF = OPF;
            entity.PhysicalAddress = PhysicalAddress;
            entity.JuridicalAddress = JuridicalAddress;
            entity.NameOfParentCompany = NameOfParentCompany;
            entity.ShareOfOwnership = ShareOfOwnership;
            entity.INN = INN;
            entity.KPP = KPP;
            entity.CurrentAccount = CurrentAccount;
            Okved.UpdateEntity(entity.Okved);
            Head.UpdateEntity(entity.Head);
            EnergyPerson.UpdateEntity(entity.EnergyPerson);
            TechnicalPerson.UpdateEntity(entity.TechnicalPerson);
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.Client entity)
        {
            Name = entity.Name;
            FullName = entity.FullName;
            OPF = entity.OPF;
            PhysicalAddress = entity.PhysicalAddress;
            JuridicalAddress = entity.JuridicalAddress;
            NameOfParentCompany = entity.NameOfParentCompany;
            ShareOfOwnership = entity.ShareOfOwnership;
            INN = entity.INN;
            KPP = entity.KPP;
            CurrentAccount = entity.CurrentAccount;
            Okved = new OKVED.OKVEDEditModel(entity.Okved);
            Head = new Person.PersonEditModel(entity.Head);
            EnergyPerson = new Person.PersonEditModel(entity.EnergyPerson);
            TechnicalPerson = new Person.PersonEditModel(entity.TechnicalPerson);
        }
    }

}