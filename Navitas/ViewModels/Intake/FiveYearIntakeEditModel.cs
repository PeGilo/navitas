﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.ViewModels.Common;

namespace Navitas.ViewModels.Intake
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Модель представления для редактирования данных "Потребления энергии за 5 лет"
    /// </summary>
    public class FiveYearIntakeEditModel
    {
        private const int ROWS_COUNT = 32;

        public int BaseYear { get; set; }

        public IList<Production.ProductionNumbersEditModel> Units { get; private set; }

        public FiveYearIntakeEditModel()
        {
            Units = new List<Production.ProductionNumbersEditModel>();
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                Units.Add(new Production.ProductionNumbersEditModel());
            }
        }

        public FiveYearIntakeEditModel(BL.FiveYearIntake entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.FiveYearIntake entity)
        {
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                UpdateField(-4, i, entity, Units[i].BaseMinus4);
                UpdateField(-3, i, entity, Units[i].BaseMinus3);
                UpdateField(-2, i, entity, Units[i].BaseMinus2);
                UpdateField(-1, i, entity, Units[i].BaseMinus1);
                UpdateField(0, i, entity, Units[i].BaseMinus0);
            }
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.FiveYearIntake entity)
        {
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                Units[i].BaseMinus4 = MapField(-4, i, entity);
                Units[i].BaseMinus3 = MapField(-3, i, entity);
                Units[i].BaseMinus2 = MapField(-2, i, entity);
                Units[i].BaseMinus1 = MapField(-1, i, entity);
                Units[i].BaseMinus0 = MapField(0, i, entity);
            }

            // Установить какие строки read only
            Units[2].IsReadOnly = Units[3].IsReadOnly = Units[22].IsReadOnly = Units[23].IsReadOnly = true;
        }

        /// <summary>
        /// Возвращает значение из бизнес-объекта, соответствующее индексу строки и году
        /// </summary>
        /// <param name="year">год (0, -1, -2, ...)</param>
        /// <param name="index">индекс строки в таблице</param>
        /// <param name="intake"></param>
        /// <returns>Значение, конвертированное в текст</returns>
        private string MapField(int year, int index, BL.FiveYearIntake intake)
        {
            BL.IntakeOfPeriod period = intake.IntakeOfYears[year];
            #region "Копирование значений"
            switch (index)
            {
                case 0:
                    return ConvertHelper.Convert(period.ElectricEnergy.Value);
                case 1:
                    return ConvertHelper.Convert(period.ElectricEnergy.ValueRUR);
                case 2:
                    return ConvertHelper.Convert(period.ThermalEnergy.Value);
                case 3:
                    return ConvertHelper.Convert(period.ThermalEnergy.ValueRUR);
                case 4:
                    return ConvertHelper.Convert(period.TERHeating.Value);
                case 5:
                    return ConvertHelper.Convert(period.TERHeating.ValueRUR);
                case 6:
                    return ConvertHelper.Convert(period.TERDhw.Value);
                case 7:
                    return ConvertHelper.Convert(period.TERDhw.ValueRUR);
                case 8:
                    return ConvertHelper.Convert(period.TERVentilationInlet.Value);
                case 9:
                    return ConvertHelper.Convert(period.TERVentilationInlet.ValueRUR);
                case 10:
                    return ConvertHelper.Convert(period.HotWater.Value);
                case 11:
                    return ConvertHelper.Convert(period.HotWater.ValueRUR);
                case 12:
                    return ConvertHelper.Convert(period.ColdWater.Value);
                case 13:
                    return ConvertHelper.Convert(period.ColdWater.ValueRUR);
                case 14:
                    return ConvertHelper.Convert(period.Wastewater.Value);
                case 15:
                    return ConvertHelper.Convert(period.Wastewater.ValueRUR);
                case 16:
                    return ConvertHelper.Convert(period.Firewood.Value);
                case 17:
                    return ConvertHelper.Convert(period.Firewood.ValueRUR);
                case 18:
                    return ConvertHelper.Convert(period.Coal.Value);
                case 19:
                    return ConvertHelper.Convert(period.Coal.ValueRUR);
                case 20:
                    return ConvertHelper.Convert(period.NaturalGas.Value);
                case 21:
                    return ConvertHelper.Convert(period.NaturalGas.ValueRUR);
                case 22:
                    return ConvertHelper.Convert(period.FuelOil.Value);
                case 23:
                    return ConvertHelper.Convert(period.FuelOil.ValueRUR);
                case 24:
                    return ConvertHelper.Convert(period.TERGasoline.Value);
                case 25:
                    return ConvertHelper.Convert(period.TERGasoline.ValueRUR);
                case 26:
                    return ConvertHelper.Convert(period.TERKerosene.Value);
                case 27:
                    return ConvertHelper.Convert(period.TERKerosene.ValueRUR);
                case 28:
                    return ConvertHelper.Convert(period.TERDieselFuel.Value);
                case 29:
                    return ConvertHelper.Convert(period.TERDieselFuel.ValueRUR);
                case 30:
                    return ConvertHelper.Convert(period.TERGas.Value);
                case 31:
                    return ConvertHelper.Convert(period.TERGas.ValueRUR);
                default:
                    throw new IndexOutOfRangeException();
            }
            #endregion
        }

        /// <summary>
        /// Конвертирует и записывает значение в бизнес-объект, соответсвенно индексу строки и году
        /// </summary>
        /// <param name="year">год (0, -1, -2, ...)</param>
        /// <param name="index">индекс строки в таблице</param>
        /// <param name="intake"></param>
        /// <param name="value">значение, которое необходимо записать, предварительно его конвертировав</param>
        private void UpdateField(int year, int index, BL.FiveYearIntake intake, string value)
        {
            Soft33.EnergyPassport.V2.BL.IntakeOfPeriod period = intake.IntakeOfYears[year];
            #region "Копирование значений"
            switch (index)
            {
                case 0:
                    period.ElectricEnergy.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 1:
                    period.ElectricEnergy.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 2:
                    period.ThermalEnergy.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 3:
                    period.ThermalEnergy.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 4:
                    period.TERHeating.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 5:
                    period.TERHeating.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 6:
                    period.TERDhw.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 7:
                    period.TERDhw.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 8:
                    period.TERVentilationInlet.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 9:
                    period.TERVentilationInlet.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 10:
                    period.HotWater.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 11:
                    period.HotWater.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 12:
                    period.ColdWater.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 13:
                    period.ColdWater.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 14:
                    period.Wastewater.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 15:
                    period.Wastewater.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 16:
                    period.Firewood.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 17:
                    period.Firewood.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 18:
                    period.Coal.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 19:
                    period.Coal.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 20:
                    period.NaturalGas.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 21:
                    period.NaturalGas.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 22:
                    period.FuelOil.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 23:
                    period.FuelOil.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 24:
                    period.TERGasoline.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 25:
                    period.TERGasoline.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 26:
                    period.TERKerosene.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 27:
                    period.TERKerosene.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 28:
                    period.TERDieselFuel.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 29:
                    period.TERDieselFuel.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                case 30:
                    period.TERGas.Value = ConvertHelper.ParseDouble(value);
                    break;
                case 31:
                    period.TERGas.ValueRUR = ConvertHelper.ParseDouble(value);
                    break;
                default:
                    throw new IndexOutOfRangeException();
            }
            #endregion
        }
    }
}