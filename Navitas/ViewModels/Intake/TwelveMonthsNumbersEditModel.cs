﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Intake
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>Пришлось сделать значения по отдельности, а не коллекцией, чтобы навесить валидирующие атрибуты.</remarks>
    public class TwelveMonthsNumbersEditModel : IDataAnnotationsSwitch
    {
        /// <summary>
        /// Определяет предназначен ли объект для записи или только для чтения.
        /// Влияет на валидацию - для объектов, предназначенных только для чтения, отключаются __custom'ные__
        /// валидирующие аттрибуты.
        /// </summary>
        public bool IsReadOnly { get; set; }

        [HyphenNumber(ErrorMessageResourceName="NotFloatingPointNumber", ErrorMessageResourceType=typeof(Navitas.Resources.ValidationStrings))]
        public string January { get { return Months[0]; } set { Months[0] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string February { get { return Months[1]; } set { Months[1] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string March { get { return Months[2]; } set { Months[2] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string April { get { return Months[3]; } set { Months[3] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string May { get { return Months[4]; } set { Months[4] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string June { get { return Months[5]; } set { Months[5] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string July { get { return Months[6]; } set { Months[6] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string August { get { return Months[7]; } set { Months[7] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string September { get { return Months[8]; } set { Months[8] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string October { get { return Months[9]; } set { Months[9] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string November { get { return Months[10]; } set { Months[10] = value; } }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string December { get { return Months[11]; } set { Months[11] = value; } }

        public double Total
        {
            get
            {
                return Months.Sum(m => ConvertHelper.ParseDouble(m));
            }
        }

        public bool CheckValidationAttributes
        {
            get { return !IsReadOnly; }
        }

        public string[] Months { get; private set; }

        public TwelveMonthsNumbersEditModel()
        {
            Months = new string[12];
        }

        //public string this[int index]
            //{
            //    get
            //    {
            //        switch (index)
            //        {
            //            case 0:
            //                return BaseMinus4;
            //            case 1:
            //                return BaseMinus3;
            //            case 2:
            //                return BaseMinus2;
            //            case 3:
            //                return BaseMinus1;
            //            case 4:
            //                return BaseMinus0;
            //            default:
            //                throw new IndexOutOfRangeException();
            //        }
            //    }
            //    set
            //    {
            //        switch (index)
            //        {
            //            case 0:
            //                BaseMinus4 = value;
            //                break;
            //            case 1:
            //                BaseMinus3 = value;
            //                break;
            //            case 2:
            //                BaseMinus2 = value;
            //                break;
            //            case 3:
            //                BaseMinus1 = value;
            //                break;
            //            case 4:
            //                BaseMinus0 = value;
            //                break;
            //            default:
            //                throw new IndexOutOfRangeException();
            //        }
            //    }
        
    }
}