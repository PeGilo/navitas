﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Navitas.Infrastructure;
using Navitas.ViewModels.Common;
using Soft33.Navitas.Common;

namespace Navitas.ViewModels.Intake
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Модель представления для редактирования данных "Потребления энергии за последний год помесячно"
    /// </summary>
    public class TwelveMonthIntakeEditModel
    {
        /// <summary>
        /// Количество групп строк
        /// </summary>
        private const int ROWS_COUNT = 16;

        public int BaseYear { get; set; }

        /// <summary>
        /// Устанавливает или возвращает признак отображения строк с тарифами.
        /// </summary>
        public bool ShowTariffs { get; set; }

        /// <summary>
        /// Объемы потребления построчно
        /// </summary>
        public IList<TwelveMonthsNumbersEditModel> Values { get; private set; }

        /// <summary>
        /// Строки с единицами измериния
        /// </summary>
        public IList<string> ValueUnits { get; private set; }

        /// <summary>
        /// Затраты построчно
        /// </summary>
        public IList<TwelveMonthsNumbersEditModel> ValuesRUR { get; private set; }

        /// <summary>
        /// Тарифы построчно
        /// </summary>
        public IList<TwelveMonthsNumbersEditModel> Tariffs { get; private set; }

        public TwelveMonthIntakeEditModel()
        {
            ShowTariffs = true; // default value

            Values = new List<TwelveMonthsNumbersEditModel>();
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                Values.Add(new TwelveMonthsNumbersEditModel());
            }

            ValueUnits = new List<string>();
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                ValueUnits.Add(String.Empty);
            }

            ValuesRUR = new List<TwelveMonthsNumbersEditModel>();
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                ValuesRUR.Add(new TwelveMonthsNumbersEditModel());
            }

            Tariffs = new List<TwelveMonthsNumbersEditModel>();
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                Tariffs.Add(new TwelveMonthsNumbersEditModel());
            }
        }

        public TwelveMonthIntakeEditModel(BL.TwelveMonthIntake entity)
            : this()
        {
            ConvertEntity(entity);
        }


        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.TwelveMonthIntake entity)
        {
            TwelveMonthsNumbersEditModel numbersModel = new TwelveMonthsNumbersEditModel();

            for (int i = 0; i < ROWS_COUNT; i++)
            {
                UpdateMonthValues(MapField(1, i, entity), i, () => numbersModel.January);
                UpdateMonthValues(MapField(2, i, entity), i, () => numbersModel.February);
                UpdateMonthValues(MapField(3, i, entity), i, () => numbersModel.March);
                UpdateMonthValues(MapField(4, i, entity), i, () => numbersModel.April);
                UpdateMonthValues(MapField(5, i, entity), i, () => numbersModel.May);
                UpdateMonthValues(MapField(6, i, entity), i, () => numbersModel.June);
                UpdateMonthValues(MapField(7, i, entity), i, () => numbersModel.July);
                UpdateMonthValues(MapField(8, i, entity), i, () => numbersModel.August);
                UpdateMonthValues(MapField(9, i, entity), i, () => numbersModel.September);
                UpdateMonthValues(MapField(10, i, entity), i, () => numbersModel.October);
                UpdateMonthValues(MapField(11, i, entity), i, () => numbersModel.November);
                UpdateMonthValues(MapField(12, i, entity), i, () => numbersModel.December);
            }
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.TwelveMonthIntake entity)
        {
            TwelveMonthsNumbersEditModel numbersModel = new TwelveMonthsNumbersEditModel();

            for (int i = 0; i < ROWS_COUNT; i++)
            {
                ConvertMonthValues(MapField(1, i, entity), i, () => numbersModel.January);
                ConvertMonthValues(MapField(2, i, entity), i, () => numbersModel.February);
                ConvertMonthValues(MapField(3, i, entity), i, () => numbersModel.March);
                ConvertMonthValues(MapField(4, i, entity), i, () => numbersModel.April);
                ConvertMonthValues(MapField(5, i, entity), i, () => numbersModel.May);
                ConvertMonthValues(MapField(6, i, entity), i, () => numbersModel.June);
                ConvertMonthValues(MapField(7, i, entity), i, () => numbersModel.July);
                ConvertMonthValues(MapField(8, i, entity), i, () => numbersModel.August);
                ConvertMonthValues(MapField(9, i, entity), i, () => numbersModel.September);
                ConvertMonthValues(MapField(10, i, entity), i, () => numbersModel.October);
                ConvertMonthValues(MapField(11, i, entity), i, () => numbersModel.November);
                ConvertMonthValues(MapField(12, i, entity), i, () => numbersModel.December);

                BL.TER terField = MapField(1, i, entity);
                ValueUnits[i] = (terField != null) ? terField.Unit : String.Empty;
            }

            // Установить какие строки read only
            Values[1].IsReadOnly = Values[11].IsReadOnly = true;
            ValuesRUR[1].IsReadOnly = ValuesRUR[11].IsReadOnly = true;
            for (int i = 0; i < ROWS_COUNT; i++)
            {
                Tariffs[i].IsReadOnly = true;
            }
        }


        /// <summary>
        /// По указанному имени свойства в expression (конкретно это один из месяцев) устанавливает это свойство для
        /// Values, ValuesRUR, Tariffs.
        /// </summary>
        /// <param name="ter">Объект из которого загружать значения</param>
        /// <param name="index">Индекс строки, в которую загружаем значения</param>
        /// <param name="expression">Выражение, из которого определяется месяц.</param>
        private void ConvertMonthValues(BL.TER ter, int index, Expression<Func<string>> expression)
        {
            string propertyName = ReflectionHelper.GetPropertyName<string>(expression);

            PropertyInfo monthProperty = typeof(TwelveMonthsNumbersEditModel).GetProperty(propertyName);

            monthProperty.SetValue(Values[index], ConvertHelper.Convert(ter.Value), null);
            monthProperty.SetValue(ValuesRUR[index], ConvertHelper.Convert(ter.ValueRUR), null);
            monthProperty.SetValue(Tariffs[index], ConvertHelper.Convert(ter.Tariff), null);
        }

        /// <summary>
        /// По указанному имени свойства в expression (конкретно это один из месяцев) загружает значения этого свойство из
        /// Values, ValuesRUR, Tariffs в бизнес-объект.
        /// </summary>
        /// <param name="ter">Объект в который загружать значения.</param>
        /// <param name="index">Индекс строки, из которой загружаем значения.</param>
        /// <param name="expression">Выражение, из которого определяется месяц.</param>
        /// <remarks>Тарифы не устанавливаются, так расчитываются внутри самого объекта.</remarks>
        private void UpdateMonthValues(BL.TER ter, int index, Expression<Func<string>> expression)
        {
            string propertyName = ReflectionHelper.GetPropertyName<string>(expression);

            PropertyInfo monthProperty = typeof(TwelveMonthsNumbersEditModel).GetProperty(propertyName);

            ter.Value = ConvertHelper.ParseDouble((string)monthProperty.GetValue(Values[index], null));
            ter.ValueRUR = ConvertHelper.ParseDouble((string)monthProperty.GetValue(ValuesRUR[index], null));
        }

        /// <summary>
        /// Возвращает поле бизнес-объекта, соответствующее индексу строки и месяцу
        /// </summary>
        /// <param name="year">месяц (1...12)</param>
        /// <param name="index">индекс строки (0, 15)</param>
        /// <param name="intake"></param>
        /// <returns>Объект TER</returns>
        private BL.TER MapField(int month, int index, BL.TwelveMonthIntake intake)
        {
            BL.IntakeOfPeriod period = intake.IntakeOfMonths[month];
            #region "Извлечение необходимого поле соответсвенно индексу строки"
            //int index1 = index / 3;
            switch (index)
            {
                case 0:
                    return period.ElectricEnergy;
                case 1:
                    return period.ThermalEnergy;
                case 2:
                    return period.TERHeating;
                case 3:
                    return period.TERDhw;
                case 4:
                    return period.TERVentilationInlet;
                case 5:
                    return period.ColdWater;
                case 6:
                    return period.HotWater;
                case 7:
                    return period.Wastewater;
                case 8:
                    return period.NaturalGas;
                case 9:
                    return period.Firewood;
                case 10:
                    return period.Coal;                
                case 11:
                    return period.FuelOil;
                case 12:
                    return period.TERGasoline;
                case 13:
                    return period.TERKerosene;
                case 14:
                    return period.TERDieselFuel;
                case 15:
                    return period.TERGas;
                default:
                    throw new IndexOutOfRangeException();
            }
            #endregion
        }

        /// <summary>
        /// Возвращает строку для названия группы строк. Используется во View.
        /// </summary>
        /// <param name="index">индекс группы (0, 15)</param>
        /// <returns>Строка с названием.</returns>
        public String GetFieldTitle(int index)
        {
            switch (index)
            {
                case 0:
                    return Navitas.Resources.Strings.ElectricPower;
                case 1:
                    return Navitas.Resources.Strings.ThermalEnergy;
                case 2:
                    return Navitas.Resources.Strings.Heating;
                case 3:
                    return Navitas.Resources.Strings.DHW;
                case 4:
                    return Navitas.Resources.Strings.VentilationInlet;
                case 5:
                    return Navitas.Resources.Strings.ColdWater;
                case 6:
                    return Navitas.Resources.Strings.HotWater;
                case 7:
                    return Navitas.Resources.Strings.WastewaterGeneral;
                case 8:
                    return Navitas.Resources.Strings.NaturalGasExceptMotorFuel;
                case 9:
                    return Navitas.Resources.Strings.Wood;
                case 10:
                    return Navitas.Resources.Strings.Coal;
                case 11:
                    return Navitas.Resources.Strings.MotorFuel;
                case 12:
                    return Navitas.Resources.Strings.Gasoline;
                case 13:
                    return Navitas.Resources.Strings.Kerosene;
                case 14:
                    return Navitas.Resources.Strings.DieselFuel;
                case 15:
                    return Navitas.Resources.Strings.Gas;
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}