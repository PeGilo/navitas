﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class IllustrationPlaceElementEditModel : BaseElementEditModel
    {
        public string Comment { get; set; }

        public IllustrationPlaceElementEditModel()
        {
        }

        public IllustrationPlaceElementEditModel(IllustrationPlaceElement entity)
            :base(entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(IllustrationPlaceElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(IllustrationPlaceElement entity)
        {
            Comment = entity.Comment;
        }
    }
}