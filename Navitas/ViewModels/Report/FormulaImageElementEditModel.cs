﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class FormulaImageElementEditModel : BaseElementEditModel
    {
        private static readonly string idPrefix = "formulaimageelement";

        public string FileName { get; set; }

        public string UserTitle { get; set; }

        [XmlAttribute]
        public string TextId { get { return idPrefix + Id.ToString(); } set { Id = ParseTextId(value); } }

        public FormulaImageElementEditModel()
        {
        }

        public FormulaImageElementEditModel(FormulaImageElement entity)
            :base(entity)
        {
            ConvertEntity(entity);
        }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(FormulaImageElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(FormulaImageElement entity)
        {
            FileName = entity.FileName;
            UserTitle = entity.UserTitle;
        }
    }
}