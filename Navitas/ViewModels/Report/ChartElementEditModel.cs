﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class ChartElementEditModel : BaseElementEditModel
    {
        private static readonly string idPrefix = "chartelement";

        public string ChartTitle { get; set; }

        //public string XAxisTitle { get; set; }

        //public string YAxisTitle { get; set; }

        public string DataContent { get; set; }

        [XmlAttribute]
        public string TextId { get { return idPrefix + Id.ToString(); } set { Id = ParseTextId(value); } }

        public ChartElementEditModel()
        {
        }

        public ChartElementEditModel(ChartElement entity)
            :base(entity)
        {
            ConvertEntity(entity);
        }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(ChartElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(ChartElement entity)
        {
            ChartTitle = entity.ChartTitle;
            //XAxisTitle = entity.XAxisTitle;
            //YAxisTitle = entity.YAxisTitle;
            DataContent = entity.DataContent;
        }
    }
}