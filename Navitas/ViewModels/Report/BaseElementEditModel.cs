﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Serialization;

using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public abstract class BaseElementEditModel
    {
        public virtual int Id { get; set; }

        [XmlAttribute]
        public virtual bool Hidden { get; set; }

        public BaseElementEditModel()
        {
        }

        public BaseElementEditModel(BaseElement entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Общий метод для всех элементов. Основывается на том, что TextId всех элементов образуются
        /// путем добавления к числу строки, обозначающей тип элемента.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static int ParseTextId(string text)
        {
            Regex regex = new Regex(@"\d+$");
            MatchCollection matches = regex.Matches(text);
            if (matches.Count > 0)
            {
                return Int32.Parse(matches[0].Value);
            }
            return 0;
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BaseElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BaseElement entity)
        {
            Id = entity.Id;
            Hidden = entity.Hidden;
        }
    }
}