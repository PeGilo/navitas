﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Report;

namespace Navitas.ViewModels.Report
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class ReportEditModel
    {
        private XslTransformHelper _xlsTransformer;
        private RazorHelper _razorHelper;
        private InspectionReportEditModel _template;

        public string HtmlLayout { get; set; }

        public int InspectionId { get; set; }

        public ReportEditModel()
        {
        }
        
        public ReportEditModel(XslTransformHelper xlsTransformer, RazorHelper razorHelper,
            InspectionReport template, BL.Extended.InspectionExtended entity)
        {
            _xlsTransformer = xlsTransformer;
            _razorHelper = razorHelper;
            _template = new InspectionReportEditModel(template);

            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Extended.InspectionExtended entity)
        {
            throw new NotImplementedException("Этот объект так не обновляется. Изменяется каждая его часть в отдельности.");
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Extended.InspectionExtended entity)
        {
            string serialized = SerializationHelper.Serialize<InspectionReportEditModel>(_template);

            string htmlTemplate = _xlsTransformer.Transform(serialized);

            HtmlLayout = _razorHelper.Parse<BL.Extended.InspectionExtended>(htmlTemplate, entity);

            //HtmlLayout =  Server.HtmlDecode(htmlLayout)
        }
    }
}