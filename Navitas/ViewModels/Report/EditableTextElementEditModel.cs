﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class EditableTextElementEditModel : BaseElementEditModel
    {
        private static readonly string idPrefix = "paragraph";

        [StringLength(5000)]
        public string Content { get; set; }

        [XmlAttribute]
        public bool Hidden { get; set; }

        [XmlAttribute]
        public string TextId
        {
            get { return idPrefix + Id.ToString(); }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    Id = ParseTextId(value);
                }
            }
        }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }

        public string EncodedContent
        {
            get
            {
                return Content.Replace("@", "@@");
                //return Content.Replace("@", "&#64;");
            }
            set
            {
                // Setter указан, для того, чтобы класс сериализовывался
            }
        }

        public EditableTextElementEditModel()
        {
        }

        public EditableTextElementEditModel(EditableTextElement entity)
            : base(entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(EditableTextElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(EditableTextElement entity)
        {
            //// Заменяем внутри параграфов переносы строк с <p> , на <br/>, так чтобы
            //// и в IE и в Firefox'е Новые строки обозначались одинаково.
            //// (После загрузки из базы все теги сериализованы).
            //Content = entity.Content.Replace("&amp;", "&").Replace("&lt;p&gt;", "").Replace("&lt;/p&gt;", "&lt;/br&gt;");

            Content = entity.Content.Replace("&amp;", "&");

            // Т.к. этот объект будет сериализован в xml, то &lt; заменятся на &amp;lt;. Нам этого не надо поэтому мы
            // заменяем все &lt; на '<' (так же как и другие специальные символы).
            Content = HttpUtility.HtmlDecode(Content);

            Hidden = entity.Hidden;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj">Объект на котором проверяется рендерится ли он в шаблон Razor</param>
        /// <returns></returns>
        public IEnumerable<RuleViolation> GetRuleViolations(Soft33.EnergyPassport.V2.BL.Extended.InspectionExtended obj)
        {
            List<RuleViolation> rv = new List<RuleViolation>();

            RazorHelper razorHelper = new RazorHelper();

            try
            {
                razorHelper.Parse<Soft33.EnergyPassport.V2.BL.Extended.InspectionExtended>(Content, obj);
            }
            catch (RazorEngine.Templating.TemplateCompilationException ex)
            {
                rv.Add(new RuleViolation("Данные в тексте обозначены неверно.", "Content"));
            }
            catch (Exception ex)
            {
                rv.Add(new RuleViolation("Текст не может быть обработан.", "Content"));
            }

            return rv;
        }
    }
}