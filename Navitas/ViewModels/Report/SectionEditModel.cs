﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Infrastructure;
using Navitas.Report;
using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class SectionEditModel
    {
        private static readonly string idPrefix = "section";

        //[XmlIgnore]
        [XmlAttribute]
        public int SectionId { get; set; }

        [XmlAttribute]
        public string TextId { get { return idPrefix + SectionId.ToString(); } set { } }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }

        [XmlIgnore]
        public int InspectionId { get; set; }

        [XmlIgnore]
        public int SequenceNumber { get; set; }

        [XmlAttribute]
        public string TimeStamp { get; set; }

        [XmlArrayItem(typeof(EditableTextElementEditModel))]
        [XmlArrayItem(typeof(ImageElementEditModel))]
        [XmlArrayItem(typeof(IllustrationPlaceElementEditModel))]
        [XmlArrayItem(typeof(ReadonlyTextElementEditModel))]
        [XmlArrayItem(typeof(RequiredImageElementEditModel))]
        [XmlArrayItem(typeof(TableElementEditModel))]
        [XmlArrayItem(typeof(ChartElementEditModel))]
        [XmlArrayItem(typeof(SectionTitleElementEditModel))]
        [XmlArrayItem(typeof(StaticImageElementEditModel))]
        [XmlArrayItem(typeof(SubsectionTitleElementEditModel))]
        [XmlArrayItem(typeof(ChartStackElementEditModel))]
        [XmlArrayItem(typeof(FormulaImageElementEditModel))]
        public List<BaseElementEditModel> Elements { get; set; }

        public SectionEditModel()
        {
            Elements = new List<BaseElementEditModel>();
        }

        public SectionEditModel(Section entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Section entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Section entity)
        {
            SectionId = entity.SectionId;
            InspectionId = entity.InspectionId;
            TimeStamp = ConvertHelper.Convert(entity.TimeStamp);

            for (int i = 0; i < entity.Elements.Count; i++)
            {
                BaseElementEditModel elementEditModel = null;
                BaseElement element = entity.Elements[i];

                if (element is EditableTextElement)
                {
                    elementEditModel = new EditableTextElementEditModel(element as EditableTextElement);
                }
                else if (element is ReadonlyTextElement)
                {
                    elementEditModel = new ReadonlyTextElementEditModel(element as ReadonlyTextElement);
                }
                else if (element is ImageElement)
                {
                    elementEditModel = new ImageElementEditModel(element as ImageElement);
                }
                else if (element is IllustrationPlaceElement)
                {
                    elementEditModel = new IllustrationPlaceElementEditModel(element as IllustrationPlaceElement);
                }
                else if (element is RequiredImageElement)
                {
                    elementEditModel = new RequiredImageElementEditModel(element as RequiredImageElement);
                }
                else if (element is TableElement)
                {
                    elementEditModel = new TableElementEditModel(element as TableElement);
                }
                else if (element is ChartElement)
                {
                    elementEditModel = new ChartElementEditModel(element as ChartElement);
                }
                else if (element is ChartStackElement)
                {
                    elementEditModel = new ChartStackElementEditModel(element as ChartStackElement);
                }
                else if (element is SectionTitleElement)
                {
                    elementEditModel = new SectionTitleElementEditModel(element as SectionTitleElement);
                }
                else if (element is StaticImageElement)
                {
                    elementEditModel = new StaticImageElementEditModel(element as StaticImageElement);
                }
                else if (element is FormulaImageElement)
                {
                    elementEditModel = new FormulaImageElementEditModel(element as FormulaImageElement);
                }
                else if (element is SubsectionTitleElement)
                {
                    elementEditModel = new SubsectionTitleElementEditModel(element as SubsectionTitleElement);
                }
                else
                {
                    throw new NotImplementedException();
                }

                Elements.Add(elementEditModel);
            }
        }
    }
}