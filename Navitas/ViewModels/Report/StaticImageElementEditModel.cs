﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class StaticImageElementEditModel : BaseElementEditModel
    {
        private static readonly string idPrefix = "staticimageelement";

        public string FileName { get; set; }

        public string UserTitle { get; set; }

        [XmlAttribute]
        public string TextId { get { return idPrefix + Id.ToString(); } set { Id = ParseTextId(value); } }

        public StaticImageElementEditModel()
        {
        }

        public StaticImageElementEditModel(StaticImageElement entity)
            :base(entity)
        {
            ConvertEntity(entity);
        }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(StaticImageElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(StaticImageElement entity)
        {
            FileName = entity.FileName;
            UserTitle = entity.UserTitle;
        }
    }
}