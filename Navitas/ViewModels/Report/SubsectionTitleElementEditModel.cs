﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class SubsectionTitleElementEditModel : BaseElementEditModel
    {
        private static readonly string idPrefix = "subsectiontitle";

        public int Level { get; set; }

        public string Text { get; set; }

        [XmlAttribute]
        public string TextId { get { return idPrefix + Id.ToString(); } set { Id = ParseTextId(value); } }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }

        public SubsectionTitleElementEditModel()
        {
        }

        public SubsectionTitleElementEditModel(SubsectionTitleElement entity)
            : base(entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(SubsectionTitleElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(SubsectionTitleElement entity)
        {
            Text = entity.Text;
            Hidden = entity.Hidden;
            Level = entity.Level.HasValue ? entity.Level.Value : 2; // by default 2
        }
    }
}