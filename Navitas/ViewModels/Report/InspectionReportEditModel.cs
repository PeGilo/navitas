﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Report;

namespace Navitas.ViewModels.Report
{
    public class InspectionReportEditModel
    {
        public List<SectionEditModel> Sections { get; set; }

        public InspectionReportEditModel()
        {
            Sections = new List<SectionEditModel>();
        }

        public InspectionReportEditModel(InspectionReport entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(InspectionReport entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(InspectionReport entity)
        {
            for (int i = 0; i < entity.Sections.Count; i++)
            {
                Sections.Add(new SectionEditModel(entity.Sections[i]));
            }
        }
    }
}