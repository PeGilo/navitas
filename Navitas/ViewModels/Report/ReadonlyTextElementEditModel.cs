﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class ReadonlyTextElementEditModel : BaseElementEditModel
    {
        private static readonly string idPrefix = "roparagraph";

        public string Content { get; set; }

        [XmlAttribute]
        public bool Hidden { get; set; }

        [XmlAttribute]
        public string TextId
        {
            get { return idPrefix + Id.ToString(); }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    Id = ParseTextId(value);
                }
            }
        }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }

        public string EncodedContent
        {
            get
            {
                return Content.Replace("@", "@@");
                //return Content.Replace("@", "&#64;");
            }
            set
            {
                // Setter указан, для того, чтобы класс сериализовывался
            }
        }

        public ReadonlyTextElementEditModel()
        {
        }

        public ReadonlyTextElementEditModel(ReadonlyTextElement entity)
            : base(entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(ReadonlyTextElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(ReadonlyTextElement entity)
        {
            Content = entity.Content.Replace("&amp;", "&");

            // Т.к. этот объект будет сериализован в xml, то &lt; заменятся на &amp;lt;. Нам этого не надо поэтому мы
            // заменяем все &lt; на '<' (так же как и другие специальные символы).
            Content = HttpUtility.HtmlDecode(Content);

            Hidden = entity.Hidden;
        }
    }
}