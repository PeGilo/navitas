﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Report.Elements;

namespace Navitas.ViewModels.Report
{
    public class RequiredImageElementEditModel : BaseElementEditModel
    {
        private static readonly string idPrefix = "requiredimage";

        public string Content { get; set; }

        [XmlAttribute]
        public string TextId { get { return idPrefix + Id.ToString(); } set { Id = ParseTextId(value); } }


        public RequiredImageElementEditModel()
        {
        }

        public RequiredImageElementEditModel(RequiredImageElement entity)
            : base(entity)
        {
            ConvertEntity(entity);
        }

        public static int ParseTextId(string text)
        {
            return Int32.Parse(text.Substring(idPrefix.Length));
        }


        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(RequiredImageElement entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(RequiredImageElement entity)
        {
            Hidden = entity.Hidden;
            Content = entity.Content;

            //System.Web.HttpUtility.HtmlDecode
        }
    }
}