﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Common
{
    public class VariableCompsositeRow<T> : Navitas.ViewModels.Production.VariableRow<CompositeValue<T>>
    {
        public VariableCompsositeRow()
        {
        }

        public VariableCompsositeRow(int itemsCount)
            : base(itemsCount)
        {
        }
    }
}