﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Common
{
    public struct CompositeValue<T>
    {
        public T Value { get ; set; }
        public bool IsReadonly { get; set; }

        public CompositeValue(T value, bool isReadonly)
            :this()
        {
            Value = value;
            IsReadonly = isReadonly;
        }

        public CompositeValue(T value)
            : this(value, false)
        {
        }
    }
}