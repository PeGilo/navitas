﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.OKVED
{
    public class OKVEDEditModel
    {
        public OKVEDEditModel()
        {
            Kod = String.Empty;
            Description = String.Empty;
        }

        public OKVEDEditModel(Soft33.EnergyPassport.V2.BL.OKVED entity)
        {
            ConvertEntity(entity);
        }

        public string Kod { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.OKVED entity)
        {
            entity.Kod = Kod;
            entity.Description = Description;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.OKVED entity)
        {
            Kod = entity.Kod;
            Description = entity.Description;
        }
    }

}