﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Ventilation
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class ExtractItemEditModel
    {
        [LocalizedDisplayName("VentilationOut_Name", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationOut_Name_Description", NameResourceType = typeof(Resources.Names))]
        public string Name { get; set; }

        [LocalizedDisplayName("VentilationOut_ServedPremises", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationOut_ServedPremises_Description", NameResourceType = typeof(Resources.Names))]
        public string ServedPremises { get; set; }

        [LocalizedDisplayName("VentilationOut_Brand", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationOut_Brand_Description", NameResourceType = typeof(Resources.Names))]
        public string Brand { get; set; }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationOut_AirFlow", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationOut_AirFlow_Description", NameResourceType = typeof(Resources.Names))]
        public string AirFlow { get; set; } // double

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationOut_Power", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationOut_Power_Description", NameResourceType = typeof(Resources.Names))]
        public string Power { get; set; } // double

        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationOut_HoursMonth", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationOut_HoursMonth_Description", NameResourceType = typeof(Resources.Names))]
        public string HoursMonth { get; set; } // int

        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationOut_HoursYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationOut_HoursYear_Description", NameResourceType = typeof(Resources.Names))]
        public string HoursYear { get; set; } // int

        public ExtractItemEditModel()
        {
        }

        public ExtractItemEditModel(BL.VentilationExtract entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.VentilationExtract entity)
        {
            entity.Name = Name;
            entity.ServedPremises = ServedPremises;
            entity.VentilationSystemBrand = Brand;
            entity.AirFlow = ConvertHelper.ParseDouble(AirFlow);
            entity.Power = ConvertHelper.ParseDouble(Power);
            entity.TimeHoursPerMonth = ConvertHelper.ParseInt32(HoursMonth);
            entity.TimeHoursPerYear = ConvertHelper.ParseInt32(HoursYear);
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.VentilationExtract entity)
        {
            Name = entity.Name;
            ServedPremises = entity.ServedPremises;
            Brand = entity.VentilationSystemBrand;
            AirFlow = ConvertHelper.Convert(entity.AirFlow);
            Power = ConvertHelper.Convert(entity.Power);
            HoursMonth = ConvertHelper.Convert(entity.TimeHoursPerMonth);
            HoursYear = ConvertHelper.Convert(entity.TimeHoursPerYear);
        }

    }
}