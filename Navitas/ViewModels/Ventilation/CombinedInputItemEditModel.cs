﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Ventilation
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class CombinedInputItemEditModel
    {
        [LocalizedDisplayName("VentilationIn_Name", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_Name_Description", NameResourceType = typeof(Resources.Names))]
        public string Name { get; set; }

        [LocalizedDisplayName("VentilationIn_ServedPremises", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_ServedPremises_Description", NameResourceType = typeof(Resources.Names))]
        public string ServedPremises { get; set; }

        [LocalizedDisplayName("VentilationIn_Brand", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_Brand_Description", NameResourceType = typeof(Resources.Names))]
        public string VentilationSystemBrand { get; set; }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_Airflow", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("_Description", NameResourceType = typeof(Resources.Names))]
        public string AirFlow { get; set; } // double

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_PowerInHeating", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_PowerInHeating_Description", NameResourceType = typeof(Resources.Names))]
        public string PowerInHeating { get; set; } // double

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_PowerInCooling", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_PowerInCooling_Description", NameResourceType = typeof(Resources.Names))]
        public string PowerInCooling { get; set; } // double

        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_HoursYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_HoursYear_Description", NameResourceType = typeof(Resources.Names))]
        public string TimeHoursPerYear { get; set; } // int

        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_HoursMonth", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_HoursMonth_Description", NameResourceType = typeof(Resources.Names))]
        public string TimeHoursPerMonth { get; set; } // int

        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_HoursColdPeriod", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_HoursColdPeriod_Description", NameResourceType = typeof(Resources.Names))]
        public string TimeHoursPerYearInColdPeriod { get; set; } // int
        
        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_HoursWarmPeriod", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_HoursWarmPeriod_Description", NameResourceType = typeof(Resources.Names))]
        public string TimeHoursPerYearInWarmPeriod { get; set; } // int

        [LocalizedDisplayName("VentilationIn_PumpBrand", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_PumpBrand_Description", NameResourceType = typeof(Resources.Names))]
        public string PumpBrand { get; set; }

        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("VentilationIn_PumpPower", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_PumpPower_Description", NameResourceType = typeof(Resources.Names))]
        public string PumpPower { get; set; } // double

        [LocalizedDisplayName("VentilationIn_RecyclingAvailable", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("VentilationIn_RecyclingAvailable_Description", NameResourceType = typeof(Resources.Names))]
        public bool AvailabilityOfRecycling { get; set; }

        public CombinedInputItemEditModel()
        {
        }

        public CombinedInputItemEditModel(BL.VentilationCombinedExtractInput entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.VentilationCombinedExtractInput entity)
        {
            entity.Name = Name;
            entity.ServedPremises = ServedPremises;
            entity.VentilationSystemBrand = VentilationSystemBrand;
            entity.AirFlow = ConvertHelper.ParseDouble(AirFlow);
            entity.PowerInHeating = ConvertHelper.ParseDouble(PowerInHeating);
            entity.PowerInCooling = ConvertHelper.ParseDouble(PowerInCooling);
            entity.TimeHoursPerMonth = ConvertHelper.ParseInt32(TimeHoursPerMonth);
            entity.TimeHoursPerYear = ConvertHelper.ParseInt32(TimeHoursPerYear);
            entity.TimeHoursPerYearInColdPeriod = ConvertHelper.ParseInt32(TimeHoursPerYearInColdPeriod);
            entity.TimeHoursPerYearInWarmPeriod = ConvertHelper.ParseInt32(TimeHoursPerYearInWarmPeriod);
            entity.PumpBrand = PumpBrand;
            entity.PumpPower = ConvertHelper.ParseDouble(PumpPower);
            entity.AvailabilityOfRecycling = AvailabilityOfRecycling;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.VentilationCombinedExtractInput entity)
        {
            Name = entity.Name;
            ServedPremises = entity.ServedPremises;
            VentilationSystemBrand = entity.VentilationSystemBrand;
            AirFlow = ConvertHelper.Convert(entity.AirFlow);
            PowerInHeating = ConvertHelper.Convert(entity.PowerInHeating);
            PowerInCooling = ConvertHelper.Convert(entity.PowerInCooling);
            TimeHoursPerMonth = ConvertHelper.Convert(entity.TimeHoursPerMonth);
            TimeHoursPerYear = ConvertHelper.Convert(entity.TimeHoursPerYear);
            TimeHoursPerYearInColdPeriod = ConvertHelper.Convert(entity.TimeHoursPerYearInColdPeriod);
            TimeHoursPerYearInWarmPeriod = ConvertHelper.Convert(entity.TimeHoursPerYearInWarmPeriod);
            PumpBrand = entity.PumpBrand;
            PumpPower = ConvertHelper.Convert(entity.PumpPower);
            AvailabilityOfRecycling = entity.AvailabilityOfRecycling;
        }

    }
}