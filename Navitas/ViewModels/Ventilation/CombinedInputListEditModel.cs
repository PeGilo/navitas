﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Ventilation
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class CombinedInputListEditModel
    {
        public IList<CombinedInputItemEditModel> Items { get; private set; }

        public CombinedInputListEditModel()
        {
            Items = new List<CombinedInputItemEditModel>();
        }

        public CombinedInputListEditModel(BL.VentilationCombinedExtractInputList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.VentilationCombinedExtractInputList entity)
        {
            List<BL.VentilationCombinedExtractInput> newList = new List<BL.VentilationCombinedExtractInput>();
            foreach (var item in Items)
            {
                BL.VentilationCombinedExtractInput entityItem = new BL.VentilationCombinedExtractInput();

                item.UpdateEntity(entityItem);
                newList.Add(entityItem);
            }
            entity.VentilationCombinedExtractInputs = newList;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.VentilationCombinedExtractInputList entity)
        {
            Items = new List<CombinedInputItemEditModel>();

            foreach (var entityItem in entity.VentilationCombinedExtractInputs)
            {
                Items.Add(new CombinedInputItemEditModel(entityItem));
            }
        }

    }
}