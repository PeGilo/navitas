﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Ventilation
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class ExtractListEditModel
    {
        public IList<ExtractItemEditModel> Items { get; private set; }

        public ExtractListEditModel()
        {
            Items = new List<ExtractItemEditModel>();
        }

        public ExtractListEditModel(BL.VentilationExtractList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.VentilationExtractList entity)
        {
            List<BL.VentilationExtract> newList = new List<BL.VentilationExtract>();
            foreach (var item in Items)
            {
                BL.VentilationExtract entityItem = new BL.VentilationExtract();

                item.UpdateEntity(entityItem);
                newList.Add(entityItem);
            }
            entity.VentilationExtracts = newList;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.VentilationExtractList entity)
        {
            Items = new List<ExtractItemEditModel>();

            foreach (var entityItem in entity.VentilationExtracts)
            {
                Items.Add(new ExtractItemEditModel(entityItem));
            }
        }
    }
}