﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Staff
{
    public class StaffUnitEditModel
    {
        [LocalizedDisplayName("Staff_FIO", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_FIO_Description", NameResourceType = typeof(Resources.Names))]
        public string FIO { get; set; }

        [LocalizedDisplayName("Staff_Position", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_Position_Description", NameResourceType = typeof(Resources.Names))]
        public string Position { get; set; }

        [LocalizedDisplayName("Staff_EducationalOrganizationName", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_EducationalOrganizationName_Description", NameResourceType = typeof(Resources.Names))]
        public string EducationalOrganizationName { get; set; }

        [LocalizedDisplayName("Staff_EducationalOrganizationAddress", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_EducationalOrganizationAddress_Description", NameResourceType = typeof(Resources.Names))]
        public string EducationalOrganizationAddress { get; set; }

        [LocalizedDisplayName("Staff_EducationalOrganizationLicense", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_EducationalOrganizationLicense_Description", NameResourceType = typeof(Resources.Names))]
        public string EducationalOrganizationLicense { get; set; }

        [LocalizedDisplayName("Staff_CourseTitle", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_CourseTitle_Description", NameResourceType = typeof(Resources.Names))]
        public string CourseTitle { get; set; }

        [LocalizedDisplayName("Staff_CourseType", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_CourseType_Description", NameResourceType = typeof(Resources.Names))]
        public string CourseType { get; set; }

        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [UIHint("DateTimePicker")]
        [LocalizedDisplayName("Staff_StartDate", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_StartDate_Description", NameResourceType = typeof(Resources.Names))]
        public DateTime? StartDate { get; set; }

        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [UIHint("DateTimePicker")]
        [LocalizedDisplayName("Staff_ExpirationDate", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_ExpirationDate_Description", NameResourceType = typeof(Resources.Names))]
        public DateTime? ExpirationDate { get; set; }

        [LocalizedDisplayName("Staff_EducationalDiplom", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_EducationalDiplom_Description", NameResourceType = typeof(Resources.Names))]
        public string EducationalDiplom { get; set; }

        [LocalizedDisplayName("Staff_Qualification", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Staff_Qualification_Description", NameResourceType = typeof(Resources.Names))]
        public string Qualification { get; set; }

        public StaffUnitEditModel()
        {
            FIO = String.Empty;
            Position = String.Empty;
            EducationalOrganizationName = String.Empty;
            EducationalOrganizationAddress = String.Empty;
            EducationalOrganizationLicense = String.Empty;
            CourseTitle = String.Empty;
            CourseType = String.Empty;
            EducationalDiplom = String.Empty;
            Qualification = String.Empty;
        }

        public StaffUnitEditModel(Soft33.EnergyPassport.V2.BL.StaffUnit entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.StaffUnit entity)
        {
            entity.FIO = FIO;
            entity.Position = Position;
            entity.EducationalOrganizationName = EducationalOrganizationName;
            entity.EducationalOrganizationAddress = EducationalOrganizationAddress;
            entity.EducationalOrganizationLicense = EducationalOrganizationLicense;
            entity.CourseTitle = CourseTitle;
            entity.CourseType = CourseType;
            entity.StartDate = StartDate;
            entity.ExpirationDate = ExpirationDate;
            entity.EducationalDiplom = EducationalDiplom;
            entity.Qualification = Qualification;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.StaffUnit entity)
        {
            FIO = entity.FIO;
            Position = entity.Position;
            EducationalOrganizationName = entity.EducationalOrganizationName;
            EducationalOrganizationAddress = entity.EducationalOrganizationAddress;
            EducationalOrganizationLicense = entity.EducationalOrganizationLicense;
            CourseTitle = entity.CourseTitle;
            CourseType = entity.CourseType;
            StartDate = entity.StartDate;
            ExpirationDate = entity.ExpirationDate;
            EducationalDiplom = entity.EducationalDiplom;
            Qualification = entity.Qualification;
        }
    }
}