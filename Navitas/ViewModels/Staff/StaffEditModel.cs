﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Staff
{
    public class StaffEditModel
    {
        public IList<StaffUnitEditModel> Units { get; private set; }

        public StaffEditModel()
        {
            Units = new List<StaffUnitEditModel>();
        }

        public StaffEditModel(Soft33.EnergyPassport.V2.BL.Staff entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.Staff entity)
        {
            entity.Units.Clear();
            foreach (var unit in Units)
            {
                var entityUnit = new Soft33.EnergyPassport.V2.BL.StaffUnit();
                unit.UpdateEntity(entityUnit);
                entity.Units.Add(entityUnit);
            }
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.Staff entity)
        {
            Units = new List<StaffUnitEditModel>();

            foreach (var entityUnit in entity.Units)
            {
                var unit = new ViewModels.Staff.StaffUnitEditModel(entityUnit);
                Units.Add(unit);
            }
        }
    }
}