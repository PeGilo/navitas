﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class IllustrationItemEditModel
    {
        public string FileId { get; set; }

        public string FileName { get; set; }

        public string UserTitle { get; set; }

        public string DivisionName { get; set; }

        public string ImageUrl { get; set; }

        public string ThumbUrl { get; set; }

        public IllustrationItemEditModel()
        {
        }

        public IllustrationItemEditModel(BL.Illustration entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Illustration entity)
        {

        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Illustration entity)
        {
            FileId = entity.IllustrationID.ToString();
            FileName = entity.FileName;
            UserTitle = entity.UserTitle;
            DivisionName = entity.DivisionName;
            ImageUrl = "/File/Illustration/" + entity.FileName;
            ThumbUrl = "/File/Thumb/" + entity.FileName;
        }
    }
}