﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class BuildingListEditModel
    {
        public IList<BuildingItemEditModel> Items { get; private set; }

        /// <summary>
        /// Список назначений.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList BuildingPurposeList
        {
            get;
            set;
        }

        public BuildingListEditModel()
        {
            Items = new List<BuildingItemEditModel>();
        }

        public BuildingListEditModel(IEnumerable<BL.Building> entities)
        {
            ConvertEntity(entities);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Inspection entity)
        {
            List<BL.Building> buildings = new List<BL.Building>();
            foreach (var item in Items)
            {
                // Создать здание через метод обследования, чтобы были добавлены пустые строки в разные свойства.
                // На изменение зданий это не повлияет, т.к. будет изменяться только нужная часть.
                BL.Building building = entity.CreateBuilding(); //new BL.Building();
                
                item.UpdateEntity(building);
                buildings.Add(building);
            }
            entity.Buildings = buildings;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(IEnumerable<BL.Building> entities)
        {
            Items = new List<BuildingItemEditModel>();

            foreach (var building in entities)
            {
                Items.Add(new BuildingItemEditModel(building));
            }
        }
    }
}