﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class PointOfWaterItemEditModel
    {
        /* Атрибут Required определяется для целочисленных, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("WaterPoints_Point", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("WaterPoints_Point_Description", NameResourceType = typeof(Resources.Names))]
        public string PointOfWaterUID { get; set; }

        [LocalizedDisplayName("WaterPoints_Device", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("WaterPoints_Device_Description", NameResourceType = typeof(Resources.Names))]
        public string PointOfWaterDeviceTypeUID { get; set; }

        [LocalizedDisplayName("WaterPoints_Number", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("WaterPoints_Number_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public int Number { get; set; }

        [LocalizedDisplayName("WaterPoints_Description", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("WaterPoints_Description_Description", NameResourceType = typeof(Resources.Names))]
        public string Description { get; set; }

        public PointOfWaterItemEditModel()
        {
        }

        public PointOfWaterItemEditModel(BL.PointOfWater entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.PointOfWater entity)
        {
            entity.Number = Number;
            entity.Description = Description;
            entity.PointOfWaterUID = ConvertHelper.ParseGuid(PointOfWaterUID);
            entity.PointOfWaterDeviceTypeUID = ConvertHelper.ParseGuid(PointOfWaterDeviceTypeUID);
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.PointOfWater entity)
        {
            Number = entity.Number;
            Description = entity.Description;
            PointOfWaterUID = ConvertHelper.Convert(entity.PointOfWaterUID);
            PointOfWaterDeviceTypeUID = ConvertHelper.Convert(entity.PointOfWaterDeviceTypeUID);
        }
    }
}