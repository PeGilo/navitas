﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class ElectroloadListEditModel
    {
        /// <summary>
        /// Для хранения идентификатора списка на клиенте, чтобы не перезагружать его на каждом POST.
        /// </summary>
        public string ListIdentifier { get; set; }

        /// <summary>
        /// Для хранения наименования списка на клиенте, чтобы не перезагружать его на каждом POST.
        /// </summary>
        public string ListName { get; set; }

        public IList<ElectroloadItemEditModel> Items { get; private set; }

        public ElectroloadListEditModel()
        {
            Items = new List<ElectroloadItemEditModel>();
        }

        public ElectroloadListEditModel(BL.ElectroloadList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.ElectroloadList entity)
        {
            List<BL.Electroload> newList = new List<BL.Electroload>();
            foreach (var item in Items)
            {
                BL.Electroload electroload = new BL.Electroload();

                item.UpdateEntity(electroload);
                newList.Add(electroload);
            }
            entity.Electroloads = newList;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.ElectroloadList entity)
        {
            Items = new List<ElectroloadItemEditModel>();

            foreach (var electroload in entity.Electroloads)
            {
                Items.Add(new ElectroloadItemEditModel(electroload));
            }
        }
    }
}