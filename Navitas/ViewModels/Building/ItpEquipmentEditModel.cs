﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class ItpEquipmentEditModel
    {
        /// <summary>
        /// Кол-во вводов в здании, шт. -
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_InputCount", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_InputCount_Description", NameResourceType = typeof(Resources.Names))]
        public int? InputCount { get; set; }

        /// <summary>
        /// Кол-во ИТП в здании, шт -
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_ItpCount", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_ItpCount_Description", NameResourceType = typeof(Resources.Names))]
        public int? ItpCount { get; set; }

        public List<HeatExchangerItemEditModel> HeatExchangers { get; set; }

        public List<BoilerItemEditModel> Boilers { get; set; }

        public List<PumpingEquipmentItemEditModel> PumpingEquipment { get; set; }

        [LocalizedDisplayName("ItpEquipment_PipelinesTotalLength", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PipelinesTotalLength_Description", NameResourceType = typeof(Resources.Names))]
        public double? PipelinesTotalLength { get; set; }

        [LocalizedDisplayName("ItpEquipment_ItpIsolationType", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_ItpIsolationType_Description", NameResourceType = typeof(Resources.Names))]
        public string ItpIsolationType { get; set; }

        public ItpEquipmentEditModel()
        {
            HeatExchangers = new List<HeatExchangerItemEditModel>();
            Boilers = new List<BoilerItemEditModel>();
            PumpingEquipment = new List<PumpingEquipmentItemEditModel>();
        }

        public ItpEquipmentEditModel(BL.ItpEquipment entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.ItpEquipment entity)
        {
            entity.HeatExchangers = HeatExchangers.Select(he => { var bo = new BL.HeatExchanger(); he.UpdateEntity(bo); return bo; }).ToList();

            entity.Boilers = Boilers.Select(b => { var bo = new BL.Boiler(); b.UpdateEntity(bo); return bo; }).ToList();

            entity.PumpingEquipment = PumpingEquipment.Select(pe => { var bo = new BL.PumpingEquipment(); pe.UpdateEntity(bo); return bo; }).ToList();

            entity.InputCount = InputCount;
            entity.ItpCount = ItpCount;
            entity.PipelinesTotalLength = PipelinesTotalLength;
            entity.ItpIsolationType = ItpIsolationType;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.ItpEquipment entity)
        {
            foreach (var he in entity.HeatExchangers)
            {
                HeatExchangers.Add(new HeatExchangerItemEditModel(he));
            }

            foreach (var b in entity.Boilers)
            {
                Boilers.Add(new BoilerItemEditModel(b));
            }

            foreach (var pe in entity.PumpingEquipment)
            {
                PumpingEquipment.Add(new PumpingEquipmentItemEditModel(pe));
            }

            InputCount = entity.InputCount;
            ItpCount = entity.ItpCount;
            PipelinesTotalLength = entity.PipelinesTotalLength;
            ItpIsolationType = entity.ItpIsolationType;
        }

    }
}