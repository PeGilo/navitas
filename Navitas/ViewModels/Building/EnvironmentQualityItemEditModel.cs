﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class EnvironmentQualityItemEditModel
    {
        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("EnvironmentQuality_RoomType", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("EnvironmentQuality_RoomType_Description", NameResourceType = typeof(Resources.Names))]
        public string RoomTypeUID { get; set; }

        [LocalizedDisplayName("EnvironmentQuality_Humidity", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("EnvironmentQuality_Humidity_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double Humidity { get; set; }

        [LocalizedDisplayName("EnvironmentQuality_Illumination", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("EnvironmentQuality_Illumination_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double Illumination { get; set; }

        [LocalizedDisplayName("EnvironmentQuality_Temperature", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("EnvironmentQuality_Temperature_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double Temperature { get; set; }

        [LocalizedDisplayName("EnvironmentQuality_WindSpeed", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("EnvironmentQuality_WindSpeed_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double WindSpeed { get; set; }


        public EnvironmentQualityItemEditModel()
        {
        }

        public EnvironmentQualityItemEditModel(BL.EnvironmentQuality entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.EnvironmentQuality entity)
        {
            entity.Humidity = Humidity;
            entity.Illumination = Illumination;
            entity.Temperature = Temperature;
            entity.WindSpeed = WindSpeed;

            entity.RoomTypeUID = ConvertHelper.ParseGuid(RoomTypeUID);
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.EnvironmentQuality entity)
        {
            Humidity = entity.Humidity;
            Illumination = entity.Illumination;
            Temperature = entity.Temperature;
            WindSpeed = entity.WindSpeed;

            RoomTypeUID = ConvertHelper.Convert(entity.RoomTypeUID);
        }
    }
}