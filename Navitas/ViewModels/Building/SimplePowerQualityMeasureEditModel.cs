﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    public class SimplePowerQualityMeasureEditModel
    {
        /// <summary>
        /// Время замера (без даты)
        /// </summary>
        [UIHint("TimePicker")]
        [LocalizedDisplayName("SimplePowerQty_Time", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_Time_Description", NameResourceType = typeof(Resources.Names))]
        public DateTime? Time { get; set; }

        [LocalizedDisplayName("SimplePowerQty_UPhaseA", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_UPhaseA_Description", NameResourceType = typeof(Resources.Names))]
        public double? UPhaseA { get; set; }

        [LocalizedDisplayName("SimplePowerQty_UPhaseB", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_UPhaseB_Description", NameResourceType = typeof(Resources.Names))]
        public double? UPhaseB { get; set; }

        [LocalizedDisplayName("SimplePowerQty_UPhaseC", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_UPhaseC_Description", NameResourceType = typeof(Resources.Names))]
        public double? UPhaseC { get; set; }

        [LocalizedDisplayName("SimplePowerQty_IPhaseA", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_IPhaseA_Description", NameResourceType = typeof(Resources.Names))]
        public double? IPhaseA { get; set; }

        [LocalizedDisplayName("SimplePowerQty_IPhaseB", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_IPhaseB_Description", NameResourceType = typeof(Resources.Names))]
        public double? IPhaseB { get; set; }

        [LocalizedDisplayName("SimplePowerQty_IPhaseC", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_IPhaseC_Description", NameResourceType = typeof(Resources.Names))]
        public double? IPhaseC { get; set; }
    }

}