﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Базовая модель для дверей и окон.
    /// </summary>
    public class TechWindowEditModel
    {
        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("Technical_MaterialCaption", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_MaterialCaption_Description", NameResourceType = typeof(Resources.Names))]
        public string MaterialUid { get; set; }

        [LocalizedDisplayName("Technical_Number", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_Number_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public int NumberOfWindows { get; set; }

        [LocalizedDisplayName("Technical_AverageYardage", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_AverageYardage_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double AverageYardage { get; set; }

        public TechWindowEditModel()
        {
        }

        public TechWindowEditModel(BL.Window entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Window entity)
        {
            entity.NumberOfWindows = NumberOfWindows;
            entity.AverageYardage = AverageYardage;

            if (!String.IsNullOrEmpty(MaterialUid))
            {
                entity.WindowTypeUID = ConvertHelper.ParseGuid(MaterialUid);
            }
            else
            {
                entity.WindowTypeUID = Guid.Empty;
            }
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Window entity)
        {
            NumberOfWindows = entity.NumberOfWindows;
            AverageYardage = entity.AverageYardage;
            MaterialUid = ConvertHelper.Convert(entity.WindowTypeUID);
        }
    }
}