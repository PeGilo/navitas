﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class BuildingItemEditModel
    {
        public int BuildingID { get; set; }
        public System.Guid BuildingGuid { get; set; }

        /* Атрибут Required определяется для целочисленных, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("Building_PhysicalAddress", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_PhysicalAddress_Description", NameResourceType = typeof(Resources.Names))]
        [StringLength(2048, ErrorMessageResourceName = "StringLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        public string PhysicalAddress { get; set; }

        [LocalizedDisplayName("Building_Name", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_Name_Description", NameResourceType = typeof(Resources.Names))]
        [StringLength(2048, ErrorMessageResourceName = "StringLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        public string Name { get; set; }

        [LocalizedDisplayName("Building_Purpose", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_Purpose_Description", NameResourceType = typeof(Resources.Names))]
        public int Purpose { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Building_RegularStaff", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_RegularStaff_Description", NameResourceType = typeof(Resources.Names))]
        public int RegularStaff { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Building_SupportStaff", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_SupportStaff_Description", NameResourceType = typeof(Resources.Names))]
        public int SupportStaff { get; set; }

        [LocalizedDisplayName("Building_OtherPeople", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_OtherPeople_Description", NameResourceType = typeof(Resources.Names))]
        public int OtherPeople { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Building_Visitors", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_Visitors_Description", NameResourceType = typeof(Resources.Names))]
        public int Visitors { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Building_Residents", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_Residents_Description", NameResourceType = typeof(Resources.Names))]
        public int Residents { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Building_Patients", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_Patients_Description", NameResourceType = typeof(Resources.Names))]
        public int Patients { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        [LocalizedDisplayName("Building_Students", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Building_Students_Description", NameResourceType = typeof(Resources.Names))]
        public int Students { get; set; }

        public BuildingItemEditModel()
        {
            PhysicalAddress = String.Empty;
            Name = String.Empty;
            BuildingID = -1;
            BuildingGuid = Guid.NewGuid();
        }

        public BuildingItemEditModel(BL.Building entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Building entity)
        {
            // При обновлении entity Id не должен перезаписываться.
            entity.BuildingID = BuildingID;
            entity.BuildingGUID = BuildingGuid;
            entity.GeneralInformation.PhysicalAddress = PhysicalAddress;
            entity.GeneralInformation.Name = Name;
            entity.GeneralInformation.RegularStaff = RegularStaff;
            entity.GeneralInformation.SupportStaff = SupportStaff;
            entity.GeneralInformation.Visitors = Visitors;
            entity.GeneralInformation.Students = Students;
            entity.GeneralInformation.Residents = Residents;
            entity.GeneralInformation.Patients = Patients;
            entity.GeneralInformation.Purpose = (BL.BuildingPurpose)Purpose;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Building entity)
        {
            BuildingID = entity.BuildingID; // При загрузке из BL id должен сохраняться.
            BuildingGuid = entity.BuildingGUID;
            PhysicalAddress = entity.GeneralInformation.PhysicalAddress;
            Name = entity.GeneralInformation.Name;
            RegularStaff = entity.GeneralInformation.RegularStaff;
            SupportStaff = entity.GeneralInformation.SupportStaff;
            Visitors = entity.GeneralInformation.Visitors;
            Students = entity.GeneralInformation.Students;
            Residents = entity.GeneralInformation.Residents;
            Patients = entity.GeneralInformation.Patients;
            OtherPeople = entity.GeneralInformation.OtherPeople;
            Purpose = (int)entity.GeneralInformation.Purpose;
        }
    }
}