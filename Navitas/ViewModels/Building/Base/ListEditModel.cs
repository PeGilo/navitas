﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Navitas.ViewModels.Building.Base
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Базовый класс для классов редактируемых пользователем списков.
    /// </summary>
    public class ListEditModel
    {
        /// <summary>
        /// Строка идентифицирует список
        /// </summary>
        public string ListIdentifier
        {
            get;
            set;
        }

        private string _typeName = typeof(BaseItemEditModel).FullName;

        /// <summary>
        /// Полное имя типа содержащихся элементов в Items.
        /// </summary>
        public virtual string ItemsTypeName
        {
            get
            {
                return _typeName;
            }
            //set { _typeName = value; }
        }

        public IList<BaseItemEditModel> Items { get; set; }

        /// <summary>
        /// Название страницы
        /// </summary>
        public virtual string Title { get; set; }

        /// <summary>
        /// Заголовок на странице
        /// </summary>
        public virtual string Fieldset { get; set; }

        /// <summary>
        /// Заполняет контроллером
        /// </summary>
        public virtual string AddItemUrl { get; set; }

        public ListEditModel()
        {
            Items = new List<BaseItemEditModel>();
        }

        public ListEditModel(ListEditModel baseModel)
        {
            ListIdentifier = baseModel.ListIdentifier;
            Title = baseModel.Title;
            Fieldset = baseModel.Fieldset;
            AddItemUrl = baseModel.AddItemUrl;

            var copyList = new List<BaseItemEditModel>();
            foreach (BaseItemEditModel item in baseModel.Items)
            {
                copyList.Add((BaseItemEditModel)item.Clone());
            }
            Items = copyList;
        }

    }
}