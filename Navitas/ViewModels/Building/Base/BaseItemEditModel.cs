﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Building.Base
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Базовый класс для всех классов элементов редактируемых пользователем списков.
    /// </summary>
    public class BaseItemEditModel : ICloneable
    {
        public BaseItemEditModel()
        {
        }

        public BaseItemEditModel(BaseItemEditModel item)
        {
        }

        public BaseItemEditModel(BL.Meter meter)
        {
        }

        /// <summary>
        /// Наследуемые классы должны обязательно переопределять метод Clone.
        /// </summary>
        /// <returns></returns>
        public virtual object Clone()
        {
            return new BaseItemEditModel(this);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public virtual void UpdateEntity(BL.Meter entity)
        {
        }
    }
}