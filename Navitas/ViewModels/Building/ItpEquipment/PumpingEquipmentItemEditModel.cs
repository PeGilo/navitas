﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class PumpingEquipmentItemEditModel
    {
        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        /// <summary>
        /// Назначение насоса
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_PumpingEquipment_Purpose", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PumpingEquipment_Purpose_Description", NameResourceType = typeof(Resources.Names))]
        public string Purpose { get; set; }

        /// <summary>
        /// Марка насоса
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_PumpingEquipment_Name", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PumpingEquipment_Name_Description", NameResourceType = typeof(Resources.Names))]
        public string Name { get; set; }

        /// <summary>
        /// Кол-во, шт.
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_PumpingEquipment_Count", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PumpingEquipment_Count_Description", NameResourceType = typeof(Resources.Names))]
        public int? Count { get; set; }

        /// <summary>
        /// Электродвигатель Марка
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_PumpingEquipment_EngineName", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PumpingEquipment_EngineName_Description", NameResourceType = typeof(Resources.Names))]
        public string EngineName { get; set; }

        /// <summary>
        /// Электродвигатель Мощность, кВт	
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_PumpingEquipment_EnginePower", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PumpingEquipment_EnginePower_Description", NameResourceType = typeof(Resources.Names))]
        public double? EnginePower { get; set; }

        /// <summary>
        /// Часы работы в год, час
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_PumpingEquipment_HoursPerYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PumpingEquipment_HoursPerYear_Description", NameResourceType = typeof(Resources.Names))]
        public double? HoursPerYear { get; set; }

        /// <summary>
        /// Примечание (где установлен, работает или нет, в каком состоянии)
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_PumpingEquipment_Comment", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_PumpingEquipment_Comment_Description", NameResourceType = typeof(Resources.Names))]
        public string Comment { get; set; }

        public PumpingEquipmentItemEditModel()
        {
        }

        public PumpingEquipmentItemEditModel(BL.PumpingEquipment entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.PumpingEquipment entity)
        {
            entity.Name = Name;
            entity.Purpose = Purpose;
            entity.HoursPerYear = HoursPerYear;
            entity.Count = Count;
            entity.Comment = Comment;
            entity.EngineName = EngineName;
            entity.EnginePower = EnginePower;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.PumpingEquipment entity)
        {
            Name = entity.Name;
            Purpose = entity.Purpose;
            HoursPerYear = entity.HoursPerYear;
            Count = entity.Count;
            Comment = entity.Comment;
            EngineName = entity.EngineName;
            EnginePower = entity.EnginePower;
        }
    }
}