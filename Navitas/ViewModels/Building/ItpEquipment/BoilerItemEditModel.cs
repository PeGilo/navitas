﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class BoilerItemEditModel
    {
        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("ItpEquipment_Boiler_Name", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_Boiler_Name_Description", NameResourceType = typeof(Resources.Names))]
        public string Name { get; set; }

        [LocalizedDisplayName("ItpEquipment_Boiler_Power", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_Boiler_Power_Description", NameResourceType = typeof(Resources.Names))]
        public double? Power { get; set; }

        [LocalizedDisplayName("ItpEquipment_Boiler_HoursPerYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_Boiler_HoursPerYear_Description", NameResourceType = typeof(Resources.Names))]
        public double? HoursPerYear { get; set; }

        [LocalizedDisplayName("ItpEquipment_Boiler_Comment", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_Boiler_Comment_Description", NameResourceType = typeof(Resources.Names))]
        public string Comment { get; set; }

        public BoilerItemEditModel()
        {
        }

        public BoilerItemEditModel(BL.Boiler entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Boiler entity)
        {
            entity.Name = Name;
            entity.Power = Power;
            entity.HoursPerYear = HoursPerYear;
            entity.Comment = Comment;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Boiler entity)
        {
            Name = entity.Name;
            Power = entity.Power;
            HoursPerYear = entity.HoursPerYear;
            Comment = entity.Comment;
        }
    }
}