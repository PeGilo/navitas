﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class HeatExchangerItemEditModel
    {
        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        /// <summary>
        /// Назначение
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_HeatExchanger_Purpose", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_HeatExchanger_Purpose_Description", NameResourceType = typeof(Resources.Names))]
        public string Purpose { get; set; }

        /// <summary>
        /// Тип, марка
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_HeatExchanger_Name", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_HeatExchanger_Name_Description", NameResourceType = typeof(Resources.Names))]
        public string Name { get; set; }

        /// <summary>
        /// Кол-во секций, шт.
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_HeatExchanger_SectionsCount", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_HeatExchanger_SectionsCount_Description", NameResourceType = typeof(Resources.Names))]
        public int? SectionsCount { get; set; }

        /// <summary>
        /// Часы работы в год, час
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_HeatExchanger_HoursPerYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_HeatExchanger_HoursPerYear_Description", NameResourceType = typeof(Resources.Names))]
        public double? HoursPerYear { get; set; }

        /// <summary>
        /// Примечание (где установлен, работает или нет, в каком состоянии)
        /// </summary>
        [LocalizedDisplayName("ItpEquipment_HeatExchanger_Comment", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ItpEquipment_HeatExchanger_Comment_Description", NameResourceType = typeof(Resources.Names))]
        public string Comment { get; set; }

        public HeatExchangerItemEditModel()
        {
        }

        public HeatExchangerItemEditModel(BL.HeatExchanger entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.HeatExchanger entity)
        {
            entity.Name = Name;
            entity.Purpose = Purpose;
            entity.HoursPerYear = HoursPerYear;
            entity.SectionsCount = SectionsCount;
            entity.Comment = Comment;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.HeatExchanger entity)
        {
            Name = entity.Name;
            Purpose = entity.Purpose;
            HoursPerYear = entity.HoursPerYear;
            SectionsCount = entity.SectionsCount;
            Comment = entity.Comment;
        }
    }
}