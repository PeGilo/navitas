﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class IllustrationListEditModel
    {
        public IList<IllustrationItemEditModel> Items { get; set; }

        public string IllustrationJson
        {
            get { return JsonHelper.Serialize(Items); }
        }

        public IllustrationListEditModel()
        {
            Items = new List<IllustrationItemEditModel>();
        }

        public IllustrationListEditModel(IList<BL.Illustration> entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(IList<BL.Illustration> entity)
        {
            
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(IList<BL.Illustration> entity)
        {
            foreach (var entityItem in entity)
            {
                Items.Add(new IllustrationItemEditModel(entityItem));
            }
        }

        public string GetJsonDataFilteredBySectionName(string divisionName)
        {
            var filteredList = Items.Where(itemEditModel => itemEditModel.DivisionName == divisionName);
            return JsonHelper.Serialize(filteredList);
        }
    }
}