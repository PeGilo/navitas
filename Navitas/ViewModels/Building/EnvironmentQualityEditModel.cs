﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class EnvironmentQualityEditModel
    {
        public IList<EnvironmentQualityItemEditModel> Items { get; private set; }

        /// <summary>
        /// Список мест замеров.
        /// Инициализируется контроллером.
        /// </summary>
        public SelectList RoomTypes
        {
            get;
            set;
        }

        public EnvironmentQualityEditModel()
        {
            Items = new List<EnvironmentQualityItemEditModel>();
        }

        public EnvironmentQualityEditModel(BL.EnvironmentQualityList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.EnvironmentQualityList entity)
        {
            entity.Environments = Items.Select(item => { var bo = new BL.EnvironmentQuality(); item.UpdateEntity(bo); return bo; }).ToList();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.EnvironmentQualityList entity)
        {
            Items = new List<EnvironmentQualityItemEditModel>();

            foreach (var entityItem in entity.Environments)
            {
                Items.Add(new EnvironmentQualityItemEditModel(entityItem));
            }
        }
    }

}