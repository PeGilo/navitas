﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class PointOfWaterListEditModel
    {
        public IList<PointOfWaterItemEditModel> Items { get; private set; }

        /// <summary>
        /// Список мест установки.
        /// Инициализируется контроллером.
        /// </summary>
        public SelectList PointsList
        {
            get;
            set;
        }

        /// <summary>
        /// Список приборов.
        /// Инициализируется контроллером.
        /// </summary>
        public SelectList DevicesList
        {
            get;
            set;
        }

        public PointOfWaterListEditModel()
        {
            Items = new List<PointOfWaterItemEditModel>();
        }

        public PointOfWaterListEditModel(BL.PointOfWaterList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.PointOfWaterList entity)
        {
            entity.Points = Items.Select(item => { var bo = new BL.PointOfWater(); item.UpdateEntity(bo); return bo; }).ToList();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.PointOfWaterList entity)
        {
            Items = new List<PointOfWaterItemEditModel>();

            foreach (var entityItem in entity.Points)
            {
                Items.Add(new PointOfWaterItemEditModel(entityItem));
            }
        }
    }
}