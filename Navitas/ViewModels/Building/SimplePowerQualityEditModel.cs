﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class SimplePowerQualityEditModel
    {
        public IList<SimplePowerQualityItemEditModel> Items { get; private set; }

        public SimplePowerQualityEditModel()
        {
            Items = new List<SimplePowerQualityItemEditModel>();
        }

        public SimplePowerQualityEditModel(BL.PowerQualityList entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.PowerQualityList entity)
        {
            entity.Powers = Items.Select(item => { var bo = new BL.PowerQuality(); item.UpdateEntity(bo); return bo; }).ToList();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.PowerQualityList entity)
        {
            Items = new List<SimplePowerQualityItemEditModel>();

            foreach (var entityItem in entity.Powers)
            {
                if (entityItem.QualityType == BL.PowerQualityType.Simple)
                {
                    Items.Add(new SimplePowerQualityItemEditModel(entityItem));
                }
            }
        }


    }
}