﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class ElectroloadItemEditModel
    {
        /* Атрибут Required определяется для целочисленных, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("ElectroloadName", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ElectroloadName_Description", NameResourceType = typeof(Resources.Names))]
        public string Name { get; set; }

        [LocalizedDisplayName("ElectroloadCount", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ElectroloadCount_Description", NameResourceType = typeof(Resources.Names))]
        [HyphenInteger(ErrorMessageResourceName = "NotIntegerNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string Count { get; set; } // Int32

        [LocalizedDisplayName("ElectroloadPower", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ElectroloadPower_Description", NameResourceType = typeof(Resources.Names))]
        [HyphenNumber(ErrorMessageResourceName = "NotFloatingPointNumber", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public string Power { get; set; } // double

        [LocalizedDisplayName("ElectroloadHours", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("ElectroloadHours_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double TimeHoursPerYear { get; set; }

        public ElectroloadItemEditModel()
        {
            Name = "";
            Count = "0";
            Power = "0";
            TimeHoursPerYear = 0;
        }

        public ElectroloadItemEditModel(BL.Electroload entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Electroload entity)
        {
            entity.Name = Name;
            entity.Count = ConvertHelper.ParseInt32(Count);
            entity.Power = ConvertHelper.ParseDouble(Power);
            entity.TimeHoursPerYear = TimeHoursPerYear;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Electroload entity)
        {
            Name = entity.Name;
            Count = ConvertHelper.Convert(entity.Count);
            Power = ConvertHelper.Convert(entity.Power);
            TimeHoursPerYear = entity.TimeHoursPerYear;
        }
    }
}