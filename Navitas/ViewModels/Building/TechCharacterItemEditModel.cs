﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class TechCharacterItemEditModel
    {
        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("Technical_MaterialCaption", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_MaterialCaption_Description", NameResourceType = typeof(Resources.Names))]
        public string MaterialUid { get; set; }

        [LocalizedDisplayName("Technical_Thickness", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_Thickness_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double Thickness { get; set; }

        [LocalizedDisplayName("Technical_WearPercantage", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_WearPercantage_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double WearPercantage { get; set; }

        [LocalizedDisplayName("Technical_Description", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_Description_Description", NameResourceType = typeof(Resources.Names))]
        public string TechnicalCondition { get; set; }

        public TechCharacterItemEditModel()
        {
        }

        public TechCharacterItemEditModel(BL.WallLayer entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.WallLayer entity)
        {
            entity.Thickness = Thickness;
            entity.TechnicalCondition = TechnicalCondition;
            entity.WearPercantage = WearPercantage;
            if (!String.IsNullOrEmpty(MaterialUid))
            {
                entity.Material.WallMaterialUID = ConvertHelper.ParseGuid(MaterialUid);
            }
            else
            {
                entity.Material = null;
            }
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.WallLayer entity)
        {
            Thickness = entity.Thickness;
            TechnicalCondition = entity.TechnicalCondition;
            WearPercantage = entity.WearPercantage;
            MaterialUid = (entity.Material != null) ? ConvertHelper.Convert(entity.Material.WallMaterialUID) : String.Empty;
        }
    }

    public class WallLayerEditModel : TechCharacterItemEditModel
    {
        public WallLayerEditModel(BL.WallLayer entity)
            : base(entity)
        {
        }
    }

    public class AtticFloorEditModel : TechCharacterItemEditModel
    {
        public AtticFloorEditModel(BL.WallLayer entity)
            : base(entity)
        {
        }
    }

    public class BasementFloorEditModel : TechCharacterItemEditModel
    {
        public BasementFloorEditModel(BL.WallLayer entity)
            : base(entity)
        {
        }
    }
}