﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class AddInfoEditModel
    {
        //[LocalizedDisplayName("BuildingGeneral_Name", NameResourceType = typeof(Resources.Names))]
        //[LocalizedDescription("BuildingGeneral_Name_Description", NameResourceType = typeof(Resources.Names))]
        //public string Name { get; set; }

        //[LocalizedDisplayName("BuildingGeneral_Purpose", NameResourceType = typeof(Resources.Names))]
        //[LocalizedDescription("BuildingGeneral_Purpose_Description", NameResourceType = typeof(Resources.Names))]
        //public string Purpose { get; set; }

        /* Атрибут Required определяется для целочисленных, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("BuildingGeneral_StoreysNumber", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_StoreysNumber_Description", NameResourceType = typeof(Resources.Names))]
        [Range(1, 200, ErrorMessageResourceName = "NumberOfFloorExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public int NumberOfFloor { get; set; }

        [LocalizedDisplayName("BuildingGeneral_EntranceNumber", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_EntranceNumber_Description", NameResourceType = typeof(Resources.Names))]
        [Range(1, 20, ErrorMessageResourceName = "NumberOfEntrancesExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public int NumberOfEntrances { get; set; }

        /// <summary>
        /// Год постройки
        /// </summary>
        [LocalizedDisplayName("BuildingGeneral_BuildingYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_BuildingYear_Description", NameResourceType = typeof(Resources.Names))]        
        public double? BuildingYear { get; set; }

        [LocalizedDisplayName("BuildingGeneral_PowerSupply", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_PowerSupply_Description", NameResourceType = typeof(Resources.Names))]
        public int Electro { get; set; }

        [LocalizedDisplayName("BuildingGeneral_HeatingSupply", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_HeatingSupply_Description", NameResourceType = typeof(Resources.Names))]
        public int Heat { get; set; }

        [LocalizedDisplayName("BuildingGeneral_WaterSupply", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_WaterSupply_Description", NameResourceType = typeof(Resources.Names))]
        public int Water { get; set; }

        [LocalizedDisplayName("BuildingGeneral_WaterDisposal", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_WaterDisposal_Description", NameResourceType = typeof(Resources.Names))]
        public int Sewerage { get; set; }

        [LocalizedDisplayName("BuildingGeneral_GasSupply", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_GasSupply_Description", NameResourceType = typeof(Resources.Names))]
        public int Gas { get; set; }

        [LocalizedDisplayName("BuildingGeneral_City", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("BuildingGeneral_City_Description", NameResourceType = typeof(Resources.Names))]
        public int City { get; set; }

        public SelectList ResourceTypeList
        {
            get;
            set;
        }

        public SelectList CityList
        {
            get;
            set;
        }


        public AddInfoEditModel()
        {
            //ResourceTypeList = new SelectList(
            //    new[] { 
            //        new SelectListItem() { Value="0", Text="" },
            //        new SelectListItem() { Value="1", Text="ва" },
            //        new SelectListItem() { Value="2", Text="ываы" },
            //        new SelectListItem() { Value="3", Text="ываыв" }
            //    }, "Value", "Text");
        }

        public AddInfoEditModel(BL.BuildingAdditionalInformation entity)
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.BuildingAdditionalInformation entity)
        {
            entity.NumberOfFloor = NumberOfFloor;
            entity.NumberOfEntrances = NumberOfEntrances;
            entity.BuildingYear = BuildingYear;

            entity.Resources.ElectricityResource = (Electro != 0) ? new BL.ResourceSupplyType(Electro, String.Empty) : null;
            entity.Resources.HeatResource = (Heat != 0) ? new BL.ResourceSupplyType(Heat, String.Empty) : null;
            entity.Resources.WaterResource = (Water != 0) ? new BL.ResourceSupplyType(Water, String.Empty) : null;
            entity.Resources.SewerageResource = (Sewerage != 0) ? new BL.ResourceSupplyType(Sewerage, String.Empty) : null;
            entity.Resources.GasResource = (Gas != 0) ? new BL.ResourceSupplyType(Gas, String.Empty) : null;

            entity.CityCoeffs = (City != 0) ? new BL.CityCoeffs(City, String.Empty) : null;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.BuildingAdditionalInformation entity)
        {
            NumberOfFloor = entity.NumberOfFloor;
            NumberOfEntrances = entity.NumberOfEntrances;
            BuildingYear = entity.BuildingYear;

            Electro = entity.Resources.ElectricityResource != null ? 
                entity.Resources.ElectricityResource.ResourceSupplyTypeID
                : 0;
            Heat = entity.Resources.HeatResource != null ? 
                entity.Resources.HeatResource.ResourceSupplyTypeID
                : 0;
            Water = entity.Resources.WaterResource != null ? 
                entity.Resources.WaterResource.ResourceSupplyTypeID
                : 0;
            Sewerage = entity.Resources.SewerageResource != null ? 
                entity.Resources.SewerageResource.ResourceSupplyTypeID
                : 0;
            Gas = entity.Resources.GasResource != null ?
                entity.Resources.GasResource.ResourceSupplyTypeID
                : 0;

            City = entity.CityCoeffs != null ? entity.CityCoeffs.CityCoeffsId : 0;
        }

        // Список типов не загружен, когда выполняется POST запрос.
        //private string GetResourceNameById(int id)
        //{
        //    if (ResourceTypeList != null)
        //    {
        //        SelectListItem item = ResourceTypeList.First(i => i.Value == id.ToString());
        //        if (item != null)
        //        {
        //            return item.Text;
        //        }
        //    }
        //    return String.Empty;
        //}
    }
}