﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class TechCharactersEditModel
    {
        // Стены
        public IList<TechCharacterItemEditModel> Walls { get; private set; }

        // Чердачные перекрытия
        public IList<TechCharacterItemEditModel> AtticFloor { get; private set; }

        // Подвальные перекрытия
        public IList<TechCharacterItemEditModel> BasementFloor { get; private set; }

        // Двери
        public IList<TechDoorEditModel> Doors { get; private set; }

        // Окна
        public IList<TechWindowEditModel> Windows { get; private set; }

        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        /// <summary>
        /// Площадь здания
        /// </summary>
        [LocalizedDisplayName("Technical_BuildingYardage", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_BuildingYardage_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double BuildingYardage { get; set; }

        /// <summary>
        /// Площадь застройки
        /// </summary>
        [LocalizedDisplayName("Technical_ConstructionYardage", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ConstructionYardage_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double ConstructionYardage { get; set; }

        /// <summary>
        /// Площадь пола
        /// </summary>
        [LocalizedDisplayName("Technical_BasementFloorYardage", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_BasementFloorYardage_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double BasementFloorYardage { get; set; }

        /// <summary>
        /// Площадь потолка
        /// </summary>
        [LocalizedDisplayName("Technical_AtticFloorYardage", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_AtticFloorYardage_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double AtticFloorYardage { get; set; }

        /// <summary>
        /// Площадь стен
        /// </summary>
        [LocalizedDisplayName("Technical_WallsYardage", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_WallsYardage_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double WallsYardage { get; set; }

        
        /// <summary>
        /// объем здания по наружному обмеру
        /// </summary>
        [LocalizedDisplayName("Technical_BuildingVolume", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_BuildingVolume_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double BuildingVolume { get; set; }

        /// <summary>
        /// Высота здания (L)
        /// </summary>
        [LocalizedDisplayName("Technical_BuildingHeight", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_BuildingHeight_Description", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]
        public double BuildingHeight { get; set; }

        ///// <summary>
        ///// Процент износа ограждающих конструкций
        ///// </summary>
        //[LocalizedDisplayName("Technical_WearPercantage", NameResourceType = typeof(Resources.Names))]
        //[LocalizedDescription("Technical_WearPercantage_Description", NameResourceType = typeof(Resources.Names))]
        //[Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Navitas.Resources.ValidationStrings))]        
        //public double WearPercantage { get; set; }


        #region "По отоплению"

        /// <summary>
        /// удельная отопительная характеристика здания при от tо = -30С
        /// </summary>
        [LocalizedDisplayName("Technical_q0", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_q0_Description", NameResourceType = typeof(Resources.Names))]
        public double? q0 { get; set; }

        /// <summary>
        /// усредненная расчетная температура внутреннего воздуха в отапливаемых помещениях здания
        /// </summary>
        [LocalizedDisplayName("Technical_Tin", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_Tin_Description", NameResourceType = typeof(Resources.Names))]
        public double? Tin { get; set; }

        #endregion

        #region "По ГВС"

        /// <summary>
        /// средний суточный расход горячей воды л/сутки одним пользователем
        /// </summary>
        [LocalizedDisplayName("Technical_gDHW", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_gDHW_Description", NameResourceType = typeof(Resources.Names))]
        public double? gDHW { get; set; }

        /// <summary>
        /// количество пользователей ГВС
        /// </summary>
        [LocalizedDisplayName("Technical_m", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_m_Description", NameResourceType = typeof(Resources.Names))]
        public double? m { get; set; }

        #endregion

        #region "По Вентиляции"

        /// <summary>
        /// удельная тепловая вентиляционная характеристика здания, зависящая от назначения и 
        /// строительного объема вентилируемого здания, ккал/м3ч°С, принимается по МДС 41-2.2000 (табл.3). 
        /// </summary>
        [LocalizedDisplayName("Technical_qv", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_qv_Description", NameResourceType = typeof(Resources.Names))]
        public double? qv { get; set; }

        #endregion

        #region "По Удельной тепловой характеристике"

        /// <summary>
        /// фактическая продолжительность отопительного периода в отчетном (базовом) году, сут 
        /// </summary>
        /// <remarks>Переопределяет значение из снипа (по-умолчанию)</remarks>
        [LocalizedDisplayName("Technical_zh_fact", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_zh_fact_Description", NameResourceType = typeof(Resources.Names))]
        public int? zh_fact { get; set; }

        /// <summary>
        /// фактическая средняя температура внутреннего воздуха за отопительный период в отчетном (базовом) году
        /// </summary>
        /// <remarks>Переопределяет значение из снипа (по-умолчанию)</remarks>
        [LocalizedDisplayName("Technical_Tint_fact", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_Tint_fact_Description", NameResourceType = typeof(Resources.Names))]
        public double? Tint_fact { get; set; }

        /// <summary>
        /// фактическая средняя температура наружного воздуха за отопительный период в 2011 году
        /// </summary>
        /// <remarks>Переопределяет значение из снипа (по-умолчанию)</remarks>
        [LocalizedDisplayName("Technical_Tout_fact", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_Tout_fact_Description", NameResourceType = typeof(Resources.Names))]
        public double? Tout_fact { get; set; }

        //[LocalizedDisplayName("Technical_NormativeThermalCharacteristics", NameResourceType = typeof(Resources.Names))]
        //[LocalizedDescription("Technical_NormativeThermalCharacteristics_Description", NameResourceType = typeof(Resources.Names))]
        //public double? NormativeThermalCharacteristics { get; set; }

        /// <summary>
        /// температуру воздуха в наиболее холодный пятидневки
        /// </summary>
        [LocalizedDisplayName("Technical_Tout_min", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_Tout_min_Description", NameResourceType = typeof(Resources.Names))]        
        public double? Tout_min { get; set; }

        #endregion

        #region "По воде"

        /// <summary>
        /// норма расхода горчей воды в час наибольшего водопотребления на человека
        /// </summary>
        [LocalizedDisplayName("Technical_gHW_Max", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_gHW_Max_Description", NameResourceType = typeof(Resources.Names))]        
        public double? gHW_Max { get; set; }

        /// <summary>
        /// норма расхода холодной воды в час наибольшего водопотребления на человека
        /// </summary>
        [LocalizedDisplayName("Technical_gCW_Max", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_gCW_Max_Description", NameResourceType = typeof(Resources.Names))]        
        public double? gCW_Max { get; set; }

        /// <summary>
        /// Количество рабочих дней в год 
        /// </summary>
        [LocalizedDisplayName("Technical_WorkDaysPerYear", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_WorkDaysPerYear_Description", NameResourceType = typeof(Resources.Names))]
        public int? WorkDaysPerYear { get; set; }

        /// <summary>
        /// Количество рабочих часов в день
        /// </summary>
        [LocalizedDisplayName("Technical_WorkHoursPerDay", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_WorkHoursPerDay_Description", NameResourceType = typeof(Resources.Names))]            
        public int? WorkHoursPerDay { get; set; }

        /// <summary>
        /// режим работы
        /// </summary>
        [LocalizedDisplayName("Technical_WorkRegime", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_WorkRegime_Description", NameResourceType = typeof(Resources.Names))]
        public string WorkRegime { get; set; }

        #endregion

        #region "По перекрытиям"

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: Стен
        /// </summary>
        [LocalizedDisplayName("Technical_ThermoResistanceWalls", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ThermoResistanceWalls_Description", NameResourceType = typeof(Resources.Names))]
        public double? ThermoResistanceWalls { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: Покрытий и перекрытий над проездами
        /// </summary>
        [LocalizedDisplayName("Technical_ThermoResistanceDriveway", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ThermoResistanceDriveway_Description", NameResourceType = typeof(Resources.Names))]
        public double? ThermoResistanceDriveway { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: Перекрытий чердачных, над неотапли- ваемыми подпольями и подвалами
        /// </summary>
        [LocalizedDisplayName("Technical_ThermoResistanceAtticFloor", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ThermoResistanceAtticFloor_Description", NameResourceType = typeof(Resources.Names))]
        public double? ThermoResistanceAtticFloor { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций:Окон и балконных дверей, витрин и витражей
        /// </summary>
        [LocalizedDisplayName("Technical_ThermoResistanceDoorsWindows", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ThermoResistanceDoorsWindows_Description", NameResourceType = typeof(Resources.Names))]
        public double? ThermoResistanceDoorsWindows { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций:Фонарей с вертикальным остеклением
        /// </summary>
        [LocalizedDisplayName("Technical_ThermoResistanceVerticalLight", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ThermoResistanceVerticalLight_Description", NameResourceType = typeof(Resources.Names))]
        public double? ThermoResistanceVerticalLight { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: остеклений
        /// </summary>
        [LocalizedDisplayName("Technical_ThermoResistanceVerticalGlass", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Technical_ThermoResistanceVerticalGlass_Description", NameResourceType = typeof(Resources.Names))]
        public double? ThermoResistanceVerticalGlass { get; set; }

        #endregion

        /// <summary>
        /// Список материалов для стен.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList WallMaterials
        {
            get;
            set;
        }

        /// <summary>
        /// Список материалов для чердачных перекрытий.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList AtticFloorMaterials
        {
            get;
            set;
        }

        /// <summary>
        /// Список материалов для подвальных перекрытий.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList BasementFloorMaterials
        {
            get;
            set;
        }

        /// <summary>
        /// Список типов дверей.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList DoorTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Список типов окон.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList WindowTypes
        {
            get;
            set;
        }

        ///// <summary>
        ///// Список типов подвалов.
        ///// Должен инициализироваться контроллером.
        ///// </summary>
        //public SelectList FoundationTypes
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Тип подвала
        /// </summary>
        public int BasementType { get; set; }

        /// <summary>
        /// Список типов подвалов.
        /// Должен инициализироваться контроллером.
        /// </summary>
        public SelectList BasementTypes
        {
            get;
            set;
        }

        public TechCharactersEditModel()
        {
            Walls = new List<TechCharacterItemEditModel>();
            AtticFloor = new List<TechCharacterItemEditModel>();
            BasementFloor = new List<TechCharacterItemEditModel>();
            Doors = new List<TechDoorEditModel>();
            Windows = new List<TechWindowEditModel>();
        }

        public TechCharactersEditModel(BL.Walling entity)
            : this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.Walling entity)
        {
            entity.Walls = Walls.Select(wall => { var bo = new BL.WallLayer(); wall.UpdateEntity(bo); return bo; }).ToList();

            entity.AtticFloor = AtticFloor.Select(atf => { var bo = new BL.WallLayer(); atf.UpdateEntity(bo); return bo; }).ToList();

            entity.BasementFloor = BasementFloor.Select(bsf => { var bo = new BL.WallLayer(); bsf.UpdateEntity(bo); return bo; }).ToList();

            entity.Windows = Windows.Select(window => { var bo = new BL.Window(); window.UpdateEntity(bo); return bo; }).ToList();

            entity.Doors = Doors.Select(door => { var bo = new BL.Door(); door.UpdateEntity(bo); return bo; }).ToList();

            entity.Basement = (BL.BasementType)BasementType;

            entity.BuildingYardage = BuildingYardage;
            entity.ConstructionYardage = ConstructionYardage;
            entity.WallsYardage = WallsYardage;
            entity.AtticFloorYardage = AtticFloorYardage;
            entity.BasementFloorYardage = BasementFloorYardage;

            entity.BuildingVolume = BuildingVolume;
            entity.BuildingHeight = BuildingHeight;
            entity.BuildingThermoCoeffs.q0 = q0;
            entity.BuildingThermoCoeffs.Tin = Tin;
            entity.BuildingThermoCoeffs.gDHW = gDHW;
            entity.BuildingThermoCoeffs.m = m;
            entity.BuildingThermoCoeffs.qv = qv;
            entity.BuildingThermoCoeffs.zh_fact = zh_fact;
            entity.BuildingThermoCoeffs.Tint_fact = Tint_fact;
            entity.BuildingThermoCoeffs.Tout_fact = Tout_fact;
            //entity.BuildingThermoCoeffs.NormativeThermalCharacteristics = NormativeThermalCharacteristics;
            entity.BuildingThermoCoeffs.Tout_min = Tout_min;

            entity.BuildingWaterCoeffs.gHW_Max = gHW_Max;
            entity.BuildingWaterCoeffs.gCW_Max = gCW_Max;

            entity.BuildingCommonCoeffs.WorkDaysPerYear = WorkDaysPerYear;
            entity.BuildingCommonCoeffs.WorkHoursPerDay = WorkHoursPerDay;
            entity.BuildingCommonCoeffs.WorkRegime = WorkRegime;

            entity.BuildingWallingCoeffs.ThermoResistanceWalls = ThermoResistanceWalls;
            entity.BuildingWallingCoeffs.ThermoResistanceDriveway = ThermoResistanceDriveway;
            entity.BuildingWallingCoeffs.ThermoResistanceAtticFloor = ThermoResistanceAtticFloor;
            entity.BuildingWallingCoeffs.ThermoResistanceDoorsWindows = ThermoResistanceDoorsWindows;
            entity.BuildingWallingCoeffs.ThermoResistanceVerticalLight = ThermoResistanceVerticalLight;
            entity.BuildingWallingCoeffs.ThermoResistanceVerticalGlass = ThermoResistanceVerticalGlass;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Walling entity)
        {
            foreach (var wallLayer in entity.Walls)
            {
                Walls.Add(new WallLayerEditModel(wallLayer));
            }

            foreach (var atticLayer in entity.AtticFloor)
            {
                AtticFloor.Add(new AtticFloorEditModel(atticLayer));
            }

            foreach (var basementLayer in entity.BasementFloor)
            {
                BasementFloor.Add(new BasementFloorEditModel(basementLayer));
            }

            foreach (var window in entity.Windows)
            {
                Windows.Add(new TechWindowEditModel(window));
            }

            foreach (var door in entity.Doors)
            {
                Doors.Add(new TechDoorEditModel(door));
            }

            BasementType = (int)entity.Basement;
            BuildingYardage = entity.BuildingYardage;
            ConstructionYardage = entity.ConstructionYardage;
            WallsYardage = entity.WallsYardage;
            AtticFloorYardage = entity.AtticFloorYardage;
            BasementFloorYardage = entity.BasementFloorYardage;

            BuildingVolume = entity.BuildingVolume;
            BuildingHeight = entity.BuildingHeight;

            q0 = entity.BuildingThermoCoeffs.q0;
            Tin = entity.BuildingThermoCoeffs.Tin;
            gDHW = entity.BuildingThermoCoeffs.gDHW;
            m = entity.BuildingThermoCoeffs.m;
            qv = entity.BuildingThermoCoeffs.qv;
            zh_fact = entity.BuildingThermoCoeffs.zh_fact;
            Tint_fact = entity.BuildingThermoCoeffs.Tint_fact;
            Tout_fact = entity.BuildingThermoCoeffs.Tout_fact;
            Tout_min = entity.BuildingThermoCoeffs.Tout_min;
            //NormativeThermalCharacteristics = entity.BuildingThermoCoeffs.NormativeThermalCharacteristics;

            gHW_Max = entity.BuildingWaterCoeffs.gHW_Max;
            gCW_Max = entity.BuildingWaterCoeffs.gCW_Max;

            WorkDaysPerYear = entity.BuildingCommonCoeffs.WorkDaysPerYear;
            WorkHoursPerDay = entity.BuildingCommonCoeffs.WorkHoursPerDay;
            WorkRegime = entity.BuildingCommonCoeffs.WorkRegime;

            ThermoResistanceWalls = entity.BuildingWallingCoeffs.ThermoResistanceWalls;
            ThermoResistanceDriveway = entity.BuildingWallingCoeffs.ThermoResistanceDriveway;
            ThermoResistanceAtticFloor = entity.BuildingWallingCoeffs.ThermoResistanceAtticFloor;
            ThermoResistanceDoorsWindows = entity.BuildingWallingCoeffs.ThermoResistanceDoorsWindows;
            ThermoResistanceVerticalLight = entity.BuildingWallingCoeffs.ThermoResistanceVerticalLight;
            ThermoResistanceVerticalGlass = entity.BuildingWallingCoeffs.ThermoResistanceVerticalGlass;
        }
    }
}