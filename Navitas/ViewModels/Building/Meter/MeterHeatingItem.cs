﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterHeatingItem : MeterBaseItem
    {
        public MeterHeatingItem()
        {
        }

        public MeterHeatingItem(MeterHeatingItem item)
            : base(item)
        {
        }

        public MeterHeatingItem(BL.Meter entity)
            : base(entity)
        {
        }

        public override object Clone()
        {
            return new MeterHeatingItem(this);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public override void UpdateEntity(BL.Meter entity)
        {
            base.UpdateEntity(entity);
        }
    }
}