﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.ViewModels.Building.Base;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterBaseItem : BaseItemEditModel
    {
        /* Атрибут Required определяется, чтобы переопределить дефолтовое сообщение data-val-required.
        */

        [LocalizedDisplayName("Meters_InstallationSite", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_InstallationSite_Description", NameResourceType = typeof(Resources.Names))]
        public string InstallationSiteUID { get; set; }

        [LocalizedDisplayName("Meters_InstallationSite", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_InstallationSite_Description", NameResourceType = typeof(Resources.Names))]
        [StringLength(512, ErrorMessageResourceName="StringLengthExceeded", ErrorMessageResourceType=typeof(Resources.ValidationStrings))]
        public string InstallationSite { get; set; }

        [LocalizedDisplayName("Meters_Caption", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_Caption_Description", NameResourceType = typeof(Resources.Names))]
        public string CaptionUID { get; set; }

        [LocalizedDisplayName("Meters_Caption", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_Caption_Description", NameResourceType = typeof(Resources.Names))]
        [StringLength(512, ErrorMessageResourceName="StringLengthExceeded", ErrorMessageResourceType=typeof(Resources.ValidationStrings))]
        public string Caption { get; set; }

        [LocalizedDisplayName("Meters_AccuracyClass", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_AccuracyClass_Description", NameResourceType = typeof(Resources.Names))]
        public string AccuracyClassUID { get; set; }

        [LocalizedDisplayName("Meters_AccuracyClass", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_AccuracyClass_Description", NameResourceType = typeof(Resources.Names))]
        [StringLength(512, ErrorMessageResourceName="StringLengthExceeded", ErrorMessageResourceType=typeof(Resources.ValidationStrings))]
        public string AccuracyClass { get; set; }

        [LocalizedDisplayName("Meters_Sealing", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_Sealing_Description", NameResourceType = typeof(Resources.Names))]
        [UIHint("YesNo")]
        public bool Sealing { get; set; }

        [LocalizedDisplayName("Meters_Year", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_Year_Description", NameResourceType = typeof(Resources.Names))]
        public int? Year { get; set; }

        [LocalizedDisplayName("Meters_DateOfVerification", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_DateOfVerification_Description", NameResourceType = typeof(Resources.Names))]
        [UIHint("DateTimePicker")]
        public DateTime? DateOfVerification { get; set; }

        [LocalizedDisplayName("Meters_SerialNumber", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_SerialNumber_Description", NameResourceType = typeof(Resources.Names))]
        public String SerialNumber { get; set; }

        [LocalizedDisplayName("Meters_Comment", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_Comment_Description", NameResourceType = typeof(Resources.Names))]
        public string Comment { get; set; }

        public MeterBaseItem()
        {
        }

        public MeterBaseItem(MeterBaseItem item)
            : base(item)
        {
            InstallationSiteUID = item.InstallationSiteUID;
            CaptionUID = item.CaptionUID;
            AccuracyClassUID = item.AccuracyClassUID;
            InstallationSite = item.InstallationSite;
            Caption = item.Caption;
            AccuracyClass = item.AccuracyClass;
            Sealing = item.Sealing;
            Year = item.Year;
            DateOfVerification = item.DateOfVerification;
            SerialNumber = item.SerialNumber;
            Comment = item.Comment;
        }

        public MeterBaseItem(BL.Meter entity)
            : base(entity)
        {
            ConvertEntity(entity);
        }

        public override object Clone()
        {
            return new MeterBaseItem(this);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public override void UpdateEntity(BL.Meter entity)
        {
            entity.InstallationSiteUID = ConvertHelper.ParseGuid(InstallationSiteUID);
            entity.CaptionUID = ConvertHelper.ParseGuid(CaptionUID);
            entity.AccuracyClassUID = ConvertHelper.ParseGuid(AccuracyClassUID);
            entity.InstallationSite = InstallationSite;
            entity.Caption = Caption;
            entity.AccuracyClass = AccuracyClass;
            entity.Sealing = Sealing;
            entity.Year = Year;
            entity.DateOfVerification = DateOfVerification;
            entity.SerialNumber = SerialNumber;
            entity.Comment = Comment;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.Meter entity)
        {
            InstallationSiteUID = ConvertHelper.Convert(entity.InstallationSiteUID);
            CaptionUID = ConvertHelper.Convert(entity.CaptionUID);
            AccuracyClassUID = ConvertHelper.Convert(entity.AccuracyClassUID);
            InstallationSite = entity.InstallationSite;
            Caption = entity.Caption;
            AccuracyClass = entity.AccuracyClass;
            Sealing = entity.Sealing;
            Year = entity.Year;
            DateOfVerification = entity.DateOfVerification;
            SerialNumber = entity.SerialNumber;
            Comment = entity.Comment;
        }
    }
}