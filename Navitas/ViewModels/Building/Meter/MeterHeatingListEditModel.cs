﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.ViewModels.Building.Base;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterHeatingListEditModel : MeterBaseListEditModel
    {
        public override string ItemsTypeName
        {
            get
            {
                return typeof(MeterHeatingItem).FullName;
            }
        }

        public override string Title { get { return Resources.Strings.Meters_Heating_Title; } }

        public override string Fieldset { get { return Resources.Strings.Meters_Heating_Fieldset; } }

        public MeterHeatingListEditModel()
        {
        }

        public MeterHeatingListEditModel(ListEditModel baseModel)
            : base(baseModel)
        {
        }

        public MeterHeatingListEditModel(BL.MeterLists entity)
           : base(entity)
        {
            ConvertEntity(entity);
        }

        public override void UpdateEntity(BL.MeterLists entity)
        {
            entity.Heating = Items.Select(item => { var bo = new BL.MeterHeating(); item.UpdateEntity(bo); return bo; }).ToList();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.MeterLists entity)
        {
            Items = new List<BaseItemEditModel>();

            foreach (var entityItem in entity.Heating)
            {
                Items.Add(new MeterHeatingItem(entityItem));
            }
        }
    }
}