﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.ViewModels.Building.Base;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterElectricEnergyListEditModel : MeterBaseListEditModel
    {
        public override string ItemsTypeName
        {
            get
            {
                return typeof(MeterElectricEnergyItem).FullName;
            }
        }

        public override string Title { get { return Resources.Strings.Meters_ElectricEnergy_Title; } }

        public override string Fieldset { get { return Resources.Strings.Meters_ElectricEnergy_Fieldset; } }

        //private SelectList _list;

        //public SelectList List
        //{
        //    get
        //    {
        //        return _list;
        //    }
        //    set
        //    {
        //        _list = value;

        //        foreach (var item in Items)
        //        {
        //            ViewModels.Items.ListItemElectricity elitem = item as ViewModels.Items.ListItemElectricity;
        //            elitem.List = _list;
        //        }
        //    }
        //}

        public MeterElectricEnergyListEditModel()
        {
        }

        public MeterElectricEnergyListEditModel(ListEditModel baseModel)
            : base(baseModel)
        {
        }

        public MeterElectricEnergyListEditModel(BL.MeterLists entity)
           : base(entity)
        {
            ConvertEntity(entity);
        }

        public override void UpdateEntity(BL.MeterLists entity)
        {
            entity.ElectricEnergy = Items.Select(item => { var bo = new BL.MeterElectricEnergy(); item.UpdateEntity(bo); return bo; }).ToList();
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.MeterLists entity)
        {
            Items = new List<BaseItemEditModel>();

            foreach (var entityItem in entity.ElectricEnergy)
            {
                Items.Add(new MeterElectricEnergyItem(entityItem));
            }
        }
    }
}