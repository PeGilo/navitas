﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterGasItem : MeterBaseItem
    {
        public MeterGasItem()
        {
        }

        public MeterGasItem(MeterGasItem item)
            : base(item)
        {
        }

        public MeterGasItem(BL.Meter entity)
            : base(entity)
        {
        }

        public override object Clone()
        {
            return new MeterGasItem(this);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public override void UpdateEntity(BL.Meter entity)
        {
            base.UpdateEntity(entity);
        }
    }
}