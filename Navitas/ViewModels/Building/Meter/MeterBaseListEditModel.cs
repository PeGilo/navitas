﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.ViewModels.Building.Base;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Базовый класс для списков типов измерительных приборов.
    /// </summary>
    public class MeterBaseListEditModel : ListEditModel
    {

        public SelectList CaptionList
        {
            get;
            set;
        }

        public SelectList InstallationSiteList
        {
            get;
            set;
        }

        public SelectList AccurasyClassList
        {
            get;
            set;
        }

        public MeterBaseListEditModel()
        {
        }

        public MeterBaseListEditModel(ListEditModel baseModel)
            : base(baseModel)
        {
        }

        public MeterBaseListEditModel(BL.MeterLists entity)
        {

        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public virtual void UpdateEntity(BL.MeterLists entity)
        {
        }

    }
}