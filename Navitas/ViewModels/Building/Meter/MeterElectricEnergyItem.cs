﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.ViewModels.Building.Base;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterElectricEnergyItem : MeterBaseItem
    {
        public MeterElectricEnergyItem()
        {
        }

        public MeterElectricEnergyItem(MeterElectricEnergyItem item)
            : base(item)
        {
        }

        public MeterElectricEnergyItem(BL.Meter entity)
            : base(entity)
        {
        }

        public override object Clone()
        {
            return new MeterElectricEnergyItem(this);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public override void UpdateEntity(BL.Meter entity)
        {
            base.UpdateEntity(entity);
        }
    }
}