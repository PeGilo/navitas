﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.ViewModels.Building.Base;

namespace Navitas.ViewModels.Building.Meter
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterHotWaterItem  : MeterBaseItem
    {
        [LocalizedDisplayName("Meters_MeasurementRange", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("Meters_MeasurementRange_Description", NameResourceType = typeof(Resources.Names))]
        public string MeasurementRange { get; set; }

        public MeterHotWaterItem()
        {
        }

        public MeterHotWaterItem(MeterHotWaterItem item)
            : base(item)
        {
            MeasurementRange = item.MeasurementRange;
        }

        public MeterHotWaterItem(BL.Meter entity)
            : base(entity)
        {
            BL.MeterHotWater specificEntity = (BL.MeterHotWater)entity;
            MeasurementRange = specificEntity.MeasurementRange;
        }

        public override object Clone()
        {
            return new MeterHotWaterItem(this);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public override void UpdateEntity(BL.Meter entity)
        {
            base.UpdateEntity(entity);

            BL.MeterHotWater specificEntity = (BL.MeterHotWater)entity;
            specificEntity.MeasurementRange = MeasurementRange;
        }
    }
}