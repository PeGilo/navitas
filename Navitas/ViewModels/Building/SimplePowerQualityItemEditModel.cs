﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Navitas.Infrastructure.MvcHelpers;

namespace Navitas.ViewModels.Building
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class SimplePowerQualityItemEditModel
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [LocalizedDisplayName("SimplePowerQty_PowerInput", NameResourceType = typeof(Resources.Names))]
        [LocalizedDescription("SimplePowerQty_PowerInput_Description", NameResourceType = typeof(Resources.Names))]
        public string PowerInput { get; set; }

        public IList<SimplePowerQualityMeasureEditModel> Measurements { get; set; }

        public SimplePowerQualityItemEditModel()
        {
            Measurements = new List<SimplePowerQualityMeasureEditModel>();
            Measurements.Add(new SimplePowerQualityMeasureEditModel());
            Measurements.Add(new SimplePowerQualityMeasureEditModel());
            Measurements.Add(new SimplePowerQualityMeasureEditModel());
            Measurements.Add(new SimplePowerQualityMeasureEditModel());
            Measurements.Add(new SimplePowerQualityMeasureEditModel());
        }

        public SimplePowerQualityItemEditModel(BL.PowerQuality entity)
            :this()
        {
            ConvertEntity(entity);
        }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(BL.PowerQuality entity)
        {
            if (entity.QualityType == BL.PowerQualityType.Simple)
            {
                entity.PowerInput = PowerInput;
                for(int i = 0; i < Measurements.Count; i++)
                {
                    entity.Simple[i].Time = Measurements[i].Time;
                    entity.Simple[i].UPhaseA = Measurements[i].UPhaseA;
                    entity.Simple[i].UPhaseB = Measurements[i].UPhaseB;
                    entity.Simple[i].UPhaseC = Measurements[i].UPhaseC;
                    entity.Simple[i].IPhaseA = Measurements[i].IPhaseA;
                    entity.Simple[i].IPhaseB = Measurements[i].IPhaseB;
                    entity.Simple[i].IPhaseC = Measurements[i].IPhaseC;
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(BL.PowerQuality entity)
        {
            if (entity.QualityType == BL.PowerQualityType.Simple)
            {
                Measurements = new List<SimplePowerQualityMeasureEditModel>();

                PowerInput = entity.PowerInput;
                for(int i = 0; i < entity.Simple.Count; i++)
                {
                    SimplePowerQualityMeasureEditModel item = new SimplePowerQualityMeasureEditModel();

                    item.Time = entity.Simple[i].Time;
                    item.UPhaseA = entity.Simple[i].UPhaseA;
                    item.UPhaseB = entity.Simple[i].UPhaseB;
                    item.UPhaseC = entity.Simple[i].UPhaseC;
                    item.IPhaseA = entity.Simple[i].IPhaseA;
                    item.IPhaseB = entity.Simple[i].IPhaseB;
                    item.IPhaseC = entity.Simple[i].IPhaseC;

                    Measurements.Add(item);
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}