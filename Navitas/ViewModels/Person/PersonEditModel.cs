﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Navitas.ViewModels.Person
{
    /// <summary>
    /// Должностное лицо
    /// </summary>
    public class PersonEditModel
    {
        public PersonEditModel()
        {
        }

        public PersonEditModel(Soft33.EnergyPassport.V2.BL.Person entity)
        {
            ConvertEntity(entity);
        }

        [DisplayName("1. Должность")]
        [Description("Должность")]
        public string Position { get; set; }

        [DisplayName("2. ФИО")]
        [Description("ФИО")]
        public string FullName { get; set; }

        [DisplayName("3. Телефон")]
        [Description("Телефон")]
        public string Phone { get; set; }

        [DisplayName("4. Факс")]
        [Description("Факс")]
        public string Fax { get; set; }

        /// <summary>
        /// Загружает данные в объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Soft33.EnergyPassport.V2.BL.Person entity)
        {
            entity.Position = Position;
            entity.FullName = FullName;
            entity.Phone = Phone;
            entity.Fax = Fax;
        }

        /// <summary>
        /// Загружает данные в модель из объекта бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        private void ConvertEntity(Soft33.EnergyPassport.V2.BL.Person entity)
        {
            Position = entity.Position;
            FullName = entity.FullName;
            Phone = entity.Phone;
            Fax = entity.Fax;
        }
    }
}