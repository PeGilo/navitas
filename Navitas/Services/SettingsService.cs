﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Models;
using Navitas.Services.Structures;
using Navitas.Services.EntityConverters;

namespace Navitas.Services
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class SettingsService
    {
        Models.DataManager _dataManager;

        /// <summary>
        /// Максимальное количество элементов выбираемы при поиске похожих слов.
        /// </summary>
        private const int MAX_GUESS_COUNT = 15;

        public SettingsService(Models.DataManager dataManager)
        {
            _dataManager = dataManager;
        }

        //private IEntityConverter<BL.CalculationCoeffs, Models.CalculationCoeff> _coeffsConverter = null;

        //private IEntityConverter<BL.CalculationCoeffs, Models.CalculationCoeff> CoeffsConverter
        //{
        //    get
        //    {
        //        if (_coeffsConverter == null)
        //        {
        //            _coeffsConverter = EntityConverterFactory.Instance.Create<BL.CalculationCoeffs, Models.CalculationCoeff>();
        //        }
        //        return _coeffsConverter;
        //    }
        //}

        public IList<ListItem> GetFuelTypes()
        {
            return new List<ListItem>() {
                new ListItem(1, "Бензин"),
                new ListItem(2, "Керосин"),
                new ListItem(3, "Дизельное топливо"),
                new ListItem(4, "Газ")
            };
        }

        public IList<ListItem> GetOdometerTypes()
        {
            return new List<ListItem>() {
                new ListItem(1, "Электронный"),
                new ListItem(2, "Механический")
            };
        }

        public IList<ListItem> GetBuildingPurposes()
        {
            return new List<ListItem>() {
                new ListItem(0, ""),
                new ListItem(1, "Жилое"),
                new ListItem(2, "Общественное"),
                new ListItem(3, "Административное"),
                new ListItem(4, "Промышленное"),
                new ListItem(5, "Сельскохозяйственное")
            };
        }

        public IList<ListItem> GetResourceSupplyTypes()
        {
            IEnumerable<ResourceSupplyType> rst = _dataManager.Directory.GetResourceSupplyTypes();
            List<ListItem> list = new List<ListItem>();

            foreach (ResourceSupplyType rstItem in rst)
            {
                list.Add(new ListItem(rstItem.ResourceSupplyTypeID, rstItem.Name));
            }

            return list;
        }

        public IList<ListItem> GetCityCoeffs()
        {
            IEnumerable<CityCoeff> ccList = _dataManager.Directory.GetCityCoeffs();
            List<ListItem> list = new List<ListItem>();

            foreach (var cc in ccList)
            {
                list.Add(new ListItem(cc.CityCoeffsID, cc.CityName));
            }

            return list;
        }

        public IList<ListTextItem> GetWallMaterials()
        {
            //return new List<ListTextItem>() {
            //    new ListTextItem("E8A788D9-92EB-4EB7-BBF7-AD64FF292380", "Глиняный обыкновенный кирпич"),
            //    new ListTextItem("E8A788D9-92EB-4EB7-BBF7-AD64FF292382", "Орихалковый слиток")
            //};

            return (from wm in _dataManager.Directory.GetWallMaterials()
                    select new ListTextItem(wm.WallMaterialUID.ToString(), wm.Caption)).ToList();
        }

        public IList<ListTextItem> GetAtticFloorMaterials()
        {
            return (from atf in _dataManager.Directory.GetAtticFloorMaterials()
                    select new ListTextItem(atf.AtticFloorMaterialUID.ToString(), atf.Caption)).ToList();
        }

        public IList<ListTextItem> GetBasementFloorMaterials()
        {
            return (from bsf in _dataManager.Directory.GetBasementFloorMaterials()
                    select new ListTextItem(bsf.BasementFloorMaterialUID.ToString(), bsf.Caption)).ToList();
        }

        public IList<ListTextItem> GetWaterPointsList()
        {
            return (from powt in _dataManager.Directory.GetPointOfWaterTypes()
                    select new ListTextItem(powt.PointOfWaterTypeUID.ToString(), powt.Caption)).ToList();
        }

        public IList<ListTextItem> GetDevicesLists()
        {
            return (from device in _dataManager.Directory.GetPointOfWaterDeviceTypes()
                    select new ListTextItem(device.PointOfWaterDeviceTypeUID.ToString(), device.Caption)).ToList();
        }

        public IList<ListTextItem> GetRoomTypes()
        {
            return (from roomType in _dataManager.Directory.GetRoomTypes()
                    select new ListTextItem(roomType.RoomTypeUID.ToString(), roomType.Caption)).ToList();
        }

        public IList<ListTextItem> GetDoorTypes()
        {
            return (from doorType in _dataManager.Directory.GetDoorTypes()
                    select new ListTextItem(doorType.DoorTypeUID.ToString(), doorType.Caption)).ToList();
        }

        public IList<ListTextItem> GetWindowTypes()
        {
            return (from windowType in _dataManager.Directory.GetWindowTypes()
                    select new ListTextItem(windowType.WindowTypeUID.ToString(), windowType.Caption)).ToList();
        }

        //public IList<ListTextItem> GetFoundationTypes()
        //{
        //    return (from foundationType in _dataManager.Directory.GetFoundationTypes()
        //            select new ListTextItem(foundationType.FoundationTypeUID.ToString(), foundationType.Caption)).ToList();
        //}

        public IEnumerable<string> GetMeterCaptionsElectricEnergy(string captionText)
        {
            return _dataManager.Directory.GetMeterCaptionsElectricEnergy(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterCaptionsColdWater(string captionText)
        {
            return _dataManager.Directory.GetMeterCaptionsColdWater(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterCaptionsHotWater(string captionText)
        {
            return _dataManager.Directory.GetMeterCaptionsHotWater(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterCaptionsHeating(string captionText)
        {
            return _dataManager.Directory.GetMeterCaptionsHeating(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterCaptionsGas(string captionText)
        {
            return _dataManager.Directory.GetMeterCaptionsGas(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterPlacesElectricEnergy(string captionText)
        {
            return _dataManager.Directory.GetMeterPlacesElectricEnergy(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterPlacesColdWater(string captionText)
        {
            return _dataManager.Directory.GetMeterPlacesColdWater(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterPlacesHotWater(string captionText)
        {
            return _dataManager.Directory.GetMeterPlacesHotWater(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterPlacesHeating(string captionText)
        {
            return _dataManager.Directory.GetMeterPlacesHeating(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterPlacesGas(string captionText)
        {
            return _dataManager.Directory.GetMeterPlacesGas(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterClassesElectricEnergy(string captionText)
        {
            return _dataManager.Directory.GetMeterClassesElectricEnergy(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterClassesColdWater(string captionText)
        {
            return _dataManager.Directory.GetMeterClassesColdWater(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterClassesHotWater(string captionText)
        {
            return _dataManager.Directory.GetMeterClassesHotWater(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterClassesHeating(string captionText)
        {
            return _dataManager.Directory.GetMeterClassesHeating(captionText, MAX_GUESS_COUNT);
        }

        public IEnumerable<string> GetMeterClassesGas(string captionText)
        {
            return _dataManager.Directory.GetMeterClassesGas(captionText, MAX_GUESS_COUNT);
        }

        public BL.CalculationThermoCoeffs GetCalculationThermoCoeffs()
        {
            CalculationCoeff entity = _dataManager.CalculationCoeffsModels.GetCalculationCoeffByName("Теплоснабжение");
            if (entity != null)
            {
                return ConverterHelper.Deserialize<BL.CalculationThermoCoeffs>(entity.Coeffs);
            }
            return null;
        }

        public BL.CalculationFinanceCoeffs GetCalculationFinanceCoeffs()
        {

            CalculationCoeff entity = _dataManager.CalculationCoeffsModels.GetCalculationCoeffByName("Финансы");
            if (entity != null)
            {
                return ConverterHelper.Deserialize<BL.CalculationFinanceCoeffs>(entity.Coeffs);
            }
            return null;
        }
    }
}