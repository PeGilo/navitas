﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services
{
    public class ServiceManager
    {
        private Models.DataManager _dataManager;
        private InspectionService _inspectionService;
        private BuildingService _buildingService;
        private SettingsService _settingsService;
        private ReportService _reportService;

        public ServiceManager(Models.DataManager dataManager)
        {
            _dataManager = dataManager;
        }

        public SettingsService SettingsService
        {
            get
            {
                if (_settingsService == null)
                {
                    _settingsService = new SettingsService(_dataManager);
                }
                return _settingsService;
            }
        }

        public InspectionService InspectionService
        {
            get 
            {
                if (_inspectionService == null)
                {
                    _inspectionService = new InspectionService(_dataManager);
                }
                return _inspectionService;
            }
        }

        public BuildingService BuildingService
        {
            get
            {
                if (_buildingService == null)
                {
                    _buildingService = new BuildingService(_dataManager);
                }
                return _buildingService;
            }
        }

        public ReportService ReportService
        {
            get
            {
                if (_reportService == null)
                {
                    _reportService = new ReportService(_dataManager);
                }
                return _reportService;
            }
        }
    }
}