﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

using Navitas.Infrastructure;
using Navitas.Services.Exceptions;

namespace Navitas.Services
{
    public abstract class ServiceBase
    {
        Models.DataManager _dataManager;

        public ServiceBase(Models.DataManager dataManager)
        {
            _dataManager = dataManager;
        }

        /// <summary>
        /// Преобразует возникающие в процессе доступа к данным исключения в 
        /// исключения определенные для данного слоя. Может также выполнять дополнительные действия, например,
        /// логирование исключений.
        /// </summary>
        /// <param name="action">Метод, в котором могут возникнуть исключения</param>
        protected virtual void HandleExceptions(Action action)
        {
            try
            {
                action();
            }
            catch (System.Data.Linq.ChangeConflictException ex)
            {
                throw new ConcurrencyException("Объект уже изменен другим пользователем.", ex);
            }
            catch (DataAccess.Exceptions.ReadWriteException ex)
            {
                throw new Navitas.Services.Exceptions.ReadWriteException("Ошибка чтения данных", ex);
            }
            catch (DataAccess.Exceptions.ConcurrencyException ex)
            {
                throw new Navitas.Services.Exceptions.ConcurrencyException("Объект уже изменен другим пользователем.", ex);
            }
            catch (DataAccess.Exceptions.DataAccessBaseException ex)
            {
                throw new Navitas.Services.Exceptions.ServiceBaseException("Неизвестная ошибка в слое доступа к данным", ex);
            }
            catch (Navitas.Services.Exceptions.ServiceBaseException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Преобразует возникающие в процессе доступа к данным исключения в 
        /// исключения определенные для данного слоя. Может также выполнять дополнительные действия, например,
        /// логирование исключений.
        /// </summary>
        /// <param name="action">Метод, в котором могут возникнуть исключения</param>
        protected virtual T HandleExceptions<T>(Func<T> function)
        {
            try
            {
                return function();
            }
            catch (System.Data.Linq.ChangeConflictException ex)
            {
                throw new ConcurrencyException("Объект уже изменен другим пользователем.", ex);
            }
            catch (DataAccess.Exceptions.ReadWriteException ex)
            {
                throw new Navitas.Services.Exceptions.ReadWriteException("Ошибка чтения данных", ex);
            }
            catch (DataAccess.Exceptions.ConcurrencyException ex)
            {
                throw new Navitas.Services.Exceptions.ConcurrencyException("Объект уже изменен другим пользователем.", ex);
            }
            catch (DataAccess.Exceptions.DataAccessBaseException ex)
            {
                throw new Navitas.Services.Exceptions.ServiceBaseException("Неизвестная ошибка в слое доступа к данным", ex);
            }
            catch (Navitas.Services.Exceptions.ServiceBaseException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        protected virtual T HandleExceptions<T, O>(FuncOut<T,O> func, out O param)
        {
            try
            {
                return func(out param);
            }
            catch (System.Data.Linq.ChangeConflictException ex)
            {
                throw new ConcurrencyException("Объект уже изменен другим пользователем.", ex);
            }
            catch (DataAccess.Exceptions.ReadWriteException ex)
            {
                throw new Navitas.Services.Exceptions.ReadWriteException("Ошибка чтения данных", ex);
            }
            catch (DataAccess.Exceptions.ConcurrencyException ex)
            {
                throw new Navitas.Services.Exceptions.ConcurrencyException("Объект уже изменен другим пользователем.", ex);
            }
            catch (DataAccess.Exceptions.DataAccessBaseException ex)
            {
                throw new Navitas.Services.Exceptions.ServiceBaseException("Неизвестная ошибка в слое доступа к данным", ex);
            }
            catch (Navitas.Services.Exceptions.ServiceBaseException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}