﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

using Navitas.Services.EntityConverters;

namespace Navitas.Services
{
    public class EntityConverterFactory
    {
        protected Dictionary<Type, IEntityConverter> _converters;

        protected static EntityConverterFactory _instance;

        public static EntityConverterFactory Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new EntityConverterFactory();
                return _instance;
            }
        }

        protected EntityConverterFactory()
        {
            Init();
        }

        private void Init()
        {
            Assembly asm = Assembly.GetCallingAssembly();

            _converters = new Dictionary<Type, IEntityConverter>();

            Type[] allTypes = asm.GetTypes();
            foreach (Type type in allTypes)
            {
                if (type.IsClass && !type.IsAbstract)
                {
                    Type iFactoryProduct = type.GetInterface("IEntityConverter");
                    if (iFactoryProduct != null)
                    {
                        var inst = asm.CreateInstance(type.FullName, true,
                            BindingFlags.CreateInstance, null, null, null, null) as IEntityConverter;

                        if (inst != null)
                        {
                            IEntityConverter keyDesc = (IEntityConverter)inst;
                            Type key = keyDesc.GetModelType();
                            inst = null;

                            _converters.Add(key, keyDesc);
                        }
                    }
                }
            }
        }

        public IEntityConverter<TModel, TEntity> Create<TModel, TEntity>()
            where TModel : class
            where TEntity : class
        {
            return _converters.Where(conv => conv.Key == typeof(TModel)).Select(conv => conv.Value).FirstOrDefault() as IEntityConverter<TModel, TEntity>;
        }	
    }
}