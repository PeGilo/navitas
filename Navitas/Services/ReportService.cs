﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
//using System.Linq.Dynamic;
using System.Transactions;
using System.Web;

using Navitas.Infrastructure;
using Navitas.Report;
using Navitas.Report.Elements;
using Navitas.Services.EntityConverters;
using Navitas.Services.Exceptions;
using Soft33.Navitas.Common;

namespace Navitas.Services
{
    using BL = Soft33.EnergyPassport.V2.BL;
    using BLExt = Soft33.EnergyPassport.V2.BL.Extended;

    public class ReportService : ServiceBase
    {
        private static readonly object _tableLocker = new Object();

        private Models.DataManager _dataManager;

        private IEntityConverter<Report.InspectionReport, Models.InspectionReportList> _listConverter = null;

        private IEntityConverter<Report.InspectionReport, Models.InspectionReportList> ListConverter
        {
            get
            {
                if (_listConverter == null)
                {
                    _listConverter = EntityConverterFactory.Instance.Create<Report.InspectionReport, Models.InspectionReportList>();
                }
                return _listConverter;
            }
        }

        private IEntityConverter<Report.Section, Models.InspectionReport> _sectionConverter = null;

        private IEntityConverter<Report.Section, Models.InspectionReport> SectionConverter
        {
            get
            {
                if (_sectionConverter == null)
                {
                    _sectionConverter = EntityConverterFactory.Instance.Create<Report.Section, Models.InspectionReport>();
                }
                return _sectionConverter;
            }
        }

        private IEntityConverter<Report.MasterReportTemplate, Models.MasterReportTemplate> _masterReportTemplateConverter = null;

        private IEntityConverter<Report.MasterReportTemplate, Models.MasterReportTemplate> MasterReportTemplateConverter
        {
            get
            {
                if (_masterReportTemplateConverter == null)
                {
                    _masterReportTemplateConverter = EntityConverterFactory.Instance.Create<Report.MasterReportTemplate, Models.MasterReportTemplate>();
                }
                return _masterReportTemplateConverter;
            }
        }

        public ReportService(Models.DataManager dataManager)
            : base(dataManager)
        {
            _dataManager = dataManager;
        }

        /// <summary>
        /// Возвращает идентификатор обследования, к которому относится раздел отчета с указанным идентификатором.
        /// </summary>
        /// <param name="sectionId">Идентификатор раздела.</param>
        /// <returns>Идентификатор обследования.</returns>
        public int? GetInspectionIdOfSection(int sectionId)
        {
            Models.InspectionReport section = _dataManager.InspectionReport.GetById(sectionId);
            if (section != null)
            {
                return section.InspectionID;
            }
            else
            {
                return null;
            }
        }
        
        /// <summary>
        /// Создает отчет копированием разделов из мастер-отчета.
        /// </summary>
        /// <param name="masterReportId">Идентификатор мастер-отчета.</param>
        /// <param name="inspectionId">Идентификатор обследования, для которого создается отчет.</param>
        private void InstantiateReport(int masterReportId, int inspectionId, BL.Inspection inspection)
        {
            //// Скопировать строки из таблицы MasterReportTemplate
            //// в таблицу для отчетов по обследованиям. Некоторые должны повторятся для каких-либо списков (Building)
            //// Если есть такие строки уже, то сгенерировтаь исключение.

            List<Models.InspectionReport> inspectionSections = new List<Models.InspectionReport>();

            // 1. Прочитать списки MasterReportTemplate
            List<Models.MasterReportTemplate> masterSections = _dataManager.MasterReportModels.GetSections(masterReportId);

            // 2. Отсортировать их
            masterSections.Sort(new Comparison<Models.MasterReportTemplate>((Models.MasterReportTemplate lhs, Models.MasterReportTemplate rhs) =>
                lhs.SequenceNumber - rhs.SequenceNumber));

            // 3. Для каждого MasterReportTemplate инстанцируем его в один или несколько InspectionReport
            // Здесь еще присвоить InspectionID
            foreach (Models.MasterReportTemplate section in masterSections)
            {
                Report.MasterReportTemplate mrt = MasterReportTemplateConverter.ConvertToModel(section);

                if(mrt.IsRepeatable)
                {
                    int count = ReflectionHelper.GetCollectionItemsCount<BL.Inspection>(inspection, mrt.RepeatOn);
                    
                    for (int i = 0; i < count; i++)
			        {
                        inspectionSections.Add(mrt.Instantiate(i));
			        }
                }
                else
                {
                    inspectionSections.Add(mrt.Instantiate());
                }
            }

            // 4. Пронумеровать заново список
            for(int i = 0; i < inspectionSections.Count; i++) {
                inspectionSections[i].SequenceNumber = i + 1;
            }

            // 5. Перенумеровать Id
            RenumerateElementIds(inspectionSections, inspection);

            // 6. Необходимо присвоить InspectionID
            foreach (var sections in inspectionSections)
            {
                sections.InspectionID = inspectionId;
            }

            // 7. Сохраняем список InspectionReport
            _dataManager.InspectionReport.AddRange(inspectionSections);
        }

        /// <summary>
        /// Перенумеровать Id элементов в отчете, чтобы они были уникальными
        /// </summary>
        /// <param name="inspectionSections"></param>
        /// <param name="inspection"></param>
        private void RenumerateElementIds(List<Models.InspectionReport> inspectionSections, BL.Inspection inspection)
        {
            // 1. Сконвертировать разделы отчета в объекты, с которыми можно работать (чтобы содержание XML десериализовалось)
            IList<Report.Section> sections = (from i in inspectionSections select SectionConverter.ConvertToModel(i)).ToList();

            // 2. Пройтись по всем разделам и обработать картинки
            List<Report.Section> updatedSections = new List<Section>();

            int uniqueIndex = 1;

            for (int i = 0; i < sections.Count; i++)
            {
                // Обычные свойства раздела просто копируются
                Report.Section updatedSection = new Section();
                updatedSection.SectionId = sections[i].SectionId;
                //updatedSection.InspectionId = sections[i].InspectionId; // не имеет смысла копировать, т.к. элементы мастер-шаблона не связаны с Inspection
                updatedSection.SequenceNumber = sections[i].SequenceNumber;
                updatedSection.TimeStamp = sections[i].TimeStamp;

                // Элементы, включенные в раздел, перебираются
                for (int index = 0; index < sections[i].Elements.Count; index++)
                {
                    BaseElement sourceElement = sections[i].Elements[index];
                    BaseElement destElement;

                    //if (sourceElement is ChartStackElement)
                    //{
                    //    BaseElement[] destElements = ((ChartStackElement)sourceElement).Split();
                    //    // Присвоить всем уникальные индексы
                    //    foreach (var el in destElements) { el.Id = uniqueIndex++; }
                    //    updatedSection.Elements.AddRange(destElements);
                    //}
                    //else
                    //{
                        // Копируем все свойства
                        destElement = (BaseElement)sourceElement.Clone();
                        // Идентификатор устанавливаем
                        destElement.Id = uniqueIndex++;
                        updatedSection.Elements.Add(destElement);
                    //}
                }

                // ?? updatedSections.Add(updatedSection);
                // Преобразовать разделы обратно в XML, чтобы сохранить в базе
                SectionConverter.UpdateEntity(inspectionSections[i], updatedSection);
            }
        }

        //private void SubstituteIllustrationIds(List<Models.InspectionReport> inspectionSections, BL.Inspection inspection)
        //{
        //    // Для картинок необходимо подменить Id, который был в базе, на Id установленный для картинки пользователем.
        //    // При этом картинок с одинаквым идентификатором из базы может быть несклько - их необходимо размножить.

        //    // 1. Получить все картинки, относящиеся к обследованию из базы
        //    IList<Models.Illustration> illustrations = _dataManager.IllustrationModels.GetIllustrationsByInspectionId(inspection.InspectionID);

        //    // 2. Сконвертировать разделы отчета в объекты, с которыми можно работать (чтобы содержание XML десериализовалось)
        //    IList<Report.Section> sections = (from i in inspectionSections select SectionConverter.ConvertToModel(i)).ToList();

        //    // 3. Пройтись по всем разделам и обработать картинки
        //    List<Report.Section> updatedSections = new List<Section>();

        //    for (int i = 0; i < sections.Count; i++)
        //    {
        //        // Обычные свойства раздела просто копируются
        //        Report.Section updatedSection = new Section();
        //        updatedSection.SectionId = sections[i].SectionId;
        //        //updatedSection.InspectionId = sections[i].InspectionId; // не имеет смысла копировать, т.к. элементы мастер-шаблона не связаны с Inspection
        //        updatedSection.SequenceNumber = sections[i].SequenceNumber;
        //        updatedSection.TimeStamp = sections[i].TimeStamp;

        //        // Элементы, включенные в раздел, перебираются
        //        for (int index = 0; index < sections[i].Elements.Count; index++)
        //        {
        //            BaseElement sourceElement = sections[i].Elements[index];
        //            BaseElement destElement;

        //            if(sourceElement is Report.Elements.ImageElement)
        //            {
        //                Report.Elements.ImageElement sourceIllustration = sourceElement as Report.Elements.ImageElement;
        //                // Для элементов - илююстраций необходимо заменить Id
        //                // ... Сначала найти сколько иллюстраций с таким id содержит Обследование.
        //                IEnumerable<Models.Illustration> sameId = illustrations.Where(ill => ill.DivisionName == sourceIllustration.TextId);
        //                //if(!String.IsNullOrEmpty(sourceIllustration.SelectionStatementConstraint))
        //                //{
        //                //    sameId = sameId.AsQueryable().Where(sourceIllustration.SelectionStatementConstraint);
        //                //}

        //                foreach (var ill in sameId)
        //                {
        //                    destElement = new Report.Elements.ImageElement()
        //                    {
        //                        Id = sourceIllustration.Id,
        //                        ImageUrl = sourceIllustration.ImageUrl,
        //                        TextId = ill.UserTitle                  // Здесь подменяем общий Id, на тот, который указал пользователь.
        //                    };
        //                    updatedSection.Elements.Add(destElement);
        //                }
        //            }
        //            else
        //            {
        //                // Все остальные элементы просто копируем
        //                destElement = (BaseElement)sourceElement.Clone();
        //                updatedSection.Elements.Add(destElement);
        //            }
        //        }

        //        // ?? updatedSections.Add(updatedSection);
        //        // Преобразовать разделы обратно в XML, чтобы сохранить в базе
        //        SectionConverter.UpdateEntity(inspectionSections[i], updatedSection);
        //    }
        //}

        /// <summary>
        /// Возвращает отчет по обследованию, либо создает новый, если его не существует.
        /// </summary>
        /// <param name="masterReportId">Идентификатор мастер-шаблона для отчета</param>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <returns>Отчет</returns>
        public InspectionReport CreateOrGetExistingReport(int masterReportId, BL.Inspection inspection) //int inspectionId)
        {
            // Проверка существования до блокирования таблицы, чтобы не блокировать ее каждый раз
            bool exists = _dataManager.InspectionReport.ReportExists(inspection.InspectionID);

            if (!exists)
            {
                // Исключить одновременную работу со строкам в таблице во время генерации отчета
                lock (_tableLocker)
                {
                    // Использован режим Serializable, чтобы заблокировать таблицы от изменений
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required,
                                                    new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }))
                    {
                        // Повторная проверка существования отчета, т.к. он мог уже добавиться
                        exists = _dataManager.InspectionReport.ReportExists(inspection.InspectionID);

                        // Добавление отчета
                        if (!exists)
                        {
                            InstantiateReport(masterReportId, inspection.InspectionID, inspection);
                            transaction.Complete();
                        }
                    }
                }
            }

            // Выбираем отчет, он должен существовать к этому времени
            IList<Models.InspectionReport> reportSections = _dataManager.InspectionReport.SelectReportSections(inspection.InspectionID);

            if (reportSections.Count > 0)
            {
                Report.InspectionReport report = ListConverter.ConvertToModel(new Models.InspectionReportList(reportSections));
                return report;
            }
            else
            {
                throw new NotFoundException("Невозможно выбрать отчет.");
            }
        }

        public InspectionReport RegenerateReport(int masterReportId, BL.Inspection inspection) //int inspectionId)
        {
            // Исключить одновременную работу со строкам в таблице во время генерации отчета
            lock(_tableLocker)
            {
                // Использован режим Serializable, чтобы заблокировать таблицы от изменений
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required,
                                                new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }))
                {
                    // Удалить строки отчета
                    _dataManager.InspectionReport.DeleteReport(inspection.InspectionID);

                    // Добавление отчета
                    InstantiateReport(masterReportId, inspection.InspectionID, inspection);
                    transaction.Complete();
                }
            }

            // Выбираем отчет, он должен существовать к этому времени
            IList<Models.InspectionReport> reportSections = _dataManager.InspectionReport.SelectReportSections(inspection.InspectionID);

            if (reportSections.Count > 0)
            {
                return ListConverter.ConvertToModel(new Models.InspectionReportList(reportSections));
            }
            else
            {
                throw new NotFoundException("Невозможно выбрать отчет.");
            }
        }

        /// <summary>
        /// Подготавливает объект Обследования для передачи его в RazorEngine для рендеринга.
        /// В том числе пересоздает все файлы графиков в отчете.
        /// </summary>
        /// <param name="inspectionExt"></param>
        /// <remarks>Каждый график необходимо отрисовать в отдельности</remarks>
        public void PrerenderInspection(BLExt.InspectionExtended inspectionExt, Infrastructure.Environment.AppEnvironment env)
        {
            // Сгенерировать файлы графиков для показа в отчете.
            foreach (var building in inspectionExt.Buildings)
            {
                building.ChartSpecificEnergyConsumption.Chart.ImageFileName = RenderChartToFile(building.ChartSpecificEnergyConsumption.Chart, env);

                building.ChartNormAndFactHeatingConsumption.Chart.ImageFileName = RenderChartToFile(building.ChartNormAndFactHeatingConsumption.Chart, env);
                building.ChartNormAndFactDHWConsumption.Chart.ImageFileName = RenderChartToFile(building.ChartNormAndFactDHWConsumption.Chart, env);
                building.ChartNormAndFactVentinConsumption.Chart.ImageFileName = RenderChartToFile(building.ChartNormAndFactVentinConsumption.Chart, env);

                foreach (var chart in building.Charts)
                {
                    chart.ImageFileName = RenderChartToFile(chart, env);
                }
            }

            inspectionExt.ChartPaymentStructure.Chart.ImageFileName =
                RenderChartToFile(inspectionExt.ChartPaymentStructure.Chart, env);

            inspectionExt.ChartEnergyConsumptionDynamic.Chart.ImageFileName =
                RenderChartToFile(inspectionExt.ChartEnergyConsumptionDynamic.Chart, env);

            inspectionExt.ChartElectricEnergyConsumptionDynamic.Chart.ImageFileName =
                RenderChartToFile(inspectionExt.ChartElectricEnergyConsumptionDynamic.Chart, env);

            inspectionExt.ChartElectricEnergyConsumptionStructure.Chart.ImageFileName =
                RenderChartToFile(inspectionExt.ChartElectricEnergyConsumptionStructure.Chart, env);

            inspectionExt.ChartHeatEnergyConsumptionDynamic.Chart.ImageFileName =
                RenderChartToFile(inspectionExt.ChartHeatEnergyConsumptionDynamic.Chart, env);

            inspectionExt.WFR.Chart.ImageFileName =
                RenderChartToFile(inspectionExt.WFR.Chart, env);
        }

        /// <summary>
        /// Отрисовывает график в файл
        /// </summary>
        /// <param name="chartData"></param>
        /// <param name="env"></param>
        /// <returns></returns>
        private string RenderChartToFile(BL.Charts.TemplateChartData chartData, Infrastructure.Environment.AppEnvironment env)
        {
            ChartManager chartManager = new ChartManager(env);
            return chartManager.RenderChartToFile(chartData);
        }


        /// <summary>
        /// Изменяет данные одной из текстовых частей раздела.
        /// </summary>
        /// <param name="sectionId">Идентификатор раздела, содержащего текстовый параграф.</param>
        /// <param name="elementId">Идентификатор текстового раздела.</param>
        /// <param name="text">Новый текст, который необходимо записать.</param>
        /// <param name="timestamp">Timestamp раздела до обновления.</param>
        /// <param name="newTimestamp">Timestamp раздела после его обновления</param>
        /// <returns>Обновленный текстовый параграф, который необходимо передать на клиента.</returns>
        public EditableTextElement UpdateEditableTextElement(int sectionId, int elementId, string text, 
            Binary timestamp, out Binary newTimestamp)
        {
            return HandleExceptions<EditableTextElement, System.Data.Linq.Binary>(delegate(out Binary newtimestamp)
                {
                    newtimestamp = default(Binary);

                    Models.InspectionReport entity = _dataManager.InspectionReport.GetById(sectionId);
                    if (entity != null)
                    {
                        Report.Section section = SectionConverter.ConvertToModel(entity);
                        // проверить чтобы timestamp совпадал
                        if (section.TimeStamp.ToArray().SequenceEqual(timestamp.ToArray()))
                        {

                            // Изменение
                            EditableTextElement updatedElement = section.UpdateEditableTextElement(elementId, PreprocessInputText(text));

                            // Сохранение
                            SectionConverter.UpdateEntity(entity, section);

                            _dataManager.Save();

                            newtimestamp = entity.TimeStamp; // Timestamp обновился и можно передать его в вызывающий код.
                            return updatedElement;
                        }
                        else
                        {
                            throw new ConcurrencyException("Данные уже изменены.");
                        }
                    }
                    else
                    {
                        throw new NotFoundException("Раздела с указанным идентификатором не обнаружено.");
                    }
                },
                out newTimestamp
            );
        }

        /// <summary>
        /// Скрывает текстовый параграф в разделе.
        /// </summary>
        /// <param name="sectionId">Идентификатор раздела.</param>
        /// <param name="elementId">Идентификатор текстового параграфа.</param>
        /// <param name="timestamp">Временная метка раздела пользовательского эксземпляра.</param>
        /// <returns>Обновленная временная метка раздела</returns>
        public System.Data.Linq.Binary HideEditableTextElement(int sectionId, int elementId, Binary timestamp)
        {
            return HandleExceptions<System.Data.Linq.Binary>(delegate()
            {
                Models.InspectionReport entity = _dataManager.InspectionReport.GetById(sectionId);
                if (entity != null)
                {
                    Report.Section section = SectionConverter.ConvertToModel(entity);
                    // проверить чтобы timestamp совпадал
                    if (section.TimeStamp.ToArray().SequenceEqual(timestamp.ToArray()))
                    {
                        // Изменение
                        section.HideEditableTextElement(elementId);

                        // Сохранение
                        SectionConverter.UpdateEntity(entity, section);

                        _dataManager.Save();

                        return entity.TimeStamp;// Timestamp обновился и можно передать его в вызывающий код.
                    }
                    else
                    {
                        throw new ConcurrencyException("Данные уже изменены.");
                    }
                }
                else
                {
                    throw new NotFoundException("Раздела с указанным идентификатором не обнаружено.");
                }
            });
        }

        //public System.Data.Linq.Binary HideRequiredImageElement(int sectionId, int blockId, Binary timestamp)
        //{
        //    return HandleExceptions<System.Data.Linq.Binary>(delegate()
        //    {
        //        Models.InspectionReport entity = _dataManager.InspectionReport.GetById(sectionId);
        //        if (entity != null)
        //        {
        //            Report.Section section = SectionConverter.ConvertToModel(entity);
        //            // проверить чтобы timestamp совпадал
        //            if (section.TimeStamp.ToArray().SequenceEqual(timestamp.ToArray()))
        //            {
        //                // Изменение
        //                section.HideRequiredImageElement(blockId);

        //                // Сохранение
        //                SectionConverter.UpdateEntity(entity, section);

        //                _dataManager.Save();

        //                return entity.TimeStamp;// Timestamp обновился и можно передать его в вызывающий код.
        //            }
        //            else
        //            {
        //                throw new ConcurrencyException("Данные уже изменены.");
        //            }
        //        }
        //        else
        //        {
        //            throw new NotFoundException("Раздела с указанным идентификатором не обнаружено.");
        //        }
        //    });
        //}

        //public System.Data.Linq.Binary RemoveImageElement(int sectionId, int elementId, Binary timestamp)
        //{
        //    return HandleExceptions<System.Data.Linq.Binary>(delegate()
        //    {
        //        Models.InspectionReport entity = _dataManager.InspectionReport.GetById(sectionId);
        //        if (entity != null)
        //        {
        //            Report.Section section = SectionConverter.ConvertToModel(entity);
        //            // проверить чтобы timestamp совпадал
        //            if (section.TimeStamp.ToArray().SequenceEqual(timestamp.ToArray()))
        //            {
        //                // Изменение
        //                section.RemoveImageElement(elementId);

        //                // Сохранение
        //                SectionConverter.UpdateEntity(entity, section);

        //                _dataManager.Save();

        //                return entity.TimeStamp;// Timestamp обновился и можно передать его в вызывающий код.
        //            }
        //            else
        //            {
        //                throw new ConcurrencyException("Данные уже изменены.");
        //            }
        //        }
        //        else
        //        {
        //            throw new NotFoundException("Раздела с указанным идентификатором не обнаружено.");
        //        }
        //    });
        //}

        public System.Data.Linq.Binary HideElement(int sectionId, int elementId, Binary timestamp)
        {
            return HandleExceptions<System.Data.Linq.Binary>(delegate()
            {
                Models.InspectionReport entity = _dataManager.InspectionReport.GetById(sectionId);
                if (entity != null)
                {
                    Report.Section section = SectionConverter.ConvertToModel(entity);
                    // проверить чтобы timestamp совпадал
                    if (section.TimeStamp.ToArray().SequenceEqual(timestamp.ToArray()))
                    {
                        // Изменение
                        section.HideElement(elementId);

                        // Сохранение
                        SectionConverter.UpdateEntity(entity, section);

                        _dataManager.Save();

                        return entity.TimeStamp;// Timestamp обновился и можно передать его в вызывающий код.
                    }
                    else
                    {
                        throw new ConcurrencyException("Данные уже изменены.");
                    }
                }
                else
                {
                    throw new NotFoundException("Раздела с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Вставляет иллюстрацию после текстово элемента с указанным id
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="elementId"></param>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public ImageElement InsertImage(int sectionId, int anchorElementId, int illustrationId, Binary timestamp, bool before,
            out System.Data.Linq.Binary newTimestamp)
        {
            return HandleExceptions<ImageElement, System.Data.Linq.Binary>(delegate(out Binary newtimestamp)
            {
                newtimestamp = default(Binary);

                Models.Illustration illustration = _dataManager.IllustrationModels.GetIllustrationById(illustrationId);
                if (illustration == null)
                {
                    throw new NotFoundException("Иллюстрации с указанным идентификатором не обнаружено.");
                }

                Models.InspectionReport entity = _dataManager.InspectionReport.GetById(sectionId);
                if (entity != null)
                {
                    Report.Section section = SectionConverter.ConvertToModel(entity);
                    // проверить чтобы timestamp совпадал
                    if (section.TimeStamp.ToArray().SequenceEqual(timestamp.ToArray()))
                    {
                        // Изменение
                        int newImageId = section.GenereateNewElementId();
                        ImageElement insertedElement = section.InsertImage(anchorElementId, newImageId, illustration.FileName, illustration.UserTitle, before);

                        // Сохранение
                        SectionConverter.UpdateEntity(entity, section);

                        _dataManager.Save();

                        newtimestamp = entity.TimeStamp;
                        return insertedElement;
                    }
                    else
                    {
                        throw new ConcurrencyException("Данные уже изменены.");
                    }
                }
                else
                {
                    throw new NotFoundException("Раздела с указанным идентификатором не обнаружено.");
                }
            },
                out newTimestamp
            );
        }

        /// <summary>
        /// Вставляет текстовый блок после текстово элемента с указанным id
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="anchorElementId">Элемент, рядом с которым вставляется заданный</param>
        /// <param name="content"></param>
        /// <param name="timestamp"></param>
        /// <param name="before">true, если вставить до, false - после</param>
        /// <param name="newTimestamp"></param>
        /// <returns></returns>
        public EditableTextElement InsertText(int sectionId, int anchorElementId, string content, Binary timestamp, bool before,
            out System.Data.Linq.Binary newTimestamp)
        {
            return HandleExceptions<EditableTextElement, System.Data.Linq.Binary>(delegate(out Binary newtimestamp)
            {
                newtimestamp = default(Binary);

                Models.InspectionReport entity = _dataManager.InspectionReport.GetById(sectionId);
                if (entity != null)
                {
                    Report.Section section = SectionConverter.ConvertToModel(entity);
                    // проверить чтобы timestamp совпадал
                    if (section.TimeStamp.ToArray().SequenceEqual(timestamp.ToArray()))
                    {
                        // Изменение
                        int newElementId = section.GenereateNewElementId();
                        EditableTextElement insertedElement =
                            section.InsertText(anchorElementId, newElementId, PreprocessInputText(content), before);

                        // Сохранение
                        SectionConverter.UpdateEntity(entity, section);

                        _dataManager.Save();

                        newtimestamp = entity.TimeStamp;
                        return insertedElement;
                    }
                    else
                    {
                        throw new ConcurrencyException("Данные уже изменены.");
                    }
                }
                else
                {
                    throw new NotFoundException("Раздела с указанным идентификатором не обнаружено.");
                }
            },
                out newTimestamp
            );
        }

        /// <summary>
        /// Для текстовых элементов, обрабатывает разметку.
        /// Необходимо для обработки текста введенного клиентом, чтобы унифицировать поведение разных браузеров.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private string PreprocessInputText(string text)
        {
            // IE вставляет для переноса строк <p>
            // Firefox - <br/>
            // Chrome - <div>
            // Заменяем все на <br/>
            return //text.Replace("&lt;p&gt;", "").Replace("&lt;/p&gt;", "&lt;br/&gt;").
                text.Replace("&lt;div&gt;", "&lt;br/&gt;").Replace("&lt;/div&gt;", "");
        }

    }
}