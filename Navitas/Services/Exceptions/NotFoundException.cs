﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.Exceptions
{
    /// <summary>
    /// Исключение, сигнализирующее о том, что какой-либо объект не найден в базе.
    /// </summary>
    [Serializable]
    public class NotFoundException : ServiceBaseException
    {
        public NotFoundException() { }
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(string message, Exception inner) : base(message, inner) { }
        protected NotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}