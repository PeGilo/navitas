﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.Exceptions
{
    /// <summary>
    /// Исключение, сигнализирующее об ошибке при чтении/записи данных.
    /// </summary>
    [Serializable]
    public class ReadWriteException : ServiceBaseException
    {
        public ReadWriteException() { }
        public ReadWriteException(string message) : base(message) { }
        public ReadWriteException(string message, Exception inner) : base(message, inner) { }
        protected ReadWriteException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}