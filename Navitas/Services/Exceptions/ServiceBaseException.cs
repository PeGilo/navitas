﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.Exceptions
{
    /// <summary>
    /// Базовое исключение для всех исключений, возникающих в слое доступа к данным.
    /// </summary>
    [Serializable]
    public class ServiceBaseException : ApplicationException
    {
        public ServiceBaseException() { }
        public ServiceBaseException(string message) : base(message) { }
        public ServiceBaseException(string message, Exception inner) : base(message, inner) { }
        protected ServiceBaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}