﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.Structures
{
    public struct ListTextItem
    {
        private string _id;
        private string _text;

        public string Id { get { return _id; } set { _id = value; } }
        public string Text { get { return _text;} set { _text = value; } }

        public ListTextItem(string id, string text)
        {
            _id = id;
            _text = text;
        }
    }
}