﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.Structures
{
    public struct ListItem
    {
        private int _id;
        private string _text;

        public int Id { get { return _id; } set { _id = value; } }
        public string Text { get { return _text;} set { _text = value; } }

        public ListItem(int id, string text)
        {
            _id = id;
            _text = text;
        }
    }
}