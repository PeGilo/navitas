﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Services.Exceptions;
using Navitas.Services.EntityConverters;

namespace Navitas.Services
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Содержит методы для работы с объектами "Здания".
    /// Бизнес-логика.
    /// </summary>
    public class BuildingService : ServiceBase
    {
        private Models.DataManager _dataManager;
        private IEntityConverter<BL.Building, Models.Building> _converter = null;

        private IEntityConverter<BL.Building, Models.Building> Converter
        {
            get
            {
                if (_converter == null)
                {
                    _converter = EntityConverterFactory.Instance.Create<BL.Building, Models.Building>();
                }
                return _converter;
            }
        }

        public BuildingService(Models.DataManager dataManager)
            : base(dataManager)
        {
            _dataManager = dataManager;
        }

        public BL.Building CreateBuilding()
        {
            BL.Building building = new BL.Building();

            return building;
        }

        /// <summary>
        /// Создает объект "Потребление за последний год (12 месяцев)"
        /// </summary>
        /// <returns></returns>
        public BL.TwelveMonthIntake CreateIntakeTwelveMonths()
        {
            return new BL.TwelveMonthIntake();
        }

        /// <summary>
        /// Создает объект "Элемент перечня электрооборудования"
        /// </summary>
        /// <returns></returns>
        public BL.Electroload CreateElectroloadItem()
        {
            return new BL.Electroload();
        }

        /// <summary>
        /// Создает объект "Перечень электрооборудования"
        /// </summary>
        /// <returns></returns>
        public BL.ElectroloadList CreateElectroloadList()
        {
            return new BL.ElectroloadList();
        }

        public BL.ItpEquipment CreateItpEquipment()
        {
            return new BL.ItpEquipment();
        }

        /// <summary>
        /// Создает элемент списка "Приточной вентиляции"
        /// </summary>
        /// <returns></returns>
        public BL.VentilationCombinedExtractInput CreateCombinedVentIn()
        {
            return new BL.VentilationCombinedExtractInput();
        }

        /// <summary>
        /// Создает элемент списка "Вытяжной вентиляции"
        /// </summary>
        /// <returns></returns>
        public BL.VentilationExtract CreateVentOut()
        {
            return new BL.VentilationExtract();
        }

        /// <summary>
        /// Создает элемент списка "Приборов отопления"
        /// </summary>
        /// <returns></returns>
        public BL.Radiator CreateRadiator()
        {
            return new BL.Radiator();
        }

        /// <summary>
        /// Создает объект для "Технических характеристика здания"
        /// </summary>
        /// <returns></returns>
        public BL.Walling CreateWalling()
        {
            return new BL.Walling();
        }

        /// <summary>
        /// Создает элемент списка "Технических характеристик здания"
        /// </summary>
        /// <returns></returns>
        public BL.WallLayer CreateWallLayer()
        {
            return new BL.WallLayer();
        }

        /// <summary>
        /// Создает элемент списка дверей "Технических характеристик здания"
        /// </summary>
        /// <returns></returns>
        public BL.Door CreateDoor()
        {
            return new BL.Door();
        }

        /// <summary>
        /// Создает элемент списка окон "Технических характеристик здания"
        /// </summary>
        /// <returns></returns>
        public BL.Window CreateWindow()
        {
            return new BL.Window();
        }

        /// <summary>
        /// Создает элемент списка "Сведений о точках водоразбора"
        /// </summary>
        /// <returns></returns>
        public BL.PointOfWater CreatePointOfWater()
        {
            return new BL.PointOfWater();
        }

        /// <summary>
        /// Создает список "Сведений о точках водоразбора"
        /// </summary>
        /// <returns></returns>
        public BL.PointOfWaterList CreatePointOfWaterList()
        {
            return new BL.PointOfWaterList();
        }

        public BL.EnvironmentQuality CreateEnvironmentQuality()
        {
            return new BL.EnvironmentQuality();
        }

        public BL.EnvironmentQualityList CreateEnvironmentQualityList()
        {
            return new BL.EnvironmentQualityList();
        }

        public BL.PowerQuality CreatePowerQuality()
        {
            return new BL.PowerQuality();
        }

        public BL.MeterLists CreateMeterLists()
        {
            return new BL.MeterLists();
        }

        public BL.MeterElectricEnergy CreateMeterElectricEnergyItem()
        {
            return new BL.MeterElectricEnergy();
        }

        public BL.MeterColdWater CreateMeterColdWaterItem()
        {
            return new BL.MeterColdWater();
        }

        public BL.MeterHotWater CreateMeterHotWaterItem()
        {
            return new BL.MeterHotWater();
        }

        public BL.MeterHeating CreateMeterHeatingItem()
        {
            return new BL.MeterHeating();
        }

        public BL.MeterGas CreateMeterGasItem()
        {
            return new BL.MeterGas();
        }

        /// <summary>
        /// Возвращает здание с указанным идентификатором или null, если
        /// здание с таким идентификатором не найдено
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <returns></returns>
        public BL.Building GetBuildingById(int id)
        {
            return HandleExceptions<BL.Building>(delegate()
            {
                Models.Building building = _dataManager.Building.GetBuildingById(id);

                return (building != null) ? Converter.ConvertToModel(building) : null;
            });
        }

        public BL.Building GetFullBuildingById(int id)
        {
            return HandleExceptions<BL.Building>(delegate()
            {
                _dataManager.Inspection.Options = Models.LoadOptions.IncludeIllustrations;

                Models.Building building = _dataManager.Building.GetBuildingById(id);

                return (building != null) ? Converter.ConvertToModel(building, Models.LoadOptions.IncludeIllustrations) : null;
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildingId"></param>
        /// <returns>Идентификатор или 0, если не нейден.</returns>
        public int GetInspectionIdByBuildingId(int buildingId)
        {
            return HandleExceptions<int>(delegate()
            {
                Models.Building building = _dataManager.Building.GetBuildingById(buildingId);

                return (building != null) ? building.InspectionID : 0;
            });
        }

        /// <summary>
        /// Выполняет изменение здания с указанным идентификатором.
        /// Изменяется _только_ TwelveMonthsIntake.
        /// </summary>
        /// <param name="buildingId">Идентификатор здания</param>
        /// <param name="intake5years">Новые данные по потреблению за последний год (12 месяцев).</param>
        public void UpdateIntakeLastYear(int buildingId, BL.TwelveMonthIntake intakeLastYear)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);
                    bo.TwelveMonth = intakeLastYear;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateBuilding(int buildingId, Action<BL.Building> actionOnBuilding)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    // Вызвать внешнюю функцию для изменения объекта
                    actionOnBuilding(bo);

                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateVentIn(int buildingId, BL.VentilationCombinedExtractInputList ventin)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);
                    bo.VentilationCombinedExtractInputs = ventin;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateVentOut(int buildingId, BL.VentilationExtractList ventout)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);
                    bo.VentilationExtracts = ventout;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateRadiators(int buildingId, BL.RadiatorList radiators)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);
                    bo.Radiators = radiators;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateAdditionalInformation(int buildingId, BL.BuildingAdditionalInformation addInfo)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);
                    bo.AdditionalInformation = addInfo;
                    Converter.UpdateEntity(_dataManager, entity, bo, Models.LoadOptions.IncludeResourcesSupplyTypes | Models.LoadOptions.IncludeCityCoeffs);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateWalling(int buildingId, BL.Walling walling)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    // Здесь необходимо дополнить в XML данные по WallMaterials, т.к. с клиента поступает только UID материала.
                    FillMaterialsInformation(walling);

                    // Так же необходимо удалить пустые строки
                    walling.Walls.RemoveAll(layer => layer.IsEmpty());
                    walling.AtticFloor.RemoveAll(layer => layer.IsEmpty());
                    walling.BasementFloor.RemoveAll(layer => layer.IsEmpty());
                    walling.Doors.RemoveAll(door => door.IsEmpty());
                    walling.Windows.RemoveAll(window => window.IsEmpty());

                    bo.WallingOfBuilding = walling;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateItpEquipment(int buildingId, BL.ItpEquipment ie)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    // Необходимо удалить пустые строки
                    ie.HeatExchangers.RemoveAll(item => item.IsEmpty());
                    ie.Boilers.RemoveAll(item => item.IsEmpty());
                    ie.PumpingEquipment.RemoveAll(item => item.IsEmpty());

                    bo.ItpEquipment = ie;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateSimplePowerQuality(int buildingId, BL.PowerQualityList spq)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    // Здесь необходимо изменить в списко только элементы, относящиеся к типу простого измерения
                    // (PowerQualityType.Simple)

                    // 1. Удалить все старые PowerQualityType.Simple
                    bo.PowerQualitys.Powers.RemoveAll(pq => (pq.QualityType == BL.PowerQualityType.Simple));

                    // 2. Добавить новые
                    bo.PowerQualitys.Powers.AddRange(spq.Powers);

                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateMetersColdWater(int buildingId, BL.MeterLists list)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    FillMetersInformation(list.ColdWater);

                    bo.Meters.ColdWater = list.ColdWater;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }


        public void UpdateMetersElectricEnergy(int buildingId, BL.MeterLists list)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    FillMetersInformation(list.ElectricEnergy);

                    bo.Meters.ElectricEnergy = list.ElectricEnergy;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateMetersGas(int buildingId, BL.MeterLists list)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    FillMetersInformation(list.Gas);

                    bo.Meters.Gas = list.Gas;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateMetersHeating(int buildingId, BL.MeterLists list)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    FillMetersInformation(list.Heating);

                    bo.Meters.Heating = list.Heating;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateMetersHotWater(int buildingId, BL.MeterLists list)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    FillMetersInformation(list.HotWater);

                    bo.Meters.HotWater = list.HotWater;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        public void UpdateEnvironmentQualityList(int buildingId, BL.EnvironmentQualityList list)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    // Дополнить данные в XML по типу помещения, так как с клиента поступают только UID.
                    FillRoomTypeInformation(list);

                    // Необходимо удалить пустые строки
                    list.Environments.RemoveAll(item => item.IsEmpty());

                    bo.EnvironmentQualitys = list;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }


        public void UpdatePointOFWater(int buildingId, BL.PointOfWaterList list)
        {
            HandleExceptions(delegate()
            {
                Models.Building entity = _dataManager.Building.GetBuildingById(buildingId);

                if (entity != null)
                {
                    BL.Building bo = Converter.ConvertToModel(entity);

                    // Дополнить данные в XML по точкам и устройствам, так как с клиента поступают только UID.
                    FillWaterPointInformation(list);

                    // Необходимо удалить пустые строки
                    list.Points.RemoveAll(point => point.IsEmpty());

                    bo.PointOfWaters = list;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }
            });
        }

        //public void DeleteIllustration(int illustrationId)
        //{
        //}


        /// <summary>
        /// Заполняет в бизнес-объекте информацию о материалах. Информацию берет в таблице-справочнике.
        /// Guid материала должен быть известен.
        /// </summary>
        /// <param name="walling"></param>
        private void FillMaterialsInformation(BL.Walling walling)
        {
            foreach (var layer in walling.Walls)
            {
                FillWallMaterialInformation(layer);
            }

            foreach (var layer in walling.AtticFloor)
            {
                FillAtticFloorMaterialInformation(layer);
            }

            foreach (var layer in walling.BasementFloor)
            {
                FillBasementFloorMaterialInformation(layer);
            }

            foreach (var door in walling.Doors)
            {
                FillDoorMaterialInformation(door);
            }

            foreach (var window in walling.Windows)
            {
                FillWindowMaterialInformation(window);
            }
        }

        /// <summary>
        /// Заполняет в бизнес-объекте информацию о материале. Информацию берет в таблице-справочнике по Стенам.
        /// Guid материала должен быть известен.
        /// </summary>
        /// <param name="wallLayer"></param>
        private void FillWallMaterialInformation(BL.WallLayer wallLayer)
        {
            if(wallLayer.Material != null && wallLayer.Material.WallMaterialUID != Guid.Empty)
            {
                Models.WallMaterial material = _dataManager.Directory.GetWallMaterialByUid(wallLayer.Material.WallMaterialUID);

                if (material != null)
                {
                    wallLayer.Material.Caption = material.Caption;
                    wallLayer.Material.ThermalResistant = material.ThermalResistance;
                }
            }
        }

        /// <summary>
        /// Заполняет в бизнес-объекте информацию о материале. Информацию берет в таблице-справочнике по Чердачным 
        /// перекрытиям.
        /// Guid материала должен быть известен.
        /// </summary>
        /// <param name="wallLayer"></param>
        private void FillAtticFloorMaterialInformation(BL.WallLayer wallLayer)
        {
            if (wallLayer.Material != null && wallLayer.Material.WallMaterialUID != Guid.Empty)
            {
                Models.AtticFloorMaterial material = _dataManager.Directory.GetAtticFloorMaterialByUid(wallLayer.Material.WallMaterialUID);

                if (material != null)
                {
                    wallLayer.Material.Caption = material.Caption;
                    wallLayer.Material.ThermalResistant = material.ThermalResistance;
                }
            }
        }

        /// <summary>
        /// Заполняет в бизнес-объекте информацию о материале. Информацию берет в таблице-справочнике по Подвальным 
        /// перекрытиям.
        /// Guid материала должен быть известен.
        /// </summary>
        /// <param name="wallLayer"></param>
        private void FillBasementFloorMaterialInformation(BL.WallLayer wallLayer)
        {
            if (wallLayer.Material != null && wallLayer.Material.WallMaterialUID != Guid.Empty)
            {
                Models.BasementFloorMaterial material = _dataManager.Directory.GetBasementFloorMaterialByUid(wallLayer.Material.WallMaterialUID);

                if (material != null)
                {
                    wallLayer.Material.Caption = material.Caption;
                    wallLayer.Material.ThermalResistant = material.ThermalResistance;
                }
            }
        }


        /// <summary>
        /// Заполняет в бизнес-объекте информацию о материале. Информацию берет в таблице-справочнике по Типам дверей.
        /// Guid материала должен быть известен.
        /// </summary>
        /// <param name="door"></param>
        private void FillDoorMaterialInformation(BL.Door door)
        {
            if (door.DoorTypeUID != Guid.Empty)
            {
                Models.DoorType doorType = _dataManager.Directory.GetDoorTypeByUid(door.DoorTypeUID);

                if (doorType != null)
                {
                    door.DoorTypeCaption = doorType.Caption;
                    door.ThermalResistance = doorType.ThermalResistance;
                }
            }
        }


        /// <summary>
        /// Заполняет в бизнес-объекте информацию о материале. Информацию берет в таблице-справочнике по Типам окон.
        /// Guid материала должен быть известен.
        /// </summary>
        /// <param name="window"></param>
        private void FillWindowMaterialInformation(BL.Window window)
        {
            if (window.WindowTypeUID != Guid.Empty)
            {
                Models.WindowType windowType = _dataManager.Directory.GetWindowTypeByUid(window.WindowTypeUID);

                if (windowType != null)
                {
                    window.WindowTypeCaption = windowType.Caption;
                    window.ThermalResistance = windowType.ThermalResistance;
                }
            }
        }

        /// <summary>
        /// Заполняет в бизнес-объекте информацию о типе точки водоразбора. Информацию получает из таблицы-справочника.
        /// Guid типа должен быть известен.
        /// </summary>
        /// <param name="list"></param>
        private void FillWaterPointInformation(BL.PointOfWaterList list)
        {
            foreach (BL.PointOfWater item in list.Points)
            {
                FillWaterPointInformation(item);
            }
        }

        /// <summary>
        /// Заполняет в бизнес-объекте информацию о типе точки водоразбора. Информацию получает из таблицы-справочника.
        /// Guid типа должен быть известен.
        /// </summary>
        /// <param name="item"></param>
        private void FillWaterPointInformation(BL.PointOfWater item)
        {
            if (item.PointOfWaterUID != Guid.Empty)
            {
                Models.PointOfWaterType pow = _dataManager.Directory.GetPointOfWaterTypeByUid(item.PointOfWaterUID);

                if (pow != null)
                {
                    item.PointOfWaterCaption = pow.Caption;
                }
            }

            if (item.PointOfWaterDeviceTypeUID != Guid.Empty)
            {
                Models.PointOfWaterDeviceType dev = _dataManager.Directory.GetPointOfWaterDeviceTypeByUid(item.PointOfWaterDeviceTypeUID);
                if (dev != null)
                {
                    item.PointOfWaterDeviceTypeCaption = dev.Caption;
                }
            }
        }

        /// <summary>
        /// Заполняет в бизнес-объекте информацию о типе помещения. Информацию получает из таблицы-справочника.
        /// Guid типа должен быть известен.
        /// </summary>
        /// <param name="item"></param>
        private void FillRoomTypeInformation(BL.EnvironmentQualityList list)
        {
            foreach (BL.EnvironmentQuality item in list.Environments)
            {
                FillRoomTypeInformation(item);
            }
        }

        /// <summary>
        /// Заполняет в бизнес-объекте информацию о типе помещения. Информацию получает из таблицы-справочника.
        /// Guid типа должен быть известен.
        /// </summary>
        /// <param name="item"></param>
        private void FillRoomTypeInformation(BL.EnvironmentQuality item)
        {
            if (item.RoomTypeUID != Guid.Empty)
            {
                Models.RoomType rt = _dataManager.Directory.GetRoomTypeByUid(item.RoomTypeUID);

                if (rt != null)
                {
                    item.RoomTypeCaption = rt.Caption;
                }
            }
        }

        /// <summary>
        /// Заполняет GUID у элементов. Ищет его по заголовку.
        /// </summary>
        /// <param name="list"></param>
        private void FillMetersInformation(List<BL.MeterElectricEnergy> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                BL.Meter meter = list[i];

                meter.InstallationSiteUID = GetMeterCaptionUid(meter.InstallationSite, Models.CaptionsTypeID.MeterPlacesElectricEnergy);
                meter.CaptionUID = GetMeterCaptionUid(meter.Caption, Models.CaptionsTypeID.MeterCaptionsElectricEnergy);
                meter.AccuracyClassUID = GetMeterCaptionUid(meter.AccuracyClass, Models.CaptionsTypeID.MeterClassesElectricEnergy);
            }
        }

        /// <summary>
        /// Заполняет GUID у элементов. Ищет его по заголовку.
        /// </summary>
        /// <param name="list"></param>
        private void FillMetersInformation(List<BL.MeterColdWater> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                BL.Meter meter = list[i];

                meter.InstallationSiteUID = GetMeterCaptionUid(meter.InstallationSite, Models.CaptionsTypeID.MeterPlacesColdWater);
                meter.CaptionUID = GetMeterCaptionUid(meter.Caption, Models.CaptionsTypeID.MeterCaptionsColdWater);
                meter.AccuracyClassUID = GetMeterCaptionUid(meter.AccuracyClass, Models.CaptionsTypeID.MeterClassesColdWater);
            }
        }

        /// <summary>
        /// Заполняет GUID у элементов. Ищет его по заголовку.
        /// </summary>
        /// <param name="list"></param>
        private void FillMetersInformation(List<BL.MeterHotWater> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                BL.Meter meter = list[i];

                meter.InstallationSiteUID = GetMeterCaptionUid(meter.InstallationSite, Models.CaptionsTypeID.MeterPlacesHotWater);
                meter.CaptionUID = GetMeterCaptionUid(meter.Caption, Models.CaptionsTypeID.MeterCaptionsHotWater);
                meter.AccuracyClassUID = GetMeterCaptionUid(meter.AccuracyClass, Models.CaptionsTypeID.MeterClassesHotWater);
            }
        }

        /// <summary>
        /// Заполняет GUID у элементов. Ищет его по заголовку.
        /// </summary>
        /// <param name="list"></param>
        private void FillMetersInformation(List<BL.MeterHeating> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                BL.Meter meter = list[i];

                meter.InstallationSiteUID = GetMeterCaptionUid(meter.InstallationSite, Models.CaptionsTypeID.MeterPlacesHeating);
                meter.CaptionUID = GetMeterCaptionUid(meter.Caption, Models.CaptionsTypeID.MeterCaptionsHeating);
                meter.AccuracyClassUID = GetMeterCaptionUid(meter.AccuracyClass, Models.CaptionsTypeID.MeterClassesHeating);
            }
        }

        /// <summary>
        /// Заполняет GUID у элементов. Ищет его по заголовку.
        /// </summary>
        /// <param name="list"></param>
        private void FillMetersInformation(List<BL.MeterGas> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                BL.Meter meter = list[i];

                meter.InstallationSiteUID = GetMeterCaptionUid(meter.InstallationSite, Models.CaptionsTypeID.MeterPlacesGas);
                meter.CaptionUID = GetMeterCaptionUid(meter.Caption, Models.CaptionsTypeID.MeterCaptionsGas);
                meter.AccuracyClassUID = GetMeterCaptionUid(meter.AccuracyClass, Models.CaptionsTypeID.MeterClassesGas);
            }
        }

        /// <summary>
        /// По тексту заголовка и типу находит его Guid. Либо добавляет новый, если не находит.
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="type"></param>
        /// <returns>UID добавленного или найденго заголовка</returns>
        private Guid GetMeterCaptionUid(string caption, Models.CaptionsTypeID type)
        {
            if (!String.IsNullOrEmpty(caption))
            {
                Models.CaptionsRegister cap = _dataManager.Directory.GetExistingOrAddCaption(caption, type);
                if (cap != null)
                {
                    return cap.CaptionsRegisterUID;
                }
            }
            return Guid.Empty;
        }
    }
}