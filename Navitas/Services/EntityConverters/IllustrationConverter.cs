﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Report;

namespace Navitas.Services.EntityConverters
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class IllustrationConverter : EntityConverter<BL.Illustration, Models.Illustration>, IEntityConverter<BL.Illustration, Models.Illustration>
    {
        public override void UpdateEntity(
            Models.DataManager dm,
            Models.Illustration oldEntity,
            BL.Illustration model,
            dynamic parameters)
        {
            oldEntity.IllustrationID = model.IllustrationID; // ??
            oldEntity.InspectionID = model.InspectionID;
            oldEntity.BuildingID = model.BuildingID;
            oldEntity.DivisionName = model.DivisionName;
            oldEntity.UserTitle = model.UserTitle;
            oldEntity.FileName = model.FileName;
        }

        public override BL.Illustration ConvertToModel(Models.DataManager dm, Models.Illustration entity, dynamic parameters)
        {
            BL.Illustration bo = new BL.Illustration();

            bo.IllustrationID = entity.IllustrationID;
            bo.InspectionID = entity.InspectionID;
            bo.BuildingID = entity.BuildingID;
            bo.DivisionName = entity.DivisionName;
            bo.UserTitle = entity.UserTitle;
            bo.FileName = entity.FileName;

            return bo;
        }
    }
}