﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data.Linq;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Navitas.Services.EntityConverters
{
    public static class ConverterHelper
    {
        /// <summary>
        /// Сериализует объект для записи в базу.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static XElement Serialize<T>(T obj)
        {
            StringWriter writer = null;
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));

                XElement xmlClient = new XElement(typeof(T).ToString());

                writer = new StringWriter();
                xs.Serialize(writer, obj);
                return System.Xml.Linq.XElement.Parse(writer.ToString());
            }
            catch (Exception ex)
            {
                throw new Services.Exceptions.ReadWriteException("Ошибка сериализации.", ex);
            }
            finally
            {
                if (writer != null)
                {
                    writer.Dispose();
                }
            }
        }

        /// <summary>
        /// Десериализует объект при чтении из базы.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T Deserialize<T>(XElement xml)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                return (T)xs.Deserialize(xml.CreateReader());
            }
            catch (Exception ex)
            {
                throw new Services.Exceptions.ReadWriteException("Ошибка десериализации.", ex);
            }
        }
    }
}