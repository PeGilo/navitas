﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    public abstract class EntityConverter<TModel, TEntity> : IEntityConverter<TModel, TEntity>
        where TModel : class
        where TEntity : class
    {
        public abstract void UpdateEntity(Models.DataManager dm, TEntity oldEntity, TModel model, dynamic parameters);
        public virtual void UpdateEntity(Models.DataManager dm, TEntity oldEntity, TModel model)
        {
            UpdateEntity(dm, oldEntity, model, new { });
        }

        public virtual void UpdateEntity(TEntity oldEntity, TModel model, dynamic parameters)
        {
            UpdateEntity(null, oldEntity, model, parameters);
        }

        public virtual void UpdateEntity(TEntity oldEntity, TModel model)
        {
            UpdateEntity(null, oldEntity, model, new { });
        }

        public abstract TModel ConvertToModel(Models.DataManager dm, TEntity entity, dynamic parameters);

        public virtual TModel ConvertToModel(Models.DataManager dm, TEntity entity)
        {
            return ConvertToModel(dm, entity, new { });
        }

        public virtual TModel ConvertToModel(TEntity entity, dynamic parameters)
        {
            return ConvertToModel(null, entity, parameters);
        }

        public virtual TModel ConvertToModel(TEntity entity)
        {
            return ConvertToModel(null, entity, new { });
        }

        #region IEntityConverter Members

        public Type GetModelType()
        {
            return typeof(TModel);
        }

        public Type GetEntityType()
        {
            return typeof(TEntity);
        }

        #endregion

        //protected ParameterValue<T> GetParameterValue<T>(dynamic parameters, string parameterName)
        //{
        //    PropertyInfo property = parameters.GetType().GetProperty(parameterName, BindingFlags.Public | BindingFlags.Instance);
        //    var result = new ParameterValue<T>();
        //    result.HasValue = property != null;

        //    if (result.HasValue)
        //    {
        //        result.Value = (T)property.GetValue(parameters, null);
        //    }
        //    return result;
        //}

        //protected class ParameterValue<T>
        //{
        //    public T Value { get; set; }
        //    public bool HasValue { get; set; }
        //}
    }

}