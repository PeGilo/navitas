﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class CityCoeffsConverter : EntityConverter<BL.CityCoeffs, Models.CityCoeff>, IEntityConverter<BL.CityCoeffs, Models.CityCoeff>
    {
        public override void UpdateEntity(
            Models.DataManager dm,
            Models.CityCoeff oldEntity,
            BL.CityCoeffs model,
            dynamic parameters)
        {
            throw new InvalidOperationException();
        }

        public override BL.CityCoeffs ConvertToModel(Models.DataManager dm, Models.CityCoeff entity, dynamic parameters)
        {
            if (entity == null)
            {
                return null;
            }
            else
            {
                return new BL.CityCoeffs()
                {
                    CityCoeffsId = entity.CityCoeffsID,
                    CityName = entity.CityName,
                    Values = ConverterHelper.Deserialize<BL.CityCoeffsValues>(entity.Coeffs)
                };
            }
        }
    }
}