﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class InspectionReportListConverter : EntityConverter<Report.InspectionReport, Models.InspectionReportList>, IEntityConverter<Report.InspectionReport, Models.InspectionReportList>
    {
        private IEntityConverter<Report.Section, Models.InspectionReport> _sectionConverter = null;

        private IEntityConverter<Report.Section, Models.InspectionReport> SectionConverter
        {
            get
            {
                if (_sectionConverter == null)
                {
                    _sectionConverter = EntityConverterFactory.Instance.Create<Report.Section, Models.InspectionReport>();
                }
                return _sectionConverter;
            }
        }

        public override void UpdateEntity(
            Models.DataManager dm,
            Models.InspectionReportList oldEntity,
            Report.InspectionReport model,
            dynamic parameters)
        {
            throw new NotImplementedException("Этот объект так не обновляется. Изменяется каждая его часть в отдельности.");
        }

        public override Report.InspectionReport ConvertToModel(Models.DataManager dm, Models.InspectionReportList entity, dynamic parameters)
        {
            Report.InspectionReport report = new Report.InspectionReport();

            // Каждый раздель конвертировать отдельно
            for (int i = 0; i < entity.Items.Count; i++)
            {
                Report.Section section = SectionConverter.ConvertToModel(entity.Items[i]);

                report.Sections.Add(section);
            }

            // Отсортировать список по порядковому номеру
            report.Sections.Sort(new Comparison<Report.Section>((sec1, sec2) => sec1.SequenceNumber - sec2.SequenceNumber));

            return report;
        }
    }
}