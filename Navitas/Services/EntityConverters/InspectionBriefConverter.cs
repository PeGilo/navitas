﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class InspectionBriefConverter : EntityConverter<BL.InspectionBrief, Models.Inspection>, IEntityConverter<BL.InspectionBrief, Models.Inspection>
    {
        /// <summary>
        /// Изменяет указанный объект базы, используя значения из объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="businessObject"></param>
        public override void UpdateEntity(
            Models.DataManager dm,
            Models.Inspection oldEntity,
            BL.InspectionBrief model,
            dynamic parameters)
        {
            throw new InvalidOperationException("Для изменения Models.Inspection необходимо использовать BL.Inspection");
        }

        /// <summary>
        /// Конвертирует объект модели данных в объек бизнес-логики.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override BL.InspectionBrief ConvertToModel(Models.DataManager dm, Models.Inspection entity, dynamic parameters)
        {
            BL.InspectionBrief bo = new BL.InspectionBrief();

            bo.InspectionID = entity.InspectionID;
            bo.ClientCaption = entity.ClientCaption;
            bo.AuditorFullName = entity.Auditor.FullName;

            return bo;
        }
    }
}