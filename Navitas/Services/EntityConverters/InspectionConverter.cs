﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class InspectionConverter : EntityConverter<BL.Inspection, Models.Inspection>, IEntityConverter<BL.Inspection, Models.Inspection>
    {
        private IEntityConverter<BL.Building, Models.Building> _buildingConverter = null;

        private IEntityConverter<BL.Building, Models.Building> BuildingConverter
        {
            get
            {
                if (_buildingConverter == null)
                {
                    _buildingConverter = EntityConverterFactory.Instance.Create<BL.Building, Models.Building>();
                }
                return _buildingConverter;
            }
        }

        /// <summary>
        /// Изменяет указанный объект базы, используя значения из объект бизнес-логики
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="businessObject"></param>
        public override void UpdateEntity(
            Models.DataManager dm,
            Models.Inspection oldEntity,
            BL.Inspection model,
            dynamic parameters)
        {
            oldEntity.InspectionID = model.InspectionID;
            //oldEntity.ClientCaption = model.FullName;
            // При сохранение для ClientCaption использовать поле Client.Name (краткое наименование)
            oldEntity.ClientCaption = !String.IsNullOrEmpty(model.Client.Name) ? model.Client.Name : String.Empty;
            oldEntity.BaseYear = model.BaseYear;
            oldEntity.AuditorID = model.AuditorID;
            oldEntity.Client = ConverterHelper.Serialize<BL.Client>(model.Client);
            oldEntity.Production = ConverterHelper.Serialize<BL.Production>(model.Production);
            oldEntity.Staff = ConverterHelper.Serialize<BL.Staff>(model.Staff);
            oldEntity.Intake = ConverterHelper.Serialize<BL.Intake>(model.Intake);
            oldEntity.Transport = ConverterHelper.Serialize<BL.VehicleList>(model.Vehicle);
            oldEntity.CalculationCoeffs = ConverterHelper.Serialize<BL.CalculationCoeffsOfInspection>(model.CalculationCoeffs);

            if (parameters is Models.LoadOptions)
            {
                Models.LoadOptions options = (Models.LoadOptions)parameters;

                // Если необходимо обновить также список зданий, то проверить какие изменения были внесены в список
                if ((options & Models.LoadOptions.IncludeBuildings) != 0)
                {
                    // Проверить есть ли отредактированные здания или удаленные
                    foreach (var persistedBuilding in oldEntity.Building)
                    {
                        BL.Building updatedBuilding =
                            model.Buildings.Where(b => b.BuildingID == persistedBuilding.BuildingID).FirstOrDefault();

                        // check if it is edited or deleted
                        if (updatedBuilding != null)
                        {
                            // Общий Update затирает информацию пустыми значениями:
                            //BuildingConverter.UpdateEntity(persistedBuilding, updatedBuilding);
                            // Необходимо поменять только общую информацию, чтобы другие не затерлись.
                            persistedBuilding.GeneralInformation = ConverterHelper.Serialize<BL.BuildingGeneralInformation>(updatedBuilding.GeneralInformation);
                        }
                        else
                        {
                            // Building was deleted
                            dm.Building.Remove(persistedBuilding);
                        }
                    }

                    // Проверить есть ли добавленные здания
                    foreach (var addedBuilding in model.Buildings.Where(b => b.BuildingID == -1))
                    {
                        Models.Building newBuilding = new Models.Building();
                        BuildingConverter.UpdateEntity(newBuilding, addedBuilding);

                        oldEntity.Building.Add(newBuilding);
                    }
                }
            }
        }

        /// <summary>
        /// Конвертирует объект модели данных в объек бизнес-логики.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override BL.Inspection ConvertToModel(Models.DataManager dm, Models.Inspection entity, dynamic parameters)
        {
            BL.Inspection bo = new BL.Inspection();

            bo.InspectionID = entity.InspectionID;
            bo.FullName = entity.ClientCaption;
            bo.BaseYear = entity.BaseYear;
            bo.AuditorID = entity.AuditorID;
            bo.Client = ConverterHelper.Deserialize<BL.Client>(entity.Client);
            bo.Production = ConverterHelper.Deserialize<BL.Production>(entity.Production);
            bo.Staff = ConverterHelper.Deserialize<BL.Staff>(entity.Staff);
            bo.Intake = ConverterHelper.Deserialize<BL.Intake>(entity.Intake);
            bo.Vehicle = ConverterHelper.Deserialize<BL.VehicleList>(entity.Transport);
            bo.CalculationCoeffs =
                entity.CalculationCoeffs != null ? ConverterHelper.Deserialize<BL.CalculationCoeffsOfInspection>(entity.CalculationCoeffs) : new BL.CalculationCoeffsOfInspection();

            if (parameters is Models.LoadOptions)
            {
                Models.LoadOptions options = (Models.LoadOptions)parameters;

                // Если необходимо включить здания, то конвертировать список зданий
                if ((options & Models.LoadOptions.IncludeBuildings) != 0)
                {
                    IList<BL.Building> buildings = new List<BL.Building>();

                    foreach (var buildingEntity in entity.Building)
                    {
                        buildings.Add(BuildingConverter.ConvertToModel(buildingEntity, options));
                    }

                    bo.Buildings = buildings;
                }
            }

            return bo;
        }
    }
}