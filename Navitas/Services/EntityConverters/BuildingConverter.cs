﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class BuildingConverter : EntityConverter<BL.Building, Models.Building>, IEntityConverter<BL.Building, Models.Building>
    {
        private IEntityConverter<BL.ResourceSupplyType, Models.ResourceSupplyType> _resourceSupplyTypeConverter = null;

        private IEntityConverter<BL.ResourceSupplyType, Models.ResourceSupplyType> ResourceSupplyTypeConverter
        {
            get
            {
                if (_resourceSupplyTypeConverter == null)
                {
                    _resourceSupplyTypeConverter = EntityConverterFactory.Instance.Create<BL.ResourceSupplyType, Models.ResourceSupplyType>();
                }
                return _resourceSupplyTypeConverter;
            }
        }

        private IEntityConverter<BL.Illustration, Models.Illustration> _illustrationConverter = null;

        private IEntityConverter<BL.Illustration, Models.Illustration> IllustrationConverter
        {
            get
            {
                if (_illustrationConverter == null)
                {
                    _illustrationConverter = EntityConverterFactory.Instance.Create<BL.Illustration, Models.Illustration>();
                }
                return _illustrationConverter;
            }
        }

        private IEntityConverter<BL.CityCoeffs, Models.CityCoeff> _cityCoeffsConverter = null;

        private IEntityConverter<BL.CityCoeffs, Models.CityCoeff> CityCoeffsConverter
        {
            get
            {
                if (_cityCoeffsConverter == null)
                {
                    _cityCoeffsConverter = EntityConverterFactory.Instance.Create<BL.CityCoeffs, Models.CityCoeff>();
                }
                return _cityCoeffsConverter;
            }
        }

        public override void UpdateEntity(
            Models.DataManager dm,
            Models.Building oldEntity,
            BL.Building model,
            dynamic parameters)
        {
            oldEntity.BuildingID = model.BuildingID;
            oldEntity.BuildingGUID = model.BuildingGUID;

            oldEntity.GeneralInformation = ConverterHelper.Serialize<BL.BuildingGeneralInformation>(model.GeneralInformation);
            oldEntity.AdditionalInformation = ConverterHelper.Serialize<BL.BuildingAdditionalInformation>(model.AdditionalInformation);
            oldEntity.TwelveMonth = ConverterHelper.Serialize<BL.TwelveMonthIntake>(model.TwelveMonth);

            oldEntity.HouseholdEquipment = ConverterHelper.Serialize<BL.ElectroloadList>(model.HouseholdEquipment);
            oldEntity.ProcessEquipment = ConverterHelper.Serialize<BL.ElectroloadList>(model.ProcessEquipment);
            oldEntity.OfficeEquipment = ConverterHelper.Serialize<BL.ElectroloadList>(model.OfficeEquipment);
            oldEntity.HeatingEquipment = ConverterHelper.Serialize<BL.ElectroloadList>(model.HeatingEquipment);
            oldEntity.InternalLightingEquipment = ConverterHelper.Serialize<BL.ElectroloadList>(model.InternalLightingEquipment);
            oldEntity.ExternalLightingEquipment = ConverterHelper.Serialize<BL.ElectroloadList>(model.ExternalLightingEquipment);

            oldEntity.VentilationCombinedExtractInputs = ConverterHelper.Serialize<BL.VentilationCombinedExtractInputList>(model.VentilationCombinedExtractInputs);
            oldEntity.VentilationExtractList = ConverterHelper.Serialize<BL.VentilationExtractList>(model.VentilationExtracts);
            oldEntity.Radiators = ConverterHelper.Serialize<BL.RadiatorList>(model.Radiators);

            oldEntity.Walling = ConverterHelper.Serialize<BL.Walling>(model.WallingOfBuilding);
            oldEntity.PointOfWaters = ConverterHelper.Serialize<BL.PointOfWaterList>(model.PointOfWaters);
            oldEntity.EnvironmentQualitys = ConverterHelper.Serialize<BL.EnvironmentQualityList>(model.EnvironmentQualitys);
            oldEntity.PowerQualitys = ConverterHelper.Serialize<BL.PowerQualityList>(model.PowerQualitys);

            oldEntity.Meters = ConverterHelper.Serialize<BL.MeterLists>(model.Meters);
            oldEntity.ItpEquipment = ConverterHelper.Serialize<BL.ItpEquipment>(model.ItpEquipment);

            if (parameters is Models.LoadOptions)
            {
                Models.LoadOptions options = (Models.LoadOptions)parameters;

                // Если необходимо перезаписывать ресурсы
                if ((options & Models.LoadOptions.IncludeResourcesSupplyTypes) != 0)
                {
                    // Для каждого из видов ресурсов делаем выборку из справочника по ID, т.к. установить
                    // ID напрямую не можем.
                    BL.ResourceSupply resources = model.AdditionalInformation.Resources;

                    oldEntity.ElectricityResourceSupplyType = resources.ElectricityResource != null ?
                        dm.Directory.GetResourceSupplyTypeById(resources.ElectricityResource.ResourceSupplyTypeID)
                        : null;
                    oldEntity.HeatResourceSupplyType = resources.HeatResource != null ?
                        dm.Directory.GetResourceSupplyTypeById(resources.HeatResource.ResourceSupplyTypeID)
                        : null;
                    oldEntity.WaterResourceSupplyType = resources.WaterResource != null ?
                        dm.Directory.GetResourceSupplyTypeById(resources.WaterResource.ResourceSupplyTypeID)
                        : null;
                    oldEntity.SewerageResourceSupplyType = resources.SewerageResource != null ?
                        dm.Directory.GetResourceSupplyTypeById(resources.SewerageResource.ResourceSupplyTypeID)
                        : null;
                    oldEntity.GasResourceSupplyType = resources.GasResource != null ?
                        dm.Directory.GetResourceSupplyTypeById(resources.GasResource.ResourceSupplyTypeID)
                        : null;
                }

                // Если необходимо перезаписывать коэф-ты
                if ((options & Models.LoadOptions.IncludeCityCoeffs) != 0)
                {
                    // Делаем выборку из справочника по ID, т.к. установить ID напрямую не можем.
                    oldEntity.CityCoeff = model.AdditionalInformation.CityCoeffs != null ?
                        dm.Directory.GetCityCoeffsById(model.AdditionalInformation.CityCoeffs.CityCoeffsId)
                        : null;
                }
            }
        }

        public override BL.Building ConvertToModel(Models.DataManager dm, Models.Building entity, dynamic parameters)
        {
            BL.Building bo = new BL.Building();

            bo.BuildingID = entity.BuildingID;
            bo.BuildingGUID = entity.BuildingGUID;

            bo.GeneralInformation = ConverterHelper.Deserialize<BL.BuildingGeneralInformation>(entity.GeneralInformation);
            bo.AdditionalInformation = ConverterHelper.Deserialize<BL.BuildingAdditionalInformation>(entity.AdditionalInformation);
            bo.TwelveMonth = ConverterHelper.Deserialize<BL.TwelveMonthIntake>(entity.TwelveMonth);

            bo.HouseholdEquipment = ConverterHelper.Deserialize<BL.ElectroloadList>(entity.HouseholdEquipment);
            bo.ProcessEquipment = ConverterHelper.Deserialize<BL.ElectroloadList>(entity.ProcessEquipment);
            bo.OfficeEquipment = ConverterHelper.Deserialize<BL.ElectroloadList>(entity.OfficeEquipment);
            bo.HeatingEquipment = ConverterHelper.Deserialize<BL.ElectroloadList>(entity.HeatingEquipment);
            bo.InternalLightingEquipment = ConverterHelper.Deserialize<BL.ElectroloadList>(entity.InternalLightingEquipment);
            bo.ExternalLightingEquipment = ConverterHelper.Deserialize<BL.ElectroloadList>(entity.ExternalLightingEquipment);

            bo.VentilationCombinedExtractInputs = ConverterHelper.Deserialize<BL.VentilationCombinedExtractInputList>(entity.VentilationCombinedExtractInputs);
            bo.VentilationExtracts = ConverterHelper.Deserialize<BL.VentilationExtractList>(entity.VentilationExtractList);
            bo.Radiators = ConverterHelper.Deserialize<BL.RadiatorList>(entity.Radiators);

            bo.AdditionalInformation.Resources.ElectricityResource = ResourceSupplyTypeConverter.ConvertToModel(entity.ElectricityResourceSupplyType);
            bo.AdditionalInformation.Resources.HeatResource = ResourceSupplyTypeConverter.ConvertToModel(entity.HeatResourceSupplyType);
            bo.AdditionalInformation.Resources.WaterResource = ResourceSupplyTypeConverter.ConvertToModel(entity.WaterResourceSupplyType);
            bo.AdditionalInformation.Resources.SewerageResource = ResourceSupplyTypeConverter.ConvertToModel(entity.SewerageResourceSupplyType);
            bo.AdditionalInformation.Resources.GasResource = ResourceSupplyTypeConverter.ConvertToModel(entity.GasResourceSupplyType);

            bo.AdditionalInformation.CityCoeffs = CityCoeffsConverter.ConvertToModel(entity.CityCoeff);

            bo.WallingOfBuilding = ConverterHelper.Deserialize<BL.Walling>(entity.Walling);
            bo.PointOfWaters = ConverterHelper.Deserialize<BL.PointOfWaterList>(entity.PointOfWaters);
            bo.EnvironmentQualitys = ConverterHelper.Deserialize<BL.EnvironmentQualityList>(entity.EnvironmentQualitys);
            bo.PowerQualitys = ConverterHelper.Deserialize<BL.PowerQualityList>(entity.PowerQualitys);

            bo.Meters = ConverterHelper.Deserialize<BL.MeterLists>(entity.Meters);
            if (entity.ItpEquipment != null)
            {
                bo.ItpEquipment = ConverterHelper.Deserialize<BL.ItpEquipment>(entity.ItpEquipment);
            }
            else
            {
                bo.ItpEquipment = new BL.ItpEquipment();
            }

            if (parameters is Models.LoadOptions)
            {
                Models.LoadOptions options = (Models.LoadOptions)parameters;

                // Если необходимо считать список иллюстраций
                if ((options & Models.LoadOptions.IncludeIllustrations) != 0)
                {
                    IList<BL.Illustration> illustrations = new List<BL.Illustration>();

                    foreach (var ill in entity.Illustration)
                    {
                        illustrations.Add(IllustrationConverter.ConvertToModel(ill));
                    }

                    bo.Illustrations = illustrations;
                }
            }
            
            return bo;
        }
    }
}