﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class ResourceSupplyTypeConverter : EntityConverter<BL.ResourceSupplyType, Models.ResourceSupplyType>, IEntityConverter<BL.ResourceSupplyType, Models.ResourceSupplyType>
    {
        public override void UpdateEntity(
            Models.DataManager dm,
            Models.ResourceSupplyType oldEntity,
            BL.ResourceSupplyType model,
            dynamic parameters)
        {
            throw new InvalidOperationException();
        }

        public override BL.ResourceSupplyType ConvertToModel(Models.DataManager dm, Models.ResourceSupplyType entity, dynamic parameters)
        {
            if (entity == null)
            {
                return null;
            }
            else
            {
                return new BL.ResourceSupplyType(entity.ResourceSupplyTypeID, entity.Name);
            }
        }
    }
}