﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Services.EntityConverters
{
    public interface IEntityConverter
    {
        Type GetModelType();
        Type GetEntityType();
    }

    public interface IEntityConverter<TModel, TEntity> : IEntityConverter
        where TModel : class
        where TEntity : class
    {
        void UpdateEntity(TEntity oldEntity, TModel model);
        void UpdateEntity(TEntity oldEntity, TModel model, dynamic parameters);
        void UpdateEntity(Models.DataManager dm, TEntity oldEntity, TModel model);
        void UpdateEntity(Models.DataManager dm, TEntity oldEntity, TModel model, dynamic parameters);

        TModel ConvertToModel(Models.DataManager dm, TEntity entity, dynamic parameters);
        TModel ConvertToModel(Models.DataManager dm, TEntity entity);
        TModel ConvertToModel(TEntity entity, dynamic parameters);
        TModel ConvertToModel(TEntity entity);
    }
}