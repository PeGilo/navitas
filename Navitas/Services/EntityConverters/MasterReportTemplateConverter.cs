﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure;

namespace Navitas.Services.EntityConverters
{
    public class MasterReportTemplateConverter : EntityConverter<Report.MasterReportTemplate, Models.MasterReportTemplate>, IEntityConverter<Report.MasterReportTemplate, Models.MasterReportTemplate>
    {
        public override void UpdateEntity(
            Models.DataManager dm,
            Models.MasterReportTemplate oldEntity,
            Report.MasterReportTemplate model,
            dynamic parameters)
        {
            throw new NotImplementedException("Этот объект не предназначен для изменения записи из базы данных.");
        }

        public override Report.MasterReportTemplate ConvertToModel(Models.DataManager dm, Models.MasterReportTemplate entity, dynamic parameters)
        {
            Report.MasterReportTemplate section = ConverterHelper.Deserialize<Report.MasterReportTemplate>(entity.Content);

            section.MasterReportTemplateId = entity.MasterReportTemplateID;
            section.SectionId = entity.SectionID;
            section.SequenceNumber = entity.SequenceNumber;
            section.RepeatOn = entity.RepeatOn;
            section.Content = entity.Content;//.ToString();

            return section;
        }
    }
}