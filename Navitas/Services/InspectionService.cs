﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Services.Exceptions;
using Navitas.Services.EntityConverters;


namespace Navitas.Services
{
    using BL = Soft33.EnergyPassport.V2.BL;

    /// <summary>
    /// Содержит методы для работы с объектами "Обследования".
    /// Бизнес-логика.
    /// </summary>
    public class InspectionService : ServiceBase
    {
        private Models.DataManager _dataManager;
        private IEntityConverter<BL.Inspection, Models.Inspection> _converter = null;

        private IEntityConverter<BL.Inspection, Models.Inspection> Converter
        {
            get
            {
                if (_converter == null)
                {
                    _converter = EntityConverterFactory.Instance.Create<BL.Inspection, Models.Inspection>();
                }
                return _converter;
            }
        }

        private IEntityConverter<BL.InspectionBrief, Models.Inspection> _briefConverter = null;

        private IEntityConverter<BL.InspectionBrief, Models.Inspection> BriefConverter
        {
            get
            {
                if (_briefConverter == null)
                {
                    _briefConverter = EntityConverterFactory.Instance.Create<BL.InspectionBrief, Models.Inspection>();
                }
                return _briefConverter;
            }
        }

        public InspectionService(Models.DataManager dataManager)
            : base(dataManager)
        {
            _dataManager = dataManager;
        }

        /// <summary>
        /// Создает объект нового "Обследования"
        /// </summary>
        /// <returns></returns>
        public BL.Inspection CreateInspection()
        {
            BL.Inspection newInspection = new BL.Inspection();

            return newInspection;
        }

        /// <summary>
        /// Используется для проверки возможности парсинга HTML-разметки Razor'ом.
        /// </summary>
        /// <returns></returns>
        public BL.Extended.InspectionExtended CreateEmptyInspectionExtended()
        {
            return new BL.Extended.InspectionExtended();
        }

        public BL.Extended.InspectionExtended CreateInspectionExtended(BL.Inspection inspection, 
            BL.CalculationThermoCoeffs thermoCoeffs, 
            BL.CalculationFinanceCoeffs financeCoeffs)
        {
            return new BL.Extended.InspectionExtended(inspection, thermoCoeffs, financeCoeffs);
        }

        /// <summary>
        /// Создает объект нового "Клиента"
        /// </summary>
        /// <returns></returns>
        public BL.Client CreateClient()
        {
            return new BL.Client();
        }

        /// <summary>
        /// Создает объект нового элемента списка "Персонала"
        /// </summary>
        /// <returns></returns>
        public BL.StaffUnit CreateStaffUnit()
        {
            return new BL.StaffUnit();
        }

        /// <summary>
        /// Создает объект нового элемента списка "Транспорт"
        /// </summary>
        /// <returns></returns>
        public BL.VehicleUnit CreateVehicleUnit()
        {
            return new BL.VehicleUnit();
        }

        /// <summary>
        /// Создает объект "Потребление за 5 лет"
        /// </summary>
        /// <returns></returns>
        public BL.FiveYearIntake CreateIntake5Years()
        {
            return new BL.FiveYearIntake();
        }

        /// <summary>
        /// Создает объект "Потребление за последний год (12 месяцев)"
        /// </summary>
        /// <returns></returns>
        public BL.TwelveMonthIntake CreateIntakeTwelveMonths()
        {
            return new BL.TwelveMonthIntake();
        }

        /// <summary>
        /// Создает объект "Расчетные параметры обследования"
        /// </summary>
        /// <returns></returns>
        public BL.CalculationCoeffsOfInspection CreateCalculationCoeffs()
        {
            return new BL.CalculationCoeffsOfInspection();
        }


        /// <summary>
        /// Добавляет новое обследование.
        /// </summary>
        /// <param name="inspection"></param>
        /// <returns>Идентификатор добавленного обследования.</returns>
        public int Add(BL.Inspection inspection)
        {
            Models.Inspection entity = new Models.Inspection();
            Converter.UpdateEntity(entity, inspection);

            return _dataManager.Inspection.Add<int>(entity, "InspectionID");
        }

        /// <summary>
        /// Возвращает обследование с указанным идентификатором или null, если
        /// обследование с таким идентификатором не найдено
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <returns></returns>
        public BL.Inspection GetInspectionById(int inspectionId)
        {
            return HandleExceptions<BL.Inspection>(delegate()
            {
                Models.Inspection inspection = _dataManager.Inspection.GetInspectionById(inspectionId);

                return (inspection != null) ? Converter.ConvertToModel(inspection) : null;
            });
        }

        /// <summary>
        /// Подгружает все связанные с Обследованием объекты, например, Здания
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <returns></returns>
        public BL.Inspection GetFullInspectionById(int inspectionId)
        {
            return HandleExceptions<BL.Inspection>(delegate()
            {
                _dataManager.Inspection.Options = Models.LoadOptions.IncludeBuildings;

                Models.Inspection inspection = _dataManager.Inspection.GetInspectionById(inspectionId);

                return (inspection != null) ? Converter.ConvertToModel(inspection, Models.LoadOptions.IncludeBuildings | Models.LoadOptions.IncludeIllustrations) : null;
            });
        }

        public IEnumerable<BL.InspectionBrief> GetInspectionsByAuditor(int auditorId, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            _dataManager.Inspection.Options = Models.LoadOptions.IncludeAuditor;

            return (from entity in _dataManager.Inspection.GetInspectionsByAuditor(auditorId, pageSize, pageNumber, sortField, asc, out totalRecords)
                    select BriefConverter.ConvertToModel(entity));
        }

        public int GetBaseYearByBuildingId(int buildingId)
        {
            return HandleExceptions<int>(delegate()
            {
                Models.Building building = _dataManager.Building.GetBuildingById(buildingId);
                if (building == null)
                {
                    throw new ConcurrencyException("Здание с указанным идентификатором не обнаружено.");
                }

                Models.Inspection inspection = _dataManager.Inspection.GetInspectionById(building.InspectionID);
                if(inspection == null)
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }

                BL.Inspection bo = Converter.ConvertToModel(inspection);
                return bo.BaseYear.Value;
            });
        }

        /// <summary>
        /// Выполняет изменение обследования с указанным идентификатором.
        /// Изменяется _только_ Client.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <param name="client">Новые значени клиента.</param>
        public void UpdateClient(int inspectionId, BL.Client client)
        {
            HandleExceptions(delegate()
            {
                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity);
                    bo.Client = client;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Выполняет изменение обследования с указанным идентификатором.
        /// Изменяется _только_ Production.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <param name="production">Новые значени продукции.</param>
        public void UpdateProduction(int inspectionId, BL.Production production)
        {
            HandleExceptions(delegate()
            {
                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity);
                    bo.Production = production;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Выполняет изменение обследования с указанным идентификатором.
        /// Изменяется _только_ Staff.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <param name="staff">Новое значение по персоналу.</param>
        public void UpdateStaff(int inspectionId, BL.Staff staff)
        {
            HandleExceptions(delegate()
            {
                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity);
                    bo.Staff = staff;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Выполняет изменение обследования с указанным идентификатором.
        /// Изменяется _только_ FiveYearIntake.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <param name="intake5years">Новые данные по потреблению за последние 5 лет.</param>
        public void UpdateIntake5Years(int inspectionId, BL.FiveYearIntake intake5years)
        {
            HandleExceptions(delegate()
            {
                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity);
                    bo.Intake.FiveYear = intake5years;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Выполняет изменение обследования с указанным идентификатором.
        /// Изменяется _только_ TwelveMonthsIntake.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <param name="intake5years">Новые данные по потреблению за последний год (12 месяцев).</param>
        public void UpdateIntakeLastYear(int inspectionId, BL.TwelveMonthIntake intakeLastYear)
        {
            HandleExceptions(delegate()
            {
                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity);
                    bo.Intake.TwelveMonth = intakeLastYear;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Выполняет изменение обследования с указанным идентификатором.
        /// Изменяется _только_ Vehicles.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <param name="vehicles">Новые данные по транспорту.</param>
        public void UpdateTransport(int inspectionId, Soft33.EnergyPassport.V2.BL.VehicleList vehicles)
        {
            HandleExceptions(delegate()
            {
                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity);
                    bo.Vehicle = vehicles;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Выполняет изменение обследования с указанным идентификатором.
        /// Изменяется _только_ CalculationCoeffs.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследования</param>
        /// <param name="coeffs">Расчетные параметры</param>
        public void UpdateCalculationCoeffs(int inspectionId, BL.CalculationCoeffsOfInspection coeffs)
        {
            HandleExceptions(delegate()
            {
                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity);
                    bo.CalculationCoeffs = coeffs;
                    Converter.UpdateEntity(entity, bo);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }

        /// <summary>
        /// Возвращает список зданий связанных с обследованием.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследование.</param>
        /// <returns>Список зданий</returns>
        public IEnumerable<BL.Building> GetBuildingsByInspectionId(int inspectionId)
        {
            return HandleExceptions<IEnumerable<Soft33.EnergyPassport.V2.BL.Building>>(delegate()
            {
                _dataManager.Inspection.Options = Models.LoadOptions.IncludeBuildings;

                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity, Models.LoadOptions.IncludeBuildings);
                    return bo.Buildings;
                }
                else
                {
                    return null;
                }
            });
        }

        /// <summary>
        /// Изменяет список связанных с обследованием зданий согласно переданной коллекции.
        /// </summary>
        /// <param name="inspectionId">Идентификатор обследование, которое необходимо изменить.</param>
        /// <param name="building">Новый список зданий</param>
        public void UpdateBuildings(int inspectionId, IList<BL.Building> buildings)
        {
            HandleExceptions(delegate()
            {
                _dataManager.Inspection.Options = Models.LoadOptions.IncludeBuildings;

                Models.Inspection entity = _dataManager.Inspection.GetInspectionById(inspectionId);

                if (entity != null)
                {
                    BL.Inspection bo = Converter.ConvertToModel(entity, Models.LoadOptions.IncludeBuildings);
                    bo.Buildings = buildings;
                    Converter.UpdateEntity(_dataManager, entity, bo, Models.LoadOptions.IncludeBuildings);

                    _dataManager.Save();
                }
                else
                {
                    throw new ConcurrencyException("Обследование с указанным идентификатором не обнаружено.");
                }
            });
        }
    }
}