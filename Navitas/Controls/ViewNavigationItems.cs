﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Controls
{
    /// <summary>
    /// Флаги для указания кнопок, которые надо показывать
    /// </summary>
    [Flags]
    public enum ViewNavigationItems
    {
        Next = 0x01,
        Prev = 0x02,
        Save = 0x04
    }
}