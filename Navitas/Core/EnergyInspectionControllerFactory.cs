﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

using Navitas.Models;
using Navitas.Services;

namespace Navitas.Core
{
    public class EnergyInspectionControllerFactory: DefaultControllerFactory
    {
        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                return base.GetControllerInstance(requestContext, controllerType);
            }

            string connectionString = ConfigurationManager.ConnectionStrings["EnergyConnectionString"].ConnectionString;
            DataManager dataManager = new DataManager(connectionString);
            return Activator.CreateInstance(controllerType, dataManager, new ServiceManager(dataManager)) as IController;
        }
    }
}