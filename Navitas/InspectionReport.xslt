<?xml version="1.0" encoding="utf-16"?>
<xsl:transform version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">

    <xsl:apply-templates/>

  </xsl:template>

  <xsl:template match="SectionEditModel">
    
    <xsl:variable name="sectionidvalue" select="@TextId"/>
    <xsl:variable name="timestamp" select="@TimeStamp"/>
    
    <div class="section" id="{$sectionidvalue}" data-timestamp="{$timestamp}">
      <xsl:apply-templates/>
    </div>
  </xsl:template>
  
  <xsl:template match="EditableTextElementEditModel">
    
    <xsl:variable name="textid" select="@TextId"/>

    <xsl:if test="@Hidden != 'true'">

      <!--<a class="edittext" href="javascript:void(0)">Редактировать</a>-->
      <div class="editabletext" id="{$textid}">
        <!--<div class="displaytext">-->
        <!--<xsl:value-of select="Content"/>-->
        <!-- Запретить кодировать амперсанды, чтобы, например &gt; не превращалось в &amp;gt;
        Еще один уровень кодирования нам не нужен. -->
        <xsl:value-of select="Content" disable-output-escaping="yes"/>
        
        <!--</div>-->
        <div class="basetext" style="display:none;">
          <!--<xsl:value-of select="EncodedContent"/>-->
          <!-- Запретить кодировать амперсанды, чтобы, например &gt; не превращалось в &amp;gt;
        Еще один уровень кодирования нам не нужен. -->
          <xsl:value-of select="EncodedContent" disable-output-escaping="yes"/>
        </div>
      </div>
    </xsl:if>

  </xsl:template>

  <xsl:template match="ReadonlyTextElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>

    <xsl:if test="@Hidden != 'true'">

      <!--<a class="edittext" href="javascript:void(0)">Редактировать</a>-->
      <div class="readonlytext" id="{$textid}">

        <!-- Запретить кодировать амперсанды, чтобы, например &gt; не превращалось в &amp;gt;
        Еще один уровень кодирования нам не нужен. -->
        <xsl:value-of select="Content" disable-output-escaping="yes"/>

      </div>
    </xsl:if>

  </xsl:template>

  <xsl:template match="ImageElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>
    <xsl:variable name="usertitle" select="UserTitle"/>
    <xsl:variable name="filename" select="FileName"/>
    <xsl:variable name="imageUrl" select="concat('@IllustrationUrl(&quot;', $filename, '&quot;)')" />

    <xsl:if test="@Hidden != 'true'">
      <div class="imageelement" id="{$textid}">
        <img src='{$imageUrl}' alt="{$usertitle}" />

        <!--<xsl:element name="img">
        <xsl:attribute name="src">
          <xsl:value-of select="ImageUrl" disable-output-escaping="yes"/>
        </xsl:attribute>
        <xsl:attribute name="alt">
          <xsl:value-of select="UserTitle"/>
        </xsl:attribute>
      </xsl:element>-->

        <div class="imageelement-caption">
          <!--<span><xsl:value-of select="UserTitle"/></span>-->
          <xsl:value-of select="UserTitle"/>
        </div>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="StaticImageElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>
    <xsl:variable name="usertitle" select="UserTitle"/>
    <xsl:variable name="filename" select="FileName"/>
    <xsl:variable name="imageUrl" select="concat('@StaticImageUrl(&quot;', $filename, '&quot;)')" />

    <xsl:if test="@Hidden != 'true'">
      <div class="staticimageelement" id="{$textid}">
        <img src='{$imageUrl}' alt="{$usertitle}" />
        <div class="staticimageelement-caption">
          <xsl:value-of select="UserTitle"/>
        </div>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="FormulaImageElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>
    <xsl:variable name="usertitle" select="UserTitle"/>
    <xsl:variable name="filename" select="FileName"/>
    <xsl:variable name="imageUrl" select="concat('@StaticImageUrl(&quot;', $filename, '&quot;)')" />

    <xsl:if test="@Hidden != 'true'">
      <div class="formulaimageelement" id="{$textid}">

        <table>
          <tr>
            <td class="formula">
              <img src='{$imageUrl}' alt="{$usertitle}" />
            </td>
            <td class="caption">
              <xsl:text disable-output-escaping="yes">&lt;div class="formulaimageelement-caption"&gt;&lt;/div&gt;</xsl:text>
            </td>
          </tr>
        </table>
        <!--<div class="formulacontainer">-->
          
        <!--</div>-->
        <!-- empty div breaks layout -->
        
        <xsl:text disable-output-escaping="yes">&lt;div class="clear"&gt;&lt;/div&gt;</xsl:text>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="IllustrationPlaceElementEditModel">
    <!--<div class="illustrationplace">
      <span>
        <xsl:value-of select="Comment"/>
      </span>
    </div>-->
  </xsl:template>

  <xsl:template match="RequiredImageElementEditModel">
    
    <xsl:variable name="textid" select="@TextId"/>

    <xsl:if test="@Hidden != 'true'">
      <div class="requiredimage" id="{$textid}">
          <xsl:value-of select="Content" disable-output-escaping="yes"/>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="TableElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>

    <xsl:if test="@Hidden != 'true'">
      <div class="tableelement" id="{$textid}">
        <div class="tableelement-caption">
          <xsl:value-of select="TableName" />
        </div>
        <xsl:value-of select="Content" disable-output-escaping="yes" />
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="ChartElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>

    <xsl:if test="@Hidden != 'true'">
      <div class="chartelement" id="{$textid}">
        @Chart(<xsl:value-of select="DataContent"/>)
        <div class="chartelement-caption">
          <xsl:value-of select="ChartTitle" />
        </div>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="ChartStackElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>

    <xsl:if test="@Hidden != 'true'">
      <div class="chartstackelement" id="{$textid}">
        @ChartStack(<xsl:value-of select="DataContent"/>)
        <div class="chartstackelement-caption">
          <xsl:value-of select="ChartTitle" />
        </div>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="SectionTitleElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>
    
    <xsl:if test="@Hidden != 'true'">
      <xsl:if test="@Numbered = 'true'">
        <h1 id="{$textid}" class="counter">
          <xsl:value-of select="Text" />
        </h1>
      </xsl:if>
      <xsl:if test="@Numbered != 'true'">
        <h1 id="{$textid}" class="no-counter">
          <xsl:value-of select="Text" />
        </h1>
      </xsl:if>      
    </xsl:if>

  </xsl:template>

  <xsl:template match="SubsectionTitleElementEditModel">

    <xsl:variable name="textid" select="@TextId"/>
    
    <xsl:if test="@Hidden != 'true'">

      <xsl:if test="Level = 2">
        <h2>
          <xsl:value-of select="Text" />
        </h2>
      </xsl:if>

      <xsl:if test="Level = 3">
        <h3>
          <xsl:value-of select="Text" />
        </h3>
      </xsl:if>

    </xsl:if>
  </xsl:template>
</xsl:transform>