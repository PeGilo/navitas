﻿using System;
using System.Linq;

namespace Navitas.Parsing
{
    public class Header
    {
        private readonly string _value;
        private readonly string[] _values;

        public Header(string value)
        {
            _value = value;
            _values = new[] { value };
        }

        public Header(string[] values)
        {
            _value = values.Aggregate((sum, next) => sum + "|" + next);
            _values = values;
        }

        public bool Matches(string value)
        {
            return _value.ToUpper().Contains(value.ToUpper());
        }

        public bool MatchesAll(string[] values)
        {
            var value = _value.ToUpper();
            return values.All(v => value.Contains(v.ToUpper()));
        }

        public bool Matches(int index, string value)
        {
            return _values[index].Contains(value.ToUpper());
        }

        public bool Matches(params string[] values)
        {
            var count = Math.Min(_value.Length, values.Length);
            for (var index = 0; index < count; index++)
            {
                if (!Matches(index, values[index]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}