﻿using System;
using System.Linq;

namespace Navitas.Parsing
{
    public class TableParser
    {
        public static StringTable Parse(string data)
        {
            return new StringTable(GetTable(data));
        }

        public static HeaderedStringTable Parse(string data, int rowHeaderCount, int colHeaderCount)
        {
            return new HeaderedStringTable(GetTable(data), rowHeaderCount, colHeaderCount);
        }

        private static string[][] GetTable(string data)
        {
            data = data.Trim('\r', '\n', ' ');
            var lines = data.Split(new[] { "\r\n" }, StringSplitOptions.None);
            var dataArray = lines.Select(l => l.Split('\t').Select(c => c.Trim()).ToArray()).ToArray();

            var colCount = dataArray.Max(r => r.Length);

            for (var rowIndex = 0; rowIndex < dataArray.Length; rowIndex++)
            {
                var row = dataArray[rowIndex];
                if (row.Length < colCount)
                {
                    var newRow = dataArray[rowIndex] = new string[colCount];
                    row.CopyTo(newRow, 0);
                }
            }

            return dataArray;
        }
    }
}