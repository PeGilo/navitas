﻿using System.Collections.Generic;

namespace Navitas.Parsing
{
    public class StringTable : Table<StringString, StringString>
    {
        public StringTable(string[][] data)
            : base(data)
        {
        }

        protected override StringString CreateRow(IEnumerable<string> data, int index)
        {
            return new StringString(data);
        }

        protected override StringString CreateCol(IEnumerable<string> data, int index)
        {
            return new StringString(data);
        }
    }
}