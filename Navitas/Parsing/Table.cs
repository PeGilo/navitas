﻿using System.Collections.Generic;
using System.Linq;

namespace Navitas.Parsing
{
    public abstract class Table<TRow, TColumn>
    {
        private readonly string[][] _data;
        private readonly int _rowCount;
        private readonly int _colCount;

        private TRow[] _rows;
        private TColumn[] _cols;

        public int RowCount { get { return _rowCount; } }
        public int ColCount { get { return _colCount; } }

        protected TRow[] StringRows
        {
            get { return _rows ?? (_rows = _data.Select(CreateRow).ToArray()); }
        }

        protected TColumn[] StringCols
        {
            get
            {
                if (_cols != null)
                {
                    return _cols;
                }
                _cols = Enumerable.Range(0, _colCount)
                                  .Select(col => Enumerable.Range(0, _rowCount)
                                                           .Select(row => _data[row][col])
                                                           .ToArray())
                                  .Select(CreateCol)
                                  .ToArray();
                return _cols;
            }
        }

        protected Table(string[][] data)
        {
            _data = data;
            _rowCount = _data.Length;
            _colCount = _data[0].Length;
        }

        public IEnumerable<TRow> Rows()
        {
            return StringRows;
        }

        public TRow Row(int index)
        {
            return StringRows[index];
        }

        public IEnumerable<TColumn> Columns()
        {
            return StringCols;
        }

        public TColumn Column(int index)
        {
            return StringCols[index];
        }

        public string Cell(int row, int col)
        {
            return _data[row][col];
        }

        protected abstract TRow CreateRow(IEnumerable<string> data, int index);
        protected abstract TColumn CreateCol(IEnumerable<string> data, int index);
    }
}