﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Navitas.Parsing
{
    public class StringString : IEnumerable<string>
    {
        private readonly IEnumerable<string> _data;
        private string[] _dataArray;

        protected string[] DataArray
        {
            get { return _dataArray ?? (_dataArray = _data.ToArray()); }
        }

        public StringString(IEnumerable<string> data)
        {
            _data = data;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return ((IEnumerable<string>) DataArray).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public string Cell(int index)
        {
            return DataArray[index];
        }
    }
}