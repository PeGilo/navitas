﻿using System.Collections.Generic;
using System.Linq;

namespace Navitas.Parsing
{
    public class HeaderedStringString : StringString
    {
        public Header Header { get; private set; }
        public Header[] ValueHeaders { get; private set; }

        public HeaderedStringString(IEnumerable<string> data, string[] headers, string[][] valueHeaders)
            : base(data)
        {
            Header = new Header(headers);
            ValueHeaders = valueHeaders.Select(h => new Header(h)).ToArray();
        }

        public string Cell(string value)
        {
            var match = ValueHeaders.Select((h, index) => new
            {
                Header = h,
                Index = index
            }).FirstOrDefault(_ => _.Header.Matches(value));

            return match == null ? null : DataArray[match.Index];
        }

        public bool Matches(string value)
        {
            return Header.Matches(value);
        }

        public bool Matches(params string[] values)
        {
            return Header.Matches(values);
        }

        public bool MatchesAll(string[] values)
        {
            return Header.MatchesAll(values);
        }
    }
}