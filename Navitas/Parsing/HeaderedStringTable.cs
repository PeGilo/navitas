﻿using System.Collections.Generic;
using System.Linq;
using Navitas.Extensions;

namespace Navitas.Parsing
{
    public class HeaderedStringTable : Table<HeaderedStringString, HeaderedStringString>
    {
        private readonly string[][] _rowHeaders;
        private readonly string[][] _colHeaders;

        public HeaderedStringTable(string[][] data, int rowHeaderCount, int colHeaderCount)
            : base(data.Skip(colHeaderCount).Select(row => row.Skip(rowHeaderCount).ToArray()).ToArray())
        {
            _rowHeaders = data.Skip(colHeaderCount).Select(row => row.TakeExact(rowHeaderCount).ToArray()).ToArray();
            for (var row = 1; row < RowCount; row++)
            {
                for (var col = 0; col < rowHeaderCount; col++)
                {
                    if (string.IsNullOrEmpty(_rowHeaders[row][col]))
                    {
                        _rowHeaders[row][col] = _rowHeaders[row - 1][col];
                    }
                }
            }
            
            _colHeaders = Enumerable.Range(rowHeaderCount, ColCount).Select(col => Enumerable.Range(0, colHeaderCount).Select(row => data[row][col]).ToArray()).ToArray();
            for (var col = 1; col < ColCount; col++)
            {
                for (var row = 0; row < colHeaderCount; row++)
                {
                    if (string.IsNullOrEmpty(_colHeaders[col][row]))
                    {
                        _colHeaders[col][row] = _colHeaders[col - 1][row];
                    }
                }
            }
        }

        public HeaderedStringString Column(string value)
        {
            return StringCols.FirstOrDefault(col => col.Matches(value));
        }

        public int GetColumnIndex(string value)
        {
            for (var index = 0; index < ColCount; index++)
            {
                if (StringCols[index].Matches(value))
                {
                    return index;
                }
            }
            return -1;
        }

        public HeaderedStringString Row(string value)
        {
            return StringRows.FirstOrDefault(col => col.Matches(value));
        }

        public int GetRowIndex(string value)
        {
            for (var index = 0; index < ColCount; index++)
            {
                if (StringRows[index].Matches(value))
                {
                    return index;
                }
            }
            return -1;
        }

        protected override HeaderedStringString CreateRow(IEnumerable<string> data, int index)
        {
            return new HeaderedStringString(data, _rowHeaders[index], _colHeaders);
        }

        protected override HeaderedStringString CreateCol(IEnumerable<string> data, int index)
        {
            return new HeaderedStringString(data, _colHeaders[index], _rowHeaders);
        }
    }
}