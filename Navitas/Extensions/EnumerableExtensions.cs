﻿using System.Collections.Generic;

namespace Navitas.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> TakeExact<T>(this IEnumerable<T> source, int count)
        {
            if (count == 0)
            {
                yield break;
            }

            foreach (var item in source)
            {
                if (count-- == 0)
                {
                    yield break;
                }
                yield return item;
            }

            while (count-- > 0)
            {
                yield return default(T);
            }
        }
    }
}