﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.DataAccess.Exceptions
{
    /// <summary>
    /// Исключение, сигнализирующее об ошибке совместного доступа к данным.
    /// </summary>
    [Serializable]
    public class ConcurrencyException : DataAccessBaseException
    {
        public ConcurrencyException() { }
        public ConcurrencyException(string message) : base(message) { }
        public ConcurrencyException(string message, Exception inner) : base(message, inner) { }
        protected ConcurrencyException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}