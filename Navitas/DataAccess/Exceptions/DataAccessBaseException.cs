﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.DataAccess.Exceptions
{
    /// <summary>
    /// Базовое исключение для всех исключений, возникающих в слое доступа к данным.
    /// </summary>
    [Serializable]
    public class DataAccessBaseException : ApplicationException
    {
        public DataAccessBaseException() { }
        public DataAccessBaseException(string message) : base(message) { }
        public DataAccessBaseException(string message, Exception inner) : base(message, inner) { }
        protected DataAccessBaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}