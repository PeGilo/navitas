﻿var Navitas = Navitas || {};

Navitas.Utils = {
    trim: function(s) { return s.replace(/^\s+|\s+$/g, ''); },
    removespaces: function(s) { return s.replace(/\s+/g, ''); }
};