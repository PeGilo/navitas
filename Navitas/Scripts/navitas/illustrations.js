﻿
var IllustrationViewHandler = function (container) {

    var 
        m_container = container, // should be jquery object

        update = function (data) {
            m_container.empty();
            $("#photoTemplate").tmpl(data).appendTo(m_container);
        },

        showMessage = function (message) {
            alert(message);
        };

    return {
        update: update,
        showMessage: showMessage
    };
};

var IllustrationFormHandler = function (index, view, imagesContainer, sectionIdValue, form, data) {

    var m_index = index,
        m_view = view,
    m_sectionIdValue = sectionIdValue,
    m_form = form,
    m_imagesContainer = imagesContainer,
    m_data = data,

    messageExtError = 'Файлы с заданным расширением не поддерживаются.', //DrCom.Resources.ValidationStrings.UploadExtInvalid'; 
    messageEmptyField = 'Файл не выбран.', //'DrCom.Resources.ValidationStrings.UploadEmptyField';
    messageErrorOnUploding = 'Ошибка при загрузке изображения.',
    messageErrorOnRequest = 'Возникла ошибка при обращении к серверу.',
    messageDeleteConfirm = 'Удалить иллюстрацию?',

    // allowed extensions for uploading photos
    allowedExt = {
        '.jpg': 1,
        '.jpeg': 1,
        '.png': 1,
        '.gif': 1
    },

    init = function () {
        initAllImages();
    },

    initAllImages = function () {

        //$("#photoTemplate").tmpl(m_data).appendTo(m_imagesContainer);
        updateView();

        // init photo items
        //        m_imagesContainer.find('.photoItem').each(function (index, el) {
        //            initImageItem(el);
        //        });
    },

    initImageItem = function (el) {

    },

    updateView = function () {
        m_view.update(m_data);
        rebindViewEventHandlers();
    },

    // подключает обработчики событий к элементам списка картинок
    rebindViewEventHandlers = function () {
        m_imagesContainer.find('.photoItem').each(function (index, el) {
            //initImageItem(el);

            $('.previewDelete', el).click(function () {
                onDeleteClick(this);
            })
            .hover(
                function () {
                    $(this).attr({ src: '/content/images/del-hover.png' });
                },
                function () {
                    $(this).attr({ src: '/content/images/del.png' });
                }
            );

        });
    },

    onImageUploaded = function (fileDescriptor) {

        m_data.push(fileDescriptor);
        updateView();
        //var newEl = $("#photoTemplate").tmpl([{ 'Id': imageId, 'Name': fileName, 'ImageUrl': imageUrl, 'ThumbUrl': thumbUrl}]);
        //newEl.appendTo(m_imagesContainer);

    },

    onImageDeleted = function (fileId) {
        for (var i = 0; i < m_data.length; i++) {
            if (m_data[i].FileId == fileId) {
                m_data.splice(i, 1);
                updateView();
                return;
            }
        }
    },

    handleSubmitClick = function () {

        // check that file is selected
        var inputText = $(m_form).find('.inputFile').attr('value');

        if (inputText && inputText.length && inputText.length > 0) {
            if (validateFileExt(inputText)) {
                $(m_form).submit(); //ajaxSubmit();
            }
            else {
                m_view.showMessage(messageExtError);
            }
        }
        else {
            m_view.showMessage(messageEmptyField);
        }
    },

    onFormSubmit = function () {

        var options = {
            iframe: true,
            type: "post",
            dataType: "json",
            resetForm: true,

            beforeSubmit: function () {

                onBeforeSubmit();
            },
            success: function (data) {

                onRequestSuccess(data);
                //formObj.resetForm();
            },
            error: function (xhr, textStatus, errorThrown) {

                onRequestError(xhr, textStatus, errorThrown);
                //formObj.resetForm();
            }
        };

        // Необходимо уточнить у пользователя название иллюстрации
        var userTitle = prompt("Введите название иллюстрации:", "");

        if (userTitle) {
            // Добавить название иллюстрации в форму, чтобы оно попало на сервер.
            // При этом элемент для сохранения значения, может уже сущестововать на форме.
            var $input = $('input[type="hidden"][name="usertitle"]', $(this));
            if ($input.length > 0) {
                $input.val(userTitle);
            }
            else {
                var input = $("<input>").attr("type", "hidden").attr("name", "usertitle").val(userTitle);
                $(this).append($(input));
            }
            $(this).ajaxSubmit(options);
        }

        return false;
    },

    deleteImage = function (fileId) {

        $.ajax({
            type: "POST",
            url: "/file/asyncdelete",
            dataType: "json",
            data: {
                fileId: fileId
            },
            success: function (data) {
                if (data.errorState == true) {

                    m_view.showMessage(data.errorMessage);
                }
                else {
                    onImageDeleted(fileId);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                m_view.showMessage(messageErrorOnRequest);
            }
        });
    },

    onBeforeSubmit = function () {

        $.blockUI({ message: '<h3><img src="/Content/images/busy.gif" />Загрузка изображения на сервер...</h3>' });
    },

    onRequestSuccess = function (data) {

        $.unblockUI();

        // check result
        if (data.errorState == true) {
            //$.growlUI(null, data.errorMessage);
            m_view.showMessage(data.errorMessage);
        }
        else {
            onImageUploaded(data.fileDescriptor);
        }
    },

    onRequestError = function (xhr, textStatus, errorThrown) {

        $.unblockUI();
        //$.growlUI(null, 'Error uploading file');
        m_view.showMessage(messageErrorOnUploding);
    },

    onDeleteClick = function (obj) {
        var fileId = $(obj).attr("data-fileid");
        if (confirm(messageDeleteConfirm)) {
            deleteImage(fileId);
        }
    },

    onInputFileChanged = function (ev) {

        if (!validateFileExt(this.value)) {
            m_view.showMessage(messageExtError);
        }
    },

    // validates image file by its extension
    validateFileExt = function (fileName) {
        var ext = fileName.substr(fileName.lastIndexOf('.')).toLowerCase() || '';
        return (allowedExt[ext] === 1);
    };

    return {
        init: init,
        initAllImages: initAllImages,
        initImageItem: initImageItem,
        rebindViewEventHandlers: rebindViewEventHandlers,
        onFormSubmit: onFormSubmit,
        handleSubmitClick: handleSubmitClick,
        onBeforeSubmit: onBeforeSubmit,
        onRequestSuccess: onRequestSuccess,
        onRequestError: onRequestError,
        onImageUploaded: onImageUploaded,
        onImageDeleted: onImageDeleted,
        onDeleteClick: onDeleteClick,
        onInputFileChanged: onInputFileChanged,
        updateView: updateView
    };
};

$(document).ready(function () {

    $("form.ajaxuploadform").each(function (index, value) {

        var formObj = $(value);

        // Получаем название секции, для которой загружаются картинки
        var sectionIdValue = formObj.find(".sectionid").attr('value');

        // Получаем информацию об имеющихся картинках
        var dataSerialized = formObj.find(".data").attr('value');
        var data = jQuery.parseJSON(dataSerialized);

        // Дополнительные объекты
        var imagesContainer = formObj.find(".imagesContainer");

        // Создаем объект, который будет обрабатывать события для формы.
        var viewHandler = new IllustrationViewHandler(imagesContainer);
        var formHandler = new IllustrationFormHandler(index, viewHandler, imagesContainer, sectionIdValue, value, data);

        formHandler.init();

        formObj.submit(formHandler.onFormSubmit);

//        formObj.ajaxForm({
//            iframe: true,
//            type: "post",
//            dataType: "json",
//            resetForm: true,
//            //crossDomain: true,
//            beforeSubmit: function () {

//                formHandler.onBeforeSubmit();
//            },
//            success: function (data) {

//                formHandler.onRequestSuccess(data);
//                //formObj.resetForm();
//            },
//            error: function (xhr, textStatus, errorThrown) {

//                formHandler.onRequestError(xhr, textStatus, errorThrown);
//                //formObj.resetForm();
//            }
//        });

        formObj.find(".ajaxUploadButton").click(formHandler.handleSubmitClick);

        formObj.find('.inputFile').change(formHandler.onInputFileChanged);

    });
});