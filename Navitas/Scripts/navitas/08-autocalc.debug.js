﻿/*
Plugin allows to set some field to automatically summarize other fields' values.

Using: 
// Initialize pluging:
$(this).autocalc('init', {
fractionDigits: 2,
fractionSeparator: ',',
emptyText: 'n/a'            
});

// Bind autosum action to elements
$('#input13').autocalc('sum', ['#input11', '#input12']);
*/
(function ($) {

    // default options
    var settings = {
        fractionDigits: 2,
        fractionSeparator: ',',
        emptyText: 'n/a'
    };

    function handleSum(target, elementsSelectors) {
        var total = 0.0;
        var valid = true;
        for (var i = 0; i < elementsSelectors.length; i++) {
            var text = $(elementsSelectors[i]).val();
            var value = parseFloat(text.replace(",", "."));
            if (text === '-') {
                // считаем, что в ячейке 0
            }
            else if (!isNaN(value)) {
                total += value;
            }
            else {
                valid = false;
                break;
            }
        }
        if (valid) {
            // Перевести float в текст
            var text = total.toFixed(settings.fractionDigits);
            // Заменить точки на указанный символ
            text = text.replace(".", settings.fractionSeparator);
            target.val(text);
        }
        else {
            target.val(settings.emptyText);
        }
    }

    function handleDiv(target, elementsSelectors) {
        var dividend = parseFloat($(elementsSelectors[0]).val().replace(",", "."));
        var divisor = parseFloat($(elementsSelectors[1]).val().replace(",", "."));

        if (!isNaN(dividend) && !isNaN(divisor) && divisor != 0) {
            // Перевести float в текст
            var text = (dividend / divisor).toFixed(settings.fractionDigits);
            // Заменить точки на указанный символ
            text = text.replace(".", settings.fractionSeparator);
            target.val(text);
        }
        else {
            target.val(settings.emptyText);
        }
    }

    var methods = {
        init: function (options) {

            // Create some defaults, extending them with any options that were provided
            settings = $.extend(settings, options);
        },

        // elementsSelectors - array of selectors
        sum: function (elementsSelectors) {

            var target = this;

            for (var i = 0; i < elementsSelectors.length; i++) {
                $(elementsSelectors[i]).keyup(function () {
                    handleSum(target, elementsSelectors);
                });
            }

            return this;
        },

        // @dividendSelector - селектор для делимого
        // @divisorSelector - селектор для делителя
        div: function (dividendSelector, divisorSelector) {
            var target = this;

            $(dividendSelector).keyup(function () {
                handleDiv(target, [dividendSelector, divisorSelector]);
            });

            $(divisorSelector).keyup(function () {
                handleDiv(target, [dividendSelector, divisorSelector]);
            });
        }
    };

    $.fn.autocalc = function (method) {


        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.autocalc');
        }

    };

})(jQuery);