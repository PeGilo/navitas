﻿// Объект содержит обрабтчики событий и методы для нумерации заголовков и других элементов
function ReportNumerator(containerSelector, elementsCombinedSelector) {

    var 
        self = this,
        _containerSelector = containerSelector,
        _elementsCombinedSelector = elementsCombinedSelector,

        // Удаляет нумерацию из элемента
        clearCaptionTitle = function (prefix, suffix, elObj) {

            var regex = new RegExp('^' + prefix + '\\d+\\.\\d{0,}\\.{0,1}\\d{0,}\\.{0,1}\\d{0,}\\.{0,1}\\s{1}' + suffix);

            var content = elObj.html();
            var matches = regex.exec(content);

            if (matches && matches.length > 0) {
                elObj.html(content.substring(matches[0].length));
            }
        },

        clearCaptionElement = function (prefix, suffix, elObj) {

            var regex = new RegExp('^' + prefix + '\\d+\\.\\d{0,}\\.{0,1}\\d{0,}\\.{0,1}\\d{0,}\\s{0,1}\\-{0,1}\\s{0,1}' + suffix);

            var content = elObj.html();
            var matches = regex.exec(content);

            if (matches && matches.length > 0) {
                elObj.html(content.substring(matches[0].length));
            }
        },

        clearFormulaElement = function (prefix, suffix, elObj) {

//            var regex = new RegExp('^' + prefix + '\\d+\\.\\d{0,}\\.{0,1}\\d{0,}\\.{0,1}\\d{0,}\\s{0,1}\\-{0,1}\\s{0,1}' + suffix);

//            var content = elObj.html();
//            var matches = regex.exec(content);

//            if (matches && matches.length > 0) {
//                elObj.html(content.substring(matches[0].length));
//            }
            elObj.html("");
        },

        // Формирует строку с нумерацией
        _buildElementNumber = function (sectionCounter, subsectionCounter, subsectionCounter3, elementCounter, addPoint) {

            if (subsectionCounter > 0 && subsectionCounter3 > 0 ) {
                return sectionCounter.toString() + "." + subsectionCounter.toString() + "." + subsectionCounter3.toString() + "." + elementCounter.toString() + (addPoint ? " - " : "");
            }
            else if (subsectionCounter > 0) {
                return sectionCounter.toString() + "." + subsectionCounter.toString() + "." + elementCounter.toString() + (addPoint ? " - " : "");
            }
            else {
                return sectionCounter.toString() + "." + elementCounter.toString() + (addPoint ? " - " : "");
            }
        };

//        _buildElementNumberTitle = function (sectionCounter, subsectionCounter, elementCounter) {
//            if (subsectionCounter > 0) {
//                return sectionCounter.toString() + "." + subsectionCounter.toString() + "." + elementCounter.toString() + ". ");
//            }
//            else {
//                return sectionCounter.toString() + "." + elementCounter.toString() + ". ");
//            }
//        };

        this.init = function () {
        };

        this.numerate = function () {
            var sectionCounter = 0,
                subsectionCounter = 0,
                subsectionCounter3 = 0,
                imageCounter = 0,
                tableCounter = 0,
            //chartCounter = 0,
                formulaCounter = 0;

            $(_elementsCombinedSelector, _containerSelector)
                .each(function (index, value) {

                    var elObj = $(value);

                    if (elObj.is('h1')) {

                        sectionCounter++;
                        clearCaptionTitle('', '', elObj);
                        elObj.html(sectionCounter.toString() + ". " + elObj.html());

                        subsectionCounter = 0;
                        subsectionCounter3 = 0;
                        imageCounter = 0;
                        //chartCounter = 0;
                        tableCounter = 0;
                        formulaCounter = 0;
                    }
                    else if (elObj.is('h2')) {

                        subsectionCounter++;
                        clearCaptionTitle('', '', elObj);
                        elObj.html(sectionCounter.toString() + "." + subsectionCounter.toString() + ". " + elObj.html());

                        subsectionCounter3 = 0;
                        imageCounter = 0;
                        //chartCounter = 0;
                        tableCounter = 0;
                        formulaCounter = 0;
                    }
                    else if (elObj.is('h3')) {
                        subsectionCounter3++;
                        clearCaptionTitle('', '', elObj);
                        elObj.html(sectionCounter.toString() + "." + subsectionCounter.toString() + "." + subsectionCounter3.toString() + ". " + elObj.html());

                        imageCounter = 0;
                        //chartCounter = 0;
                        tableCounter = 0;
                        formulaCounter = 0;
                    }
                    else if (elObj.hasClass('imageelement-caption') || elObj.hasClass('required-illustration-caption')) {  //elObj.is('span')) {

                        imageCounter++;
                        clearCaptionElement('Рисунок ', '', elObj);
                        elObj.html('Рисунок ' + _buildElementNumber(sectionCounter, subsectionCounter, subsectionCounter3, imageCounter, true) + elObj.html());
                    }
                    else if (elObj.hasClass('tableelement-caption')) {

                        tableCounter++;
                        clearCaptionElement('Таблица ', '', elObj);
                        elObj.html('Таблица ' + _buildElementNumber(sectionCounter, subsectionCounter, subsectionCounter3, tableCounter, true) + elObj.html());
                    }
                    else if (elObj.hasClass('chartelement-caption') || elObj.hasClass('chartstackelement-caption')) {

                        imageCounter++;
                        clearCaptionElement('Рисунок ', '', elObj);
                        elObj.html('Рисунок ' + _buildElementNumber(sectionCounter, subsectionCounter, subsectionCounter3, imageCounter, true) + elObj.html());
                    }
                    else if (elObj.hasClass('formulaimageelement-caption')) {

                        formulaCounter++;
                        clearFormulaElement('(', ')', elObj);
                        elObj.html('(' + _buildElementNumber(sectionCounter, subsectionCounter, subsectionCounter3, formulaCounter, false) + elObj.html() + ')');
                    }
                    else {

                    }
                });
        };

    self.init();
}