﻿function SelectImageDialog(view, options) {
    var 
        self = this,
        _view = view,
        _options = options,

        _bindedEvents = [];

    this.init = function () {

        $(_options.dialogSelector).dialog({
            autoOpen: false,
            height: 300,
            width: 300,
            modal: true,
            buttons: {
                "OK": this.onDialog_ok,
                "Отмена": function () {
                    $(this).dialog("close");
                }
            },
            open: this.onDialog_open,
            close: this.onDialog_close,
            resizeStop: function (event, ui) {

            }
        });

    };

    this.show = function (eventHandlers, getImagesDelegate) {

        // binding events
        if (eventHandlers) {
            for (var key in eventHandlers) {
                this.subscribe(key, eventHandlers[key]);
                _bindedEvents.push(key);
            }
        }

        $("#imageSelect").empty();

        $(_options.dialogSelector).dialog("open");

        getImagesDelegate(function (illustrations) {

            for (var i = 0; i < illustrations.length; i++) {

                var value = illustrations[i].IllustrationId;
                var text = illustrations[i].UserTitle;

                $("#imageSelect").append($('<option value="' + value + '">' + text + '</option>'));
            }            
        });
    };

//    // Выполняется в контексте диалога
//    this.onDialog_open = function (event, ui) {
//// Очистить список
//                


    //};

    // Выполняется в контексте диалога
    this.onDialog_close = function () {

        // unbindevents
        for (var i = 0; i < _bindedEvents.length; i++) {
            self.unsubscribe(_bindedEvents[i]);
        }
        _bindedEvents = [];
    };

    // Выполняется в контексте диалога
    this.onDialog_ok = function () {

        // Проверить, что иллюстрация выбрана
        var selectedValue = $("#imageSelect option:selected").val();
        if (selectedValue) {

            $(this).trigger("dialogimageselected", [selectedValue]);

            $(this).dialog("close");
        }
        else {
            alert("Выберите иллюстрацию");
        }
    };

    this.subscribe = function (eventName, handler) {
        $(_options.dialogSelector).bind(eventName, handler);
    };

    this.unsubscribe = function (eventName) {
        $(_options.dialogSelector).unbind(eventName);
    };

    self.init();
}