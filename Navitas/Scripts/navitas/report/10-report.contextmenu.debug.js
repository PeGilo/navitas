﻿function ContextMenu(menuSelector, containerSelector, menuItemsOptions, controller) {
    var 
        self = this,
        _menuSelector = menuSelector,
        _containerSelector = containerSelector,
        _toolbarSelector = "#toolbar",
        _menuItemsOptions = menuItemsOptions,
        _controller = controller,
        _mouseEntered = false,

        _menuContainer = null,
        _toolbar = null,

        _toolbarObj = null,
        _hoveredElement = null;

    this.getOptions = function (element) {
        return _menuItemsOptions[element.className];
    };

    this.init = function () {

        this._menuContainer = $(_menuSelector);

        // Инициализация TOOLBAR'а
        this._toolbarObj = new Toolbar(_toolbarSelector, function () { return !_mouseEntered; });
        this._toolbarObj.subscribe("selected", function (event, nameOrId) { self.onToolbar_selected(event, nameOrId); });
        this._toolbarObj.subscribe("hiding", function (event) { self.onToolbar_hiding(event); });

        // Склеить все селекторы в один через запятую
        var selector = "";
        for (var key in _menuItemsOptions) {
            selector += "." + key + ",";
        }
        selector = selector.replace(/,$/, '');

        // Подписаться на события
        $(_containerSelector).on("mouseenter", selector, function (event) {
            _mouseEntered = true;
            return self.onElement_mouseenter(event);
        });

        $(_containerSelector).on("mouseleave", selector, function (event) {
            _mouseEntered = false;
            return self.onElement_mouseleave(event);
        });
    };

    this.onElement_mouseenter = function (event) {

        _hoveredElement = event.currentTarget;
        var position = $(event.currentTarget).offset();
        var parentPosition = $(this._menuContainer).parent().position();

        this._menuContainer.css({ right: 0, top: (position.top - parentPosition.top) }); // position
        this._toolbarObj.setOptions(self.getOptions(event.currentTarget));
        this._toolbarObj.show();

        _controller.highlightElement(_hoveredElement, true);

        return false;
    };

    this.onElement_mouseleave = function (event) {

        this._toolbarObj.hide();
       
        return false;
    };

    this.onToolbar_hiding = function (event) {
        _controller.clearHighlighting();
    };

    this.onToolbar_selected = function (event, nameOrId) {

        // Спрятать тулбар немедленно
        this._toolbarObj.hide({ immediate: true }); // BUG: Не работает, потому что сразу же ловится события hoverin на
                                                    // тулбаре

        // Найти обработчик и передать ему _hoveredElement
        var options = this.getOptions(_hoveredElement);
        for (var item in options) {

            // Проверить свойства объекта
            if (nameOrId == item) {
                if (options[item].handler) { options[item].handler(_hoveredElement); }
                return;
            }
            else if (options[item].submenuHandlers) {
                // Пройтись по вложенным элементам
                for (var subItem in options[item].submenuHandlers) {
                    if (nameOrId == subItem) {
                        //if (options[item].submenuHandlers[subItem].handler) { options[item].handler(_hoveredElement); }
                        options[item].submenuHandlers[subItem](_hoveredElement);
                        return;
                    }
                }
            }
        }
    };

    self.init();
}