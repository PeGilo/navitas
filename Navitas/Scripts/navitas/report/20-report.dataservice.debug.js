﻿function ReportDataService(inspectionId, errorHandler) {

    var
        self = this,
        _inspectionId = inspectionId,
        _errorHandler = errorHandler;

    this.getImagesList = function (callbackOnSucceed) {


        $.ajax({
            type: "POST",
            url: "/file/asyncgetimages/",
            dataType: "json",
            data: {
                inspectionId: _inspectionId
            },
            success: function (data) {
                if (data.errorState == 0) {
                    callbackOnSucceed(data);
                }
                else {
                    errorHandler.handleFailureCode(data);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                errorHandler.handleErrorOnRequest(xhr, textStatus, errorThrown);
            }
        });
    };

    this.hideElement = function (sectionId, elementId, timestamp, callbackOnSucceed) {

        // Отправить запрос на удаление текста
        $.ajax({
            type: "POST",
            url: "/report/hideelement",
            dataType: "json",
            data: {
                sectionId: sectionId,
                elementId: elementId,
                timestamp: timestamp
            },
            success: function (data) {
                if (data.errorCode == 0) {
                    callbackOnSucceed(data);
                }
                else {
                    _errorHandler.handleFailureCode(data);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                _errorHandler.handleErrorOnRequest(xhr, textStatus, errorThrown);
            }
        });
    };

//    this.hideTextElement = function (sectionId, paragraphId, timestamp, callbackOnSucceed) {

//        // Отправить запрос на удаление текста
//        $.ajax({
//            type: "POST",
//            url: "/report/hidetext",
//            dataType: "json",
//            data: {
//                sectionId: sectionId,
//                paragraphId: paragraphId,
//                timestamp: timestamp
//            },
//            success: function (data) {
//                if (data.errorCode == 0) {
//                    callbackOnSucceed(data);
//                    //self.contentChanged();
//                }
//                else {
//                    _errorHandler.handleFailureCode(data);
//                }
//            },
//            error: function (xhr, textStatus, errorThrown) {
//                _errorHandler.handleErrorOnRequest(xhr, textStatus, errorThrown);
//            }
//        });
//    };

//    this.hideRequiredImage = function (sectionId, elementId, timestamp, callbackOnSucceed) {

//        // Отправить запрос на удаление иллюстрации
//        $.ajax({
//            type: "POST",
//            url: "/report/hiderequiredimageblock",
//            dataType: "json",
//            data: {
//                sectionId: sectionId,
//                blockId: elementId,
//                timestamp: timestamp
//            },
//            success: function (data) {
//                if (data.errorCode == 0) {
//                    callbackOnSucceed(data);
//                }
//                else {
//                    errorHandler.handleFailureCode(data);
//                }
//            },
//            error: function (xhr, textStatus, errorThrown) {
//                errorHandler.handleErrorOnRequest(xhr, textStatus, errorThrown);
//            }
//        });
//    };

    this.insertImage = function (sectionId, elementId, illustrationId, timestamp, options, callbackOnSucceed) {

        var before = (options && typeof (options.before) != "undefined") ? options.before : true;

        // Запрос на вставку рисунка
        $.ajax({
            type: "POST",
            url: "/report/insertillustration",
            dataType: "json",
            data: {
                illustrationId: illustrationId,
                sectionId: sectionId,
                anchorElementId: elementId,
                timestamp: timestamp,
                before: before
            },
            success: function (data) {

                if (data.errorCode == 0) {
                    callbackOnSucceed(data);
                }
                else {
                    errorHandler.handleFailureCode(data);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                errorHandler.handleErrorOnRequest(xhr, textStatus, errorThrown);
            }
        });
    };

    this.insertTextElement = function (sectionId, elementId, timestamp, text, options, callbackOnSucceed) {

        var before = (options && typeof (options.before) != "undefined") ? options.before : true;

        // Запрос на вставку текс
        $.ajax({
            type: "POST",
            url: "/report/inserttext",
            dataType: "json",
            data: {
                inspectionId: _inspectionId,
                sectionId: sectionId,
                anchorElementId: elementId,
                TextId: "",
                Content: htmlEncode(text),
                timestamp: timestamp,
                before: before
            },
            success: function (data) {

                if (data.errorCode == 0) {
                    callbackOnSucceed(data);
                }
                else {
                    errorHandler.handleFailureCode(data);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                errorHandler.handleErrorOnRequest(xhr, textStatus, errorThrown);
            }
        });
    };

    this.updateTextElement = function (sectionId, paragraphId, timestamp, text, callbackOnSucceed) {
        // Отправить запрос на изменение текста
        $.ajax({
            type: "POST",
            url: "/report/updatetext",
            dataType: "json",
            data: {
                inspectionId: _inspectionId,
                sectionId: sectionId,
                TextId: paragraphId,
                Content: htmlEncode(text),
                timestamp: timestamp
            },
            success: function (data) {
                if (data.errorCode == 0) {
                    callbackOnSucceed(data);
                }
                else {
                    errorHandler.handleFailureCode(data);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                errorHandler.handleErrorOnRequest(xhr, textStatus, errorThrown);
            }
        });        
    };
}