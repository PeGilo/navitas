﻿function ReportController(view, dataservice, editTextDialog, selectImageDialog, numerator, options) {
    var 
        self = this,
        _view = view,
        _dataservice = dataservice,
        _editTextDialog = editTextDialog,
        _selectImageDialog = selectImageDialog,
        _numerator = numerator,
        _options = options;

    this.init = function () {
        _numerator.numerate();
    };

    this.clearHighlighting = function () {
        _view.clearHighlighting();
    };

    this.editEditableText = function (element) {

        var props = _view.getElementProps(element);
        if (props) {
            _editTextDialog.show(props.basetext, {
                "dialogokvalidated": function (event, text) { return self.onEditTextDialog_okValidated(props.sectionId, props.elementId, props.timestamp, text); },
                "dialogclosing": function () { return self.onEditTextDialog_close(); } // не используется close, чтобы не стирать его
            });
        }
    };

    this.hideElement = function (element) {

        var props = _view.getElementProps(element);
        if (props) {
            if (confirm(_options.deleteBlockConfirmation)) {
                _dataservice.hideElement(props.sectionId, props.elementId, props.timestamp,
                function (data) {
                    _view.removeElement(props.elementId, props.sectionId, data.result.sectionTimestamp);
                    _numerator.numerate();
                });
            }
        }
    };

    this.highlightElement = function (element, on) {
        _view.highlightElement(element, on);
    };

    this.insertImage = function (element, options) {

        var props = _view.getElementProps(element);
        if (props) {
            _selectImageDialog.show(
                {
                    "dialogimageselected": function (event, selectedValue) {
                        return self.onSelectImageDialog_selected(selectedValue, props.sectionId, props.elementId, props.timestamp, options);
                    }
                },
                // Передаем в диалог функцию, которую он вызовет для получения списка иллюстраций.
                function (delegate) {
                    _dataservice.getImagesList(function (data) {
                        delegate(data.illustrations); 
                    });
             });
        }
    };

    this.insertText = function (element, options) {
        var props = _view.getElementProps(element);
        if (props) {
            _editTextDialog.show("", {
                "dialogokvalidated": function (event, text) { return self.onAddTextDialog_okValidated(props.sectionId, props.elementId, props.timestamp, text, options); }
            });            
        }
    };

    this.onAddTextDialog_okValidated = function (sectionId, elementId, timestamp, text, options) {

        _dataservice.insertTextElement(sectionId, elementId, timestamp, text, options,
            function (data) {
                return _view.insertTextBlock(elementId, sectionId, data.result.sectionTimestamp, data.result.htmlLayout, options);
            });
    };

    this.onEditTextDialog_okValidated = function (sectionId, elementId, timestamp, text) {

        _dataservice.updateTextElement(sectionId, elementId, timestamp, text,
            function (data) {
                return _view.updateTextBlock(elementId, sectionId, data.result.sectionTimestamp, data.result.htmlLayout); 
            });
    };

    this.onEditTextDialog_close = function () {
    };

    this.onSelectImageDialog_selected = function (selectedValue, sectionId, elementId, timestamp, options) {

        var illustrationId = selectedValue.toString();

        _dataservice.insertImage(sectionId, elementId, illustrationId, timestamp, options,
            function (data) {
                _view.insertImage(elementId, sectionId, data.result.sectionTimestamp, data.result.htmlLayout, options);
                _numerator.numerate();
            });
    };

    self.init();
}