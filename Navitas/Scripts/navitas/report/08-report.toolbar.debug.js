﻿function Toolbar(toolbarSelector, confirmHideCallback) {

    var 
        self = this,
        _toolbarSelector = toolbarSelector,
        _toolbar = null,
        _hideToolbarTask = null,
        _subMenu_InsBef = null,
        _subMenu_InsAft = null,
        _pinned = false,
        _confirmHideCallback = confirmHideCallback;

    this.canHide = true;

    this.show = function () {
       
        _hideToolbarTask.abort();
        _toolbar.css("display", "inline-block");
        // Что если здесь прятать submenus
        if (_subMenu_InsBef) { _subMenu_InsBef.hide({ immediate: true }); }
    };

    this.hide = function (options) {
        if (options && options.immediate) {
            _hideToolbarTask.abort();
            this._hideAllSubmenus();
            $(_toolbarSelector).trigger("hiding");
            $(_toolbarSelector).hide();
        }
        else {
            _hideToolbarTask.start();
        }
    };

    this.init = function () {

        _toolbar = $(_toolbarSelector);

        _toolbar.hover(
                    function () { self.onToolbar_hoverin(); },
                    function () { self.onToolbar_hoverout(); });
        _toolbar.css("display", "none");
        _hideToolbarTask = new DefferedTask(function () {
            $(_toolbarSelector).trigger("hiding");
            _toolbar.css("display", "none");
        });

        _toolbar.find(".edit").button({
            icons: {
                primary: "ui-icon-pencil"
            },
            text: false
        });

        _toolbar.find(".hide").button({
            icons: {
                primary: "ui-icon-close"
            },
            text: false
        });

        $(".insertbefore", _toolbarSelector)
            .hover(
                function () { self._onInsertBefore_hoverin(); },
                function () { self._onInsertBefore_hoverout(); }
            )
            .button({
                icons: {
                    primary: "ui-icon-plus",
                    secondary: "ui-icon-triangle-1-n"
                },
                text: false
            });

        $(".insertafter", _toolbarSelector)
            .hover(
                function () { self._onInsertAfter_hoverin(); },
                function () { self._onInsertAfter_hoverout(); }
            )
            .button({
                icons: {
                    primary: "ui-icon-plus",
                    secondary: "ui-icon-triangle-1-s"
                },
                text: false
            });

        $("button", _toolbarSelector).each(function (index, value) {
            //(ui.item.attr('name') || ui.item.attr('id'))
            $value = $(value);
            $value.click(function (event) {
                $(_toolbarSelector).trigger("selected", event.currentTarget.name || event.currentTarget.id);
                return false;
            });
        });

        _subMenu_InsBef = new SubMenu("#insertbefore", _toolbar);
        _subMenu_InsBef.subscribe("hidden", function () { self._onSubmenuInsBef_hidden(); });
        _subMenu_InsBef.subscribe("shown", function () { self._onSubmenuInsBef_shown(); });
        _subMenu_InsBef.subscribe("selected", function (event, nameOrId) { self._onSubmenuInsBef_selected(event, nameOrId); });

        _subMenu_InsAft = new SubMenu("#insertafter", _toolbar);
        _subMenu_InsAft.subscribe("hidden", function () { self._onSubmenuInsAft_hidden(); });
        _subMenu_InsAft.subscribe("shown", function () { self._onSubmenuInsAft_shown(); });
        _subMenu_InsAft.subscribe("selected", function (event, nameOrId) { self._onSubmenuInsAft_selected(event, nameOrId); });
    };

    this.onToolbar_hoverin = function () {
        self.canHide = false;
        _hideToolbarTask.abort();
    };
    this.onToolbar_hoverout = function () {
        if (!self._pinned) {
            _hideToolbarTask.start();
        }
    };

    this._onInsertBefore_hoverin = function () {
        this._hideAllSubmenus();
        _subMenu_InsBef.show();
    };
    this._onInsertBefore_hoverout = function () {
        _subMenu_InsBef.hide();
    };

    this._onInsertAfter_hoverin = function () {
        this._hideAllSubmenus();
        _subMenu_InsAft.show();
    };
    this._onInsertAfter_hoverout = function () {
        _subMenu_InsAft.hide();
    };

    this._onSubmenuInsBef_selected = function (event, nameOrId) {
        $(_toolbarSelector).trigger("selected", nameOrId);
    };
    this._onSubmenuInsAft_selected = function (event, nameOrId) {
        $(_toolbarSelector).trigger("selected", nameOrId);
    };

    this.setOptions = function (options) {
        if (typeof (options) == "undefined") {
            return;
        }
        $("button", _toolbarSelector).each(function (index, value) {
            var $el = $(value);
            var specifier = options[value.name];
            if (specifier && specifier.visible) {
                $el.show();
            }
            else {
                $el.hide();
            }
        });
    };

    this._onSubmenuInsBef_hidden = function () {
        // Когда submenu невидимо - можно спрятать тулбар
        self._pinned = false;
        // Прячем тулбар, если мышка на него не наведена - надо еще проверить, что не наведена на сам элемента
        if ((!_toolbar.is(":hover")) && _confirmHideCallback && _confirmHideCallback()) {
            _hideToolbarTask.start();
        }
    };

    this._onSubmenuInsBef_shown = function() {
        // Когда submenu видимо - прятать тулбар нельзя
        self._pinned = true;
    };

    this._onSubmenuInsAft_hidden = function () {
        // Когда submenu невидимо - можно спрятать тулбар
        self._pinned = false;
        // Прячем тулбар, если мышка на него не наведена - надо еще проверить, что не наведена на сам элемента
        if ((!_toolbar.is(":hover")) && _confirmHideCallback && _confirmHideCallback()) {
            _hideToolbarTask.start();
        }
    };

    this._onSubmenuInsAft_shown = function () {
        // Когда submenu видимо - прятать тулбар нельзя
        self._pinned = true;
    };

    this._hideAllSubmenus = function() {
        _subMenu_InsBef.hide({ immediate: true });
        _subMenu_InsAft.hide({ immediate: true });
    };

    this.subscribe = function (eventName, handler) {
        $(_toolbarSelector).bind(eventName, handler);
    };

    self.init();
}