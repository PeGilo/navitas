﻿function EditTextDialog(view, options) {
    var 
        self = this,
        _view = view,
        _options = options,
        _contentSelector = "#content",
        _$content = null,
        _$dialog = null,
        _htmlAreaObject = null,
        _bindedEvents = [];

    this.init = function () {

        _$content = $(_contentSelector, _options.dialogSelector);

        $(_options.dialogSelector).dialog({
            autoOpen: false,
            height: $(window).height() - 100, //300,
            width: $(window).width() - 100,
            modal: true,
            buttons: {
                "OK": this.onDialog_ok,
                "Отмена": function () {
                    $(this).dialog("close");
                }
            },
            open: function (event, ui) {
                var dialogContent = _$content; // $("#content", _options.dialogSelector);
                dialogContent.width($(this).width() - 0);
                dialogContent.height($(this).height() - 80);
            },
            close: this.onDialog_close,
            resizeStop: function (event, ui) {
                self.onResizeStop(event, ui);
            }
        });
    };

    this.show = function (text, eventHandlers) {

        $content = $(_contentSelector, _options.dialogSelector)
        $content.val(text);
        $(_options.dialogSelector).dialog("open");

        _htmlAreaObject = $content.htmlarea({
            css: '/Content/jHtmlArea.Editor.css',
            toolbar: [
                     ["bold", "italic", "underline", "strikethrough", "|", "subscript", "superscript", "|", "p"],
                     ["foreColor"],
                     ["increasefontsize", "decreasefontsize"],
                     ["orderedlist", "unorderedlist"],
                     ["justifyleft", "justifycenter", "justifyright"],
                     ["cut", "copy", "paste"]
                     ],

//        ["html"], ["bold", "italic", "underline", "strikethrough", "|", "subscript", "superscript"],
//        ["increasefontsize", "decreasefontsize"],
//        ["orderedlist", "unorderedlist"],
//        ["indent", "outdent"],
//        ["justifyleft", "justifycenter", "justifyright"],
//        ["link", "unlink", "image", "horizontalrule"],
//        ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
//        ["cut", "copy", "paste"]
            toolbarText: {
                bold: "Полужирный", italic: "Курсив", underline: "Подчеркнутый", strikethrough: "Зачеркнутый",
                cut: "Вырезать", copy: "Копировать", paste: "Вставить",
                h1: "Heading 1", h2: "Heading 2", h3: "Heading 3", h4: "Heading 4", h5: "Heading 5", h6: "Heading 6", p: "Абзац",
                indent: "Indent", outdent: "Outdent", horizontalrule: "Insert Horizontal Rule",
                justifyleft: "Выровнять текст по левому краю", justifycenter: "Выровнять текст по центру", justifyright: "Выровнять текст по правому краю",
                increasefontsize: "Увеличить размер шрифта", decreasefontsize: "Уменьшить размер шрифта", forecolor: "Цвет текста",
                link: "Insert Link", unlink: "Remove Link", image: "Insert Image",
                orderedlist: "Начало нумерованного списка", unorderedlist: "Начало маркированного списка",
                subscript: "Подстрочный текст", superscript: "Надстрочный текст",
                html: "Показать/Скрыть исходный код HTML"
            }
        });

        // binding events
        if (eventHandlers) {
            for (var key in eventHandlers) {
                this.subscribe(key, eventHandlers[key]);
                _bindedEvents.push(key);
            }
        }

    };

    // Выполняется в контексте диалога
    this.onDialog_close = function () {

        self.updateValidationMessage("");

        //var htmlAreaObject = $(this.options.dialogContentSelector);
        //if (htmlAreaObject != null) {
        //htmlAreaObject[0].jhtmlareaObject.dispose();
        //}
        if (_htmlAreaObject) {
            _htmlAreaObject[0].jhtmlareaObject.dispose();
        }

        $(this).trigger("dialogclosing");

        // unbindevents
        for (var i = 0; i < _bindedEvents.length; i++) {
            self.unsubscribe(_bindedEvents[i]);
        }
        _bindedEvents = [];
    };

    // Выполняется в контексте диалога
    this.onDialog_ok = function () {
        var bValid = true;

        var dialogContent = _$content; // $("#content");
        dialogContent.removeClass("ui-state-error");

        bValid = bValid && self.checkLength(dialogContent, "текста", 0, 5000);

        if (bValid) {
            var updatedText = dialogContent.val();
            $(this).trigger("dialogokvalidated", updatedText);
            $(this).dialog("close");
        }
    };

    this.onResizeStop = function (event, ui) {
        $("div.jHtmlArea").width(ui.size.width - 30).height(ui.size.height - 180);
        $("iframe", "div.jHtmlArea").width(ui.size.width - 30).height(ui.size.height - 180);
    };


    // Валидация
    this.updateValidationMessage = function (t) {
        if (typeof (t) == 'string' && t.length > 0) {
            $("#dialog-validationMessage")
				    .text(t)
                    .removeClass("hidden")
				    .addClass("ui-state-highlight");
        }
        else {
            $("#dialog-validationMessage")
                    .text("")
                    .addClass("hidden");
        }

        //            setTimeout(function () {
        //                $(".validateTips").removeClass("ui-state-highlight", 1500);
        //            }, 500);
    };

    this.checkLength = function (o, n, min, max) {
        if (o.val().length > max || o.val().length < min) {
            o.addClass("ui-state-error");
            this.updateValidationMessage("Длина " + n + " не должна превышать " + max + ".");
            return false;
        } else {
            return true;
        }
    };

    this.subscribe = function (eventName, handler) {
        $(_options.dialogSelector).bind(eventName, handler);
    };

    this.unsubscribe = function (eventName) {
        $(_options.dialogSelector).unbind(eventName);
    };

    self.init();
}