﻿function SubMenu(menuSelector, $dockTo) {
    var 
        self = this,
        _menuSelector = menuSelector,
        _$dockTo = $dockTo,
        _menu = null,
        _hideTask = null;

    this.init = function () {
        _menu = $(_menuSelector)
                .hover(
                    function () { self.onHoverIn(); },
                    function () { self.onHoverOut(); }
                )
                .hide().menu({
                    selected: function (event, ui) {
                        //$(this).hide();
                        //$("#log").append("<div>Selected " + ui.item.text() + "</div>");
                        $(_menuSelector).trigger("selected", (ui.item.attr('name') || ui.item.attr('id')));
                    }
                    //,input: $(this)
                });

        _hideTask = new DefferedTask(function () {
            $(_menuSelector).hide();

            // event
            $(_menuSelector).trigger("hidden");
        });
    };

    this.onHoverIn = function () {
        _hideTask.abort();
    };

    this.onHoverOut = function () {
        _hideTask.start();
    };

    this.show = function () {
        _hideTask.abort();

        $(_menuSelector).show().css({ top: 0, left: 0 })
                                .position({
                                    my: "right top",
                                    at: "right bottom",
                                    of: _$dockTo
                                });

        $(_menuSelector).trigger("shown");
    };

    this.hide = function (options) {
        if (options && options.immediate) {
            _hideTask.abort();
            $(_menuSelector).hide();
        }
        else {
            _hideTask.start();
        }
    };

    this.subscribe = function (eventName, handler) {
        $(_menuSelector).bind(eventName, handler);
    };

    self.init();
}
