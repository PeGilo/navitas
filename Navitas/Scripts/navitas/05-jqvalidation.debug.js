﻿// Собирает информацию о валидности прикрепленных к элементы валидирующих функций
// Возвращает структуру, например { required: false, __dummy__: true }
// source: http://stackoverflow.com/questions/7648727/jquery-validate-check-which-rule-is-not-fullfiled
$.validator.prototype.ruleValidationStatus = function (element) {
    element = $(element)[0];
    var rules = $(element).rules();
    var errors = {};
    for (var method in rules) {
        var rule = { method: method, parameters: rules[method] };
        try {
            var result = $.validator.methods[method].call(this, element.value.replace(/\r/g, ""), element, rule.parameters);

            errors[rule.method] = result;

        } catch (e) {
            console.log(e);
        }
    }
    return errors;
};

// Добавить валидириующие методы и адаптеры для валидатора WeakRequired
(function ($) {
    // element = <input ...
    $.validator.addMethod("jqweakrequired", function (value, element, params) {
        return (typeof (value) == 'string' && value.length > 0);
    });

    // Adds a new adapter to convert unobtrusive HTML into a jQuery Validate validation.
    // data-val-weakrequired
    // params: [data-val-weakrequired-param1, data-val-weakrequired-param2,...]
    // function - The function to call, which adapts the values from the HTML
    // attributes into jQuery Validate rules and/or messages.
    $.validator.unobtrusive.adapters.add("weakrequired", [], function (options) {

        options.rules["jqweakrequired"] = {};  // element becomes "params" in callback
        if (options.message) {
            options.messages["jqweakrequired"] = options.message;
        }
    });

    $.validator.addMethod("jqhyphennumber", function (value, element, params) {

        if (typeof (value) == 'string' && value.length > 0) {
            var number = Globalize.parseFloat(value);
            if (value === '-' || !isNaN(number)) {
                return true;
            }
        }
        return false;
    });

    // Adds a new adapter to convert unobtrusive HTML into a jQuery Validate validation.
    // data-val-hyphennumber
    $.validator.unobtrusive.adapters.add("hyphennumber", [], function (options) {

        options.rules["jqhyphennumber"] = {};  // element becomes "params" in callback
        if (options.message) {
            options.messages["jqhyphennumber"] = options.message;
        }
    });

    $.validator.addMethod("jqhypheninteger", function (value, element, params) {

        var intRegex = /^\d+$/;
        if (typeof (value) == 'string' && value.length > 0) {

            if (value === '-' || intRegex.test(value)) {
                return true;
            }
        }
        return false;
    });

    // Adds a new adapter to convert unobtrusive HTML into a jQuery Validate validation.
    // data-val-hypheninteger
    $.validator.unobtrusive.adapters.add("hypheninteger", [], function (options) {

        options.rules["jqhypheninteger"] = {};  // element becomes "params" in callback
        if (options.message) {
            options.messages["jqhypheninteger"] = options.message;
        }
    });

} (jQuery));