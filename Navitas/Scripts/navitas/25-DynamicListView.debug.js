﻿Navitas.DynamicListView = function () {

    // Для обработчиков событий
    var self = this;

    this.containerId = "";
    this.formId = "";
    this.rowSelector = "";
    this.numberContainerSelector = "";
    this.urlNewUnit = "";
    //this.addButtonId = "";
    this.removeButtonSelector = "";


};

// Унаследоваться от базового представления
extend(Navitas.DynamicListView, Navitas.ViewBase);

// Переопределить функции

Navitas.DynamicListView.prototype.vbase_onLoad = Navitas.ViewBase.prototype.onLoad;
Navitas.DynamicListView.prototype.onLoad = function () {
    this.vbase_onLoad();
    //this.initControls("#" + this.containerId);
    this.initControls();
};

Navitas.DynamicListView.prototype.vbase_onUnload = Navitas.ViewBase.prototype.onUnload;
Navitas.DynamicListView.prototype.onUnload = function () {
    this.vbase_onUnload();

};

// Инициализует элементы формы при загрузке
Navitas.DynamicListView.prototype.vbase_initControls = Navitas.ViewBase.prototype.initControls;
Navitas.DynamicListView.prototype.initControls = function (container) {

    this.vbase_initControls(container);
};

// Переписывает индексы элементов в строках после изменения списка, например, после удаления,
// какого либо элемента
Navitas.DynamicListView.prototype.reindexItems = function () {

    var rows = $(this.rowSelector);

    this.reindexElementCollection(rows);

    // Так же перенумеровать порядковые номера строк
    for (var i = 0; i < rows.size(); i++) {
        var row = rows[i];
        $(row).find(this.numberContainerSelector).each(function (index, el) {
            el.innerHTML = i + 1;
        });
    }
};

Navitas.DynamicListView.prototype.addItem = function () {

    var indexValue = $(this.rowSelector).size();

    $.ajax({
        type: 'GET',
        url: this.urlNewUnit,
        data: { index: indexValue },
        dataType: 'html',
        context: this, // Передать контекст для обработчиков success, error, ...
        beforeSend: function (jqXHR, settings) {
            this.beforeRequestAddItem();
        },
        success: function (data, textStatus, jqXHR) {
            //$('#' + this.containerId).append(data);
            this.attachItemToDom(data);

            this.afterItemAdded();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            this.showErrorMessage(Navitas.resources.ajaxErrorMessage);
        },
        complete: function (jqXHR, textStatus) {
            this.afterRequestAddItem();
        }
    });
};

// Вставляет новый элемент списка в DOM формы
Navitas.DynamicListView.prototype.attachItemToDom = function (data) {
    $('#' + this.containerId).append(data);
};

// Фукнция вызываемая перед отправкой запроса на добавление элемента
Navitas.DynamicListView.prototype.beforeRequestAddItem = function () {

};

// Функция, вызываемы после выполнения запроса на добавление элемента
Navitas.DynamicListView.prototype.afterRequestAddItem = function () {

};

// Функция вызываемая после добавления нового элемента
Navitas.DynamicListView.prototype.afterItemAdded = function () {

    this.initControls($(this.rowSelector).last());
    this.afterDomChanged();
};

// Обработчик события нажатия на кнопку удаления элемента
Navitas.DynamicListView.prototype.removeItem = function (button) {
    $(button).closest(this.rowSelector).remove();

    this.afterItemRemoved();
};


// Функция вызываемая после удаления элемента
Navitas.DynamicListView.prototype.afterItemRemoved = function () {
    this.reindexItems();
    this.afterDomChanged();
};

// Функция, вызываемая при изменении DOM формы
Navitas.DynamicListView.prototype.afterDomChanged = function () {
    this.updateFormValidation("#" + this.formId);
};