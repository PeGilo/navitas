﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using Navitas.Report.Elements;

namespace Navitas.Report
{
    public class Section
    {
        [XmlIgnore]
        public int SectionId { get; set; }

        [XmlIgnore]
        public int InspectionId { get; set; }

        [XmlIgnore]
        public int SequenceNumber { get; set; }

        [XmlIgnore]
        public System.Data.Linq.Binary TimeStamp { get; set; }

        [XmlArrayItem(typeof(EditableTextElement))]
        [XmlArrayItem(typeof(ImageElement))]
        [XmlArrayItem(typeof(IllustrationPlaceElement))]
        [XmlArrayItem(typeof(ReadonlyTextElement))]
        [XmlArrayItem(typeof(RequiredImageElement))]
        [XmlArrayItem(typeof(TableElement))]
        [XmlArrayItem(typeof(ChartElement))]
        [XmlArrayItem(typeof(ChartStackElement))]
        [XmlArrayItem(typeof(SectionTitleElement))]
        [XmlArrayItem(typeof(StaticImageElement))]
        [XmlArrayItem(typeof(SubsectionTitleElement))]
        [XmlArrayItem(typeof(FormulaImageElement))]
        public List<BaseElement> Elements { get; set; }

        public Section()
        {
            Elements = new List<BaseElement>();
        }

        public EditableTextElement UpdateEditableTextElement(int elementId, string text)
        {
            foreach (BaseElement el in Elements)
            {
                if (el is EditableTextElement)
                {
                    EditableTextElement textElement = el as EditableTextElement;
                    if (textElement.Id == elementId)
                    {
                        textElement.Content = text;
                        return textElement;
                    }
                }
            }
            throw new InvalidOperationException("Не найден EditableTextElement с заданным идентификатором");
        }

        public void HideEditableTextElement(int elementId)
        {
            foreach (BaseElement el in Elements)
            {
                if (el is EditableTextElement)
                {
                    EditableTextElement textElement = el as EditableTextElement;
                    if (textElement.Id == elementId)
                    {
                        textElement.Hidden = true;
                        return;
                    }
                }
            }
            throw new InvalidOperationException("Не найден EditableTextElement с заданным идентификатором");
        }

        //public void HideRequiredImageElement(int elementId)
        //{
        //    foreach (BaseElement el in Elements)
        //    {
        //        if (el is RequiredImageElement)
        //        {
        //            RequiredImageElement element = el as RequiredImageElement;
        //            if (element.Id == elementId)
        //            {
        //                element.Hidden = true;
        //                return;
        //            }
        //        }
        //    }
        //    throw new InvalidOperationException("Не найден RequiredImageElement с заданным идентификатором");
        //}

        //public void RemoveImageElement(int elementId)
        //{
        //    for (int i = 0; i < Elements.Count; i++)
        //    {
        //        if (Elements[i].Id == elementId && Elements[i] is ImageElement)
        //        {
        //            Elements.RemoveAt(i);
        //            return;
        //        }
        //    }

        //    throw new InvalidOperationException("Не найден ImageElement с заданным идентификатором");
        //}

        public void HideElement(int elementId)
        {
            foreach (BaseElement el in Elements)
            {
                if (el.Id == elementId)
                {
                    el.Hidden = true;
                    return;
                }
            }
            throw new InvalidOperationException("Не найден RequiredImageElement с заданным идентификатором");
        }

        public ImageElement InsertImage(int anchorElementId, int newImageElementId, string newImageFileName, string newImageUserTitle, bool before)
        {
            for (int i = 0; i < Elements.Count; i++)
            {
                if (Elements[i].Id == anchorElementId)// && Elements[i] is EditableTextElement)
                {
                    //EditableTextElement textElement = Elements[i] as EditableTextElement;

                    ImageElement newImage = new ImageElement();
                    newImage.Id = newImageElementId;
                    newImage.UserTitle = newImageUserTitle;
                    newImage.FileName = newImageFileName;

                    Elements.Insert((before ? i : i + 1), newImage);
                    return newImage;
                }
            }
            throw new InvalidOperationException("Не найден EditableTextElement с заданным идентификатором");
        }

        public EditableTextElement InsertText(int anchorElementId, int newElementId, string content, bool before)
        {
            for (int i = 0; i < Elements.Count; i++)
            {
                if (Elements[i].Id == anchorElementId)// && Elements[i] is EditableTextElement)
                {
                    EditableTextElement newEditableText = new EditableTextElement();
                    newEditableText.Content = content;
                    newEditableText.Id = newElementId;
                    newEditableText.Hidden = false;

                    Elements.Insert((before ? i : i + 1), newEditableText);
                    return newEditableText;
                }
            }
            throw new InvalidOperationException("Не найден элемент с заданным идентификатором");
        }

        public int GenereateNewElementId()
        {
            // Максимальный индекс увеличивается на 1
            int newId = (from e in Elements
                         select e.Id).Max() + 1;
            return newId;
        }

    }
}