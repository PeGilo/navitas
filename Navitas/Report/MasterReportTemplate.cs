﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Navitas.Report
{
    [XmlRoot(ElementName="Section")]
    public class MasterReportTemplate
    {
        //[XmlIgnore]
        public int MasterReportTemplateId { get; set; }

        //[XmlIgnore]
        public int SectionId { get; set; }

        //[XmlIgnore]
        public int SequenceNumber { get; set; }

        public System.Xml.Linq.XElement Content { get; set; }
        
        //// Объекты из InspectionReport таблицы, возможно необходимы свои объекты для элементов раздела
        //[XmlArrayItem(typeof(Report.Elements.EditableTextElement))]
        //[XmlArrayItem(typeof(Report.Elements.ImageElement))]
        //public List<Report.Elements.BaseElement> Elements { get; set; }

        //[XmlAttribute]
        public string RepeatOn { get; set; }

        //[XmlIgnore]
        public bool IsRepeatable { get { return !String.IsNullOrEmpty(RepeatOn); } }

        public MasterReportTemplate()
        {
        }

        public Models.InspectionReport Instantiate(int index)
        {
            Models.InspectionReport inspectionReport = new Models.InspectionReport();

            inspectionReport.SequenceNumber = SequenceNumber;
            inspectionReport.Content = System.Xml.Linq.XElement.Parse(Content.ToString().Replace("%INDEX%", index.ToString()));

            return inspectionReport;
        }

        public Models.InspectionReport Instantiate()
        {
            Models.InspectionReport inspectionReport = new Models.InspectionReport();
            
            inspectionReport.SequenceNumber = SequenceNumber;
            inspectionReport.Content = Content;

            return inspectionReport;
        }
    }
}