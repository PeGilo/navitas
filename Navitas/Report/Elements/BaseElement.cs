﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Report.Elements
{
    public abstract class BaseElement : ICloneable
    {
        public virtual int Id { get; set; }
        public virtual bool Hidden { get; set; }
        public abstract object Clone();
    }
}