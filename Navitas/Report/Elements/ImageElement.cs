﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Report.Elements
{
    public class ImageElement : BaseElement
    {
        //public string ImageUrl { get; set; }
        public string FileName { get; set; }

        public string UserTitle { get; set; }

        ///// <summary>
        ///// Используется в MasterReportTemplate, но не используется в InspectionReport.
        ///// TODO: Разделить эти классы (BaseLement и т.д.)
        ///// </summary>
        //public string SelectionStatementConstraint { get; set; }

        public override object Clone()
        {
            return new ImageElement() { Id = Id, FileName = FileName, UserTitle = UserTitle };
        }
    }
}