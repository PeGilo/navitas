﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Report.Elements
{
    public class IllustrationPlaceElement : BaseElement
    {
        public string Comment { get; set; }

        public override object Clone()
        {
            return new IllustrationPlaceElement() { Id = Id, Comment = Comment };
        }
    }
}