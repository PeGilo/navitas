﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Report.Elements
{
    public class FormulaImageElement : BaseElement
    {
        public string FileName { get; set; }

        public string UserTitle { get; set; }

        public override object Clone()
        {
            return new FormulaImageElement() { Id = Id, FileName = FileName, UserTitle = UserTitle };
        }
    }
}