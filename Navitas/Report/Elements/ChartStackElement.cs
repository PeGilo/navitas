﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Report.Elements
{
    public class ChartStackElement : BaseElement
    {
        public string ChartTitle { get; set; }

        //public string XAxisTitle { get; set; }

        //public string YAxisTitle { get; set; }

        public string DataContent { get; set; }

        public override object Clone()
        {
            return new ChartStackElement() { Id = Id, ChartTitle = ChartTitle, /*XAxisTitle = XAxisTitle, YAxisTitle = YAxisTitle, */DataContent = DataContent };
        }

        ///// <summary>
        ///// Разделяет на отдельные ChartElement добавлением к тексту доступа к массиву [индекс]
        ///// </summary>
        ///// <returns></returns>
        //public BaseElement[] Split()
        //{
        //    throw new NotImplementedException();
        //}
    }
}