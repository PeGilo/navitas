﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Report.Elements
{
    public class RequiredImageElement : BaseElement
    {
        public string Content { get; set; }

        public override object Clone()
        {
            return new RequiredImageElement() { Id = Id, Hidden = Hidden, Content = Content };
        }
    }
}