﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Navitas.Report.Elements
{
    public class TableElement : BaseElement
    {
        public string TableName { get; set; }

        public string Content { get; set; }

        public override object Clone()
        {
            return new TableElement() { Id = Id, TableName = TableName, Content = Content };
        }
    }
}