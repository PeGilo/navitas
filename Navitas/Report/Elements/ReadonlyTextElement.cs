﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Navitas.Report.Elements
{
    public class ReadonlyTextElement : BaseElement
    {
        public string Content { get; set; }

        public override object Clone()
        {
            return new ReadonlyTextElement() { Id = Id, Hidden = Hidden, Content = Content };
        }

    }
}