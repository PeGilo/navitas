﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Navitas.Report.Elements
{
    public class SectionTitleElement : BaseElement
    {
        public string Text { get; set; }

        public bool Numbered { get; set; }

        public override object Clone()
        {
            return new SectionTitleElement() { Id = Id, Hidden = Hidden, Numbered = Numbered, Text = Text };
        }
    }
}