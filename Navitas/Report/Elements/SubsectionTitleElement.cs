﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Navitas.Report.Elements
{
    public class SubsectionTitleElement : BaseElement
    {
        public string Text { get; set; }

        public int? Level { get; set; }

        public SubsectionTitleElement()
        {
            Level = 2; // by default
        }

        public override object Clone()
        {
            return new SubsectionTitleElement() { Id = Id, Hidden = Hidden, Text = Text, Level = Level };
        }
    }
}