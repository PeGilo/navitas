﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Navitas.Report.Elements
{
    public class EditableTextElement : BaseElement
    {
        public string Content { get; set; }

        //public string EncodedContent
        //{
        //    get
        //    {
        //        return Content.Replace("@", "@@");
        //        //return Content.Replace("@", "&#64;");
        //    }
        //}

        public override object Clone()
        {
            return new EditableTextElement() { Id = Id, Hidden = Hidden, Content = Content };
        }

    }
}