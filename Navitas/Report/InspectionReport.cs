﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Report
{
    public class InspectionReport
    {
        public List<Section> Sections { get; set; }

        public InspectionReport()
        {
            Sections = new List<Section>();
        }
    }
}