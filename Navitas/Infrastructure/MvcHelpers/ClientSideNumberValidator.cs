﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace Navitas.Infrastructure.MvcHelpers
{
    public class ClientSideNumberValidator : ModelValidator
    {
        public ClientSideNumberValidator(ModelMetadata metadata,
            ControllerContext controllerContext)
            : base(metadata, controllerContext) { }

        public override IEnumerable<ModelValidationResult> Validate(object container)
        {
            yield break; // Do nothing for server-side validation 
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            yield return new ModelClientValidationRule
            {
                ValidationType = "number",
                ErrorMessage = string.Format(CultureInfo.CurrentCulture,
                                             Navitas.Resources.ValidationStrings.MustBeNumber,
                                             Metadata.GetDisplayName())
            };
        }
    }
}