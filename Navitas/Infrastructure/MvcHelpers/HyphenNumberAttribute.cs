﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace Navitas.Infrastructure.MvcHelpers
{
    /// <summary>
    /// Валидирующий атрибут, применяющийся для строк, которые могут содержать число с плавающей запятой, а также символ "-",
    /// означающий 0. 
    /// </summary>
    /// <remarks>Связан с javascript-валидацией на клиенте</remarks>

    public class HyphenNumberAttribute : ValidationAttribute, IClientValidatable
    {
        public HyphenNumberAttribute()
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ValidationType = "hyphennumber";

            if(!String.IsNullOrEmpty(ErrorMessage))
            {
                rule.ErrorMessage = ErrorMessage;
            }
            else if(!String.IsNullOrEmpty(ErrorMessageResourceName) && ErrorMessageResourceType != null)
            {
                rule.ErrorMessage = ResourceHelper.LookupResource(ErrorMessageResourceType, ErrorMessageResourceName);
            };
            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // Проверить по свойствам объекта, необходимо ли валидировать его свойства
            IDataAnnotationsSwitch daSwitch = validationContext.ObjectInstance as IDataAnnotationsSwitch;
            if (daSwitch != null && !daSwitch.CheckValidationAttributes)
            {
                return null;
            }

            if (value != null && value is String)
            {
                String s = (String)value;
                if (s.All(c => Char.IsDigit(c) || System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator.Contains(c))
                    || s == "-")
                {
                    return null;
                }
            }

            return new ValidationResult(ErrorMessage);
        }
    }
}