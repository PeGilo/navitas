﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

namespace Navitas.Infrastructure.MvcHelpers
{
    public class LocalizedDescriptionAttribute : DescriptionAttribute
    {
        private PropertyInfo _nameProperty;
        private Type _resourceType;

        public LocalizedDescriptionAttribute(string descriptionKey)
            : base(descriptionKey)
        {
        }

        public Type NameResourceType
        {
            get
            {
                return _resourceType;
            }

            set
            {
                _resourceType = value;

                //initialize nameProperty when type property is provided by setter
                _nameProperty = _resourceType.GetProperty(base.Description, BindingFlags.Static | BindingFlags.Public);
            }
        }

        public override string Description
        {
            get
            {
                //check if nameProperty is null and return original display name value
                if (_nameProperty == null)
                {
                    return base.Description;
                }

                return (string)_nameProperty.GetValue(_nameProperty.DeclaringType, null);
            }
        }
    }

}