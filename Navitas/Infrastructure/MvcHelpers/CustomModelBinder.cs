﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Navitas.Infrastructure.MvcHelpers
{
    public class CustomModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            // В случае, если в POST'е происходит связывание с элементом базового класса,
            // необходимо создать элемент необходимого класса-наследника и связывать данные с ним.
            if (modelType.Equals(typeof(ViewModels.Building.Base.BaseItemEditModel)))
            {
                string typeName = (string)bindingContext.ValueProvider.GetValue("ItemsTypeName").ConvertTo(typeof(string));

                Type instantiationType = Type.GetType(typeName);

                var obj = Activator.CreateInstance(instantiationType);
                bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(null, instantiationType);
                bindingContext.ModelMetadata.Model = obj;
                return obj;
            }
            return base.CreateModel(controllerContext, bindingContext, modelType);
        }
    }
}