﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Navitas.Infrastructure.MvcHelpers
{
    /// <summary>
    /// Атрибут для валидации, который не является обязательным.
    /// На клиенте его нарушение приведет к появлению сообщения.
    /// На сервере его валидность не проверяется.
    /// </summary>
    /// <remarks>Так же см. скрипт jqvalidation.js</remarks>
    public class WeakRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        public WeakRequiredAttribute()
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = ErrorMessage,
                ValidationType = "weakrequired",
            };

            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // На сервере всегда является валидным
            return null;
        }
    }

}