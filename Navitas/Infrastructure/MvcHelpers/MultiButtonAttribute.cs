﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace Navitas.Infrastructure.MvcHelpers
{
    /// <summary>
    /// Аттрибут для декларативного определения какая кнопка была нажата на форме.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class MultiButtonAttribute : ActionNameSelectorAttribute
    {
        /// <summary>
        /// Задает значение, по которому ведется точный поиск ключа в параметрах запроса
        /// </summary>
        public string MatchFormKey { get; set; }

        /// <summary>
        /// Задает регулярное выражение, по которому ведется поиск ключа в параметрах запроса
        /// </summary>
        public string RegexFormKey { get; set; }

        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            if (!String.IsNullOrEmpty(MatchFormKey))
            {
                return HasMatchKey(controllerContext, MatchFormKey);
            }
            else if (!String.IsNullOrEmpty(RegexFormKey))
            {
                return HasRegexMatchKey(controllerContext, RegexFormKey);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Определяет есть ли в запросе параметр с заданным ключом.
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        protected virtual bool HasMatchKey(ControllerContext controllerContext, string key)
        {
            return controllerContext.HttpContext.Request[key] != null;
        }
        
        /// <summary>
        /// Определяет есть ли в запросе параметр совпадающий с указанным регулярным выражением.
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        protected virtual bool HasRegexMatchKey(ControllerContext controllerContext, string pattern)
        {
            Regex regex = new Regex(pattern);

            string res = (from item in controllerContext.HttpContext.Request.Form.AllKeys
                          where regex.IsMatch(item)
                          select item).FirstOrDefault();
            return (res != null);
        }
    }
}