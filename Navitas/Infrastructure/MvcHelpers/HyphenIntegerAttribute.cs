﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace Navitas.Infrastructure.MvcHelpers
{
    /// <summary>
    /// Интерфейс реализуется моделями представления (view models) для изменения поведения
    /// __custom'ных__ валидирующих атрибутов.
    /// </summary>
    public interface IDataAnnotationsSwitch
    {
        /// <summary>
        /// Передает информацию валидирующему атрибуту, что валидация объекта не требуется.
        /// </summary>
        /// <remarks>Работает только для тех, валидирующих атрибутов, в которых есть проверка этого интерфейса.</remarks>
        bool CheckValidationAttributes { get; }
    }

    /// <summary>
    /// Валидирующий атрибут, применяющийся для строк, которые могут содержать ЦЕЛОЕ число, а также символ "-",
    /// означающий 0. 
    /// </summary>
    /// <remarks>Связан с javascript-валидацией на клиенте</remarks>
    public class HyphenIntegerAttribute : ValidationAttribute, IClientValidatable
    {
        public HyphenIntegerAttribute()
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ValidationType = "hypheninteger";

            if(!String.IsNullOrEmpty(ErrorMessage))
            {
                rule.ErrorMessage = ErrorMessage;
            }
            else if(!String.IsNullOrEmpty(ErrorMessageResourceName) && ErrorMessageResourceType != null)
            {
                rule.ErrorMessage = ResourceHelper.LookupResource(ErrorMessageResourceType, ErrorMessageResourceName);
            };

            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // Проверить по свойствам объекта, необходимо ли валидировать его свойства
            IDataAnnotationsSwitch daSwitch = validationContext.ObjectInstance as IDataAnnotationsSwitch;
            if (daSwitch != null && !daSwitch.CheckValidationAttributes)
            {
                return null;
            }

            if (value != null && value is String)
            {
                String s = (String)value;
                if (s.All(c => Char.IsDigit(c)) || s == "-")
                {
                    return null;
                }
            }

            return new ValidationResult(ErrorMessage);
        }

        

    }
}