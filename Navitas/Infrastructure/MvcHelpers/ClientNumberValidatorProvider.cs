﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace Navitas.Infrastructure.MvcHelpers
{
    public class ClientNumberValidatorProvider : ClientDataTypeModelValidatorProvider
    {
        public override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata,
                                                               ControllerContext context)
        {
            bool isNumericField = base.GetValidators(metadata, context).Any();
            if (isNumericField)
                yield return new ClientSideNumberValidator(metadata, context);
        }
    }
}