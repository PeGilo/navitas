﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RazorEngine.Templating;

namespace Navitas.Infrastructure.RazorHelpers
{
    using BL = Soft33.EnergyPassport.V2.BL;
    using BLExt = Soft33.EnergyPassport.V2.BL.Extended;

    public abstract class RazorCustomTemplateBase<T> : TemplateBase<T>
    {
        public string ToUpperCase(string name)
        {
            return name.ToUpper();
        }

        public string IllustrationUrl(string fileName)
        {
            return "/file/illustration/" + fileName;
        }

        public string StaticImageUrl(string fileName)
        {
            return "/content/images/" + fileName;
        }

        /// <summary>
        /// Возвращает наименование css класса для указанного типа строки в таблице.
        /// </summary>
        /// <param name="rowType"></param>
        /// <returns></returns>
        public string RowTypeClass(Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTableRowType rowType)
        {
            if (rowType == BL.TemplateCalculations.TemplateTableRowType.Header)
            {
                return "rowtype-header";
            }
            else if (rowType == BL.TemplateCalculations.TemplateTableRowType.Data)
            {
                return "rowtype-data";
            }
            else if (rowType == BL.TemplateCalculations.TemplateTableRowType.Footer)
            {
                return "rowtype-footer";
            }
            else if (rowType == BL.TemplateCalculations.TemplateTableRowType.DataBold)
            {
                return "rowtype-data bold";
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Оставляет 2 цифры после запятой
        /// </summary>
        /// <returns></returns>
        public string FormatDouble2(object d)
        {
            if (d == null)
            {
                return String.Empty;
            }

            if (d is double || d is float)
            {
                return FormatDouble2((double)d);
            }
            else
            {
                return d.ToString();
            }
        }

        public string FormatDouble2(double d)
        {
            if (!Double.IsNaN(d) && !Double.IsInfinity(d))
            {
                return d.ToString("0.00"); //((double)d).ToString("0.##");
            }
            else
            {
                return "-";
            }
        }

        public string FormatDouble(object d, int digits)
        {
            if (d == null)
            {
                return String.Empty;
            }

            if (d is double || d is float)
            {
                return FormatDouble((double)d, digits);
            }
            else
            {
                return d.ToString();
            }
        }

        public string FormatDouble(double d, int digits)
        {
            if (!Double.IsNaN(d) && !Double.IsInfinity(d))
            {
                return d.ToString("0." + new String('#', digits));// "0" "00"); //((double)d).ToString("0.##");
            }
            else
            {
                return "-";
            }
        }

        /// <summary>
        /// Выбирает форматирование по типу данных
        /// </summary>
        /// <returns></returns>
        public string CommonFormat(object o)
        {
            if (o == null)
            {
                return String.Empty;
            }

            if ((o is double || o is float))
            {
                return FormatDouble2((double)o);
            }
            else
            {
                return o.ToString();
            }
        }

        public System.Web.Mvc.MvcHtmlString Chart(BL.Charts.TemplateChartData chartData)
        {
            string fileName = "/file/chart/" + chartData.ImageFileName;
            return new System.Web.Mvc.MvcHtmlString("<img src=\"" + fileName + "\" alt=\"\" />");
        }

        public System.Web.Mvc.MvcHtmlString ChartStack(IEnumerable<BL.Charts.TemplateChartData> chartData)
        {
            string s = "";
            
            foreach (var chart in chartData)
            {
                string fileName = "/file/chart/" + chart.ImageFileName;
                s += "<img src=\"" + fileName + "\" alt=\"\" /><br/>";
            }

            return new System.Web.Mvc.MvcHtmlString(s);
        }
    }
}