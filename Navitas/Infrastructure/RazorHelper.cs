﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Infrastructure.RazorHelpers;

namespace Navitas.Infrastructure
{
    public class RazorHelper
    {
        public RazorHelper()
        {
        }

        public string Parse<T>(string template, T model)
        {
            RazorEngine.Razor.SetTemplateBase(typeof(RazorCustomTemplateBase<>));
            return RazorEngine.Razor.Parse<T>(template.Replace("&quot;", "\""), model);
        }
    }
}