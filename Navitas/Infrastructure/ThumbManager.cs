﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;

using Navitas.Infrastructure.Environment;

namespace Navitas.Infrastructure
{
    public class ThumbManager
    {
        private AppEnvironment _environment;

        public ThumbManager(AppEnvironment env)
        {
            _environment = env;
        }

        //public bool Exists(string imageName, int w, int h)
        //{
        //    string imageFullPath = Path.Combine(_environment.AppSettings.ThumbFolder, GenerateThumbFileName(imageName, w, h));
        //    return File.Exists(imageFullPath);
        //}

        /// <summary>
        /// Returns full path for thumbnail. Can return null.
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns>If not exists returns null</returns>
        public string GetThumb(string imageName, int w, int h)
        {
            string imageFullPath = Path.Combine(_environment.AppSettings.ThumbFolder, GenerateThumbFileName(imageName, w, h));

            return File.Exists(imageFullPath) ? imageFullPath : null;
        }

        public string CreateThumb(string imageName, int w, int h)
        {
            // Check if the folder exists
            if (!Directory.Exists(_environment.AppSettings.ThumbFolder))
            {
                Directory.CreateDirectory(_environment.AppSettings.ThumbFolder);
            }

            //string sourceFullPath = Path.Combine(_environment.AppSettings.UploadFolder, imageName);
            string sourceFullPath = (new FileManager(_environment)).GetInspectionIllustrationFullPath(imageName);

            string thumbFullPath = Path.Combine(_environment.AppSettings.ThumbFolder, GenerateThumbFileName(imageName, w, h));

            return ImageProcessor.SaveConverted(sourceFullPath, thumbFullPath, 80, w, h);
        }

        protected virtual string GenerateThumbFileName(/*int inspectionId,*/ string shortfileName, int w, int h)
        {
            // 1. Replace ext by "jpg"
            //string ext = Path.GetExtension(fileName).TrimStart(new char[] { '.' });

            return /*inspectionId.ToString() + "_" + */ "thumb_" + "_w" + w.ToString() + "_h" + h.ToString() + "_"
                + Path.GetFileNameWithoutExtension(shortfileName) + ".jpg";
        }

        //protected virtual int extractInspectionId(string thumbShortName)
        //{
        //    int inspectionId;
        //    string[] splits = thumbShortName.Split(new char[] { '_' }, 1);
        //    if (splits.Length == 1 && Int32.TryParse(splits[0], out inspectionId))
        //    {
        //        return inspectionId;
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}

    }

}