﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Navitas.Infrastructure
{
    public static class SerializationHelper
    {
        public static string Serialize<T>(T obj)
        {
            StringWriter writer = null;
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));

                XElement xmlClient = new XElement(typeof(T).ToString());

                writer = new StringWriter();
                xs.Serialize(writer, obj);

                return writer.ToString();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ошибка сериализации.", ex);
            }
            finally
            {
                if (writer != null)
                {
                    writer.Dispose();
                }
            }
        }

        public static T Deserialize<T>(string text)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                return (T)xs.Deserialize(new StringReader(text));
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Ошибка десериализации.", ex);
            }
        }
    }
}