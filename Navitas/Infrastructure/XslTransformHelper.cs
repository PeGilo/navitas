﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Xsl;

namespace Navitas.Infrastructure
{
    public class XslTransformHelper
    {
        private string _xsltFileFullName;

        public XslTransformHelper(string xsltFileFullName)
        {
            _xsltFileFullName = xsltFileFullName;
        }

        public string Transform(string text)
        {
            StringWriter result = new StringWriter();

            XslCompiledTransform xslt = new XslCompiledTransform();
            lock (this)
            {
                xslt.Load(_xsltFileFullName);
            }
            xslt.Transform(new XmlTextReader(new StringReader(text)), new XmlTextWriter(result));
           
            return result.ToString();
        }
    }
}