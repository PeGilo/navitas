﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Navitas.Infrastructure
{
    public class NavigationHandler
    {
        public static ActionResult HandleSubmit(ControllerBase controller, FormCollection form, int? id)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("navItem")
                          select item).FirstOrDefault();

            if (String.IsNullOrEmpty(res))
            {
                throw new InvalidOperationException("Невозможно обработать параметр навигации в POST запросе.");
            }

            System.Web.Routing.RouteValueDictionary routes = new System.Web.Routing.RouteValueDictionary();

            // Извлечь имя контроллера и action-метода из атрибута name элемента.
            string[] parts = res.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length >= 3)
            {
                string controllerName = parts[1].ToLowerInvariant();
                string actionName = parts[2].ToLowerInvariant();

                routes.Add("controller", controllerName);
                routes.Add("action", actionName);
                if(id.HasValue){
                    routes.Add("id", id.Value.ToString());
                }

                // Извлечь дополнительные параметры из имени элемента
                for (int i = 3; i < parts.Length; i++)
                {
                    string[] queryParams = parts[i].Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                    routes.Add(queryParams[0], queryParams[1]);
                }

                return new RedirectToRouteResult(routes);
                    //new System.Web.Routing.RouteValueDictionary(new { controller = controllerName, action = actionName, id = id }));
            }
            else
            {
                throw new InvalidOperationException("Невозможно обработать параметр навигации в POST запросе.");
            }
        }
    }
}