﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

using System.Web.UI.DataVisualization.Charting;

using Navitas.Infrastructure.Environment;

namespace Navitas.Infrastructure
{
    using BL = Soft33.EnergyPassport.V2.BL;
    using BLExt = Soft33.EnergyPassport.V2.BL.Extended;

    public class ChartManager
    {
        //public enum ChartType
        //{
        //    Column,
        //    Line,
        //    Pie
        //}

        private AppEnvironment _environment;

        public ChartManager(AppEnvironment env)
        {
            _environment = env;
        }

        public string RenderChartToFile(BL.Charts.TemplateChartData chartData)
        {
            using (Chart chart = new Chart())
            {
                // Area
                ChartArea area = new ChartArea("Area");

                area.AxisX.Title = chartData.XAxisTitle;
                area.AxisY.Title = chartData.YAxisTitle;
                area.AxisX.Enabled = AxisEnabled.True;
                area.AxisY.Enabled = AxisEnabled.True;

                if (chartData.Type == BL.Charts.ChartType.Column
                    || chartData.Type == BL.Charts.ChartType.Line)
                {
                    area.AxisX.Interval = 1;
                }

                // Если не показываются все x values:
                //cht.ChartAreas("MainChartArea").AxisX.MinorTickMark.Enabled = True
                //cht.ChartAreas("MainChartArea").AxisX.Interval = 1
                //cht.ChartAreas("MainChartArea").AxisX.IsLabelAutoFit = True
                //cht.ChartAreas("MainChartArea").AxisX.LabelStyle.IsStaggered = True
                //cht.ChartAreas("MainChartArea").AxisX.LabelAutoFitStyle =  LabelAutoFitStyles.DecreaseFont
                // 

                if (chartData.Type == BL.Charts.ChartType.Pie)
                {
                    // Прежде чем инициализировать 3D, проверить что есть данные
                    bool isEmpty = chartData.Series.Select(ts => ts.IsEmpty()).Aggregate((b1, b2) => b1 && b2);
                    if (!isEmpty)
                    {
                        area.Area3DStyle.Enable3D = true;
                    }
                    
                }

                chart.ChartAreas.Add(area);

                // Title
                //Title t = new Title("????", Docking.Top, new System.Drawing.Font("Trebuchet MS", 14, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(26, 59, 105));
                //chart.Titles.Add(t);

                // Legend
                Legend legend = new Legend("Legend");
                legend.Enabled = chartData.LegendEnabled;

                chart.Legends.Add(legend);


                foreach (var dataSeries in chartData.Series)
                {
                    // series.xValues; series.yValues; series.Title;

                    Series series = new Series(dataSeries.Title);
                    series.BorderWidth = 3;
                    series.ChartType = ConvertType(chartData.Type);
                    
                    //series1.XValueType = ChartValueType.;

                    series.Points.DataBindXY(dataSeries.xValues, dataSeries.yValues);

                    chart.Series.Add(series);
                }

                chart.Width = 640;
                chart.Height = 480;

                //chart1.RenderType = RenderType.BinaryStreaming;
                //chart1.ImageType = ChartImageType.Png;
                //chart.ImageLocation = Path.Combine(rootPath, "ChartsRendered\\chart1_for_location.png");
                string fileName = GenereateUniqueFileName() + ".png";

                // Check if the folder exists
                if (!Directory.Exists(_environment.AppSettings.ChartFolder))
                {
                    Directory.CreateDirectory(_environment.AppSettings.ChartFolder);
                }
                chart.SaveImage(Path.Combine(_environment.AppSettings.ChartFolder, fileName), ChartImageFormat.Png);

                return fileName;
            }
        }

        public string GetChartFullPath(string fileName)
        {
            return Path.Combine(_environment.AppSettings.ChartFolder, fileName);
        }

        private string GenereateUniqueFileName()
        {
            return Guid.NewGuid().ToString();
        }



        //private ChartValueType GetXType(object[] xValues)
        //{
        //    if(x
        //}

        private SeriesChartType ConvertType(BL.Charts.ChartType type)
        {
            if (type == BL.Charts.ChartType.Column)
            {
                return SeriesChartType.Column;
            }
            else if (type == BL.Charts.ChartType.Line)
            {
                return SeriesChartType.Line;
            }
            else if (type == BL.Charts.ChartType.Pie)
            {
                return SeriesChartType.Pie;
            }
            else
            {
                throw new ArgumentException("Неизвестный тип графика", "type");
            }
        }
    }
}