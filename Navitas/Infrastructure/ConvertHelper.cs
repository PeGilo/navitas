﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Navitas.Infrastructure
{
    /// <summary>
    /// Класс, содержащий методы конвертации чисел и строк в представлениях.
    /// </summary>
    public class ConvertHelper
    {
        public static string Convert(double value)
        {
            return value.ToString("0.##");
        }

        public static string Convert(float value)
        {
            return value.ToString("0.##");
        }

        public static string Convert(int value)
        {
            return value.ToString();
        }

        public static string Convert(Guid guid)
        {
            return guid.ToString();
        }

        public static string Convert(System.Data.Linq.Binary binary)
        {
            //return binary.ToString();
            return System.Convert.ToBase64String(binary.ToArray());
        }

        /// <summary>
        /// Мягкая конвертация строки в double. Если конвертировать текст не получается, то возвращает 0.
        /// Используется, когда в тексте допускается наличие символа, обозначающего отсутствие числа, например, '-'.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static double ParseDouble(string text)
        {
            double number;
            if (double.TryParse(text, System.Globalization.NumberStyles.Float, System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat, out number))
            {
                return number;
            }
            return 0.0F;
        }

        /// <summary>
        /// Мягкая конвертация строки в float. Если конвертировать текст не получается, то возвращает 0.
        /// Используется, когда в тексте допускается наличие символа, обозначающего отсутствие числа, например, '-'.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static float ParseFloat(string text)
        {
            float number;
            if (float.TryParse(text, System.Globalization.NumberStyles.Float, System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat, out number))
            {
                return number;
            }
            return 0.0F;
        }

        /// <summary>
        /// Мягкая конвертация строки в Int32. Если конвертировать текст не получается, то возвращает 0.
        /// Используется, когда в тексте допускается наличие символа, обозначающего отсутствие числа, например, '-'.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Int32 ParseInt32(string text)
        {
            Int32 number;
            if (Int32.TryParse(text, System.Globalization.NumberStyles.Float, System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat, out number))
            {
                return number;
            }
            return 0;
        }

        /// <summary>
        /// Если не получается распарсить, то возвращает Guid.Empty.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Guid ParseGuid(string text)
        {
            Guid result;
            if (Guid.TryParse(text, out result))
            {
                return result;
            }
            else
            {
                return Guid.Empty;
            }
        }

        public static System.Data.Linq.Binary ParseBinary(string text)
        {
            return System.Convert.FromBase64String(text);
        }
    }
}