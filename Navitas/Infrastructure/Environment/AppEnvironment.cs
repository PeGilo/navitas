﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Navitas.Infrastructure.Environment
{
    public class AppEnvironment
    {
        Controller _controller;

        private AppSettings _appSettings;

        public AppSettings AppSettings
        {
            get
            {
                if (_appSettings == null)
                {
                    string reportXsltFileName = WebConfigurationManager.AppSettings["ReportXsltFileName"];

                    string reportXsltFileFullName = Path.Combine(_controller.Server.MapPath("~"), reportXsltFileName);

                    _appSettings = new AppSettings(reportXsltFileFullName);

                    string illustrationFolder = WebConfigurationManager.AppSettings["IllustrationFolder"];
                    string thumbFolder = WebConfigurationManager.AppSettings["ThumbFolder"];
                    string chartFolder = WebConfigurationManager.AppSettings["ChartFolder"];

                    if (!String.IsNullOrEmpty(illustrationFolder)
                        && !String.IsNullOrEmpty(thumbFolder)
                        && !String.IsNullOrEmpty(chartFolder))
                    {
                        _appSettings.IllustrationFolder = Path.Combine(_controller.Server.MapPath("~"), illustrationFolder);
                        _appSettings.ThumbFolder = Path.Combine(_controller.Server.MapPath("~"), thumbFolder);
                        _appSettings.ChartFolder = Path.Combine(_controller.Server.MapPath("~"), chartFolder);
                    }
                    else
                    {
                        throw new ApplicationException("No folder settings");
                    }

                    int inspectionListSize;
                    if (Int32.TryParse(WebConfigurationManager.AppSettings["InspectionListSize"], out inspectionListSize))
                    {
                        _appSettings.InspectionListSize = inspectionListSize;
                    }
                    else
                    {
                        _appSettings.InspectionListSize = 15;
                    }

                    int uploadIllustrationWidth, uploadIllustrationHeight;
                    if (Int32.TryParse(WebConfigurationManager.AppSettings["UploadIllustrationWidth"], out uploadIllustrationWidth)
                        && Int32.TryParse(WebConfigurationManager.AppSettings["UploadIllustrationHeight"], out uploadIllustrationHeight))
                    {
                        _appSettings.UploadIllustrationWidth = uploadIllustrationWidth;
                        _appSettings.UploadIllustrationHeight = uploadIllustrationHeight;
                    }
                    else
                    {
                        throw new ApplicationException("В конфигурации не указаны размеры загружаемых изображений.");
                    }

                }
                return _appSettings;
            }
        }

        public AppEnvironment(System.Web.Mvc.Controller controller)
        {
            _controller = controller;
        }
    }
}
