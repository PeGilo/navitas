﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Infrastructure.Environment
{
    public class AppSettings
    {
        public AppSettings(string reportXsltFileFullName)
        {
            this.ReportXsltFileFullName = reportXsltFileFullName;
        }

        public string ReportXsltFileFullName
        {
            get;
            private set;
        }

        public string IllustrationFolder
        {
            get;
            set;
        }

        public string ThumbFolder
        {
            get;
            set;
        }

        public string ChartFolder
        {
            get;
            set;
        }

        public int InspectionListSize { get; set; }

        public int UploadIllustrationWidth { get; set; }

        public int UploadIllustrationHeight { get; set; }

        
    }
}