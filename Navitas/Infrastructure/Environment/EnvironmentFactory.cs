﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Infrastructure.Environment
{
    internal static class EnvironmentFactory
    {
        public static AppEnvironment GetEnvironment(System.Web.Mvc.Controller controller)
        {
            return new AppEnvironment(controller);
        }
    }
}