﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Services.Structures;

namespace Navitas.Infrastructure
{
    /// <summary>
    /// Содержит вспомогательные методы для работы с перечислениями.
    /// </summary>
    public static class EnumConverter
    {
        /// <summary>
        /// Ищет в ресурсах строку, соответствующую указанному значению перечисления.
        /// </summary>
        /// <param name="enumerator"></param>
        /// <returns></returns>
        public static string GetDisplayName(Enum enumerator)
        {
            return GetDisplayName(enumerator.GetType(), enumerator.ToString());
        }

        /// <summary>
        /// Ищет в ресурсах строку, соответствующую указанному значению перечисления и указанному элементу перечисления.
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string GetDisplayName(Type enumType, string name)
        {
            if (enumType == null || !enumType.IsEnum)
            {
                throw new ArgumentException("enumType");
            }

            string resourceKey = String.Format("{0}_{1}", enumType.Name, name);
            string text = ResourceHelper.LookupResource(typeof(Resources.Names), resourceKey);

            if (text != null)
            {
                return text;
            }
            else
            {
                return name;
            }
        }

        /// <summary>
        /// Для указанного типа перечисления возвращает список, состоящий из его элементов,
        /// с соответствующими значениями Id.
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static IList<ListItem> ToList(Type enumType)
        {
            List<ListItem> list = new List<ListItem>();

            foreach (var value in Enum.GetValues(enumType))
            {
                string displayName = GetDisplayName(enumType, Enum.GetName(enumType, value));

                list.Add(new ListItem((int)value, displayName));
            }

            return list;
        }

    }
}