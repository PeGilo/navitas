﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

using Navitas.Infrastructure.Environment;

namespace Navitas.Infrastructure
{
    //public class FileUploadResult
    //{
    //    public string Name { get; set; }
    //    public bool Completed { get; set; }
    //    public long Size { get; set; }
    //}

    public class FileManager
    {
        private AppEnvironment _environment;

        public FileManager(AppEnvironment env)
        {
            _environment = env;
        }

        /// <summary>
        /// Returns mime type by file extension
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>Can return empty string</returns>
        public static String GetMimeType(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                string ext = Path.GetExtension(fileName).TrimStart(new char[] { '.' }).ToLower();
                string mime;
                if (_mimeTypes.TryGetValue(ext, out mime))
                {
                    return mime;
                }
            }
            return String.Empty;
        }

        public static bool IsImage(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                return _imageExt.Contains(Path.GetExtension(fileName).TrimStart(new char[] { '.' }).ToLower());
            }
            return false;
        }

        public string GetInspectionIllustrationFullPath(string illustrationShortFileName)
        {
            int inspectionId = extractInspectionId(illustrationShortFileName);
            if (inspectionId != 0)
            {
                return Path.Combine(_environment.AppSettings.IllustrationFolder, inspectionId.ToString(), illustrationShortFileName);
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="stream"></param>
        /// <returns>Short file name.</returns>
        public virtual String StoreInspectionIllustration(int inspectionId, Stream stream)// HttpPostedFileBase file)
        {
            //// 1. Make unique name
            string shortFileName = GenerateUniqueIllustrationName(inspectionId) + ".jpg";

            // 2. Save file
            string storeFolder = Path.Combine(_environment.AppSettings.IllustrationFolder, inspectionId.ToString());

            // Check if the folder exists
            if (!Directory.Exists(storeFolder))
            {
                Directory.CreateDirectory(storeFolder);
            }

            string path = Path.Combine(storeFolder, shortFileName);
            
            ImageProcessor.SaveConverted(stream, path, 80, _environment.AppSettings.UploadIllustrationWidth, _environment.AppSettings.UploadIllustrationHeight);

            return shortFileName;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="fileChunk"></param>
        ///// <param name="wholeFileLength"></param>
        ///// <returns>File name if it is uploaded, null if not all parts are recieved.</returns>
        //public virtual FileUploadResult StoreFileChunk(HttpPostedFileBase fileChunk, string fileName, long wholeFileLength)
        //{
        //    FileUploadResult uploadResult = new FileUploadResult();

        //    string uploadFolder = _environment.AppSettings.UploadFolder;

        //    // Check if the folder exists
        //    if (!Directory.Exists(uploadFolder))
        //    {
        //        Directory.CreateDirectory(uploadFolder);
        //    }

        //    string fullPath = Path.Combine(uploadFolder, Path.GetFileName(fileName));

        //    Stream inputStream = fileChunk.InputStream;

        //    using (var fs = new FileStream(fullPath, FileMode.Append, FileAccess.Write))
        //    {
        //        byte[] buffer = new byte[1024];

        //        int l = inputStream.Read(buffer, 0, 1024);
        //        while (l > 0)
        //        {
        //            fs.Write(buffer, 0, l);
        //            l = inputStream.Read(buffer, 0, 1024);
        //        }
        //        fs.Flush();
        //        fs.Close();
        //    }

        //    // Check if it was last chunk
        //    uploadResult.Size = (new FileInfo(fullPath)).Length;
        //    uploadResult.Completed = uploadResult.Size == wholeFileLength;
        //    if (uploadResult.Completed)
        //    {
        //        // Move file 
        //        string shortFileName = GenerateUniqueName(fileName);
        //        File.Move(fullPath, Path.Combine(uploadFolder, shortFileName));

        //        uploadResult.Name = shortFileName;
        //    }
        //    else
        //    {
        //        uploadResult.Name = fileChunk.FileName;
        //    }

        //    return uploadResult;
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="shortFileName"></param>
        /// <returns></returns>
        /// <remarks>TODO: replace GUID with short random string</remarks>
        //protected virtual string GenerateUniqueName(string shortFileName)
        //{
        //    string ext = Path.GetExtension(shortFileName);//.TrimStart(new char[] {'.'});

        //    // Template for name is GUID.ext
        //    return Guid.NewGuid().ToString() + ext;
        //}

        protected virtual string GenerateUniqueIllustrationName(int inspectionId)
        {
            //string ext = Path.GetExtension(shortFileName);//.TrimStart(new char[] {'.'});

            // Template for name is GUID.ext
            return inspectionId.ToString() + "_" + Guid.NewGuid().ToString();
        }

        protected virtual int extractInspectionId(string illustrationShortName)
        {
            int inspectionId;
            string[] splits = illustrationShortName.Split(new char[] { '_' }, 2);
            if (splits.Length > 0 && Int32.TryParse(splits[0], out inspectionId))
            {
                return inspectionId;
            }
            else
            {
                return 0;
            }
        }


        private static Dictionary<string, string> _mimeTypes = new Dictionary<string, string>() { 
            { "ai", "application/postscript" },
            { "jp2", "image/jp2" },
            { "jpe", "image/jpeg" },
            { "jpeg", "image/jpeg" },
            { "jpg", "image/jpeg" },
            { "gif", "image/gif" },
            { "bmp", "image/bmp" },
            { "png", "image/png" },
            { "tif", "image/tiff" },
            { "tiff", "image/tiff" }
        };

        private static System.Collections.Generic.SortedSet<string> _imageExt = new SortedSet<string>() {
            "jpg", "jpeg", "gif", "png"
        };
    }
}