﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Navitas.Infrastructure
{
    public class ImageProcessor
    {
        //private const int PREVIEW_WIDTH_PX = 800;
        //private const int PREVIEW_HEIGHT_PX = 600;

        #region Public methods

        /// <summary>
        /// Creates an image in the JPEG format with specified compression level 
        /// from source image.
        /// </summary>
        /// <param name="sourceImageFullName">Name of source file for an image.</param>
        /// <param name="destImageFullName">Name of destination file for an image.</param>
        /// <param name="lCompression">Compression factor for the generated image.</param>
        /// <returns>File name of the generated image.</returns>
        public static string SaveConverted(string sourceImageFullName, string destImageFullName, long lCompression,
            int w, int h)
        {
            Image image = Image.FromFile(sourceImageFullName);

            // Check for need to scale
            if (image.Width > w || image.Height > h)
            {
                Size scaledSize = CaclulateImageSize(image.Size, new Size(w, h));
                Image scaledImage = ScaleImage(image, scaledSize.Width, scaledSize.Height);

                image.Dispose();
                image = scaledImage;
            }

            SaveJPGWithCompressionSetting(image, destImageFullName, lCompression);

            image.Dispose();

            return destImageFullName;
        }

        public static string SaveConverted(Stream sourceStream, string destImageFullName, long lCompression,
            int w, int h)
        {
            Image image = Image.FromStream(sourceStream);

            // Check for need to scale
            if (image.Width > w || image.Height > h)
            {
                Size scaledSize = CaclulateImageSize(image.Size, new Size(w, h));
                Image scaledImage = ScaleImage(image, scaledSize.Width, scaledSize.Height);

                image.Dispose();
                image = scaledImage;
            }

            SaveJPGWithCompressionSetting(image, destImageFullName, lCompression);

            image.Dispose();

            return destImageFullName;
        }

        #endregion

        #region Private methods

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private static void SaveJPGWithCompressionSetting(Image image, string szFileName, long lCompression)
        {
            EncoderParameters eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(Encoder.Quality, lCompression);

            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

            image.Save(szFileName, ici, eps);
        }

        /// <summary>
        /// Scales image/
        /// </summary>
        /// <param name="original">Original image.</param>
        /// <param name="newWidth">Width to set in pixels.</param>
        /// <param name="newHeight">Height to set in pixels.</param>
        /// <returns>Scaled image.</returns>
        public static Image ScaleImage(Image original, int newWidth, int newHeight)
        {
            Bitmap final;

            final = new Bitmap(newWidth, newHeight);
            Graphics g = Graphics.FromImage(final);

            //...scale the image using high-quality interpolation.
            g.CompositingMode = CompositingMode.SourceCopy;
            g.InterpolationMode = InterpolationMode.High;

            g.DrawImage(original, new Rectangle(0, 0, newWidth, newHeight), new Rectangle(0, 0, original.Width, original.Height), GraphicsUnit.Pixel);
            g.Dispose();
            return final;
        }

        /// <summary>
        /// Calculates size that image need to resize to fit into specified box.
        /// </summary>
        /// <param name="imgSize">Original size of image.</param>
        /// <param name="boxSize">Size of box to fit.</param>
        /// <returns>Size that image need to have to fit into the box.</returns>
        private static Size CaclulateImageSize(Size imgSize, Size boxSize)
        {
            Size fitSize = new Size();

            // if the box is large enogh there is no need to resize the picture
            if (boxSize.Width > imgSize.Width && boxSize.Height > imgSize.Height)
            {
                fitSize = imgSize;
            }
            else
            {
                double imgRatio;
                double boxRatio;

                if (imgSize.Height > 0)
                    imgRatio = (double)imgSize.Width / imgSize.Height;
                else
                    imgRatio = Double.MaxValue;

                if (boxSize.Height > 0)
                    boxRatio = (double)boxSize.Width / boxSize.Height;
                else
                    boxRatio = Double.MaxValue;

                if (imgRatio < boxRatio)
                {
                    // resize by height
                    fitSize.Width = (int)(boxSize.Height * imgRatio);
                    fitSize.Height = boxSize.Height;
                }
                else
                {
                    // resize by width
                    fitSize.Width = boxSize.Width;
                    fitSize.Height = (int)(boxSize.Width / imgRatio);
                }
            }

            return fitSize;
        }

        #endregion
    }
}