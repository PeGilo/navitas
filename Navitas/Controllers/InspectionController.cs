﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Parsing;
using Navitas.Services;
using Navitas.ViewModels.Inspection;
using Navitas.ViewModels.Intake;
using Navitas.ViewModels.Production;

namespace Navitas.Controllers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ServiceBaseException), View = "Error", Order = 2)]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ConcurrencyException), View = "ConcurrencyError", Order = 10)]
    public class InspectionController : ControllerBase
    {
        private DataManager _dataManager;
        private ServiceManager _services;

        public InspectionController(DataManager dataManager, ServiceManager services)
            : base(services)
        {
            _dataManager = dataManager;
            _services = services;
        }

        [Authorize()]
        public ActionResult Index(int? page, string sort, string sortdir /* ASC/DESC */)
        {
            int auditorId = 1;

            // Извлекаем параметры запроса
            string sortField = "";
            bool asc = "ASC".Equals(sortdir, StringComparison.InvariantCultureIgnoreCase) ? true : false;
            if ("ClientCaption".Equals(sort, StringComparison.InvariantCultureIgnoreCase))
            {
                sortField = sort;
            }

            // Заполняем модель
            PagedInspectionsModel model = new PagedInspectionsModel();
            model.PageSize = Environment.AppSettings.InspectionListSize;
            model.PageNumber = page.HasValue ? page.Value : 1;

            int totalRows;
            IEnumerable<BL.InspectionBrief> inspections =
                _services.InspectionService.GetInspectionsByAuditor(auditorId, model.PageSize, model.PageNumber, sortField, asc, out totalRows);

            model.TotalRows = totalRows;
            model.Inspections = (from bo in inspections
                                 select new InspectionBriefModel(bo));

            return View(model);
        }

        #region "Client Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Первая вкладка
        /// </summary>
        /// <returns></returns>
        [Authorize()]
        public ActionResult Client(int? id)
        {
            ViewModels.Client.ClientEditModel model = null;
            if (!id.HasValue)
            {
                // Запрос нового обследования
                model = new ViewModels.Client.ClientEditModel(_services.InspectionService.CreateClient());
            }
            else
            {
                // Запрос существующего обследования
                BL.Inspection inspection = _services.InspectionService.GetInspectionById(id.Value);
                if (inspection != null)
                {
                    model = new ViewModels.Client.ClientEditModel(inspection.Client);
                }
                else
                {
                    return NotFound();
                }
            }
            return View("InspectionClient", model);
        }

        /// <summary>
        /// Создание нового обследования / Редактирование существующего // POST
        /// Первая вкладка
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [ActionName("Client")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult Client_NavForward(int? id, ViewModels.Client.ClientEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("InspectionClient", model);
            }

            if (!id.HasValue)
            {
                // Создание нового обследования
                int inspectionId = addNewInspection(model);

                return RedirectToAction("Production", new { id = inspectionId });
            }
            else
            {
                // Редактирование существующего обследования
                updateExistingInspection(id.Value, model);

                return RedirectToAction("Production", new { id = id.Value });
            }
        }

        [ActionName("Client")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult Client_Navigation(int? id, ViewModels.Client.ClientEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View("InspectionClient", model);
            }

            if (!id.HasValue)
            {
                // Создание нового обследования
                int inspectionId = addNewInspection(model);

                return NavigationHandler.HandleSubmit(this, form, inspectionId);
            }
            else
            {
                // Редактирование существующего обследования
                updateExistingInspection(id.Value, model);

                return NavigationHandler.HandleSubmit(this, form, id.Value);
            }
        }

        #endregion

        #region "Production Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Вторая вкладка
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize()]
        public ActionResult Production(int id)
        {
            BL.Inspection inspection = _services.InspectionService.GetInspectionById(id);
            if (inspection != null)
            {
                ViewModels.Production.ProductionEditModel model = new ViewModels.Production.ProductionEditModel(inspection.Production);
                model.BaseYear = inspection.BaseYear.Value;

                return View("InspectionProduction", model);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создание / Редактирование обследования // POST
        /// Вторая вкладка
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("Production")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        [Authorize()]
        public ActionResult Production_NavBack(int id, string import, ViewModels.Production.ProductionEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View("InspectionProduction", model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportProduction(import, model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Client", new { id = id });
        }

        [ActionName("Production")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        [Authorize()]
        public ActionResult Production_Save(int id, string import, ViewModels.Production.ProductionEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View("InspectionProduction", model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportProduction(import, model);
            }

            updateExistingInspection(id, model);

            // Очистить ModelState чтобы данные обновились
            ModelState.Clear();

            return Production(id);
        }

        [ActionName("Production")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult Production_NavForward(int id, string import, ViewModels.Production.ProductionEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View("InspectionProduction", model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportProduction(import, model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Staff", new { id = id });
        }

        [ActionName("Production")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult Production_Navigation(int id, string import, ViewModels.Production.ProductionEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View("InspectionProduction", model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportProduction(import, model);
            }

            updateExistingInspection(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        #endregion

        #region "Staff Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Третья вкладка
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize()]
        public ActionResult Staff(int id)
        {
            BL.Inspection inspection = _services.InspectionService.GetInspectionById(id);
            if (inspection != null)
            {
                var model = new ViewModels.Staff.StaffEditModel(inspection.Staff);

                return View("Staff", model);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создание / Редактирование обследования // POST
        /// Третья вкладка
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("Staff")]
        [HttpPost]
        [MultiButton(RegexFormKey="^remove\\d+$")]
        public ActionResult Staff_RemoveItem(int id, ViewModels.Staff.StaffEditModel model,
            FormCollection form)
        {
            string res = (from item in form.AllKeys
                      where item.StartsWith("remove")
                      select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Units.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Staff")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult Staff_AddItem(int id, ViewModels.Staff.StaffEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Staff.StaffUnitEditModel(_services.InspectionService.CreateStaffUnit());
            model.Units.Add(unitModel);

            return View(model);
        }

        [ActionName("Staff")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        [Authorize()]
        public ActionResult Staff_Back(int id, ViewModels.Staff.StaffEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Production", new { id = id });
        }

        [ActionName("Staff")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult Staff_Forward(int id, ViewModels.Staff.StaffEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Intake5Years", new { id = id });
        }

        [ActionName("Staff")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult Staff_Navigation(int id, ViewModels.Staff.StaffEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Сотрудников.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewStaffUnit(int index)
        {
            if(HttpContext.IsDebuggingEnabled)
            {
                System.Threading.Thread.Sleep(1000);
            }

            var model = new ViewModels.Staff.StaffUnitEditModel(_services.InspectionService.CreateStaffUnit());
            ViewData["Index"] = index;
            
            return PartialView("EditorTemplates/_StaffUnit", model);
        }

        #endregion

        #region "Intake 5 Years Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Четвертая вкладка - "Потребление топливно-энергетических ресурсов за последние 5 лет"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize()]
        public ActionResult Intake5Years(int id)
        {
            BL.Inspection inspection = _services.InspectionService.GetInspectionById(id);
            if (inspection != null)
            {
                var model = new ViewModels.Intake.FiveYearIntakeEditModel(inspection.Intake.FiveYear);
                model.BaseYear = inspection.BaseYear.Value;

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создание / Редактирование обследования // POST
        /// Четвертая вкладка - "Потребление топливно-энергетических ресурсов за последние 5 лет"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("Intake5Years")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        [Authorize()]
        public ActionResult Intake5Years_Back(int id, string import, ViewModels.Intake.FiveYearIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();

                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportFiveYearIntakeData(import, model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Staff", new { id = id });
        }

        [ActionName("Intake5Years")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        [Authorize()]
        public ActionResult Intake5Years_Save(int id, string import, ViewModels.Intake.FiveYearIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();

                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportFiveYearIntakeData(import, model);
            }

            updateExistingInspection(id, model);

            // Очистить ModelState чтобы данные обновились
            ModelState.Clear();

            return Intake5Years(id);
        }

        [ActionName("Intake5Years")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult Intake5Years_Forward(int id, string import, ViewModels.Intake.FiveYearIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();

                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportFiveYearIntakeData(import, model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("IntakeLastYear", new { id = id });
        }

        [ActionName("Intake5Years")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult Intake5Years_Navigation(int id, string import, ViewModels.Intake.FiveYearIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();

                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportFiveYearIntakeData(import, model);
            }

            updateExistingInspection(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        #endregion

        #region "Intake Twelve Months Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Пятая вкладка - "Помесячное потребление энергоресурсов"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize()]
        public ActionResult IntakeLastYear(int id)
        {
            BL.Inspection inspection = _services.InspectionService.GetInspectionById(id);
            if (inspection != null)
            {
                var model = new ViewModels.Intake.TwelveMonthIntakeEditModel(inspection.Intake.TwelveMonth);
                model.BaseYear = inspection.BaseYear.Value;

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Создание / Редактирование обследования // POST
        /// Пятая вкладка - "Помесячное потребление энергоресурсов"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        [Authorize()]
        public ActionResult IntakeLastYear_Back(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Intake5Years", new { id = id });
        }

        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        [Authorize()]
        public ActionResult IntakeLastYear_Save(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingInspection(id, model);

            // Очистить ModelState чтобы данные обновились
            ModelState.Clear();

            return IntakeLastYear(id);
        }

        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult IntakeLastYear_Forward(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Vehicles", new { id = id });
        }

        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult IntakeLastYear_Navigation(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingInspection(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        #endregion

        #region "Vehicles Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Шестая вкладка - "Транспорт"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize()]
        public ActionResult Vehicles(int id)
        {
            BL.Inspection inspection = _services.InspectionService.GetInspectionById(id);
            if (inspection != null)
            {
                var model = new ViewModels.Vehicle.VehicleListEditModel(inspection.Vehicle);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("Vehicles")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult Vehicles_RemoveItem(int id, ViewModels.Vehicle.VehicleListEditModel model,
            FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Vehicles.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Vehicles")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult Vehicles_AddItem(int id, ViewModels.Vehicle.VehicleListEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Vehicle.VehicleUnitEditModel(_services.InspectionService.CreateVehicleUnit());
            model.Vehicles.Add(unitModel);

            return View(model);
        }

        /// <summary>
        /// Создание / Редактирование обследования // POST
        /// Шестая вкладка - "Транспорт"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("Vehicles")]
        [HttpPost]
        [MultiButton(MatchFormKey="buttonPrev")]
        [Authorize()]
        public ActionResult Vehicles_NavBack(int id, ViewModels.Vehicle.VehicleListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("IntakeLastYear", new { id = id });
        }

        [ActionName("Vehicles")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult Vehicles_Next(int id, ViewModels.Vehicle.VehicleListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("CalculationCoeffs", new { id = id });
        }

        [ActionName("Vehicles")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult Vehicles_Navigation(int id, ViewModels.Vehicle.VehicleListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Транспорта.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewVehicleUnit(int index)
        {
            if (HttpContext.IsDebuggingEnabled)
            {
                System.Threading.Thread.Sleep(1000);
            }

            var model = new ViewModels.Vehicle.VehicleUnitEditModel(_services.InspectionService.CreateVehicleUnit());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/_VehicleUnit", model);
        }

        #endregion

        #region "Calculation Coeffs Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Вкладка - "Расчетные параметры"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize()]
        public ActionResult CalculationCoeffs(int id)
        {
            BL.Inspection inspection = _services.InspectionService.GetInspectionById(id);
            if (inspection != null)
            {
                var model = new ViewModels.Inspection.CalculationCoeffsEditModel(inspection.CalculationCoeffs);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("CalculationCoeffs")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        [Authorize()]
        public ActionResult CalculationCoeffs_Back(int id, ViewModels.Inspection.CalculationCoeffsEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Vehicles", new { id = id });
        }

        [ActionName("CalculationCoeffs")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        [Authorize()]
        public ActionResult CalculationCoeffs_Save(int id, ViewModels.Inspection.CalculationCoeffsEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            // Очистить ModelState чтобы данные обновились
            ModelState.Clear();

            return CalculationCoeffs(id);
        }

        [ActionName("CalculationCoeffs")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult CalculationCoeffs_Forward(int id, string import, ViewModels.Inspection.CalculationCoeffsEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Buildings", new { id = id });
        }

        [ActionName("CalculationCoeffs")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult CalculationCoeffs_Navigation(int id, string import, ViewModels.Inspection.CalculationCoeffsEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        #endregion


        #region "Buildings Handlers"

        /// <summary>
        /// Создание / Редактирование обследования // GET
        /// Шестая вкладка - "Здания"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize()]
        public ActionResult Buildings(int id)
        {
            //BL.Inspection inspection = _dataManager.Inspection.GetInspectionById(id);
            IEnumerable<BL.Building> buildings = _services.InspectionService.GetBuildingsByInspectionId(id);
            if (buildings != null)
            {
                var model = new ViewModels.Building.BuildingListEditModel(buildings);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("Buildings")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult Buildings_AddItem(int id, ViewModels.Building.BuildingListEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.BuildingItemEditModel();// (_services.InspectionService.CreateVehicleUnit());
            model.Items.Add(unitModel);

            return View(model);
        }

        [ActionName("Buildings")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^edit\\d+$")]
        public ActionResult Buildings_EditItem(int id, ViewModels.Building.BuildingListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("edit")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Извлечь ID редактируемого здания
                int buildingID = Int32.Parse(res.Substring(4));

                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                updateExistingInspection(id, model);

                // Перенаправить на редактирование выбранного здания
                return RedirectToAction("AddInfo", "Building", new { id = buildingID });
            }

            return View(model);
        }

        [ActionName("Buildings")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult Buildings_RemoveItem(int id, ViewModels.Building.BuildingListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        /// <summary>
        /// Создание / Редактирование обследования // POST
        /// Шестая вкладка - "Здания"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("Buildings")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        [Authorize()]
        public ActionResult Buildings_NavBack(int id, ViewModels.Building.BuildingListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("CalculationCoeffs", new { id = id });
        }

        [ActionName("Buildings")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        [Authorize()]
        public ActionResult Buildings_Save(int id, ViewModels.Building.BuildingListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return RedirectToAction("Buildings", new { id = id });
        }

        [ActionName("Buildings")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        [Authorize()]
        public ActionResult Buildings_Navigation(int id, ViewModels.Building.BuildingListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingInspection(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Зданий.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewBuildingItem(int index)
        {
            var model = new ViewModels.Building.BuildingItemEditModel();// (_services.InspectionService.CreateBuildingItem());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/_BuildingItem", model);
        }

        #endregion

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Дополнить модель для транспорта общими списками по типу топлива и типу одометров
            if (filterContext.Controller.ViewData.Model is ViewModels.Vehicle.VehicleListEditModel)
            {
                var model = (ViewModels.Vehicle.VehicleListEditModel)filterContext.Controller.ViewData.Model;
                model.FuelTypeList = GetFuelTypesList();
                model.OdometerTypeList = GetOdometerTypesList();
            }
            // Так же надо предусмотреть, что асинхронно будет загружаться Partial view для VehicleUnitEditModel
            else if (filterContext.Controller.ViewData.Model is ViewModels.Vehicle.VehicleUnitEditModel)
            {
                filterContext.Controller.ViewData["FuelTypeList"] = GetFuelTypesList();
                filterContext.Controller.ViewData["OdometerTypeList"] = GetOdometerTypesList();
            }
            // Для зданий модель необходимо дополнить списком типов использования
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.BuildingListEditModel)
            { 
                var model = (ViewModels.Building.BuildingListEditModel)filterContext.Controller.ViewData.Model;
                model.BuildingPurposeList = GetBuildingPurposeList();
                return;
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.BuildingItemEditModel)
            {
                filterContext.Controller.ViewData["BulidingPurposeList"] = GetBuildingPurposeList();
            }
        }

        /// <summary>
        /// Преобразует данные по типу топлива в структуру понятную для View
        /// </summary>
        /// <returns></returns>
        protected virtual SelectList GetFuelTypesList()
        {
            return new SelectList(_services.SettingsService.GetFuelTypes(), "Text", "Text");
            //return _services.SettingsService.GetFuelTypes();
        }

        /// <summary>
        /// Преобразует данные по типу одометров в структуру понятную для View
        /// </summary>
        /// <returns></returns>
        protected virtual SelectList GetOdometerTypesList()
        {
            return new SelectList(_services.SettingsService.GetOdometerTypes(), "Text", "Text");
            //return _services.SettingsService.GetOdometerTypes();
        }

        /// <summary>
        /// Преобразует данные по типу одометров в структуру понятную для View
        /// </summary>
        /// <returns></returns>
        protected virtual SelectList GetBuildingPurposeList()
        {
            return new SelectList(_services.SettingsService.GetBuildingPurposes(), "Id", "Text");
        }

        #region "Updating Methods"

        /// <summary>
        /// Сохранить новое обследование в базе с указанным Клиентом.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private int addNewInspection(ViewModels.Client.ClientEditModel model)
        {
            BL.Inspection inspection = new BL.Inspection();

            model.UpdateEntity(inspection.Client);

            inspection.FullName = model.Name;
            inspection.AuditorID = 1;
            //throw new DataAccess.Exceptions.ConcurrencyException();
            return _services.InspectionService.Add(inspection);
        }

        /// <summary>
        /// Изменить Клиента в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Client.ClientEditModel model)
        {
            BL.Client client = new BL.Client();
            model.UpdateEntity(client);
            _services.InspectionService.UpdateClient(inspectionId, client);
        }

        /// <summary>
        /// Изменить Продукцию в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Production.ProductionEditModel model)
        {
            BL.Production production = new BL.Production();
            model.UpdateEntity(production);
            _services.InspectionService.UpdateProduction(inspectionId, production);
        }

        /// <summary>
        /// Изменить информацию о Персонале в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Staff.StaffEditModel model)
        {
            BL.Staff staff = new BL.Staff();
            model.UpdateEntity(staff);
            _services.InspectionService.UpdateStaff(inspectionId, staff);
        }

        /// <summary>
        /// Изменить информацию о Потреблении за 5 лет в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Intake.FiveYearIntakeEditModel model)
        {
            BL.FiveYearIntake intake = new BL.FiveYearIntake();
            model.UpdateEntity(intake);
            _services.InspectionService.UpdateIntake5Years(inspectionId, intake);
        }

        /// <summary>
        /// Изменить информацию о Потреблении за последний год (12 месяцев) в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Intake.TwelveMonthIntakeEditModel model)
        {
            BL.TwelveMonthIntake intake = new BL.TwelveMonthIntake();
            model.UpdateEntity(intake);
            _services.InspectionService.UpdateIntakeLastYear(inspectionId, intake);
        }

        /// <summary>
        /// Изменить информацию о Транспорте в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Vehicle.VehicleListEditModel model)
        {
            BL.VehicleList vehicle = new BL.VehicleList();
            model.UpdateEntity(vehicle);
            _services.InspectionService.UpdateTransport(inspectionId, vehicle);
        }

        /// <summary>
        /// Изменить информацию о Расчетных параметрах в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Inspection.CalculationCoeffsEditModel model)
        {
            BL.CalculationCoeffsOfInspection coeffs = _services.InspectionService.CreateCalculationCoeffs();
            model.UpdateEntity(coeffs);
            _services.InspectionService.UpdateCalculationCoeffs(inspectionId, coeffs);
        }

        /// <summary>
        /// Изменить информацию о Зданиях в существующем обследовании.
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <param name="model"></param>
        private void updateExistingInspection(int inspectionId, ViewModels.Building.BuildingListEditModel model)
        {
            BL.Inspection inspection = _services.InspectionService.CreateInspection();
            model.UpdateEntity(inspection);
            _services.InspectionService.UpdateBuildings(inspectionId, inspection.Buildings);
        }

        #endregion

        /// <summary>
        /// Обновляет View model согласно бизнес-логике, через объект бизнес-логики
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ViewModels.Intake.FiveYearIntakeEditModel applyBusinessLogic(ViewModels.Intake.FiveYearIntakeEditModel model)
        {
            // View model должна обновиться с учетом бизнес-логики, для этого приходится
            // создавать объект бизнес-логики и новую view model.
            BL.FiveYearIntake boIntake = _services.InspectionService.CreateIntake5Years();
            model.UpdateEntity(boIntake);
            var newModel = new ViewModels.Intake.FiveYearIntakeEditModel(boIntake);

            // Сохраняем BaseYear, чтобы не загружать Inspection из базы.
            newModel.BaseYear = model.BaseYear;

            return newModel;
        }

        /// <summary>
        /// Обновляет View model согласно бизнес-логике, через объект бизнес-логики
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ViewModels.Intake.TwelveMonthIntakeEditModel applyBusinessLogic(ViewModels.Intake.TwelveMonthIntakeEditModel model)
        {
            // View model должна обновиться с учетом бизнес-логики, для этого приходится
            // создавать объект бизнес-логики и новую view model.
            BL.TwelveMonthIntake boIntake = _services.InspectionService.CreateIntakeTwelveMonths();
            model.UpdateEntity(boIntake);
            var newModel = new ViewModels.Intake.TwelveMonthIntakeEditModel(boIntake);

            // Сохраняем BaseYear, чтобы не загружать Inspection из базы.
            newModel.BaseYear = model.BaseYear;

            return newModel;
        }

        private void ImportProduction(string data, ProductionEditModel model)
        {
            var table = TableParser.Parse(data, 3, 1);
            for (var rowIndex = 0; rowIndex < table.RowCount; rowIndex++)
            {
                if (rowIndex < 2)
                {
                    var modelRow = rowIndex == 0 ? model.Row1 : model.Row2;
                    var valueIndex = 0;
                    foreach (var value in table.Row(rowIndex).Skip(1))
                    {
                        modelRow.Items[valueIndex++] = value;
                    }
                }
                else
                {
                    ProductionNumbersEditModel modelRow = null;
                    switch (rowIndex)
                    {
                        case 2:
                            modelRow = model.Row3;
                            break;
                        case 3:
                            modelRow = model.Row4;
                            break;
                        case 4:
                            modelRow = model.Row5;
                            break;
                        case 5:
                            modelRow = model.Row6;
                            break;
                        case 6:
                            model.PhysicalTerms = table.Row(rowIndex).Cell(0);
                            modelRow = model.Row7;
                            break;
                        case 7:
                            model.MainProductionPhysicalTerms = table.Row(rowIndex).Cell(0);
                            modelRow = model.Row8;
                            break;
                    }
                    if (modelRow == null)
                    {
                        break;
                    }

                    var valueIndex = 0;
                    foreach (var value in table.Row(rowIndex).Skip(1))
                    {
                        switch (valueIndex)
                        {
                            case 0:
                                modelRow.BaseMinus4 = value.Replace(" ", string.Empty);
                                break;
                            case 1:
                                modelRow.BaseMinus3 = value.Replace(" ", string.Empty);
                                break;
                            case 2:
                                modelRow.BaseMinus2 = value.Replace(" ", string.Empty);
                                break;
                            case 3:
                                modelRow.BaseMinus1 = value.Replace(" ", string.Empty);
                                break;
                            case 4:
                                modelRow.BaseMinus0 = value.Replace(" ", string.Empty);
                                break;
                        }
                        valueIndex++;
                    }
                }
            }
        }

        private static readonly List<Tuple<int, int, string[]>> intakelastyearRowsMatch = new List<Tuple<int, int, string[]>>
        {
            new Tuple<int, int, string[]>(0, 0, new[] { "Электроэнергия", "натуральном" }),
            new Tuple<int, int, string[]>(0, 1, new[] { "Электроэнергия", "затраты" }),

            new Tuple<int, int, string[]>(2, 0, new[] { "Отопление", "натуральном" }),
            new Tuple<int, int, string[]>(2, 1, new[] { "Отопление", "затраты" }),

            new Tuple<int, int, string[]>(3, 0, new[] { "ГВС", "натуральном" }),
            new Tuple<int, int, string[]>(3, 1, new[] { "ГВС", "затраты" }),

            new Tuple<int, int, string[]>(4, 0, new[] { "Вентиляция", "натуральном" }),
            new Tuple<int, int, string[]>(4, 1, new[] { "Вентиляция", "затраты" }),

            new Tuple<int, int, string[]>(5, 0, new[] { "Холодное", "натуральном" }),
            new Tuple<int, int, string[]>(5, 1, new[] { "Холодное", "затраты" }),

            new Tuple<int, int, string[]>(6, 0, new[] { "Горячее", "натуральном" }),
            new Tuple<int, int, string[]>(6, 1, new[] { "Горячее", "затраты" }),

            new Tuple<int, int, string[]>(7, 0, new[] { "Водоотведение", "натуральном" }),
            new Tuple<int, int, string[]>(7, 1, new[] { "Водоотведение", "затраты" }),

            new Tuple<int, int, string[]>(8, 0, new[] { "Дрова", "натуральном" }),
            new Tuple<int, int, string[]>(8, 1, new[] { "Дрова", "затраты" }),

            new Tuple<int, int, string[]>(9, 0, new[] { "Уголь", "натуральном" }),
            new Tuple<int, int, string[]>(9, 1, new[] { "Уголь", "затраты" }),

            new Tuple<int, int, string[]>(10, 0, new[] { "Природный", "натуральном" }),
            new Tuple<int, int, string[]>(10, 1, new[] { "Природный", "затраты" }),

            new Tuple<int, int, string[]>(12, 0, new[] { "Бензин", "натуральном" }),
            new Tuple<int, int, string[]>(12, 1, new[] { "Бензин", "затраты" }),

            new Tuple<int, int, string[]>(13, 0, new[] { "Керосин", "натуральном" }),
            new Tuple<int, int, string[]>(13, 1, new[] { "Керосин", "затраты" }),

            new Tuple<int, int, string[]>(14, 0, new[] { "Дизельное ", "натуральном" }),
            new Tuple<int, int, string[]>(14, 1, new[] { "Дизельное ", "затраты" }),

            new Tuple<int, int, string[]>(15, 0, new[] { "Газ", "натуральном" }),
            new Tuple<int, int, string[]>(15, 1, new[] { "Газ", "затраты" }),
        };

        private void ImportIntakeLastYearData(string data, TwelveMonthIntakeEditModel model)
        {
            var table = TableParser.Parse(data, 3, 2);
            var matches = new List<Tuple<int, int, string[]>>(intakelastyearRowsMatch);
            var rows = new Queue<HeaderedStringString>(table.Rows());
            while (rows.Count > 0)
            {
                var row = rows.Dequeue();
                foreach (var match in matches)
                {
                    if (row.MatchesAll(match.Item3))
                    {
                        TwelveMonthsNumbersEditModel modelRow = null;
                        switch (match.Item2)
                        {
                            case 0:
                                modelRow = model.Values[match.Item1];
                                break;
                            case 1:
                                modelRow = model.ValuesRUR[match.Item1];
                                break;
                            case 3:
                                modelRow = model.Tariffs[match.Item1];
                                break;
                        }
                        if (modelRow != null)
                        {
                            for (var index = 0; index <= 11; index++)
                            {
                                modelRow.Months[index] = row.Cell(index).Replace(" ", string.Empty);
                            }
                        }
                        matches.Remove(match);
                        break;
                    }
                }
            }
        }

        private static readonly List<Tuple<int, int, string[]>> fiveyearintakeRowsMatch = new List<Tuple<int, int, string[]>>
        {
            new Tuple<int, int, string[]>(0, 0, new[] { "Электроэнергия", "потребление" }),
            new Tuple<int, int, string[]>(0, 1, new[] { "Электроэнергия", "затраты" }),

            new Tuple<int, int, string[]>(2, 0, new[] { "Отопление", "потребление" }),
            new Tuple<int, int, string[]>(2, 1, new[] { "Отопление", "затраты" }),

            new Tuple<int, int, string[]>(3, 0, new[] { "ГВС", "потребление" }),
            new Tuple<int, int, string[]>(3, 1, new[] { "ГВС", "затраты" }),

            new Tuple<int, int, string[]>(4, 0, new[] { "Вентиляция", "потребление" }),
            new Tuple<int, int, string[]>(4, 1, new[] { "Вентиляция", "затраты" }),

            new Tuple<int, int, string[]>(5, 0, new[] { "Горячее", "потребление" }),
            new Tuple<int, int, string[]>(5, 1, new[] { "Горячее", "затраты" }),
            
            new Tuple<int, int, string[]>(6, 0, new[] { "Холодное", "потребление" }),
            new Tuple<int, int, string[]>(6, 1, new[] { "Холодное", "затраты" }),

            new Tuple<int, int, string[]>(7, 0, new[] { "Водоотведение", "потребление" }),
            new Tuple<int, int, string[]>(7, 1, new[] { "Водоотведение", "затраты" }),

            new Tuple<int, int, string[]>(8, 0, new[] { "Дрова", "потребление" }),
            new Tuple<int, int, string[]>(8, 1, new[] { "Дрова", "затраты" }),

            new Tuple<int, int, string[]>(9, 0, new[] { "Уголь", "потребление" }),
            new Tuple<int, int, string[]>(9, 1, new[] { "Уголь", "затраты" }),
            
            new Tuple<int, int, string[]>(10, 0, new[] { "Природный", "потребление" }),
            new Tuple<int, int, string[]>(10, 1, new[] { "Природный", "затраты" }),

            new Tuple<int, int, string[]>(12, 0, new[] { "Бензин", "потребление" }),
            new Tuple<int, int, string[]>(12, 1, new[] { "Бензин", "затраты" }),

            new Tuple<int, int, string[]>(13, 0, new[] { "Керосин", "потребление" }),
            new Tuple<int, int, string[]>(13, 1, new[] { "Керосин", "затраты" }),

            new Tuple<int, int, string[]>(14, 0, new[] { "Дизельное ", "потребление" }),
            new Tuple<int, int, string[]>(14, 1, new[] { "Дизельное ", "затраты" }),

            new Tuple<int, int, string[]>(15, 0, new[] { "Газ", "потребление" }),
            new Tuple<int, int, string[]>(15, 1, new[] { "Газ", "затраты" }),
        };

        private void ImportFiveYearIntakeData(string data, FiveYearIntakeEditModel model)
        {
            var table = TableParser.Parse(data, 2, 2);
            var matches = new List<Tuple<int, int, string[]>>(fiveyearintakeRowsMatch);
            var rows = new Queue<HeaderedStringString>(table.Rows());
            while (rows.Count > 0)
            {
                var row = rows.Dequeue();
                foreach (var match in matches)
                {
                    if (row.MatchesAll(match.Item3))
                    {
                        ProductionNumbersEditModel modelRow = null;
                        switch (match.Item2) // 0 или 1 - потребление или затраты
                        {
                            case 0:
                                modelRow = model.Units[match.Item1 * 2];
                                break;
                            case 1:
                                modelRow = model.Units[match.Item1 * 2 + 1];
                                break;
                        }
                        if (modelRow != null)
                        {
                            for (var index = 0; index <= 4; index++)
                            {
                                modelRow[index] = row.Cell(index).Replace(" ", string.Empty);
                            }
                        }
                        matches.Remove(match);
                        break;
                    }
                }
            }
        }

    }
}
