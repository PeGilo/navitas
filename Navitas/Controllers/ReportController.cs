﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Controllers.Structures;
using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Report;
using Navitas.Services;
using Navitas.ViewModels.Report;

namespace Navitas.Controllers
{
    using BL = Soft33.EnergyPassport.V2.BL;
    using BLExt = Soft33.EnergyPassport.V2.BL.Extended;

    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [HandleError(ExceptionType = typeof(System.Exception), View = "Error", Order = 1)]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ServiceBaseException), View = "Error", Order = 2)]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ConcurrencyException), View = "ConcurrencyError", Order = 10)]
    public class ReportController : ControllerBase
    {
        /// <summary>
        /// Идентификатор шаблона, из которого генерируются другие шаблоны.
        /// </summary>
        private const int MASTER_REPORT_ID = 1;

        private Models.DataManager _dataManager;
        private ServiceManager _services;

        private BL.CalculationThermoCoeffs CalculationThermoCoeffs
        {
            get
            {
                return _services.SettingsService.GetCalculationThermoCoeffs();
            }
        }

        private BL.CalculationFinanceCoeffs CalculationFinanceCoeffs
        {
            get
            {
                return _services.SettingsService.GetCalculationFinanceCoeffs();
            }
        }

        public ReportController(Models.DataManager dataManager, ServiceManager services)
            : base(services)
        {
            _dataManager = dataManager;
            _services = services;

            
        }

        public ActionResult Test(int id)
        {
            BL.Inspection inspection = _services.InspectionService.GetFullInspectionById(id);

            BLExt.InspectionExtended model = _services.InspectionService.CreateInspectionExtended(inspection, CalculationThermoCoeffs, CalculationFinanceCoeffs);

            XslTransformHelper xslTransformer = new XslTransformHelper(Environment.AppSettings.ReportXsltFileFullName);
            RazorHelper razorHelper = new RazorHelper();

            model.Buildings[0].CSEC.SpecificEnergyConsumptionTable.Rows[0].Cells[0] = "<script>alert('hi');</script>";

            string template = ""
                + ""
    + "@{"
        + "{"
            + "Soft33.EnergyPassport.V2.BL.TemplateCalculations.TemplateTable table = Model.Buildings[0].CSEC.SpecificEnergyConsumptionTable;"
        + "<table>"
            
            + "@for(int i = 0; i < table.Rows.Count; i++) {"

               + "<tr class=\"@RowTypeClass(table.Rows[i].RowType)\">"
                + "@if(i == 0) {"
                    + "<td>@table.Rows[i].Cells[0]</td>"
                    + "<td>@table.Rows[i].Cells[1]</td>"
                    + "<td>@table.Rows[i].Cells[2]</td>"
                    + "<td>@table.Rows[i].Cells[3]</td>"

                + "}"
                + "else {"
                    + "<td>@table.Rows[i].Cells[0]</td>"
                    + "<td>@FormatDouble2(table.Rows[i].Cells[1])</td>"
                    + "<td>@FormatDouble2(table.Rows[i].Cells[2])</td>"
                    + "<td>@FormatDouble2(table.Rows[i].Cells[3])</td>"
                + "}"
                + "</tr>"
            + "} "
        + "</table>"
        + "}"
     + "}"
                + ""
                + "";

            ViewBag.HtmlLayout = razorHelper.Parse<BLExt.InspectionExtended>(template, model);

            return View(model);
        }

        [HttpGet]
        [Authorize()]
        public ActionResult Edit(int id)
        {
            // Необходимо, чтобы объект Inspection также содержал Buildings, т.к. по ним будет строится отчет в случае генерации
            BL.Inspection inspection = _services.InspectionService.GetFullInspectionById(id);
            if (inspection != null)
            {
                BLExt.InspectionExtended extInspection = _services.InspectionService.CreateInspectionExtended(inspection, CalculationThermoCoeffs, CalculationFinanceCoeffs);

                // 1. Проверить существует ли отчет. Если еще не создан, то сгенерировать
                InspectionReport report = _services.ReportService.CreateOrGetExistingReport(MASTER_REPORT_ID, inspection);//id);

                // ... Подготовить данные отчета
                _services.ReportService.PrerenderInspection(extInspection, Environment);

                if (report == null)
                {
                    throw new InvalidOperationException("Ошибка при копировании мастер-шаблона.");
                }

                // 2. Загрузка отчета в форму
                XslTransformHelper xslTransformer = new XslTransformHelper(Environment.AppSettings.ReportXsltFileFullName);
                RazorHelper razorHelper = new RazorHelper();

                ViewModels.Report.ReportEditModel model = new ViewModels.Report.ReportEditModel(xslTransformer, razorHelper, report, extInspection);
                model.InspectionId = id;

                // В получившейся разметке необходимо декодироваться всякие &lt; &gt; получившиеся после xml-сериализации.
                // Сделать это можно либо в XLST либо здесь.
                //model.HtmlLayout = Server.HtmlDecode(model.HtmlLayout);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Authorize()]
        public ActionResult Edit(int id, string regenerate, FormCollection form)
        {
            // Проверить, чтобы регенерация отчета была только когдаль пользователь нажал на кнопку,
            // иначе просто вернуть отчет.
            if (String.IsNullOrEmpty(regenerate))
            {
                return Edit(id);
            }

            // Необходимо, чтобы объект Inspection также содержал Buildings, т.к. по ним будет строится отчет
            BL.Inspection inspection = _services.InspectionService.GetFullInspectionById(id);
            if (inspection != null)
            {
                BLExt.InspectionExtended extInspection = _services.InspectionService.CreateInspectionExtended(inspection, CalculationThermoCoeffs, CalculationFinanceCoeffs);

                // ... Подготовить данные отчета
                _services.ReportService.PrerenderInspection(extInspection, Environment);

                InspectionReport report = _services.ReportService.RegenerateReport(MASTER_REPORT_ID, inspection);//id);

                if (report == null)
                {
                    throw new InvalidOperationException("Ошибка при копировании мастер-шаблона.");
                }

                // 2. Загрузка отчета в форму
                XslTransformHelper xslTransformer = new XslTransformHelper(Environment.AppSettings.ReportXsltFileFullName);
                RazorHelper razorHelper = new RazorHelper();

                ViewModels.Report.ReportEditModel model = new ViewModels.Report.ReportEditModel(xslTransformer, razorHelper, report, extInspection);
                model.InspectionId = id;

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }


        //[HttpPost]
        //public ActionResult HideText(FormCollection form)
        //{
        //    return HandleAjaxExceptions(delegate()
        //    {
        //        // Загрузить параметры из запроса
        //        int sectionId = SectionEditModel.ParseTextId(form["sectionId"]);
        //        int paragraphId = EditableTextElementEditModel.ParseTextId(form["paragraphId"]);
        //        Binary timestamp = ConvertHelper.ParseBinary(form["timestamp"]);

        //        // Спрятать текстовый параграф раздела.
        //        Binary newTimestamp = _services.ReportService.HideEditableTextElement(sectionId, paragraphId, timestamp);

        //        JsonStructureResult result = new JsonStructureResult();
        //        result.result = new { sectionTimestamp = ConvertHelper.Convert(newTimestamp) };
        //        return result;
        //    });
        //}

        ///// <summary>
        ///// Скрывает блок, в котором отображаются обязательные картинки для разделов (напр., "общий вид здания" и т.п.)
        ///// </summary>
        ///// <param name="form"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public ActionResult HideRequiredImageBlock(FormCollection form)
        //{
        //    return HandleAjaxExceptions(delegate()
        //    {
        //        // Загрузить параметры из запроса
        //        int sectionId = SectionEditModel.ParseTextId(form["sectionId"]);
        //        int blockId = RequiredImageElementEditModel.ParseTextId(form["blockId"]);
        //        Binary timestamp = ConvertHelper.ParseBinary(form["timestamp"]);

        //        // Спрятать иллюстрацию
        //        Binary newTimestamp = _services.ReportService.HideRequiredImageElement(sectionId, blockId, timestamp);

        //        JsonStructureResult result = new JsonStructureResult();
        //        result.result = new { sectionTimestamp = ConvertHelper.Convert(newTimestamp) };
        //        return result;
        //    });
        //}

        [HttpPost]
        public ActionResult HideElement(FormCollection form)
        {
            return HandleAjaxExceptions(delegate()
            {
                // Загрузить параметры из запроса
                int sectionId = SectionEditModel.ParseTextId(form["sectionId"]);
                int elementId = BaseElementEditModel.ParseTextId(form["elementId"]);
                Binary timestamp = ConvertHelper.ParseBinary(form["timestamp"]);

                // Спрятать иллюстрацию
                Binary newTimestamp = _services.ReportService.HideElement(sectionId, elementId, timestamp);

                JsonStructureResult result = new JsonStructureResult();
                result.result = new { sectionTimestamp = ConvertHelper.Convert(newTimestamp) };
                return result;
            });
        }

        ///// <summary>
        ///// Удаляет элемент с картинкой, загруженной в отчет пользователем
        ///// </summary>
        ///// <param name="form"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public ActionResult RemoveImage(FormCollection form)
        //{
        //    return HandleAjaxExceptions(delegate()
        //    {
        //        // Загрузить параметры из запроса
        //        int sectionId = SectionEditModel.ParseTextId(form["sectionId"]);
        //        int elementId = ImageElementEditModel.ParseTextId(form["elementId"]);
        //        Binary timestamp = ConvertHelper.ParseBinary(form["timestamp"]);

        //        // Спрятать иллюстрацию
        //        Binary newTimestamp = _services.ReportService.RemoveImageElement(sectionId, elementId, timestamp);

        //        JsonStructureResult result = new JsonStructureResult();
        //        result.result = new { sectionTimestamp = ConvertHelper.Convert(newTimestamp) };
        //        return result;
        //    });
        //}

        [HttpPost]
        public ActionResult InsertIllustration(FormCollection form)
        {
            return HandleAjaxExceptions(delegate()
            {
                // Загрузить параметры из запроса
                int illustrationId = Int32.Parse(form["illustrationId"]);
                int sectionId = SectionEditModel.ParseTextId(form["sectionId"]);
                int anchorElementId = BaseElementEditModel.ParseTextId(form["anchorElementId"]);
                Binary timestamp = ConvertHelper.ParseBinary(form["timestamp"]);
                bool before = Boolean.Parse(form["before"]);

                Binary newTimestamp;

                Report.Elements.ImageElement insertedElement = _services.ReportService.InsertImage(sectionId, anchorElementId, illustrationId, timestamp, before, out newTimestamp);

                // Отформатировать обновленный текстовый раздел и отправить пользователю
                ImageElementEditModel model = new ImageElementEditModel(insertedElement);

                string serialized = SerializationHelper.Serialize<ImageElementEditModel>(model);

                XslTransformHelper xslTransformer = new XslTransformHelper(Environment.AppSettings.ReportXsltFileFullName);
                RazorHelper razorHelper = new RazorHelper();
                string htmlTemplate = xslTransformer.Transform(serialized);

                BLExt.InspectionExtended inspection = _services.InspectionService.CreateEmptyInspectionExtended();
                string htmlLayout = razorHelper.Parse<BLExt.InspectionExtended>(htmlTemplate, inspection);

                JsonStructureResult result = new JsonStructureResult();
                result.result = new { htmlLayout = htmlLayout, sectionTimestamp = ConvertHelper.Convert(newTimestamp) };
                return result;
            });
        }


        [HttpPost]
        public ActionResult InsertText(EditableTextElementEditModel model, FormCollection form)
        {
            return HandleAjaxExceptions(delegate()
            {
                // Загрузить параметры из запроса
                int inspectionId;

                int sectionId = SectionEditModel.ParseTextId(form["sectionId"]);
                Binary timestamp = ConvertHelper.ParseBinary(form["timestamp"]);
                int anchorElementId = BaseElementEditModel.ParseTextId(form["anchorElementId"]);
                bool before = Boolean.Parse(form["before"]);

                BL.Inspection inspection = null;
                BLExt.InspectionExtended extInspection = null;

                // Прочитать обследование
                if (Int32.TryParse(form["inspectionId"], out inspectionId))
                {
                    inspection = _services.InspectionService.GetFullInspectionById(inspectionId);
                    extInspection = _services.InspectionService.CreateInspectionExtended(inspection, CalculationThermoCoeffs, CalculationFinanceCoeffs);
                }

                if (inspection != null && extInspection != null)
                {
                    ModelState.AddRuleViolations(model.GetRuleViolations(extInspection));//_services.InspectionService.CreateEmptyInspectionExtended()));

                    if (!ModelState.IsValid)
                    {
                        return HandleModelStateErrors(ModelState);
                    }

                    Binary newTimestamp;
                    Report.Elements.EditableTextElement insertedElement = _services.ReportService.InsertText(sectionId, anchorElementId, model.Content, timestamp, before, out newTimestamp);

                    // Отформатировать обновленный текстовый раздел и отправить пользователю
                    model = new EditableTextElementEditModel(insertedElement);

                    string serialized = SerializationHelper.Serialize<EditableTextElementEditModel>(model);

                    string htmlLayout = RenderLayout(extInspection, serialized);

                    JsonStructureResult result = new JsonStructureResult();
                    // Так как внутренняя разметка текста закодирована, то Html выглядит примерно так: <div>&lt;strong&gt;</div>
                    // Нам необходимо выровнять разметку для передачи на клиента, так что она должна выглядеть вот так: <div><strong></div>,
                    // чтобы правильно отображалась в html.
                    //result.result = new { htmlLayout = Server.HtmlDecode(htmlLayout), sectionTimestamp = ConvertHelper.Convert(newTimestamp) };
                    result.result = new { htmlLayout = htmlLayout, sectionTimestamp = ConvertHelper.Convert(newTimestamp) };

                    return result;
                }
                else
                {
                    return JsonStructureResult.NotFound;
                }
            });
        }

        [HttpPost]
        public ActionResult UpdateText(EditableTextElementEditModel model, FormCollection form)
        {
            return HandleAjaxExceptions(delegate()
            {
                // Загрузить параметры из запроса
                int inspectionId;

                int sectionId = SectionEditModel.ParseTextId(form["sectionId"]);
                Binary timestamp = ConvertHelper.ParseBinary(form["timestamp"]);

                BL.Inspection inspection = null;
                BLExt.InspectionExtended extInspection = null;

                // Прочитать обследование
                if (Int32.TryParse(form["inspectionId"], out inspectionId))
                {
                    inspection = _services.InspectionService.GetFullInspectionById(inspectionId);

                    extInspection = _services.InspectionService.CreateInspectionExtended(inspection, CalculationThermoCoeffs, CalculationFinanceCoeffs);
                }

                if (inspection != null && extInspection != null)
                {
                    ModelState.AddRuleViolations(model.GetRuleViolations(extInspection));//_services.InspectionService.CreateEmptyInspectionExtended()));

                    if (!ModelState.IsValid)
                    {
                        return HandleModelStateErrors(ModelState);
                    }

                    // Обновить текстовый параграф раздела.
                    Binary newTimestamp;
                    Report.Elements.EditableTextElement updatedElement = _services.ReportService.UpdateEditableTextElement(sectionId, model.Id, model.Content, timestamp, out newTimestamp);

                    // Отформатировать обновленный текстовый раздел и отправить пользователю
                    model = new EditableTextElementEditModel(updatedElement);

                    string serialized = SerializationHelper.Serialize<EditableTextElementEditModel>(model);

                    string htmlLayout = RenderLayout(extInspection, serialized);

                    JsonStructureResult result = new JsonStructureResult();
                    // Так как внутренняя разметка текста закодирована, то Html выглядит примерно так: <div>&lt;strong&gt;</div>
                    // Нам необходимо выровнять разметку для передачи на клиента, так что она должна выглядеть вот так: <div><strong></div>,
                    // чтобы правильно отображалась в html.
                    //result.result = new { htmlLayout = Server.HtmlDecode(htmlLayout), sectionTimestamp = ConvertHelper.Convert(newTimestamp) };
                    result.result = new { htmlLayout = htmlLayout, sectionTimestamp = ConvertHelper.Convert(newTimestamp) };

                    return result;
                }
                else
                {
                    return JsonStructureResult.NotFound;
                }
            });
        }

        private string RenderLayout(BLExt.InspectionExtended extInspection, string serialized)
        {
            XslTransformHelper xslTransformer = new XslTransformHelper(Environment.AppSettings.ReportXsltFileFullName);
            RazorHelper razorHelper = new RazorHelper();
            string htmlTemplate = xslTransformer.Transform(serialized);

            string htmlLayout = razorHelper.Parse<BLExt.InspectionExtended>(htmlTemplate, extInspection);
            return htmlLayout;
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

        }

        /// <summary>
        /// Для валидационных ошибок в модели возвращает соответствующий Json результат для клиента.
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        private static JsonStructureResult HandleModelStateErrors(ModelStateDictionary modelState)
        {
            IEnumerable<ModelError> modelStateErrors = modelState.Keys.SelectMany(key => modelState[key].Errors);

            foreach(ModelError error in modelStateErrors)
            {
                return JsonStructureResult.ValidationError(error.ErrorMessage);
            }

            return JsonStructureResult.InternalServerError;
        }
    }
}
