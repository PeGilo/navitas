﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Models;
using Navitas.Services;

namespace Navitas.Controllers
{
    public class HomeController : Controller
    {
        private DataManager _dataManager;

        public HomeController(DataManager dataManager, ServiceManager services)
        {
            _dataManager = dataManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
