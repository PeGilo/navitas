﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Services;

namespace Navitas.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ServiceBaseException), View = "Error", Order = 2)]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ConcurrencyException), View = "ConcurrencyError", Order = 10)]
    public class DataController : ControllerBase
    {
        private DataManager _dataManager;
        private ServiceManager _services;

        public DataController(DataManager dataManager, ServiceManager services)
            : base(services)
        {
            _dataManager = dataManager;
            _services = services;
        }

        public ActionResult MeterCaptionsElectricEnergy(string term)
        {
            return Json(_services.SettingsService.GetMeterCaptionsElectricEnergy(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterCaptionsColdWater(string term)
        {
            return Json(_services.SettingsService.GetMeterCaptionsColdWater(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterCaptionsHotWater(string term)
        {
            return Json(_services.SettingsService.GetMeterCaptionsHotWater(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterCaptionsHeating(string term)
        {
            return Json(_services.SettingsService.GetMeterCaptionsHeating(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterCaptionsGas(string term)
        {
            return Json(_services.SettingsService.GetMeterCaptionsGas(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterPlacesElectricEnergy(string term)
        {
            return Json(_services.SettingsService.GetMeterPlacesElectricEnergy(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterPlacesColdWater(string term)
        {
            return Json(_services.SettingsService.GetMeterPlacesColdWater(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterPlacesHotWater(string term)
        {
            return Json(_services.SettingsService.GetMeterPlacesHotWater(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterPlacesHeating(string term)
        {
            return Json(_services.SettingsService.GetMeterPlacesHeating(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterPlacesGas(string term)
        {
            return Json(_services.SettingsService.GetMeterPlacesGas(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterClassesElectricEnergy(string term)
        {
            return Json(_services.SettingsService.GetMeterClassesElectricEnergy(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterClassesColdWater(string term)
        {
            return Json(_services.SettingsService.GetMeterClassesColdWater(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterClassesHotWater(string term)
        {
            return Json(_services.SettingsService.GetMeterClassesHotWater(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterClassesHeating(string term)
        {
            return Json(_services.SettingsService.GetMeterClassesHeating(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MeterClassesGas(string term)
        {
            return Json(_services.SettingsService.GetMeterClassesGas(term), JsonRequestBehavior.AllowGet);
        }
    }
}
