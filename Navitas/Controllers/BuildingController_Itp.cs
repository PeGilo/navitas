﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Services;
using Navitas.Services.Structures;

namespace Navitas.Controllers
{
    using BL = Soft33.EnergyPassport.V2.BL;
    
    public partial class BuildingController
    {
        [HttpGet]
        public ActionResult ItpEquipment(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                //CreateRowsIfEmpty(b.WallingOfBuilding);

                var model = new ViewModels.Building.ItpEquipmentEditModel(b.ItpEquipment);

                return View(model);
            }

            return NotFound();
        }


        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult ItpEquipment_Save(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);
            
            // Перегрузить объект заново
            return ItpEquipment(id);
        }


        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removeheatexchanger\\d+$")]
        public ActionResult ItpEquipment_RemoveHeatExchanger(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removeheatexchanger")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(19));
                model.HeatExchangers.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removeboiler\\d+$")]
        public ActionResult ItpEquipment_RemoveBoiler(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removeboiler")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(12));
                model.Boilers.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removepumpingequipment\\d+$")]
        public ActionResult ItpEquipment_RemovePumpingEquipment(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removepumpingequipment")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(22));
                model.PumpingEquipment.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult ItpEquipment_NavBack(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            // Редирект на страницу Перечня электрооборудования (Освещение наружное)
            return RedirectToAction("Electroload", new { id = id, list = "ele" });
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult ItpEquipment_NavForward(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("VentIn", new { id = id });
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult ItpEquipment_Navigation(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addHeatexchanger")]
        public ActionResult ItpEquipment_AddHeatExchanger(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.HeatExchangerItemEditModel(new BL.HeatExchanger());
            model.HeatExchangers.Add(unitModel);

            return View(model);
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addBoiler")]
        public ActionResult ItpEquipment_AddBoiler(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.BoilerItemEditModel(new BL.Boiler());
            model.Boilers.Add(unitModel);

            return View(model);
        }

        [ActionName("ItpEquipment")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addPumpingEquipment")]
        public ActionResult ItpEquipment_AddPumpingEquipment(int id, ViewModels.Building.ItpEquipmentEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.PumpingEquipmentItemEditModel(new BL.PumpingEquipment());
            model.PumpingEquipment.Add(unitModel);

            return View(model);
        }


        [HttpGet]
        public ActionResult GetNewHeatExchangerItem(int index)
        {
            var model = new ViewModels.Building.HeatExchangerItemEditModel(new BL.HeatExchanger());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "heatexchanger";
            ViewData["Prefix"] = String.Format("HeatExchangers[{0}]", index);

            return PartialView("EditorTemplates/HeatExchangerItem", model);
        }

        [HttpGet]
        public ActionResult GetNewBoiler(int index)
        {
            var model = new ViewModels.Building.BoilerItemEditModel(new BL.Boiler());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "boiler";
            ViewData["Prefix"] = String.Format("Boilers[{0}]", index);

            return PartialView("EditorTemplates/BoilerItem", model);
        }

        [HttpGet]
        public ActionResult GetNewPumpingEquipment(int index)
        {
            var model = new ViewModels.Building.PumpingEquipmentItemEditModel(new BL.PumpingEquipment());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "pumpingequipment";
            ViewData["Prefix"] = String.Format("PumpingEquipment[{0}]", index);

            return PartialView("EditorTemplates/PumpingEquipmentItem", model);
        }


        private void updateExistingBuilding(int buildingId, ViewModels.Building.ItpEquipmentEditModel model)
        {
            BL.ItpEquipment ie = _services.BuildingService.CreateItpEquipment();
            model.UpdateEntity(ie);

            _services.BuildingService.UpdateItpEquipment(buildingId, ie);
        }

    }
}