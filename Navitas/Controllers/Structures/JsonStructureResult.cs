﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Navitas.Controllers.Structures
{
    public class JsonStructureResult
    {
        public int errorCode = 0;
        public string errorMessage;
        public object result;

        public static JsonStructureResult NotFound
        {
            get
            {
                return new JsonStructureResult() { errorCode = 2, errorMessage = "Данные не обнаружены в базе. Возможно другой пользователь их изменил или удалил." };
            }
        }

        public static JsonStructureResult InternalServerError
        {
            get
            {
                return new JsonStructureResult() { errorCode = 1, errorMessage = "Внутрення ошибка сервера. Попробуйте повторить выполнение операции." };
            }
        }

        public static JsonStructureResult ConcurrencyError
        {
            get
            {
                return new JsonStructureResult() { errorCode = 3, errorMessage = "Невозможно выполнить операцию - данные уже были изменены другим пользователем." };
            }
        }

        public static JsonStructureResult StringIsTooLong
        {
            get
            {
                return new JsonStructureResult() { errorCode = 4, errorMessage = "Длина строки превышает допустимое значение." };
            }
        }

        public static JsonStructureResult ValidationError(string errorMessage)
        {
            return new JsonStructureResult() { errorCode = 5, errorMessage = errorMessage };
        }
    }
}