﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Services;

using Navitas.Controllers.Helpers;
using Navitas.ViewModels.Building.Meter;

namespace Navitas.Controllers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public partial class BuildingController
    {
        /// <summary>
        /// Определяет последовательность переходов для View с приборами учета (для навигации назад и вперед).
        /// </summary>
        /// <remarks>Note: Такой же список находится в фабрике обработчиков - требует синхронизации (-)</remarks>
        private List<string> meterListSequence = new List<string>() { "ele", "clw", "htw", "htn", "gas" };

        /// <summary>
        /// Закладка с приборами учета для здания.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="list">Идентификатор списка.</param>
        /// <returns></returns>
        /// <remarks>Работает с пятью списками приборов учета. Какой из списков загружать - определяется
        /// параметром list.</remarks>
        [HttpGet]
        public ActionResult Meters(int id, string list)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                ListRequestHandler handler = ListRequestHandlerFactory.Instance.CreateHandler(_services, list);

                MeterBaseListEditModel model = handler.CreateModel(b.Meters);

                model.ListIdentifier = list;
                model.AddItemUrl = handler.AddItemUrl;

                return View("MasterList", model);
            }

            return NotFound();
        }

        [ActionName("Meters")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult Meters_Save(int id, ViewModels.Building.Base.ListEditModel model, FormCollection form)
        {
            ListRequestHandler handler = ListRequestHandlerFactory.Instance.CreateHandler(_services, model.ListIdentifier);
            MeterBaseListEditModel specificModel = handler.ConvertModel(model);

            if (!ModelState.IsValid)
            {
                return View("MasterList", specificModel);
            }

            handler.UpdateEntity(id, specificModel);

            // Перегрузить объект заново
            return Meters(id, model.ListIdentifier);
        }

        [ActionName("Meters")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult Meters_RemoveItem(int id, ViewModels.Building.Base.ListEditModel model, FormCollection form)
        {
            ListRequestHandler handler = ListRequestHandlerFactory.Instance.CreateHandler(_services, model.ListIdentifier);
            MeterBaseListEditModel specificModel = handler.ConvertModel(model);

            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));

                specificModel.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View("MasterList", specificModel);
        }

        [ActionName("Meters")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult Meters_AddItem(int id, ViewModels.Building.Base.ListEditModel model, FormCollection form)
        {
            ListRequestHandler handler = ListRequestHandlerFactory.Instance.CreateHandler(_services, model.ListIdentifier);
            MeterBaseListEditModel specificModel = handler.ConvertModel(model);

            specificModel.Items.Add(handler.CreateItem());

            return View("MasterList", specificModel);
        }


        [ActionName("Meters")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult Meters_NavBack(int id, ViewModels.Building.Base.ListEditModel model, FormCollection form)
        {
            ListRequestHandler handler = ListRequestHandlerFactory.Instance.CreateHandler(_services, model.ListIdentifier);
            MeterBaseListEditModel specificModel = handler.ConvertModel(model);

            if (!ModelState.IsValid)
            {
                return View("MasterList", specificModel);
            }

            handler.UpdateEntity(id, specificModel);

            return RedirectToPrevMeterlist(model.ListIdentifier, id);
        }

        [ActionName("Meters")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult Meters_Next(int id, ViewModels.Building.Base.ListEditModel model, FormCollection form)
        {
            ListRequestHandler handler = ListRequestHandlerFactory.Instance.CreateHandler(_services, model.ListIdentifier);
            MeterBaseListEditModel specificModel = handler.ConvertModel(model);

            if (!ModelState.IsValid)
            {
                return View("MasterList", model);
            }

            handler.UpdateEntity(id, specificModel);

            return RedirectToNextMeterlist(model.ListIdentifier, id);
        }

        [ActionName("Meters")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult Meters_Navigation(int id, ViewModels.Building.Base.ListEditModel model, FormCollection form)
        {
            ListRequestHandler handler = ListRequestHandlerFactory.Instance.CreateHandler(_services, model.ListIdentifier);
            MeterBaseListEditModel specificModel = handler.ConvertModel(model);

            if (!ModelState.IsValid)
            {
                return View("MasterList", model);
            }

            handler.UpdateEntity(id, specificModel);

            return NavigationHandler.HandleSubmit(this, form, id);
        }


        [HttpGet]
        public ActionResult GetNewMeterColdWaterItem(int index)
        {
            // TODO: сделать через ListHandler.CreateItem()
            var model = new MeterColdWaterItem(_services.BuildingService.CreateMeterColdWaterItem());
            ViewData["Index"] = index;
            return PartialView("EditorTemplates/MeterColdWaterItem", model);
        }

        [HttpGet]
        public ActionResult GetNewMeterElectricEnergyItem(int index)
        {
            // TODO: сделать через ListHandler.CreateItem()
            var model = new MeterElectricEnergyItem(_services.BuildingService.CreateMeterElectricEnergyItem());
            ViewData["Index"] = index;
            return PartialView("EditorTemplates/MeterElectricEnergyItem", model);
        }

        [HttpGet]
        public ActionResult GetNewMeterGasItem(int index)
        {
            // TODO: сделать через ListHandler.CreateItem()
            var model = new MeterGasItem(_services.BuildingService.CreateMeterGasItem());
            ViewData["Index"] = index;
            return PartialView("EditorTemplates/MeterGasItem", model);
        }

        [HttpGet]
        public ActionResult GetNewMeterHeatingItem(int index)
        {
            // TODO: сделать через ListHandler.CreateItem()
            var model = new MeterHeatingItem(_services.BuildingService.CreateMeterHeatingItem());
            ViewData["Index"] = index;
            return PartialView("EditorTemplates/MeterHeatingItem", model);
        }

        [HttpGet]
        public ActionResult GetNewMeterHotWaterItem(int index)
        {
            // TODO: сделать через ListHandler.CreateItem()
            var model = new MeterHotWaterItem(_services.BuildingService.CreateMeterHotWaterItem());
            ViewData["Index"] = index;
            return PartialView("EditorTemplates/MeterHotWaterItem", model);
        }

        /// <summary>
        /// Для списков приборов учета определяет на какую страницу необходимо перейти.
        /// </summary>
        /// <param name="curView">Идентификатор текущего списка приборов учета.</param>
        /// <param name="id">Идентификатор здания.</param>
        /// <returns></returns>
        private ActionResult RedirectToPrevMeterlist(string curView, int id)
        {
            int index = meterListSequence.FindIndex(e => String.Equals(e, curView, StringComparison.InvariantCultureIgnoreCase));
            if (index == 0)
            {
                return RedirectToAction("WaterPoints", new { id = id });
            }
            else if (index > 0)
            {
                return RedirectToAction("Meters", new { id = id, list = meterListSequence[index - 1] });
            }
            else
            {
                throw new InvalidOperationException("Заданный идентификатор списка не обнаружен");
            }
        }

        /// <summary>
        /// Для списков приборов учета определяет на какую страницу необходимо перейти.
        /// </summary>
        /// <param name="curView">Идентификатор текущего списка приборов учета.</param>
        /// <param name="id">Идентификатор здания.</param>
        /// <returns></returns>
        private ActionResult RedirectToNextMeterlist(string curView, int id)
        {
            int index = meterListSequence.FindIndex(e => String.Equals(e, curView, StringComparison.InvariantCultureIgnoreCase));
            if (index == meterListSequence.Count - 1)
            {
                return RedirectToAction("Illustration", new { id = id });
            }
            else if (index < 0)
            {
                throw new InvalidOperationException("Заданный идентификатор списка не обнаружен");
            }
            else
            {
                return RedirectToAction("Meters", new { id = id, list = meterListSequence[index + 1] });
            }
        }
    }
}