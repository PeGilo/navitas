﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Services;
using Navitas.Services.Structures;
using Navitas.ViewModels.Building.Base;
using Navitas.ViewModels.Building.Meter;

namespace Navitas.Controllers.Helpers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterColdWaterListHandler : ListRequestHandler
    {
        private ServiceManager _services;

        public override string AddItemUrl { get { return "/Building/GetNewMeterColdWaterItem"; } }

        public MeterColdWaterListHandler(ServiceManager services)
        {
            _services = services;
        }

        public override ViewModels.Building.Meter.MeterBaseListEditModel CreateModel(BL.MeterLists entity)
        {
            return new ViewModels.Building.Meter.MeterColdWaterListEditModel(entity);
        }

        public override ViewModels.Building.Meter.MeterBaseListEditModel ConvertModel(ListEditModel baseModel)
        {
            return new ViewModels.Building.Meter.MeterColdWaterListEditModel(baseModel);
        }

        public override ViewModels.Building.Meter.MeterBaseItem CreateItem()
        {
            return new MeterColdWaterItem(_services.BuildingService.CreateMeterColdWaterItem());
        }

        public override void UpdateEntity(int id, ViewModels.Building.Meter.MeterBaseListEditModel model)
        {
            BL.MeterLists list = _services.BuildingService.CreateMeterLists();
            model.UpdateEntity(list);
            _services.BuildingService.UpdateMetersColdWater(id, list);
        }
    }
}