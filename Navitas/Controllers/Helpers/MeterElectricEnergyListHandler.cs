﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Services;
using Navitas.Services.Structures;
using Navitas.ViewModels.Building.Base;
using Navitas.ViewModels.Building.Meter;


namespace Navitas.Controllers.Helpers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterElectricEnergyListHandler : ListRequestHandler
    {
        private ServiceManager _services;

        public override string AddItemUrl { get { return "/Building/GetNewMeterElectricEnergyItem"; } }

        public MeterElectricEnergyListHandler(ServiceManager services)
        {
            _services = services;
        }

        public override ViewModels.Building.Meter.MeterBaseListEditModel CreateModel(BL.MeterLists entity)
        {
            return new ViewModels.Building.Meter.MeterElectricEnergyListEditModel(entity);
        }

        public override ViewModels.Building.Meter.MeterBaseListEditModel ConvertModel(ViewModels.Building.Base.ListEditModel baseModel)
        {
            return new ViewModels.Building.Meter.MeterElectricEnergyListEditModel(baseModel);
        }

        public override ViewModels.Building.Meter.MeterBaseItem CreateItem()
        {
            return new MeterElectricEnergyItem(_services.BuildingService.CreateMeterElectricEnergyItem());
        }

        public override void UpdateEntity(int id, ViewModels.Building.Meter.MeterBaseListEditModel model)
        {
            BL.MeterLists list = _services.BuildingService.CreateMeterLists();
            model.UpdateEntity(list);
            _services.BuildingService.UpdateMetersElectricEnergy(id, list);
        }
    }
}