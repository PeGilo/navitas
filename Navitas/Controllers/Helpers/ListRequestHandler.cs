﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.ViewModels.Building.Base;

namespace Navitas.Controllers.Helpers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public abstract class ListRequestHandler
    {
        //public abstract string Title { get; }
        public abstract string AddItemUrl { get; }

        public abstract ViewModels.Building.Meter.MeterBaseListEditModel CreateModel(BL.MeterLists lists);

        public abstract ViewModels.Building.Meter.MeterBaseListEditModel ConvertModel(ListEditModel baseModel);

        public abstract ViewModels.Building.Meter.MeterBaseItem CreateItem();

        public abstract void UpdateEntity(int id, ViewModels.Building.Meter.MeterBaseListEditModel model);
    }
}