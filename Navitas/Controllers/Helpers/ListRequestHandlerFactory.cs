﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Services;

namespace Navitas.Controllers.Helpers
{
    public class ListRequestHandlerFactory
    {
//        protected Dictionary<String, ListRequestHandler> _handlers = new Dictionary<string, ListRequestHandler>();
        protected Dictionary<String, Func<ServiceManager, ListRequestHandler>> _handlers
            = new Dictionary<string, Func<ServiceManager, ListRequestHandler>>();

        protected static ListRequestHandlerFactory _instance;

        public static ListRequestHandlerFactory Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ListRequestHandlerFactory();
                return _instance;
            }
        }

        protected ListRequestHandlerFactory()
        {
            Init();
        }

        private void Init()
        {
            //_handlers.Add("ele", );
            //_handlers.Add("clw", new MeterColdWaterListHandler());
            //_handlers.Add("htw", new MeterHotWaterListHandler());
            //_handlers.Add("htn", new MeterHeatingListHandler());
            //_handlers.Add("gas", new MeterGasListHandler());

            _handlers.Add("ele", delegate(ServiceManager services) { return new MeterElectricEnergyListHandler(services); });
            _handlers.Add("clw", delegate(ServiceManager services) { return new MeterColdWaterListHandler(services); });
            _handlers.Add("htw", delegate(ServiceManager services) { return new MeterHotWaterListHandler(services); });
            _handlers.Add("htn", delegate(ServiceManager services) { return new MeterHeatingListHandler(services); });
            _handlers.Add("gas", delegate(ServiceManager services) { return new MeterGasListHandler(services); });
        }

        public ListRequestHandler CreateHandler(ServiceManager services, string identifier)
        {
            return _handlers[identifier](services);
        }
    }
}