﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Navitas.Services;
using Navitas.Services.Structures;
using Navitas.ViewModels.Building.Base;
using Navitas.ViewModels.Building.Meter;

namespace Navitas.Controllers.Helpers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public class MeterGasListHandler : ListRequestHandler
    {
        private ServiceManager _services;

        public override string AddItemUrl { get { return "/Building/GetNewMeterGasItem"; } }

        public MeterGasListHandler(ServiceManager services)
        {
            _services = services;
        }

        public override ViewModels.Building.Meter.MeterBaseListEditModel CreateModel(BL.MeterLists entity)
        {
            return new ViewModels.Building.Meter.MeterGasListEditModel(entity);
        }

        public override ViewModels.Building.Meter.MeterBaseListEditModel ConvertModel(ViewModels.Building.Base.ListEditModel baseModel)
        {
            return new ViewModels.Building.Meter.MeterGasListEditModel(baseModel);
        }

        public override ViewModels.Building.Meter.MeterBaseItem CreateItem()
        {
            return new MeterGasItem(_services.BuildingService.CreateMeterGasItem());
        }

        public override void UpdateEntity(int id, ViewModels.Building.Meter.MeterBaseListEditModel model)
        {
            BL.MeterLists list = _services.BuildingService.CreateMeterLists();
            model.UpdateEntity(list);
            _services.BuildingService.UpdateMetersGas(id, list);
        }
    }
}