﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Services;
using Navitas.Services.Structures;

namespace Navitas.Controllers
{
    public class FileController : ControllerBase
    {
        private DataManager _dataManager;
        private ServiceManager _services;

        public FileController(DataManager dataManager, ServiceManager services)
            : base(services)
        {
            _dataManager = dataManager;
            _services = services;
        }

        //
        // GET: /File/Illustration/2h4h7572h.jpg /File/Illustration/234hhjhg45.txt
        [HttpGet]
        public ActionResult Illustration(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                // 1. Get full path of file
                //File file = _fileRepository.GetByName(fileName);
                FileManager fm = new FileManager(Environment);
                string fileFullPath = fm.GetInspectionIllustrationFullPath(fileName);

                if (!String.IsNullOrEmpty(fileFullPath))
                {
                    // 2. Response to client
                    return File(fileFullPath, FileManager.GetMimeType(fileFullPath), fileName);
                }
            }

            // return 404
            //Response.StatusCode = 404;
            //throw new HttpException(404, "Some description");
            throw new HttpException(404, "Иллюстрация с заданным именем отсутствует.");
        }

        //
        // GET: /File/Thumb/2h4h7572h.jpg
        [HttpGet]
        public ActionResult Thumb(string fileName, int? w, int? h)
        {
            // 1. Search if thumb already exists
            if (!String.IsNullOrEmpty(fileName))
            {
                if (!w.HasValue) w = 100;
                if (!h.HasValue) h = 100;

                ThumbManager tm = new ThumbManager(Environment);
                string thumbFullPath = tm.GetThumb(fileName, w.Value, h.Value);

                // if not exist, create
                if (String.IsNullOrEmpty(thumbFullPath))
                {
                    //File file = _fileRepository.GetByName(fileName);

                    thumbFullPath = tm.CreateThumb(fileName, w.Value, h.Value);
                }

                return File(thumbFullPath, FileManager.GetMimeType(thumbFullPath), fileName);
            }
            // 2. If does not exist create
            throw new HttpException(404, "Иллюстрация с заданным именем отсутствует.");
        }

        [HttpGet]
        public ActionResult Chart(string fileName)
        {
            // 1. Search if thumb already exists
            if (!String.IsNullOrEmpty(fileName))
            {
                ChartManager chm = new ChartManager(Environment);
                string chartFullPath = chm.GetChartFullPath(fileName);

                if (!String.IsNullOrEmpty(chartFullPath))
                {
                    return File(chartFullPath, FileManager.GetMimeType(chartFullPath), fileName);
                }
            }
            // 2. If does not exist create
            throw new HttpException(404, "График с заданным именем отсутствует.");
        }

        [HttpPost]
        public ActionResult AsyncUploadImage(HttpPostedFileBase file, int buildingId, string sectionId, FormCollection form)
        {
            bool errorState = false;    // default error state
            string errorMessage = String.Empty;
            string shortFileName = String.Empty;
            int id = 0;

            string userTitle = form["usertitle"];

            if(String.IsNullOrEmpty(userTitle))
            {
                errorState = true;
                errorMessage = "Не указано название рисунка";
            }
            else if (String.IsNullOrEmpty(sectionId))
            {
                errorState = true;
                errorMessage = "Не указано название раздела";
            }
            else if (file != null && file.ContentLength > 0)
            {
                try
                {
                    int inspectionId = _services.BuildingService.GetInspectionIdByBuildingId(buildingId);

                    if (inspectionId != 0)
                    {
                        // Сохранить файл
                        FileManager fm = new FileManager(Environment);
                        shortFileName = fm.StoreInspectionIllustration(inspectionId, file.InputStream);

                        // Сохранить информацию о файле в базе
                        Models.Illustration illustration = new Illustration();
                        illustration.BuildingID = buildingId;
                        illustration.InspectionID = inspectionId;
                        illustration.FileName = shortFileName;
                        illustration.DivisionName = StringHelper.Substring(sectionId, 512);
                        illustration.UserTitle = StringHelper.Substring(userTitle, 512);
                        id = _dataManager.IllustrationModels.Add<int>(illustration, "IllustrationID");
                    }
                    else
                    {
                        errorState = true;
                        errorMessage = "Обследование не найдено.";
                    }
                }
                catch (Exception ex)
                {
                    errorState = true;
                    errorMessage = ex.Message;
                }
            }

            return new AsyncUploadJsonResult
            {
                Data = new
                {
                    errorMessage = errorMessage,
                    errorState = errorState,
                    fileDescriptor = new { 
                            FileId = id,
                            FileName = shortFileName,
                            UserTitle = userTitle,
                            DivisionName = sectionId,
                            ImageUrl = "/File/Illustration/" + shortFileName,
                            ThumbUrl = "/File/Thumb/" + shortFileName
                        }
                }
            };
        }

        public class IllustrationInfo
        {
            public int IllustrationId {get; set;}
            public string FileName {get; set;}
            public string UserTitle {get; set;}
            public string DivisionName {get; set;}
            public string ImageUrl {get; set;}
            public string ThumbUrl { get; set; }
        }

        [HttpPost]
        public ActionResult AsyncGetImages(FormCollection form)
        {
            bool errorState = false;    // default error state
            string errorMessage = String.Empty;

            List<IllustrationInfo> illustrations = new List<IllustrationInfo>();

            try
            {
                int inspectionId;

                Int32.TryParse(form["inspectionId"], out inspectionId);

                if (inspectionId != 0)
                {
                    IList<Models.Illustration> models = _dataManager.IllustrationModels.GetIllustrationsByInspectionId(inspectionId);
                    illustrations = (from m in models
                                     select new IllustrationInfo()
                                     {
                                         IllustrationId = m.IllustrationID,
                                         UserTitle = m.UserTitle,
                                         FileName = m.FileName,
                                         DivisionName = m.DivisionName,
                                         ThumbUrl = "/File/Thumb/" + m.FileName,
                                         ImageUrl = "/File/Illustration/" + m.FileName
                                     }).ToList();
                }
                else
                {
                    errorState = true;
                    errorMessage = "Обследование не найдено.";
                }
            }
            catch (Exception ex)
            {
                errorState = true;
                errorMessage = ex.Message;
            }

            return Json(
                new
                {
                    errorMessage = errorMessage,
                    errorState = errorState,
                    illustrations = illustrations
                }
            );
        }



        [HttpPost]
        public ActionResult AsyncDelete(int fileId)
        {
            bool errorState = false;    // default error state
            string errorMessage = String.Empty;

            try
            {
                Models.Illustration illustration = _dataManager.IllustrationModels.GetIllustrationById(fileId);

                if (illustration != null)
                {
                    // Удалить информацию о файле в базе
                    _dataManager.IllustrationModels.Remove(illustration);
                    _dataManager.Save();
                }
                else
                {
                    errorState = true;
                    errorMessage = "Иллюстрация не найдена.";
                }
            }
            catch (Exception ex)
            {
                errorState = true;
                errorMessage = ex.Message;
            }

            return Json(
                new
                {
                    errorMessage = errorMessage,
                    errorState = errorState
                }
            );
        }

        private string ValidateFileUpload(string fileName)
        {
            if (!FileManager.IsImage(fileName))
            {
                return "acceptFileTypes";
            }
            return String.Empty;
        }

    }
}
