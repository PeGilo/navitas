﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Services;

namespace Navitas.Controllers
{
    public class ErrorController : Controller
    {
        public ErrorController(Navitas.Models.DataManager dataManager, ServiceManager services)
        {
        }

        //
        // GET: /Error/
        public ActionResult Index()
        {
            return RedirectToAction("", "Home");
        }

        public ActionResult PageNotFound()
        {
            return View();
        }

        public ActionResult NotAuthorized()
        {
            return View();
        }
    }
}
