﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Parsing;
using Navitas.Services;
using Navitas.Services.Structures;
using Navitas.ViewModels.Intake;

namespace Navitas.Controllers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ServiceBaseException), View = "Error", Order = 2)]
    [HandleError(ExceptionType = typeof(Services.Exceptions.ConcurrencyException), View = "ConcurrencyError", Order = 10)]
    public partial class BuildingController : ControllerBase
    {
        private DataManager _dataManager;
        private ServiceManager _services;

        public BuildingController(DataManager dataManager, ServiceManager services)
            : base(services)
        {
            _dataManager = dataManager;
            _services = services;
        }

        #region "General Handlers"

        [HttpGet]
        [Authorize()]
        public ActionResult AddInfo(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Building.AddInfoEditModel(b.AdditionalInformation);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("AddInfo")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        [Authorize()]
        public ActionResult AddInfo_NavNext(int id, ViewModels.Building.AddInfoEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("IntakeLastYear", new { id = id});
        }

        [ActionName("AddInfo")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult AddInfo_Navigation(int id, ViewModels.Building.AddInfoEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        #endregion

        #region "Intake Handlers"

        [HttpGet]
        public ActionResult IntakeLastYear(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Intake.TwelveMonthIntakeEditModel(b.TwelveMonth);
                model.BaseYear = _services.InspectionService.GetBaseYearByBuildingId(id);
                model.ShowTariffs = false;

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult IntakeLastYear_NavPrev(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                model.ShowTariffs = false;
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("General", new { id = id });
        }

        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult IntakeLastYear_Save(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                model.ShowTariffs = false;
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingBuilding(id, model);

            ModelState.Clear();

            return IntakeLastYear(id);
        }

        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult IntakeLastYear_NavNext(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                model.ShowTariffs = false;
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingBuilding(id, model);

            // Редирект на страницу Перечня электрооборудования (Бытовое)
            return RedirectToAction("Electroload", new { id = id, list = "he" });
        }

        [ActionName("IntakeLastYear")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult IntakeLastYear_Navigation(int id, string import, ViewModels.Intake.TwelveMonthIntakeEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                ModelState.RemoveValidElements();
                model.ShowTariffs = false;
                return View(applyBusinessLogic(model));
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportIntakeLastYearData(import, model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        #endregion

        #region "VentIn Handlers"

        [HttpGet]
        public ActionResult VentIn(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Ventilation.CombinedInputListEditModel(b.VentilationCombinedExtractInputs);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("VentIn")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult VentIn_Save(int id, ViewModels.Ventilation.CombinedInputListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            // Перегрузить объект заново
            return VentIn(id);
        }

        [ActionName("VentIn")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult VentIn_RemoveItem(int id, ViewModels.Ventilation.CombinedInputListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("VentIn")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult VentIn_AddItem(int id, ViewModels.Ventilation.CombinedInputListEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Ventilation.CombinedInputItemEditModel(_services.BuildingService.CreateCombinedVentIn());
            model.Items.Add(unitModel);

            return View(model);
        }


        [ActionName("VentIn")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult VentIn_NavBack(int id, ViewModels.Ventilation.CombinedInputListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("ItpEquipment", new { id = id });
        }

        [ActionName("VentIn")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult VentIn_Next(int id, ViewModels.Ventilation.CombinedInputListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("VentOut", new { id = id });
        }

        [ActionName("VentIn")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult VentIn_Navigation(int id, ViewModels.Ventilation.CombinedInputListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Приточной вентиляции.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewVentinItem(int index)
        {
            var model = new ViewModels.Ventilation.CombinedInputItemEditModel(_services.BuildingService.CreateCombinedVentIn());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/VentInItem", model);
        }

        #endregion

        #region "VentOut Handlers"

        [HttpGet]
        public ActionResult VentOut(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Ventilation.ExtractListEditModel(b.VentilationExtracts);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("VentOut")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult VentOut_Save(int id, ViewModels.Ventilation.ExtractListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            // Перегрузить объект заново
            return VentOut(id);
        }

        [ActionName("VentOut")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult VentOut_RemoveItem(int id, ViewModels.Ventilation.ExtractListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("VentOut")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult VentOut_AddItem(int id, ViewModels.Ventilation.ExtractListEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Ventilation.ExtractItemEditModel(_services.BuildingService.CreateVentOut());
            model.Items.Add(unitModel);

            return View(model);
        }

        [ActionName("VentOut")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult VentOut_NavBack(int id, ViewModels.Ventilation.ExtractListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("VentIn", new { id = id });
        }

        [ActionName("VentOut")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult VentOut_Next(int id, ViewModels.Ventilation.ExtractListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("Radiators", new { id = id });
        }

        [ActionName("VentOut")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult VentOut_Navigation(int id, ViewModels.Ventilation.ExtractListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Вытяжной вентиляции.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewVentoutItem(int index)
        {
            var model = new ViewModels.Ventilation.ExtractItemEditModel(_services.BuildingService.CreateVentOut());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/VentOutItem", model);
        }

        #endregion

        #region "Radiators Handlers"

        [HttpGet]
        public ActionResult Radiators(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Radiator.RadiatorListEditModel(b.Radiators);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("Radiators")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult Radiators_Save(int id, ViewModels.Radiator.RadiatorListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            // Перегрузить объект заново
            return Radiators(id);
        }

        [ActionName("Radiators")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult Radiators_RemoveItem(int id, ViewModels.Radiator.RadiatorListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Radiators")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult Radiators_AddItem(int id, ViewModels.Radiator.RadiatorListEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Radiator.RadiatorItemEditModel(_services.BuildingService.CreateRadiator());
            model.Items.Add(unitModel);

            return View(model);
        }

        [ActionName("Radiators")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult Radiators_NavBack(int id, ViewModels.Radiator.RadiatorListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("VentOut", new { id = id });
        }

        [ActionName("Radiators")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult Radiators_NavForward(int id, ViewModels.Radiator.RadiatorListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("Technical", new { id = id });
        }

        [ActionName("Radiators")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult Radiators_Navigation(int id, ViewModels.Radiator.RadiatorListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Приборов отопления.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewRadiatorItem(int index)
        {
            var model = new ViewModels.Radiator.RadiatorItemEditModel(_services.BuildingService.CreateRadiator());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/RadiatorItem", model);
        }

        #endregion

        #region "Power Quality Handlers"

        [HttpGet]
        public ActionResult PowerQty(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Building.SimplePowerQualityEditModel(b.PowerQualitys);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("PowerQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult PowerQty_Save(int id, ViewModels.Building.SimplePowerQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            // Перегрузить объект заново
            return PowerQty(id);
        }


        [ActionName("PowerQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult PowerQty_NavBack(int id, ViewModels.Building.SimplePowerQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("Technical", new { id = id });
        }

        [ActionName("PowerQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult PowerQty_NavForward(int id, ViewModels.Building.SimplePowerQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("EnvQty", new { id = id });
        }

        [ActionName("PowerQty")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult PowerQty_Navigation(int id, ViewModels.Building.SimplePowerQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        [ActionName("PowerQty")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult PowerQty_RemoveItem(int id, ViewModels.Building.SimplePowerQualityEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("PowerQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult PowerQty_AddItem(int id, ViewModels.Building.SimplePowerQualityEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.SimplePowerQualityItemEditModel(_services.BuildingService.CreatePowerQuality());
            model.Items.Add(unitModel);

            return View(model);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Замеров качества электроэнергии.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewSimplePowerQualityItem(int index)
        {
            var model = new ViewModels.Building.SimplePowerQualityItemEditModel(_services.BuildingService.CreatePowerQuality());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/SimplePowerQtyItem", model);
        }

        #endregion

        #region "Environment Quality Handlers"

        [HttpGet]
        public ActionResult EnvQty(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Building.EnvironmentQualityEditModel(b.EnvironmentQualitys);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("EnvQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult EnvQty_Save(int id, ViewModels.Building.EnvironmentQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            // Перегрузить объект заново
            return EnvQty(id);
        }


        [ActionName("EnvQty")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult EnvQty_RemoveItem(int id, ViewModels.Building.EnvironmentQualityEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("EnvQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult EnvQty_AddItem(int id, ViewModels.Building.EnvironmentQualityEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.EnvironmentQualityItemEditModel(_services.BuildingService.CreateEnvironmentQuality());
            model.Items.Add(unitModel);

            return View(model);
        }


        [ActionName("EnvQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult EnvQty_NavBack(int id, ViewModels.Building.EnvironmentQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("PowerQty", new { id = id });
        }

        [ActionName("EnvQty")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult EnvQty_NavForward(int id, ViewModels.Building.EnvironmentQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("WaterPoints", new { id = id });
        }

        [ActionName("EnvQty")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult EnvQty_Navigation(int id, ViewModels.Building.EnvironmentQualityEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Замеров эксплуатационной среды.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewEnvQualityItem(int index)
        {
            var model = new ViewModels.Building.EnvironmentQualityItemEditModel(_services.BuildingService.CreateEnvironmentQuality());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/EnvQtyItem", model);
        }

        #endregion

        #region "Point of Waters Handlers"

        [HttpGet]
        public ActionResult WaterPoints(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Building.PointOfWaterListEditModel(b.PointOfWaters);

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("WaterPoints")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult WaterPoints_Save(int id, ViewModels.Building.PointOfWaterListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            // Перегрузить объект заново
            return WaterPoints(id);
        }

        [ActionName("WaterPoints")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult WaterPoints_RemoveItem(int id, ViewModels.Building.PointOfWaterListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("WaterPoints")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult WaterPoints_AddItem(int id, ViewModels.Building.PointOfWaterListEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.PointOfWaterItemEditModel(_services.BuildingService.CreatePointOfWater());
            model.Items.Add(unitModel);

            return View(model);
        }

        [ActionName("WaterPoints")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult WaterPoints_NavBack(int id, ViewModels.Building.PointOfWaterListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("EnvQty", new { id = id });
        }

        [ActionName("WaterPoints")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult WaterPoints_NavForward(int id, ViewModels.Building.PointOfWaterListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("Meters", new { id = id, list="ele" });
        }

        [ActionName("WaterPoints")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult WaterPoints_Navigation(int id, ViewModels.Building.PointOfWaterListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Возвращает разметку для вставленной строки в таблицу Приборов отопления.
        /// </summary>
        /// <param name="index">Индекс вновь добавленной строки</param>
        /// <returns>Разметка</returns>
        [HttpGet]
        public ActionResult GetNewWaterPointItem(int index)
        {
            var model = new ViewModels.Building.PointOfWaterItemEditModel(_services.BuildingService.CreatePointOfWater());
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/WaterPointItem", model);
        }

        #endregion


        [HttpGet]
        public ActionResult Illustration(int id)
        {
            BL.Building b = _services.BuildingService.GetFullBuildingById(id);

            if (b != null)
            {
                var model = new ViewModels.Building.IllustrationListEditModel(b.Illustrations);

                ViewBag.BuildingId = id;

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [ActionName("Illustration")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult Illustration_NavPrev(int id)
        {
            return RedirectToAction("Meters", new { id = id, list = "gas" });
        }

        //[ActionName("Illustration")]
        //[HttpPost]
        //[MultiButton(MatchFormKey = "buttonSave")]
        //public ActionResult Illustration_Save(int id)
        //{
        //}

        [ActionName("Illustration")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult Illustration_Navigation(int id, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        /// <summary>
        /// Обновляет View model согласно бизнес-логике, через объект бизнес-логики
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ViewModels.Intake.TwelveMonthIntakeEditModel applyBusinessLogic(ViewModels.Intake.TwelveMonthIntakeEditModel model)
        {
            // View model должна обновиться с учетом бизнес-логики, для этого приходится
            // создавать объект бизнес-логики и новую view model.
            BL.TwelveMonthIntake boIntake = _services.BuildingService.CreateIntakeTwelveMonths();
            model.UpdateEntity(boIntake);
            var newModel = new ViewModels.Intake.TwelveMonthIntakeEditModel(boIntake);

            // Сохраняем BaseYear, чтобы не загружать Inspection из базы.
            newModel.BaseYear = model.BaseYear;

            return newModel;
        }

        #region "Updating methods"

        /// <summary>
        /// Изменить информацию о Потреблении за последний год (12 месяцев) в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Intake.TwelveMonthIntakeEditModel model)
        {
            BL.TwelveMonthIntake intake = new BL.TwelveMonthIntake();
            model.UpdateEntity(intake);
            _services.BuildingService.UpdateIntakeLastYear(buildingId, intake);
        }

        /// <summary>
        /// Изменить информацию о Комбинированной вентиляции в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Ventilation.CombinedInputListEditModel model)
        {
            BL.VentilationCombinedExtractInputList ventin = new BL.VentilationCombinedExtractInputList();
            model.UpdateEntity(ventin);
            _services.BuildingService.UpdateVentIn(buildingId, ventin);
        }

        /// <summary>
        /// Изменить информацию о Вытяжной вентиляции в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Ventilation.ExtractListEditModel model)
        {
            BL.VentilationExtractList ventout = new BL.VentilationExtractList();
            model.UpdateEntity(ventout);
            _services.BuildingService.UpdateVentOut(buildingId, ventout);
        }

        /// <summary>
        /// Изменить Общие характеристики в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Building.AddInfoEditModel model)
        {
            BL.BuildingAdditionalInformation addInfo = new BL.BuildingAdditionalInformation();
            model.UpdateEntity(addInfo);
            _services.BuildingService.UpdateAdditionalInformation(buildingId, addInfo);
        }

        /// <summary>
        /// Изменить информацию о Приборах отопления в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Radiator.RadiatorListEditModel model)
        {
            BL.RadiatorList radiators = new BL.RadiatorList();
            model.UpdateEntity(radiators);
            _services.BuildingService.UpdateRadiators(buildingId, radiators);
        }

        /// <summary>
        /// Изменить информацию о Качестве электроэнергии в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Building.SimplePowerQualityEditModel model)
        {
            BL.PowerQualityList spq = new BL.PowerQualityList();
            model.UpdateEntity(spq);
            _services.BuildingService.UpdateSimplePowerQuality(buildingId, spq);
        }

        /// <summary>
        /// Изменить информацию о Замерах эксплуатационной среды в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Building.EnvironmentQualityEditModel model)
        {
            BL.EnvironmentQualityList qtyList = _services.BuildingService.CreateEnvironmentQualityList();
            model.UpdateEntity(qtyList);
            _services.BuildingService.UpdateEnvironmentQualityList(buildingId, qtyList);
        }

        /// <summary>
        /// Изменить информацию о Точках водозабора в существующем здании.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        private void updateExistingBuilding(int buildingId, ViewModels.Building.PointOfWaterListEditModel model)
        {
            BL.PointOfWaterList waterPoints = _services.BuildingService.CreatePointOfWaterList();
            model.UpdateEntity(waterPoints);
            _services.BuildingService.UpdatePointOFWater(buildingId, waterPoints);        
        }

        #endregion

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Дополнить модель для транспорта общими списками по типу топлива и типу одометров
            if (filterContext.Controller.ViewData.Model is ViewModels.Building.AddInfoEditModel)
            {
                var model = (ViewModels.Building.AddInfoEditModel)filterContext.Controller.ViewData.Model;
                model.ResourceTypeList = GetResourceSupplyTypes();
                model.CityList = GetCityList();
            }
            // Для формы "Технические характеристика здания" дополнить модель списками по материалам
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.TechCharactersEditModel)
            {
                var model = (ViewModels.Building.TechCharactersEditModel)filterContext.Controller.ViewData.Model;
                model.WallMaterials = GetWallMaterials();
                model.AtticFloorMaterials = GetAtticFloorMaterials();
                model.BasementFloorMaterials = GetBasementFloorMaterials();
                model.BasementTypes = GetBasementTypes();
                model.DoorTypes = GetDoorTypes();
                model.WindowTypes = GetWindowTypes();
                //model.FoundationTypes = GetFoundationTypes();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.WallLayerEditModel)
            {
                ViewData["SelectionList"] = GetWallMaterials();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.AtticFloorEditModel)
            {
                ViewData["SelectionList"] = GetAtticFloorMaterials();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.BasementFloorEditModel)
            {
                ViewData["SelectionList"] = GetBasementFloorMaterials();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.TechDoorEditModel)
            {
                ViewData["SelectionList"] = GetDoorTypes();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.TechWindowEditModel)
            {
                ViewData["SelectionList"] = GetWindowTypes();
            }


            // Для формы "Точка разбора воды" дополнить модель списками точкам и устройствам
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.PointOfWaterListEditModel)
            {
                var model = (ViewModels.Building.PointOfWaterListEditModel)filterContext.Controller.ViewData.Model;
                model.PointsList = GetWaterPointsList();
                model.DevicesList = GetDevicesLists();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.PointOfWaterItemEditModel)
            {
                ViewData["PointsList"] = GetWaterPointsList();
                ViewData["DevicesList"] = GetDevicesLists();
            }
            // Для формы "Замеры эксплуатационной среды здания" дополнить модель списком типов помещений
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.EnvironmentQualityEditModel)
            {
                var model = (ViewModels.Building.EnvironmentQualityEditModel)filterContext.Controller.ViewData.Model;
                model.RoomTypes = GetRoomTypes();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.Building.EnvironmentQualityItemEditModel)
            {
                ViewData["RoomTypes"] = GetRoomTypes();
            }
        }

        /// <summary>
        /// Преобразует данные по типу ресуросов в структуру понятную для View
        /// </summary>
        /// <returns></returns>
        protected virtual SelectList GetResourceSupplyTypes()
        {
            IList<ListItem> list = _services.SettingsService.GetResourceSupplyTypes();
            list.Insert(0, new ListItem(0, ""));

            return new SelectList(list, "Id", "Text");
        }

        private SelectList GetWaterPointsList()
        {
            IList<ListTextItem> list = _services.SettingsService.GetWaterPointsList();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }

        private SelectList GetDevicesLists()
        {
            IList<ListTextItem> list = _services.SettingsService.GetDevicesLists();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }

        private SelectList GetRoomTypes()
        {
            IList<ListTextItem> list = _services.SettingsService.GetRoomTypes();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }
		
        private SelectList GetCityList()
        {
            IList<ListItem> list = _services.SettingsService.GetCityCoeffs();
            list.Insert(0, new ListItem(0, ""));

            return new SelectList(list, "Id", "Text");
        }

        private static readonly List<Tuple<int, int, string[]>> RowsMatch = new List<Tuple<int, int, string[]>>
        {
            new Tuple<int, int, string[]>(0, 0, new[] { "Электроэнергия", "натуральном" }),
            new Tuple<int, int, string[]>(0, 1, new[] { "Электроэнергия", "затраты" }),

            new Tuple<int, int, string[]>(2, 0, new[] { "Отопление", "натуральном" }),
            new Tuple<int, int, string[]>(2, 1, new[] { "Отопление", "затраты" }),

            new Tuple<int, int, string[]>(3, 0, new[] { "ГВС", "натуральном" }),
            new Tuple<int, int, string[]>(3, 1, new[] { "ГВС", "затраты" }),

            new Tuple<int, int, string[]>(4, 0, new[] { "Вентиляция", "натуральном" }),
            new Tuple<int, int, string[]>(4, 1, new[] { "Вентиляция", "затраты" }),

            new Tuple<int, int, string[]>(5, 0, new[] { "Холодное", "натуральном" }),
            new Tuple<int, int, string[]>(5, 1, new[] { "Холодное", "затраты" }),

            new Tuple<int, int, string[]>(6, 0, new[] { "Горячее", "натуральном" }),
            new Tuple<int, int, string[]>(6, 1, new[] { "Горячее", "затраты" }),

            new Tuple<int, int, string[]>(7, 0, new[] { "Водоотведение", "натуральном" }),
            new Tuple<int, int, string[]>(7, 1, new[] { "Водоотведение", "затраты" }),

            new Tuple<int, int, string[]>(8, 0, new[] { "Природный", "натуральном" }),
            new Tuple<int, int, string[]>(8, 1, new[] { "Природный", "затраты" }),

            new Tuple<int, int, string[]>(9, 0, new[] { "Дрова", "натуральном" }),
            new Tuple<int, int, string[]>(9, 1, new[] { "Дрова", "затраты" }),

            new Tuple<int, int, string[]>(10, 0, new[] { "Уголь", "натуральном" }),
            new Tuple<int, int, string[]>(10, 1, new[] { "Уголь", "затраты" }),

            new Tuple<int, int, string[]>(12, 0, new[] { "Бензин", "натуральном" }),
            new Tuple<int, int, string[]>(12, 1, new[] { "Бензин", "затраты" }),

            new Tuple<int, int, string[]>(13, 0, new[] { "Керосин", "натуральном" }),
            new Tuple<int, int, string[]>(13, 1, new[] { "Керосин", "затраты" }),

            new Tuple<int, int, string[]>(14, 0, new[] { "Дизельное ", "натуральном" }),
            new Tuple<int, int, string[]>(14, 1, new[] { "Дизельное ", "затраты" }),

            new Tuple<int, int, string[]>(15, 0, new[] { "Газ", "натуральном" }),
            new Tuple<int, int, string[]>(15, 1, new[] { "Газ", "затраты" }),
        };

        private void ImportIntakeLastYearData(string data, TwelveMonthIntakeEditModel model)
        {
            var table = TableParser.Parse(data, 3, 2);
            var matches = new List<Tuple<int, int, string[]>>(RowsMatch);
            var rows = new Queue<HeaderedStringString>(table.Rows());
            while (rows.Count > 0)
            {
                var row = rows.Dequeue();
                foreach (var match in matches)
                {
                    if (row.MatchesAll(match.Item3))
                    {
                        TwelveMonthsNumbersEditModel modelRow = null;
                        switch (match.Item2)
                        {
                            case 0:
                                modelRow = model.Values[match.Item1];
                                break;
                            case 1:
                                modelRow = model.ValuesRUR[match.Item1];
                                break;
                            case 3:
                                modelRow = model.Tariffs[match.Item1];
                                break;
                        }
                        if (modelRow != null)
                        {
                            for (var index = 0; index <= 11; index++)
                            {
                                modelRow.Months[index] = row.Cell(index).Replace(" ",string.Empty);
                            }
                        }
                        matches.Remove(match);
                        break;
                    }
                }
            }
        }
    }
}