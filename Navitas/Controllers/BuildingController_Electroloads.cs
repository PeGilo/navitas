﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Parsing;
using Navitas.Services;
using Navitas.ViewModels.Building;

namespace Navitas.Controllers
{
    using BL = Soft33.EnergyPassport.V2.BL;

    public partial class BuildingController
    {
        /// <summary>
        /// Хранит методы загрузки списков электрооборудования.
        /// string - Идентификатор списка.
        /// delegate - Определяет операцию по выборке списка из объекта.
        /// </summary>
        private Dictionary<string, Func<BL.Building, BL.ElectroloadList>> getDelegates =
            new Dictionary<string, Func<BL.Building, BL.ElectroloadList>>()
            {
                { "he", delegate(BL.Building b) { return b.HouseholdEquipment; } },
                { "pe", delegate(BL.Building b) { return b.ProcessEquipment; } },
                { "oe", delegate(BL.Building b) { return b.OfficeEquipment; } },
                { "hte", delegate(BL.Building b) { return b.HeatingEquipment; } },
                { "ile", delegate(BL.Building b) { return b.InternalLightingEquipment; } },
                { "ele", delegate(BL.Building b) { return b.ExternalLightingEquipment; } }
            };

        /// <summary>
        /// Хранит методы сохранения списков электрооборудования в бизнес-объектах.
        /// string - Идентификатор списка.
        /// delegate - Определяет операцию по сохранению списка.
        /// </summary>
        private Dictionary<string, Action<BL.Building, ViewModels.Building.ElectroloadListEditModel>> updateDelegates =
            new Dictionary<string, Action<BL.Building, ViewModels.Building.ElectroloadListEditModel>>()
            {
                { "he", delegate(BL.Building b, ViewModels.Building.ElectroloadListEditModel model) 
                            { model.UpdateEntity(b.HouseholdEquipment); } },
                { "pe", delegate(BL.Building b, ViewModels.Building.ElectroloadListEditModel model) 
                            { model.UpdateEntity(b.ProcessEquipment); } },
                { "oe", delegate(BL.Building b, ViewModels.Building.ElectroloadListEditModel model) 
                            { model.UpdateEntity(b.OfficeEquipment); } },
                { "hte", delegate(BL.Building b, ViewModels.Building.ElectroloadListEditModel model) 
                            { model.UpdateEntity(b.HeatingEquipment); } },
                { "ile", delegate(BL.Building b, ViewModels.Building.ElectroloadListEditModel model) 
                            { model.UpdateEntity(b.InternalLightingEquipment); } },
                { "ele", delegate(BL.Building b, ViewModels.Building.ElectroloadListEditModel model) 
                            { model.UpdateEntity(b.ExternalLightingEquipment); } }
            };

        /// <summary>
        /// Каждому идентификатора списка сопоставляет заголовок.
        /// </summary>
        private Dictionary<string, string> listNames = new Dictionary<string, string>()
        {
            { "he", Resources.Strings.Electrolad_HouseholdEquipment },
            { "pe", Resources.Strings.Electrolad_ProcessEquipment },
            { "oe", Resources.Strings.Electrolad_OfficeEquipment },
            { "hte", Resources.Strings.Electrolad_HeatingEquipment },
            { "ile", Resources.Strings.Electrolad_InternalLightingEquipment },
            { "ele", Resources.Strings.Electrolad_ExternalLightingEquipment }
        };

        /// <summary>
        /// Определяет последовательность переходов для View с энергооборудованием (для навигации назад и вперед).
        /// </summary>
        private List<string> listSequence = new List<string>() { "he", "pe", "oe", "hte", "ile", "ele" };

        /// <summary>
        /// Закладка с электрооборудованием для здания.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="list">Идентификатор списка.</param>
        /// <returns></returns>
        /// <remarks>Работает с шестью списками электрооборудования. Какой из списков загружать - определяется
        /// параметром list.</remarks>
        [HttpGet]
        public ActionResult Electroload(int id, string list)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                // Выбрать делегат по идентификатору списка
                Func<BL.Building, BL.ElectroloadList> getOperation;
                if (!String.IsNullOrEmpty(list) && getDelegates.TryGetValue(list, out getOperation))
                {
                    var model = new ViewModels.Building.ElectroloadListEditModel(getOperation(b));
                    model.ListIdentifier = list;
                    model.ListName = listNames[list];

                    return View(model);
                }
            }

            return NotFound();
        }

        [ActionName("Electroload")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult Electroload_Save(int id, string import, ViewModels.Building.ElectroloadListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ModelState.Clear();
                ImportElectroloadData(import, model);
            }

            if (updateExistingBuilding(id, model, model.ListIdentifier))
            {
                // Перегрузить объект заново
                return Electroload(id, model.ListIdentifier);
            }

            return NotFound();
        }



        [ActionName("Electroload")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult Electroload_RemoveItem(int id, ViewModels.Building.ElectroloadListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Electroload")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult Electroload_AddItem(int id, ViewModels.Building.ElectroloadListEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.ElectroloadItemEditModel(_services.BuildingService.CreateElectroloadItem());
            model.Items.Add(unitModel);

            return View(model);
        }


        [ActionName("Electroload")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult Electroload_NavBack(int id, string import, ViewModels.Building.ElectroloadListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportElectroloadData(import, model);
            }

            if (updateExistingBuilding(id, model, model.ListIdentifier))
            {
                return RedirectToPrevElectrolist(model.ListIdentifier, id);
            }

            return NotFound();
        }

        [ActionName("Electroload")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult Electroload_Next(int id, string import, ViewModels.Building.ElectroloadListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportElectroloadData(import, model);
            }

            if (updateExistingBuilding(id, model, model.ListIdentifier))
            {
                return RedirectToNextElectrolist(model.ListIdentifier, id);
            }

            return NotFound();
        }

        [ActionName("Electroload")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult Electroload_Navigation(int id, string import, ViewModels.Building.ElectroloadListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!string.IsNullOrEmpty(import))
            {
                ImportElectroloadData(import, model);
            }

            if (updateExistingBuilding(id, model, model.ListIdentifier))
            {
                return NavigationHandler.HandleSubmit(this, form, id);
            }

            return NotFound();
        }

        [HttpGet]
        public ActionResult GetNewElectroloadItem(int index)
        {
            var model = new ViewModels.Building.ElectroloadItemEditModel();
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/ElectroloadItem", model);
        }


        /// <summary>
        /// Изменяет список электрооборудования здания и сохраняет здание.
        /// Какой именно список изменится определяется переданным идентификатором.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="model"></param>
        /// <param name="listIdentifier">идентификатор списка</param>
        /// <returns>False, если операция, связанная с идентификатором списка, не найдена.</returns>
        private bool updateExistingBuilding(int buildingId, ViewModels.Building.ElectroloadListEditModel model,
            string listIdentifier)
        {
            // Выбрать делегат по идентификатору списка
            Action<BL.Building, ViewModels.Building.ElectroloadListEditModel> updateOperation;
            if (!String.IsNullOrEmpty(listIdentifier) && updateDelegates.TryGetValue(listIdentifier, out updateOperation))
            {
                BL.ElectroloadList elist = _services.BuildingService.CreateElectroloadList();

                model.UpdateEntity(elist);

                _services.BuildingService.UpdateBuilding(buildingId,
                    delegate(BL.Building building) { updateOperation(building, model); }
                    );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Для списков электрооборудования определяет на какую страницу необходимо перейти.
        /// </summary>
        /// <param name="curView">Идентификатор текущего списка электрооборудования.</param>
        /// <param name="id">Идентификатор здания.</param>
        /// <returns></returns>
        private ActionResult RedirectToPrevElectrolist(string curView, int id)
        {
            int index = listSequence.FindIndex(e => String.Equals(e, curView, StringComparison.InvariantCultureIgnoreCase));
            if (index == 0)
            {
                return RedirectToAction("IntakeLastYear", new { id = id });
            }
            else if (index > 0)
            {
                return RedirectToAction("Electroload", new { id = id, list = listSequence[index - 1] });
            }
            else
            {
                throw new InvalidOperationException("Заданный идентификатор списка не обнаружен");
            }
        }

        /// <summary>
        /// Для списков электрооборудования определяет на какую страницу необходимо перейти.
        /// </summary>
        /// <param name="curView">Идентификатор текущего списка электрооборудования.</param>
        /// <param name="id">Идентификатор здания.</param>
        /// <returns></returns>
        private ActionResult RedirectToNextElectrolist(string curView, int id)
        {
            int index = listSequence.FindIndex(e => String.Equals(e, curView, StringComparison.InvariantCultureIgnoreCase));
            if (index == listSequence.Count - 1)
            {
                return RedirectToAction("ItpEquipment", new { id = id });
            }
            else if (index < 0)
            {
                throw new InvalidOperationException("Заданный идентификатор списка не обнаружен");
            }
            else
            {
                return RedirectToAction("Electroload", new { id = id, list = listSequence[index + 1] });
            }
        }

        private void ImportElectroloadData(string data, ElectroloadListEditModel model)
        {
            var table = TableParser.Parse(data, 1, 2);
            var titleColumnIndex = table.GetColumnIndex("Наименование");
            var countColumnIndex = table.GetColumnIndex("Количество");
            var powerColumnIndex = table.GetColumnIndex("Мощность");
            var timeColumnIndex = table.GetColumnIndex("Время");

            model.Items.Clear();
            foreach (var row in table.Rows())
            {
                var title = row.Cell(titleColumnIndex);
                if (string.IsNullOrEmpty(title))
                {
                    continue;
                }

                model.Items.Add(new ElectroloadItemEditModel
                {
                    Name = title,
                    Count = row.Cell(countColumnIndex).Replace(" ", string.Empty),
                    Power = row.Cell(powerColumnIndex).Replace(" ", string.Empty),
                    TimeHoursPerYear = ConvertHelper.ParseDouble(row.Cell(timeColumnIndex).Replace(" ", string.Empty))
                });
            }
        }
    }
}