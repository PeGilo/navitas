﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Infrastructure;
using Navitas.Infrastructure.MvcHelpers;
using Navitas.Models;
using Navitas.Services;
using Navitas.Services.Structures;

namespace Navitas.Controllers
{
    using BL = Soft33.EnergyPassport.V2.BL;
    
    public partial class BuildingController
    {
        [HttpGet]
        public ActionResult Technical(int id)
        {
            BL.Building b = _services.BuildingService.GetBuildingById(id);

            if (b != null)
            {
                CreateRowsIfEmpty(b.WallingOfBuilding);

                var model = new ViewModels.Building.TechCharactersEditModel(b.WallingOfBuilding);

                return View(model);
            }

            return NotFound();
        }


        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult Technical_Save(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);
            
            // Перегрузить объект заново
            return Technical(id);
        }


        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removewall\\d+$")]
        public ActionResult Technical_RemoveWall(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removewall")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(10));
                model.Walls.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removeatticfloor\\d+$")]
        public ActionResult Technical_RemoveAtticFloor(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removeatticfloor")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(16));
                model.AtticFloor.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removebasementfloor\\d+$")]
        public ActionResult Technical_RemoveBasementFloor(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removebasementfloor")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(19));
                model.BasementFloor.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removedoor\\d+$")]
        public ActionResult Technical_RemoveDoor(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removedoor")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(10));
                model.Doors.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^removewindow\\d+$")]
        public ActionResult Technical_RemoveWindow(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("removewindow")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(12));
                model.Windows.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonPrev")]
        public ActionResult Technical_NavBack(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("Radiators", new { id = id });
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonNext")]
        public ActionResult Technical_NavForward(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return RedirectToAction("PowerQty", new { id = id });
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^navItem")]
        public ActionResult Technical_Navigation(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            updateExistingBuilding(id, model);

            return NavigationHandler.HandleSubmit(this, form, id);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addWall")]
        public ActionResult Technical_AddWall(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.WallLayerEditModel(_services.BuildingService.CreateWallLayer());
            model.Walls.Add(unitModel);

            return View(model);
        }

        [HttpGet]
        public ActionResult GetNewWallLayerItem(int index)
        {
            var model = new ViewModels.Building.WallLayerEditModel(_services.BuildingService.CreateWallLayer());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "wall";
            ViewData["Prefix"] = String.Format("Walls[{0}]", index);

            return PartialView("EditorTemplates/TechCharacterItem", model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addAtticFloor")]
        public ActionResult Technical_AddAtticFloor(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.AtticFloorEditModel(_services.BuildingService.CreateWallLayer());
            model.AtticFloor.Add(unitModel);

            return View(model);
        }

        [HttpGet]
        public ActionResult GetNewAtticLayerItem(int index)
        {
            var model = new ViewModels.Building.AtticFloorEditModel(_services.BuildingService.CreateWallLayer());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "atticfloor";
            ViewData["Prefix"] = String.Format("AtticFloor[{0}]", index);

            return PartialView("EditorTemplates/TechCharacterItem", model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addBasementFloor")]
        public ActionResult Technical_AddBasementFloor(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.BasementFloorEditModel(_services.BuildingService.CreateWallLayer());
            model.BasementFloor.Add(unitModel);

            return View(model);
        }

        [HttpGet]
        public ActionResult GetNewBasementLayerItem(int index)
        {
            var model = new ViewModels.Building.BasementFloorEditModel(_services.BuildingService.CreateWallLayer());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "basementfloor";
            ViewData["Prefix"] = String.Format("BasementFloor[{0}]", index);

            return PartialView("EditorTemplates/TechCharacterItem", model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addDoor")]
        public ActionResult Technical_AddDoor(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.TechDoorEditModel(_services.BuildingService.CreateDoor());
            model.Doors.Add(unitModel);

            return View(model);
        }

        [HttpGet]
        public ActionResult GetNewDoorItem(int index)
        {
            var model = new ViewModels.Building.TechDoorEditModel(_services.BuildingService.CreateDoor());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "door";
            ViewData["Prefix"] = String.Format("Doors[{0}]", index);

            return PartialView("EditorTemplates/TechDoorItem", model);
        }

        [ActionName("Technical")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addWindow")]
        public ActionResult Technical_AddWindow(int id, ViewModels.Building.TechCharactersEditModel model, FormCollection form)
        {
            var unitModel = new ViewModels.Building.TechWindowEditModel(_services.BuildingService.CreateWindow());
            model.Windows.Add(unitModel);

            return View(model);
        }

        [HttpGet]
        public ActionResult GetNewWindowItem(int index)
        {
            var model = new ViewModels.Building.TechWindowEditModel(_services.BuildingService.CreateWindow());
            ViewData["Index"] = index;
            ViewData["TypeIdentifier"] = "window";
            ViewData["Prefix"] = String.Format("Windows[{0}]", index);

            return PartialView("EditorTemplates/TechWindowItem", model);
        }

        private void updateExistingBuilding(int buildingId, ViewModels.Building.TechCharactersEditModel model)
        {
            BL.Walling walling = _services.BuildingService.CreateWalling();
            model.UpdateEntity(walling);

            _services.BuildingService.UpdateWalling(buildingId, walling);
        }


        protected virtual SelectList GetWallMaterials()
        {
            IList<ListTextItem> list = _services.SettingsService.GetWallMaterials();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }

        protected virtual SelectList GetAtticFloorMaterials()
        {
            IList<ListTextItem> list = _services.SettingsService.GetAtticFloorMaterials();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }

        protected virtual SelectList GetBasementFloorMaterials()
        {
            IList<ListTextItem> list = _services.SettingsService.GetBasementFloorMaterials();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }

        protected virtual SelectList GetDoorTypes()
        {
            IList<ListTextItem> list = _services.SettingsService.GetDoorTypes();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }

        protected virtual SelectList GetWindowTypes()
        {
            IList<ListTextItem> list = _services.SettingsService.GetWindowTypes();
            list.Insert(0, new ListTextItem("", ""));

            return new SelectList(list, "Id", "Text");
        }

        //protected virtual SelectList GetFoundationTypes()
        //{
        //    IList<ListTextItem> list = _services.SettingsService.GetFoundationTypes();
        //    list.Insert(0, new ListTextItem("", ""));

        //    return new SelectList(list, "Id", "Text");
        //}

        protected virtual SelectList GetBasementTypes()
        {
            IList<ListItem> list = EnumConverter.ToList(typeof(BL.BasementType));
            
            return new SelectList(list, "Id", "Text");
        }


        /// <summary>
        /// При первом открытии страницы, должно быть несколько пустых строк
        /// </summary>
        /// <param name="walling"></param>
        private void CreateRowsIfEmpty(BL.Walling walling)
        {
            //if (walling.Walls.Count == 0)
            //{
            //    walling.Walls.Add(_services.BuildingService.CreateWallLayer());
            //    walling.Walls.Add(_services.BuildingService.CreateWallLayer());
            //}

            //if (walling.AtticFloor.Count == 0)
            //{
            //    walling.AtticFloor.Add(_services.BuildingService.CreateWallLayer());
            //    walling.AtticFloor.Add(_services.BuildingService.CreateWallLayer());
            //}

            //if (walling.BasementFloor.Count == 0)
            //{
            //    walling.BasementFloor.Add(_services.BuildingService.CreateWallLayer());
            //    walling.BasementFloor.Add(_services.BuildingService.CreateWallLayer());
            //}
        }
    }
}