﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Navitas.Controllers.Structures;
using Navitas.Infrastructure.Environment;
using Navitas.Services;

namespace Navitas.Controllers
{
    public class ControllerBase : Controller
    {
        private ServiceManager _services;

        private AppEnvironment _environment;

        protected virtual AppEnvironment Environment
        {
            get { return _environment; }
        }

        public ControllerBase(ServiceManager services)
        {
            _services = services;

            _environment = EnvironmentFactory.GetEnvironment(this);
        }

        /// <summary>
        /// Выполняет необходимые действия при возникновении ситуации отсутствия 
        /// запрашиваемых данных в базе
        /// </summary>
        /// <returns></returns>
        protected virtual ActionResult NotFound()
        {
            return RedirectToAction("PageNotFound", "Error");
        }

        /// <summary>
        /// Записывает информацию о возникающих исключениях в структуру Json, отправляемую на клиента.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        protected virtual ActionResult HandleAjaxExceptions(Func<JsonStructureResult> action)
        {
            JsonStructureResult result;
            try
            {
                result = action();
            }
            catch(Services.Exceptions.NotFoundException ex)
            {
                result = JsonStructureResult.NotFound;
            }
            catch (Services.Exceptions.ConcurrencyException ex)
            {
                result = JsonStructureResult.ConcurrencyError;
            }
            catch (Services.Exceptions.ReadWriteException ex)
            {
                result = JsonStructureResult.InternalServerError;
            }
            catch (Exception ex)
            {
                result = JsonStructureResult.InternalServerError;
            }
            return Json(result);
        }
    }
}