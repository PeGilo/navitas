﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Сокращенный набор свойств Обследования, для списка обследований.
    /// </summary>
    public class InspectionBrief
    {
        public virtual int InspectionID { get; set; }

        public virtual string ClientCaption { get; set; }

        public virtual string AuditorFullName { get; set; }

        public InspectionBrief()
        {
        }
    }
}
