﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class VentilationExtract
    {
        public string Name { get; set; }

        public string ServedPremises { get; set; }

        public string VentilationSystemBrand { get; set; }

        public double AirFlow { get; set; }

        public double Power { get; set; }

        public int TimeHoursPerMonth { get; set; }

        public int TimeHoursPerYear { get; set; }

        public bool IsEmpty()
        {
            return (
                String.IsNullOrEmpty(Name)
                && String.IsNullOrEmpty(ServedPremises)
                && String.IsNullOrEmpty(VentilationSystemBrand)
                && AirFlow == default(double)
                && Power == default(double)
                && TimeHoursPerYear == default(int)
                && TimeHoursPerMonth == default(int)
                );
        }
    }
}
