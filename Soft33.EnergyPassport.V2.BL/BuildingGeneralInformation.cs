﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Soft33.EnergyPassport.V2.BL
{
    public class BuildingGeneralInformation
    {
        public string PhysicalAddress { get; set; }

        public string Name { get; set; }

        public BuildingPurpose Purpose { get; set; }

        public int RegularStaff { get; set; }
        public int SupportStaff { get; set; }

        [XmlIgnore]
        public int OtherPeople
        {
            get
            {
                return Visitors + Residents + Patients + Students;
            }
        }

        public int Visitors { get; set; }
        public int Residents { get; set; }
        public int Patients { get; set; }
        public int Students { get; set; }
    }
}
