﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Расчетная часовая  тепловая нагрузка на отопление здания, по укрупненным показателям 
    /// </summary>
    public class FormulaHeatingLoadPerHour
    {
        public FormulaHeatingLoadPerHour()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="cityCoeffs"></param>
        /// <param name="thermoCoeffs"></param>
        /// <param name="KHP">Расчетный коэф-ти инфильтрации</param>
        /// <returns></returns>
        public static double? Value(Building b, CityCoeffs cityCoeffs, double? KHP)
        {
            if (b == null || cityCoeffs == null || !KHP.HasValue
                || b.WallingOfBuilding.BuildingThermoCoeffs == null
                || !b.WallingOfBuilding.BuildingThermoCoeffs.Tin.HasValue
                || !b.WallingOfBuilding.BuildingThermoCoeffs.q0.HasValue
                )
            {
                return null;
            }

            double res = cityCoeffs.Values.a
                * b.WallingOfBuilding.BuildingVolume
                * b.WallingOfBuilding.BuildingThermoCoeffs.q0.Value
                * (b.WallingOfBuilding.BuildingThermoCoeffs.Tin.Value - cityCoeffs.Values.t0)
                * (1 + KHP.Value)
                * 0.000001;

            return res;
        }
    }
}
