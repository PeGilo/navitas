﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Расчетная часовая тепловая нагрузка приточной вентиляции по укрупненным показателям
    /// </summary>
    public class FormulaVentinLoadPerHourBase
    {
        public FormulaVentinLoadPerHourBase()
        {
        }

        public static double? Value(Building b, CityCoeffs cityCoeffs)
        {
            if (b != null && cityCoeffs != null
                && b.WallingOfBuilding.BuildingThermoCoeffs != null
                && b.WallingOfBuilding.BuildingThermoCoeffs.Tin.HasValue
                && b.WallingOfBuilding.BuildingThermoCoeffs.qv.HasValue
                )
            {
                return cityCoeffs.Values.a
                * b.WallingOfBuilding.BuildingVolume
                * b.WallingOfBuilding.BuildingThermoCoeffs.qv.Value
                * (b.WallingOfBuilding.BuildingThermoCoeffs.Tin.Value - cityCoeffs.Values.Tout_ventincalc)
                * 0.001;
            }
            return null;
        }
    }
}
