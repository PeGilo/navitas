﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Средний расчетный суточный объем потребления горячей воды 
    /// </summary>
    public class FormulaDhwConsumptionPerDay
    {
        //private Building building;

        public FormulaDhwConsumptionPerDay()
        {
        }

        public static double? Value(Building b)
        {
            if (b != null
                && b.WallingOfBuilding.BuildingThermoCoeffs.gDHW.HasValue
                && b.WallingOfBuilding.BuildingThermoCoeffs.m.HasValue
                )
            {
                return b.WallingOfBuilding.BuildingThermoCoeffs.gDHW.Value
                        * b.WallingOfBuilding.BuildingThermoCoeffs.m.Value
                        * 0.001;
            }
            return null;
        }
    }
}
