﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Расчетная часовая тепловая нагрузка на приточную вентиляцию
    /// </summary>
    public class FormulaVentinLoadPerHour
    {
        public FormulaVentinLoadPerHour()
        {
        }

        public static double? Value(Building b, double? QvrBase, CityCoeffs cityCoeffs)
        {
            if (b != null && cityCoeffs != null
                && QvrBase.HasValue
                && b.WallingOfBuilding.BuildingThermoCoeffs != null
                && b.WallingOfBuilding.BuildingThermoCoeffs.Tin.HasValue
                )
            {
                return QvrBase.Value
                    * (b.WallingOfBuilding.BuildingThermoCoeffs.Tin - cityCoeffs.Values.Tout_ventincalc)
                    / (b.WallingOfBuilding.BuildingThermoCoeffs.Tin - cityCoeffs.Values.Tout_ventin);
            }
            return null;
        }
    }
}
