﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Фактическую удельную тепловую характеристику на отопление
    /// </summary>
    public class FormulaSpecificThermalCharacteristics
    {
        public FormulaSpecificThermalCharacteristics()
        {
        }

        public static double? Value(Building b, CityCoeffs cityCoeffs, CalculationThermoCoeffs thermoCoeffs,
            out int? Zh_fact, out double? Tint_fact, out double? Text_fact)
        {
            Zh_fact = null; 
            Tint_fact = null;
            Text_fact = null;

            if (b != null && cityCoeffs != null && thermoCoeffs != null)
            {
                Zh_fact = GetValueOrDefault<int>(b.WallingOfBuilding.BuildingThermoCoeffs.zh_fact, cityCoeffs.Values.Zh_default);
                Tint_fact = GetValueOrDefault<double>(b.WallingOfBuilding.BuildingThermoCoeffs.Tint_fact, cityCoeffs.Values.Tint_default);
                Text_fact = GetValueOrDefault<double>(b.WallingOfBuilding.BuildingThermoCoeffs.Tout_fact, cityCoeffs.Values.Tout_default);

                double sumHeating = Enumerable.Range(1, 12).Select(i=>b.TwelveMonth.IntakeOfMonths[i].TERHeating.Value).Sum();

                return (sumHeating * 1163 * 1000)
                    / (24 * b.WallingOfBuilding.BuildingVolume * Zh_fact * thermoCoeffs.Knm * (Tint_fact - Text_fact));
            }
            return null;
        }

        /// <summary>
        /// Проверяет, если value имеет значение, то берет его. иначе берет дефолтовое значение
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        private static T GetValueOrDefault<T>(T? value, T def) where T : struct
        {
            if (value.HasValue)
            {
                return value.Value;
            }
            else
            {
                return def;
            }
        }

    }
}
