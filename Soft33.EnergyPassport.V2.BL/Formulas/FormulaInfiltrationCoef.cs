﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// расчетный коэффициент инфильтрации, обусловленной тепловым и ветровым напором, 
    /// т.е. соотношение тепловых потерь зданием с инфильтрацией и теплопередачей через наружные 
    /// ограждения при температуре наружного воздуха
    /// </summary>
    public class FormulaInfiltrationCoef
    {
        //private Building building;

        public FormulaInfiltrationCoef()
        {
        }

        public static double? Value(Building b, CityCoeffs cityCoeffs)
        {
            if (b == null || cityCoeffs == null
                //|| b.WallingOfBuilding.BuildingThermoCoeffs == null
                || !b.WallingOfBuilding.BuildingThermoCoeffs.Tin.HasValue
                //|| !b.WallingOfBuilding.BuildingThermoCoeffs.q0.HasValue
                )
            {
                return null;
            }

            double res = 0.01
                * Math.Sqrt(2 * 9.81 * b.WallingOfBuilding.BuildingHeight
                            * (1 - (cityCoeffs.Values.t0 + 273) / (b.WallingOfBuilding.BuildingThermoCoeffs.Tin.Value + 273))
                            + Math.Pow(cityCoeffs.Values.w0, 2)
                        );
            return res;
        }
    }
}
