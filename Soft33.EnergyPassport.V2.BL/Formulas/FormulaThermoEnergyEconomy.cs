﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Экономия тепловой энергии на отопление зданий
    /// </summary>
    public class FormulaThermoEnergyEconomy
    {
        public FormulaThermoEnergyEconomy()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Wa">годовое потребление тепла на отопление</param>
        /// <returns></returns>
        public static double Value(double Wa)
        {
            return Wa * 0.03 /* коэффициент отражающий экономию (3%) */;
        }
    }
}
