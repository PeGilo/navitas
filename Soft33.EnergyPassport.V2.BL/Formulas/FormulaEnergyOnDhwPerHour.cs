﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Среднечасовой за отопительный период расход тепловой энергии на горячее водоснабжение
    /// </summary>
    public class FormulaEnergyOnDhwPerHour
    {
        public FormulaEnergyOnDhwPerHour()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="Vhw">среднесуточный расчетный расход горячей воды всеми видами потребителей данного здания</param>
        /// <param name="t">температура холодной водопроводной воды в заданный перидо (в летний или зимний)</param>
        /// <param name="thermoCoeffs"></param>
        /// <returns></returns>
        public static double? Value(double? Vhw, double t, CalculationThermoCoeffs thermoCoeffs)
        {
            if (Vhw.HasValue
                && thermoCoeffs != null)
            {
                return ((Vhw.Value * (55 - t) * (1 + thermoCoeffs.Khi) * thermoCoeffs.p * thermoCoeffs.cw)
                    / (3.6 * 24)) * 860 * 0.000001;
            }
            return null;
        }
    }
}
