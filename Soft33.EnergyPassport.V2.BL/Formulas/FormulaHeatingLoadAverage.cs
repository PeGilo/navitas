﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Средняя часовая тепловая нагрузка на отопление здания 
    /// </summary>
    public class FormulaHeatingLoadAverage
    {
        private Building _building;
        private CityCoeffs _cityCoeffs;
        private double? _Qomax;

        public FormulaHeatingLoadAverage(Building b, CityCoeffs cityCoeffs, double? Qomax)
        {
            _building = b;
            _cityCoeffs = cityCoeffs;
            _Qomax = Qomax;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tout_cp">Средняя температура на месяц</param>
        /// <returns></returns>
        public double? Value(double Tout_cp)
        {
            if (_building.WallingOfBuilding.BuildingThermoCoeffs.Tin.HasValue
                && _building.AdditionalInformation.CityCoeffs != null
                && _Qomax.HasValue)
            {

                double Tj = _building.WallingOfBuilding.BuildingThermoCoeffs.Tin.Value;

                return _Qomax.Value * (Tj - Tout_cp)
                       / (Tj - _building.AdditionalInformation.CityCoeffs.Values.t0);
            }
            else
            {
                return null;
            }
        }
    }
}
