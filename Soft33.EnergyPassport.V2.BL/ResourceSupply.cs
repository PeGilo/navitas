﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class ResourceSupply
    {
        public ResourceSupplyType ElectricityResource { get; set; }

        public ResourceSupplyType HeatResource { get; set; }

        public ResourceSupplyType WaterResource { get; set; }

        public ResourceSupplyType SewerageResource { get; set; }

        public ResourceSupplyType GasResource { get; set; }
    }
}
