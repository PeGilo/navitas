﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Soft33.EnergyPassport.V2.BL
{
    public class BuildingAdditionalInformation
    {
        public BuildingAdditionalInformation()
        {
            Resources = new ResourceSupply();
            CityCoeffs = new CityCoeffs();
        }

        public int NumberOfFloor { get; set; }

        public int NumberOfEntrances { get; set; }

        //public double Height { get; set; }

        /// <summary>
        /// Год постройки
        /// </summary>
        public double? BuildingYear { get; set; }

        [XmlIgnore]
        public ResourceSupply Resources { get; set; }

        [XmlIgnore]
        public CityCoeffs CityCoeffs { get; set; }
    }
}
