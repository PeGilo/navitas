﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.Navitas.Common.Enums;

namespace Soft33.EnergyPassport.V2.BL
{
    public enum BuildingPurpose
    {
        [DisplayName(DisplayName="Неизвестно")]
        Undefined = 0,
        [DisplayName(DisplayName = "Жилое")]
        Residential = 1,
        [DisplayName(DisplayName = "Общественное")]
        Public = 2,
        [DisplayName(DisplayName = "Административное")]
        Administrative = 3,
        [DisplayName(DisplayName = "Промышленное")]
        Industrial = 4,
        [DisplayName(DisplayName = "Сельскохозяйственное")]
        Agricultural = 5
    }
}
