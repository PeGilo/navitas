﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL.TemplateCalculations
{
    /// <summary>
    /// Класс для отображения таблицы
    /// </summary>
    public class TemplateTable
    {
        public List<TemplateTableRow> Rows = new List<TemplateTableRow>();

        public TemplateTable()
        {
        }

        public TemplateTable AddRow(IEnumerable<object> values, TemplateTableRowType rowType = TemplateTableRowType.Data)
        {
            TemplateTableRow row = new TemplateTableRow(rowType);

            row.Cells.AddRange(values);

            this.Rows.Add(row);

            return this;
        }
    }
}
