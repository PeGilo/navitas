﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL.TemplateCalculations
{
    public class TemplateTableRow
    {
        public TemplateTableRow(TemplateTableRowType RowType)
        {
            this.RowType = RowType;
        }

        public TemplateTableRowType RowType { get; set; }

        public List<object> Cells = new List<object>();
    }
}
