﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL.TemplateCalculations
{
    public enum TemplateTableRowType
    {
        Header = 0,
        Data = 1,
        Footer = 2,
        DataBold = 3
    }
}
