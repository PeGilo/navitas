﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Котел
    /// </summary>
    public class Boiler
    {
        //Тип, марка		Потребляемая мощность, кВт		Часы работы в год, час	Примечание (где установлен, работает или нет, в каком состоянии)		
        public string Name { get; set; }

        public double? Power { get; set; }

        public double? HoursPerYear { get; set; }

        public string Comment { get; set; }

        public bool IsEmpty()
        {
            return !(
                !String.IsNullOrEmpty(Name)
                || Power.HasValue
                || HoursPerYear.HasValue
                || !String.IsNullOrEmpty(Comment)
                );
        }
    }
}
