﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// По умолчанию делается пять замеров с некоторым промежутком времени
    /// </summary>
    public class SimplePowerQuality
    {
        /*
         *  Элементы сделаны отдельными членами, а не коллекцией, т.к. если добавлять 
         *  элементы в коллекцию в конструкторе, то при десериализации их кол-во увеличивается.
         */

        public SimplePowerQualityItem Item0 { get; set; }
        public SimplePowerQualityItem Item1 { get; set; }
        public SimplePowerQualityItem Item2 { get; set; }
        public SimplePowerQualityItem Item3 { get; set; }
        public SimplePowerQualityItem Item4 { get; set; }

        public int Count
        {
            get { return 5; }
        }

        public SimplePowerQuality()
        {
            //Items = new List<SimplePowerQualityItem>();

            //Items.Add(new SimplePowerQualityItem());
            //Items.Add(new SimplePowerQualityItem());
            //Items.Add(new SimplePowerQualityItem());
            //Items.Add(new SimplePowerQualityItem());
            //Items.Add(new SimplePowerQualityItem());

            Item0 = new SimplePowerQualityItem();
            Item1 = new SimplePowerQualityItem();
            Item2 = new SimplePowerQualityItem();
            Item3 = new SimplePowerQualityItem();
            Item4 = new SimplePowerQualityItem();
        }

        //public List<SimplePowerQualityItem> Items { get; set; }

        public SimplePowerQualityItem this[int index] {
            get
            {
                switch (index)
                {
                    case 0:
                        return Item0;
                    case 1:
                        return Item1;
                    case 2:
                        return Item2;
                    case 3:
                        return Item3;
                    case 4:
                        return Item4;
                    default:
                        throw new IndexOutOfRangeException();
                }

            }
            set {
                switch (index)
                {
                    case 0:
                        Item0 = value;
                        break;
                    case 1:
                        Item1 = value;
                        break;
                    case 2:
                        Item2 = value;
                        break;
                    case 3:
                        Item3 = value;
                        break;
                    case 4:
                        Item4 = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }
        }

    }
}
