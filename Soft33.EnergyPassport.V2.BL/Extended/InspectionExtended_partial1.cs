﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.Calculations;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL.Charts;

namespace Soft33.EnergyPassport.V2.BL.Extended
{
    public partial class InspectionExtended
    {
        private TemplateTable analysisCostsOfWaterUse;

        /// <summary>
        /// Анализ расходов на водопотребление (таблица)
        /// </summary>
        public TemplateTable AnalysisCostsOfWaterUse
        {
            get
            {
                if (analysisCostsOfWaterUse == null)
                {
                    analysisCostsOfWaterUse = new TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[6] 
                        {
                            "Наименование", 
                            (_inspection.BaseYear-4).ToString(),
                            (_inspection.BaseYear-3).ToString(),
                            (_inspection.BaseYear-2).ToString(),
                            (_inspection.BaseYear-1).ToString(),
                            _inspection.BaseYear.ToString()
                        });

                    analysisCostsOfWaterUse.Rows.Add(header);

                    TemplateTableRow row = new TemplateTableRow(TemplateTableRowType.Data);
                    row.Cells.Add("Потребление ХВС, куб.м");
                    row.Cells.AddRange(new object[5]
                        {
                            _inspection.Intake.FiveYear.YearMinus4.ColdWater.Value,
                            _inspection.Intake.FiveYear.YearMinus3.ColdWater.Value,
                            _inspection.Intake.FiveYear.YearMinus2.ColdWater.Value,
                            _inspection.Intake.FiveYear.YearMinus1.ColdWater.Value,
                            _inspection.Intake.FiveYear.YearMinus0.ColdWater.Value
                        });

                    analysisCostsOfWaterUse.Rows.Add(row);


                    row = new TemplateTableRow(TemplateTableRowType.Data);
                    row.Cells.Add("Затраты, тыс.руб.");
                    row.Cells.AddRange(new object[5]
                        {
                            _inspection.Intake.FiveYear.YearMinus4.ColdWater.ValueRUR / 1000.0,
                            _inspection.Intake.FiveYear.YearMinus3.ColdWater.ValueRUR / 1000.0,
                            _inspection.Intake.FiveYear.YearMinus2.ColdWater.ValueRUR / 1000.0,
                            _inspection.Intake.FiveYear.YearMinus1.ColdWater.ValueRUR / 1000.0,
                            _inspection.Intake.FiveYear.YearMinus0.ColdWater.ValueRUR / 1000.0
                        });

                    analysisCostsOfWaterUse.Rows.Add(row);
                }

                return analysisCostsOfWaterUse;
            }
        }

        private double waterFlow;

        /// <summary>
        /// Нормативный расход холодной воды
        /// </summary>
        public double WaterFlow
        {
            get
            {
                if (waterFlow == null)
                {
                    waterFlow = 0;

                    foreach (BuildingExtended be in _buildings)
                    {
                        waterFlow = waterFlow + be.WaterFlow;
                    }
                }

                return waterFlow;
            }
        }

        private TemplateTable waterFlowRegulation;

        /// <summary>
        /// Нормативный расход воды по организации
        /// </summary>
        public TemplateTable WaterFlowRegulation
        {
            get
            {
                if (waterFlowRegulation == null)
                {
                    waterFlowRegulation = new TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[3] 
                    {
                        "Месяц",
                        "Нормативный",
                        "Фактический"
                    });

                    waterFlowRegulation.Rows.Add(header);

                    string[] months = {"Январь", "Февраль", "Март", "Апрель", 
                                    "Май", "Июнь", "Июль", "Август", 
                                    "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

                    TemplateTableRow row;

                    double NormVal = 0;
                    double FactVal = 0;

                    foreach (int mn in _inspection.Intake.TwelveMonth.IntakeOfMonths.Keys)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[3]
                        {
                            months[(mn-1)],
                            WaterFlow,
                            _inspection.Intake.TwelveMonth.IntakeOfMonths[mn].ColdWater.Value
                        });

                        waterFlowRegulation.Rows.Add(row);

                        NormVal = NormVal + WaterFlow;
                        FactVal = FactVal + _inspection.Intake.TwelveMonth.IntakeOfMonths[mn].ColdWater.Value;
                    }

                    TemplateTableRow footer = new TemplateTableRow(TemplateTableRowType.Footer);
                    footer.Cells.AddRange(new object[3] 
                    {
                        "Всего",
                        NormVal,
                        FactVal
                    });

                    waterFlowRegulation.Rows.Add(footer);
                }

                return waterFlowRegulation;
            }
        }

        private WaterFlowRegulation wfr;

        /// <summary>
        /// График потребления воды
        /// </summary>
        public WaterFlowRegulation WFR
        {
            get
            {
                if (wfr == null)
                {
                    wfr = new WaterFlowRegulation(_inspection, this);
                }

                return wfr;
            }
        }
    }
}
