﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL;
using Soft33.EnergyPassport.V2.BL.Calculations;
using Soft33.EnergyPassport.V2.BL.Charts;
using Soft33.Navitas.Common;
using Soft33.Navitas.Common.Collections;
using Soft33.Navitas.Common.Enums;

namespace Soft33.EnergyPassport.V2.BL.Extended
{
    /// <summary>
    /// Добавляет различные вычисляемые поля к классу Inspection для отчета по обследованию.
    /// </summary>
    public partial class InspectionExtended
    {
        private Inspection _inspection;
        private List<BuildingExtended> _buildings;
        private CalculationThermoCoeffs _thermoCoeffs;

        public string BaseYear { get; set; }
        public string BaseYear_1 { get; set; }
        public string BaseYear_2 { get; set; }
        public string BaseYear_3 { get; set; }
        public string BaseYear_4 { get; set; }

        public int BuildingsCount { get; set; }
        public string CurrentAccount { get; set; }
        public string FullName { get; private set; }
        public bool HasVentilation { get; private set; }
        public string INN { get; set; }
        public int InspectionID { get; private set; }
        public string JuridicalAddress { get; private set; }
        public string KPP { get; set; }
        public string Month { get; set; }
        public string Name { get; private set; }
        public string NameOfParentCompany { get; private set; }
        public string OPF { get; set; }
        public string OKVED { get; set; }
        public string OKVEDDescription { get; set; }
        public string PhysicalAddress { get; private set; }
        public string ShareOfOwnership { get; set; }
        public string Year { get; set; }

        public Person Head { get; set; }
        public Person EnergyPerson { get; set; }
        public Person TechnicalPerson { get; set; }

        public string ElectricEnergyConsumers { get; set; }
        public string HeatEnergyConsumers { get; set; }
        public bool UsingHeatEnergy { get; set; }
        public bool UsingWater { get; set; }

        public string HighConsumptionSources { get; set; }
        public string HighConsumptionSourcesWithPercents { get; set; }

        public string BuildingsList { get; set; }

        /// <summary>
        /// самое большое потребление электроэнергии приходится на
        /// </summary>
        public string MostConsumingElEnergyEquipment { get; set; }

        /// <summary>
        /// самое большое потребление электроэнергии приходится на (в процентах)
        /// </summary>
        public string MostConsumingElEnergyPercent { get; set; }

        /// <summary>
        /// Максимальное отклонение от номинального напряжения
        /// </summary>
        public string MaxDeviationUnom { get; set; }
        /// <summary>
        /// Максимальное отклонение от номинального напряжения в процентах
        /// </summary>
        public string MaxDeviationUnomInPercents { get; set; }

        /// <summary>
        /// Максимальное допустимое / не допустимое
        /// </summary>
        public string MaxDeviationUnomComplies { get; set; }

        /// <summary>
        /// Нормируемое годовым потреблением тепловой энергии обследуемым объектом
        /// </summary>
        public string ThermalEnergyConsumptionNorm { get; set; }

        /// <summary>
        /// Фактическое годовым потреблением тепловой энергии обследуемым объектом
        /// </summary>
        public string ThermalEnergyConsumptionFact { get; set; }

        /// <summary>
        /// Потенциал экономии тепловой энергии объектом в процентах
        /// </summary>
        public string ThermalEnergyConsumptionPotentialPercents { get; set; }

        /// <summary>
        /// Кол-во транспортных средств у предприятия
        /// </summary>
        public string TransportCount { get; set; }

        /// <summary>
        /// Экономия тепловой энергии на отопление зданий
        /// </summary>
        public string EnEco { get; set; }

        /// <summary>
        /// годовое потребление тепла на отопление
        /// </summary>
        public string Wa { get; set; }

        /// <summary>
        /// Тариф на тепловую энергию
        /// </summary>
        public string ThermoEnergyTariff { get; set; }

        /// <summary>
        /// Ежегодная экономия на тепловой энергии
        /// </summary>
        public string ThermoEconomyByYear { get; set; }

        /// <summary>
        /// Стоимость теплоотражающей пленки
        /// </summary>
        public string HeatReflectingLayerCost { get; set; }

        /// <summary>
        /// Затраты на приобретение теплоотражающей пленки оцениваются величиной
        /// </summary>
        public string HeatReflectingCost { get; set; }
        
        /// <summary>
        /// Общая площадь регистров
        /// </summary>
        public string RegistersYardage { get; set; }

        /// <summary>
        /// срок окупаемости мероприятия составит
        /// </summary>
        public string Tok { get; set; }

        /// <summary>
        /// расчетный годовой расход электроэнергии системой освещения
        /// </summary>
        public string LightsEnergyConsumptionByYear { get; set; }

        /// <summary>
        /// Установленная мощность освещения
        /// </summary>
        public string LightsPower { get; set; }

        #region "Экономия воды"

        /// <summary>
        /// Годовой расход воды, м.куб.
        /// </summary>
        public string WaterConsumptionPerYear { get; set; }
        /// <summary>
        /// Годовой расход холодной воды, м.куб.
        /// </summary>
        public string ColdWaterConsumptionPerYear { get; set; }
        /// <summary>
        /// Годовой расход горячей воды, м.куб.
        /// </summary>
        public string HotWaterConsumptionPerYear { get; set; }
        /// <summary>
        /// Тариф на холодную воду
        /// </summary>
        public string ColdWaterTariff { get; set; }
        /// <summary>
        /// Тариф на горячую воду
        /// </summary>
        public string HotWaterTariff { get; set; }
        /// <summary>
        /// Экономия горячей воды, м.куб.
        /// </summary>
        public string HotWaterEconomy { get; set; }
        /// <summary>
        /// Экономия холодной воды, м.куб.
        /// </summary>
        public string ColdWaterEconomy { get; set; }
        /// <summary>
        /// Стоимость сэкономленной горячей воды, руб
        /// </summary>
        public string HotWaterEconomyCost { get; set; }
        /// <summary>
        /// Стоимость сэкономленной холодной воды, руб
        /// </summary>
        public string ColdWaterEconomyCost { get; set; }


        #endregion

        /// <summary>
        /// Объемы энергопотребления зданиями и суммы платежей
        /// </summary>
        public CalculationEnergyConsumptionAndPayments CECAP
        {
            get;
            private set;
        }

        /// <summary>
        /// Рисунок 2.1 – Структура платежей в 2011 г.
        /// </summary>
        public ChartPaymentStructure ChartPaymentStructure
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 2.2 – Динамика энергопотребления
        /// </summary>
        public CalculationEnergyConsumptionDynamic CECD
        {
            get;
            private set;
        }

        /// <summary>
        /// Рисунок 2.2 – График динамика потребления энергетических ресурсов
        /// </summary>
        public ChartEnergyConsumptionDynamic ChartEnergyConsumptionDynamic
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 3.3.1 – Потребление электроэнергии на объекте
        /// </summary>
        public CalculationElectricEnergyConsumption CEEC
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 3.3.2 - Сведения о помесячном электропотреблении на объекте в 2011 г.
        /// </summary>
        public CalculationElectricEnergyConsumptionByMonth CEECBM
        {
            get;
            private set;
        }

        /// <summary>
        /// Рисунок 3.3.1 - Динамика электропотребления на объекте за 2011 г.
        /// </summary>
        public ChartElectricEnergyConsumptionDynamic ChartElectricEnergyConsumptionDynamic
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 3.6.1 - Баланс электроэнергии
        /// </summary>
        public CalculationElectricEnergyBalance CEEB
        {
            get;
            private set;
        }

        /// <summary>
        /// Рисунок 3.6.1- Структура расходной части баланса электроэнергии
        /// </summary>
        public ChartElectricEnergyConsumptionStructure ChartElectricEnergyConsumptionStructure
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 4.5.1 – Потребление тепловой энергии на объекте за 2007 – 2011 гг
        /// </summary>
        public CalculationHeatEnergyConsumption CHEC
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 4.5.2 - Помесячное потребление тепловой энергии на объекте за 2011 г.
        /// </summary>
        public CalculationHeatEnergyConsumptionByMonth CHECBM
        {
            get;
            private set;
        }

        /// <summary>
        /// Рисунок 4.5.1 - Динамика потребления тепловой энергии за 2011 г.
        /// </summary>
        public ChartHeatEnergyConsumptionDynamic ChartHeatEnergyConsumptionDynamic
        {
            get;
            private set;
        }

        /// <summary>
        /// Коэф-ты зданий по расчету потребления на отопление
        /// </summary>
        public CalculationBuildingHeatingCoeffs CBHC
        {
            get;
            private set;
        }

        /// <summary>
        /// Коэф-ты зданий по расчету потребления на ГВС
        /// </summary>
        public CalculationBuildingDhwCoeffs CBDHWC
        {
            get;
            private set;
        }

        /// <summary>
        /// Коэф-ты зданий по расчету потребления на Приточную вентиляцию
        /// </summary>
        public CalculationBuildingVentinCoeffs CBVIC
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица с коэф-тами удельных тепловых характеристика зданий
        /// </summary>
        public CalculationBuildingSpecificThermalChars CBSTC
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица потэнциалов экономии тепловой энергии для зданий
        /// </summary>
        public CalculationBuildingThermalEnergyEconomyCoeffs CBTEEC
        {
            get;
            private set;
        }

        public CalculationTransport CT
        {
            get;
            private set;
        }

        /// <summary>
        /// Характеристика системы  освещения
        /// </summary>
        public CalculatioLightningSystemCharacteristics CLSC
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 10.6 – Затраты на установку водосчетчика
        /// </summary>
        public CalculationInstallingWaterMetersCost CIWMC
        {
            get;
            private set;
        }

        /// <summary>
        /// д.б. IList
        /// </summary>
        public IList<BuildingExtended> Buildings { get { return _buildings; } }

        /// <summary>
        /// Предусмотреть возможность создания пустого объекта
        /// </summary>
        public InspectionExtended()
        {
            _buildings = new List<BuildingExtended>();
        }

        public InspectionExtended(Inspection inspection, CalculationThermoCoeffs thermoCoeffs, CalculationFinanceCoeffs financeCoeffs)
            : this()
        {
            _inspection = inspection;
            _thermoCoeffs = thermoCoeffs;

            InspectionID = inspection.InspectionID;
            BaseYear = inspection.BaseYear.HasValue ? inspection.BaseYear.Value.ToString() : String.Empty;
            BaseYear_1 = inspection.BaseYear.HasValue ? (inspection.BaseYear.Value - 1).ToString() : String.Empty;
            BaseYear_2 = inspection.BaseYear.HasValue ? (inspection.BaseYear.Value - 2).ToString() : String.Empty;
            BaseYear_3 = inspection.BaseYear.HasValue ? (inspection.BaseYear.Value - 3).ToString() : String.Empty;
            BaseYear_4 = inspection.BaseYear.HasValue ? (inspection.BaseYear.Value - 4).ToString() : String.Empty;

            FullName = inspection.Client.FullName;
            PhysicalAddress = inspection.Client.PhysicalAddress;
            JuridicalAddress = inspection.Client.JuridicalAddress;
            HasVentilation = CheckVentilation(inspection.Buildings);
            OPF = inspection.Client.OPF;
            Name = inspection.Client.Name;
            NameOfParentCompany = inspection.Client.NameOfParentCompany;
            ShareOfOwnership = inspection.Client.ShareOfOwnership;
            CurrentAccount = inspection.Client.CurrentAccount;
            INN = inspection.Client.INN;
            KPP = inspection.Client.KPP;
            OKVED = inspection.Client.Okved.Kod;
            OKVEDDescription = inspection.Client.Okved.Description;

            Month = DateTime.Now.ToString("MMMM");
            Year = DateTime.Now.ToString("yyyy");

            Head = new Person(inspection.Client.Head);
            EnergyPerson = new Person(inspection.Client.EnergyPerson);
            TechnicalPerson = new Person(inspection.Client.TechnicalPerson);

            ElectricEnergyConsumers = GenerateElectricEnergyConsumersText(inspection.Buildings);// "[UNSPECIFIED] оргтехника, технологическое и бытовое оборудование, оборудование отопления";
            HeatEnergyConsumers = GenerateHeatEnergyConsumers(inspection.Intake.FiveYear);// "[UNSPECIFIED] ГВС";
            UsingHeatEnergy = !String.IsNullOrEmpty(HeatEnergyConsumers);
            UsingWater = IsUsingWater(inspection.Intake.FiveYear);

            BuildingsList = GenerateBuildingsList(inspection.Buildings);

            CECAP = new CalculationEnergyConsumptionAndPayments(inspection);
            ChartPaymentStructure = new ChartPaymentStructure(inspection);

            HighConsumptionSources = "[UNSPECIFIED] электроэнергия, тепловая энергия и вода";
            HighConsumptionSourcesWithPercents = "[UNSPECIFIED] электроэнергия составляет 20,92%, тепловая энергия – 59,29%, ХВС – 1,028%, ГВС – 16,57% и водоотведение – 2,18% соответственно";

            CECD = new CalculationEnergyConsumptionDynamic(inspection);
            ChartEnergyConsumptionDynamic = new ChartEnergyConsumptionDynamic(inspection);
            CEEC = new CalculationElectricEnergyConsumption(inspection);
            CEECBM = new CalculationElectricEnergyConsumptionByMonth(inspection);
            ChartElectricEnergyConsumptionDynamic = new ChartElectricEnergyConsumptionDynamic(inspection);
            CEEB = new CalculationElectricEnergyBalance(inspection);
            ChartElectricEnergyConsumptionStructure = new ChartElectricEnergyConsumptionStructure(inspection);

            MostConsumingElEnergyEquipment = ChartElectricEnergyConsumptionStructure.MostConsumingElEnergyEquipment;
            MostConsumingElEnergyPercent = ChartElectricEnergyConsumptionStructure.MostConsumingElEnergyPercent.ToString("0.00");


            // Вычисление отклонения напряжения от номинального по всем зданиям
            double maxDeviation = CalculateMaxDeviationUnom(inspection.Buildings.SelectMany(b => b.PowerQualitys.Powers), 220);
            double maxDeviationPercents = (maxDeviation / 220) * 100;
            MaxDeviationUnom = NumberUtils.FormatDouble(maxDeviation);
            MaxDeviationUnomInPercents = NumberUtils.FormatDouble(maxDeviationPercents);
            MaxDeviationUnomComplies = maxDeviationPercents > 10 ? "не допустимая" : "допустимая";

            CHEC = new CalculationHeatEnergyConsumption(inspection);
            CHECBM = new CalculationHeatEnergyConsumptionByMonth(inspection);
            ChartHeatEnergyConsumptionDynamic = new ChartHeatEnergyConsumptionDynamic(inspection);

            foreach (BL.Building b in inspection.Buildings)
            {
                _buildings.Add(new BuildingExtended(b, thermoCoeffs));
            }

            BuildingsCount = inspection.Buildings.Count;

            CBHC = new CalculationBuildingHeatingCoeffs(_buildings);
            CBDHWC = new CalculationBuildingDhwCoeffs(_buildings);
            CBVIC = new CalculationBuildingVentinCoeffs(_buildings);
            CBSTC = new CalculationBuildingSpecificThermalChars(_buildings);
            CBTEEC = new CalculationBuildingThermalEnergyEconomyCoeffs(_buildings);

            ThermalEnergyConsumptionNorm = NumberUtils.FormatDouble(CBTEEC.ConsumptionNorm);
            ThermalEnergyConsumptionFact = NumberUtils.FormatDouble(CBTEEC.ConsumptionFact);
            ThermalEnergyConsumptionPotentialPercents = NumberUtils.FormatDouble(CBTEEC.PotentialPercents);

            CT = new CalculationTransport(inspection);

            TransportCount = CT.Total.ToString();

            #region "Установка теплоотражателей за отопительными приборами"

            // Посчитать суммарное потребление тепловой энергии на отопление
            double wa = inspection.Intake.FiveYear.YearMinus0.TERHeating.Value;
            // Экономия тепловой энергии
            double enEcoValue = FormulaThermoEnergyEconomy.Value(wa);
            // стоимость установки теплоотражателей
            double heatReflectingCost = financeCoeffs.HeatReflectingLayerCost * 293 / 1000; // !!!
            // Экономия в год на тепловой энергии = тариф * сколько_экономим_тепла
            double? thermoEconomyByYear = inspection.CalculationCoeffs.ThermoEnergyTariff.HasValue ?
                (double?)(enEcoValue * inspection.CalculationCoeffs.ThermoEnergyTariff.Value) : null;

            EnEco = NumberUtils.FormatDouble(enEcoValue);
            Wa = NumberUtils.FormatDouble(wa);
            ThermoEnergyTariff = inspection.CalculationCoeffs.ThermoEnergyTariff.HasValue ? NumberUtils.FormatDouble(inspection.CalculationCoeffs.ThermoEnergyTariff.Value) : "";
            ThermoEconomyByYear = thermoEconomyByYear.HasValue ? NumberUtils.FormatDouble(thermoEconomyByYear.Value) : "";
            HeatReflectingLayerCost = NumberUtils.FormatDouble(financeCoeffs.HeatReflectingLayerCost);
            RegistersYardage = NumberUtils.FormatDouble(293); // !!!
            HeatReflectingCost = NumberUtils.FormatDouble(heatReflectingCost);
            Tok = thermoEconomyByYear.HasValue ? NumberUtils.FormatDouble(heatReflectingCost / thermoEconomyByYear.Value, 4) : "";

            #endregion

            CLSC = new CalculatioLightningSystemCharacteristics(inspection.Buildings);

            LightsEnergyConsumptionByYear = NumberUtils.FormatDouble(CLSC.LightsEnergyConsumptionByYear);
            LightsPower = NumberUtils.FormatDouble(CLSC.LightsPower);

            #region "Расчет экономии воды"

            double coldWaterConsumptionPerYear = inspection.Intake.FiveYear.YearMinus0.ColdWater.Value;
            double hotWaterConsumptionPerYear = inspection.Intake.FiveYear.YearMinus0.HotWater.Value;
            double waterConsumptionPerYear = coldWaterConsumptionPerYear + hotWaterConsumptionPerYear;

            double? coldWaterTariff = inspection.CalculationCoeffs.ColdWaterTariff;
            double? hotWaterTariff = inspection.CalculationCoeffs.HotWaterTariff;

            double hotWaterEconomy = hotWaterConsumptionPerYear * 0.3;
            double coldWaterEconomy = coldWaterConsumptionPerYear * 0.3;

            WaterConsumptionPerYear = NumberUtils.FormatDouble(waterConsumptionPerYear);
            ColdWaterConsumptionPerYear = NumberUtils.FormatDouble(coldWaterConsumptionPerYear);
            HotWaterConsumptionPerYear = NumberUtils.FormatDouble(hotWaterConsumptionPerYear);
            ColdWaterTariff = coldWaterTariff.HasValue ? NumberUtils.FormatDouble(coldWaterTariff.Value) : "";
            HotWaterTariff = hotWaterTariff.HasValue ? NumberUtils.FormatDouble(hotWaterTariff.Value): "";
            HotWaterEconomy = NumberUtils.FormatDouble(hotWaterEconomy);
            ColdWaterEconomy = NumberUtils.FormatDouble(coldWaterEconomy);
            HotWaterEconomyCost = hotWaterTariff.HasValue ? NumberUtils.FormatDouble(hotWaterEconomy * hotWaterTariff.Value) : "";
            ColdWaterEconomyCost = coldWaterTariff.HasValue ? NumberUtils.FormatDouble(coldWaterEconomy * coldWaterTariff.Value) : "";

            #endregion

            CIWMC = new CalculationInstallingWaterMetersCost();
        }

        private string GenerateBuildingsList(IEnumerable<Building> buildings)
        {
            return TextUtils.ConcatenateText(buildings.Select(b => b.GeneralInformation.Name));
        }

        /// <summary>
        /// Определяет по всем зданиям какие электро ресурсы они используют и составляет их текстовый список.
        /// </summary>
        /// <param name="buildings"></param>
        /// <returns></returns>
        private string GenerateElectricEnergyConsumersText(IEnumerable<Building> buildings)
        {
            bool hasHouseholdEquipment = buildings.Any(b => !b.HouseholdEquipment.IsEmpty());
            bool hasProcessEquipment = buildings.Any(b => !b.ProcessEquipment.IsEmpty());
            bool hasOfficeEquipment = buildings.Any(b => !b.OfficeEquipment.IsEmpty());
            bool hasHeatingEquipment = buildings.Any(b => !b.HeatingEquipment.IsEmpty());
            bool hasExternalLightingEquipment = buildings.Any(b => !b.ExternalLightingEquipment.IsEmpty());
            bool hasInternalLightingEquipment = buildings.Any(b => !b.InternalLightingEquipment.IsEmpty());

            return TextUtils.ConcatenateText((hasHouseholdEquipment ? "бытовое оборудование" : ""),
                              (hasProcessEquipment ? "технологическое оборудование" : ""),
                              (hasOfficeEquipment ? "оргтехника" : ""),
                              (hasHeatingEquipment ? "оборудование отопления" : ""),
                              (hasInternalLightingEquipment ? "освещение (внутреннее)" : ""),
                              (hasExternalLightingEquipment ? "освещение (наружное)" : ""));
        }

        /// <summary>
        /// Определяет по данным обследования какие типы тепловой энергии используются и составляет их текстовый список
        /// </summary>
        /// <param name="intake"></param>
        /// <returns></returns>
        private string GenerateHeatEnergyConsumers(FiveYearIntake intake)
        {
            bool hasHeatingConsumption = false;
            bool hasHotWaterConsumption = false;
            bool hasVentilationInletConsumption = false;

            foreach(IntakeOfPeriod i in intake.IntakeOfYears.Values)
            {
                hasHeatingConsumption = hasHeatingConsumption || !i.TERHeating.IsZero();
                hasHotWaterConsumption = hasHotWaterConsumption || !i.TERDhw.IsZero();
                hasVentilationInletConsumption = hasVentilationInletConsumption || !i.TERVentilationInlet.IsZero();
            }

            return TextUtils.ConcatenateText((hasHeatingConsumption ? "отопление" : ""),
                              (hasHotWaterConsumption ? "ГВС" : ""),
                              (hasVentilationInletConsumption ? "вентиляция (приточная)" : ""));
        }

        /// <summary>
        /// определеяет использует ли предприятие водоснабжение
        /// </summary>
        /// <param name="inspection"></param>
        /// <returns></returns>
        private bool IsUsingWater(FiveYearIntake intake)
        {
            foreach (IntakeOfPeriod i in intake.IntakeOfYears.Values)
            {
                if (!i.ColdWater.IsZero())
                {
                    return true;
                }
                if (!i.HotWater.IsZero())
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckVentilation(IEnumerable<BL.Building> buildings)
        {
            foreach (BL.Building b in buildings)
            {
                if (b.VentilationCombinedExtractInputs.VentilationCombinedExtractInputs.All(input => input.IsEmpty())
                    && b.VentilationExtracts.VentilationExtracts.All(input => input.IsEmpty()))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Вычисляет максимальное отклонение напряжения по вводам от номинального
        /// </summary>
        /// <param name="qualities"></param>
        /// <param name="Unom"></param>
        /// <returns></returns>
        private double CalculateMaxDeviationUnom(IEnumerable<PowerQuality> qualities, double Unom)
        {
            double max = 0;
            // Для каждого ввода посчитать максимальное отклонение
            foreach (PowerQuality quality in qualities)
            {
                for (int i = 0; i < quality.Simple.Count; i++)
                {
                    if (quality.Simple[i].UPhaseA.HasValue)
                    {
                        max = Math.Max(max, Math.Abs(quality.Simple[i].UPhaseA.Value - Unom));
                    }
                    if (quality.Simple[i].UPhaseB.HasValue)
                    {
                        max = Math.Max(max, Math.Abs(quality.Simple[i].UPhaseB.Value - Unom));
                    }
                    if (quality.Simple[i].UPhaseC.HasValue)
                    {
                        max = Math.Max(max, Math.Abs(quality.Simple[i].UPhaseC.Value - Unom));
                    }
                }
            }
            return max;
        }
    }
}
