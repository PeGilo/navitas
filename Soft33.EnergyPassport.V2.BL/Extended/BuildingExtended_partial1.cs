﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL;
using Soft33.EnergyPassport.V2.BL.Calculations;
using Soft33.EnergyPassport.V2.BL.Charts;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.Navitas.Common.Collections;
using Soft33.Navitas.Common.Enums;

namespace Soft33.EnergyPassport.V2.BL.Extended
{
    public partial class BuildingExtended
    {
        private string _waterResource;

        public string WaterResource
        {
            get
            {
                if (_waterResource == null)
                {
                    if (_building.AdditionalInformation.Resources != null
                        && _building.AdditionalInformation.Resources.WaterResource != null)
                    {
                        _waterResource = _building.AdditionalInformation.Resources.WaterResource.Name;
                    }
                }
                return _waterResource;
            }
        }

        private string _sewerageResource;

        public string SewerageResource
        {
            get
            {
                if (_sewerageResource == null)
                {
                    if (_building.AdditionalInformation.Resources != null
                        && _building.AdditionalInformation.Resources.SewerageResource != null)
                    {
                        _sewerageResource = _building.AdditionalInformation.Resources.SewerageResource.Name;
                    }
                }
                return _sewerageResource;
            }
        }

        private TemplateTable coldWaterMetersTable;

        public TemplateTable ColdWaterMetersTable
        {
            get
            {
                if (coldWaterMetersTable == null)
                {
                    coldWaterMetersTable = new TemplateCalculations.TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[7]
                        {"Наименование","Диапазон измерения","Опломбировка","Класс точности",
                         "Дата выпуска","Дата последней поверки","Заводской номер"}
                    );

                    coldWaterMetersTable.Rows.Add(header);

                    TemplateTableRow row;

                    foreach (MeterColdWater mcw in _building.Meters.ColdWater)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[7] { 
                            mcw.Caption, 
                            mcw.MeasurementRange,
                            mcw.Sealing ? "Да":"Нет",
                            mcw.AccuracyClass,
                            mcw.Year,
                            mcw.DateOfVerification,
                            mcw.SerialNumber
                        });

                        coldWaterMetersTable.Rows.Add(row);
                    }
                }

                return coldWaterMetersTable;
            }
        }

        private TemplateTable pointOfWaterTable;

        public TemplateTable PointOfWaterTable
        {
            get
            {
                if (pointOfWaterTable == null)
                {
                    pointOfWaterTable = new TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[4] { "Наименование", "Тип", "Кол-во", "Описание" }
                    );

                    pointOfWaterTable.Rows.Add(header);

                    TemplateTableRow row;

                    foreach (PointOfWater pow in _building.PointOfWaters.Points)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);

                        row.Cells.AddRange(new object[4]
                            {
                                pow.PointOfWaterCaption,
                                pow.PointOfWaterDeviceTypeCaption,
                                pow.Number,
                                pow.Description
                            });

                        pointOfWaterTable.Rows.Add(row);
                    }
                }

                return pointOfWaterTable;
            }
        }

        /// <summary>
        /// Нормативный расход по этому зданию: значение
        /// </summary>
        private double? waterFlow;

        /// <summary>
        /// Нормативный расход холодной воды в месяц
        /// </summary>
        public double WaterFlow
        {
            get
            {
                if (!waterFlow.HasValue)
                {
                    if (_building.WallingOfBuilding.BuildingCommonCoeffs.WorkHoursPerDay.HasValue
                        && _building.WallingOfBuilding.BuildingCommonCoeffs.WorkDaysPerYear.HasValue
                        && _building.WallingOfBuilding.BuildingWaterCoeffs.gCW_Max.HasValue)
                    {
                        waterFlow = (_building.WallingOfBuilding.BuildingWaterCoeffs.gCW_Max.Value
                                    * _building.GeneralInformation.OtherPeople
                                    * _building.WallingOfBuilding.BuildingCommonCoeffs.WorkDaysPerYear.Value
                                    * _building.WallingOfBuilding.BuildingCommonCoeffs.WorkHoursPerDay.Value)
                                    / (1000 * 12);
                    }
                    else
                    {
                        waterFlow = null;
                    }
                }

                return waterFlow.HasValue ? waterFlow.Value : 0D;
            }
        }

        private TemplateTable waterFlowRegulation;

        public TemplateTable WaterFlowRegulation
        {
            get
            {
                if (waterFlowRegulation == null)
                {
                    waterFlowRegulation = new TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[3] 
                    {
                        "Месяц",
                        "Нормативный",
                        "Фактический"
                    });

                    waterFlowRegulation.Rows.Add(header);

                    string[] months = {"Январь", "Февраль", "Март", "Апрель", 
                                    "Май", "Июнь", "Июль", "Август", 
                                    "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

                    TemplateTableRow row;

                    double NormVal = 0;
                    double FactVal = 0;

                    foreach (int mn in _building.TwelveMonth.IntakeOfMonths.Keys)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[3]
                        {
                            months[(mn-1)],
                            WaterFlow,
                            _building.TwelveMonth.IntakeOfMonths[mn].ColdWater.Value
                        });

                        waterFlowRegulation.Rows.Add(row);

                        NormVal = NormVal + WaterFlow;
                        FactVal = FactVal + _building.TwelveMonth.IntakeOfMonths[mn].ColdWater.Value;
                    }

                    TemplateTableRow footer = new TemplateTableRow(TemplateTableRowType.Footer);
                    footer.Cells.AddRange(new object[3] 
                    {
                        "Всего",
                        NormVal,
                        FactVal
                    });

                    waterFlowRegulation.Rows.Add(footer);
                }

                return waterFlowRegulation;
            }
        }

        public string HavingVentilationCombinedExtractInput
        {
            get
            {
                if (_building.VentilationCombinedExtractInputs.VentilationCombinedExtractInputs.Count > 0)
                {
                    return "предусмотрены системы приточной/приточно-вытяжной вентиляции";
                }
                else
                {
                    return "не предусмотрены системы приточной/приточно-вытяжной вентиляции";
                }
            }
        }

        private TemplateTable ventilationInputTable;

        public TemplateTable VentilationInputTable
        {
            get
            {
                if (ventilationInputTable == null)
                {
                    ventilationInputTable = new TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[6] {
                        "Наименование системы",
                        "Место установки вентагрегата",
                        "Обслуживаемые помещения",
                        "Марка вентагрегата",
                        "Мощность вентагрегата в режиме нагрева, кВт",
                        "Мощность вентагрегата в режиме охлаждения,кВт"
                    });

                    ventilationInputTable.Rows.Add(header);

                    TemplateTableRow row;

                    foreach (VentilationCombinedExtractInput vent in _building.VentilationCombinedExtractInputs.VentilationCombinedExtractInputs)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[6] {
                            vent.Name,
                            "-",
                            vent.ServedPremises,
                            vent.VentilationSystemBrand,
                            vent.PowerInHeating,
                            vent.PowerInCooling
                        });

                        ventilationInputTable.Rows.Add(row);
                    }
                }

                return ventilationInputTable;
            }
        }

        private TemplateTable ventilationExtractTable;

        public TemplateTable VentilationExtractTable
        {
            get
            {
                if (ventilationExtractTable == null)
                {
                    ventilationExtractTable = new TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[5] {
                        "Наименование системы",
                        "Место установки вентагрегата",
                        "Обслуживаемые помещения",
                        "Марка вентагрегата",
                        "Мощность вентагрегата, кВт"
                    });

                    ventilationExtractTable.Rows.Add(header);

                    TemplateTableRow row;

                    foreach (VentilationExtract vent in _building.VentilationExtracts.VentilationExtracts)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[5] {
                            vent.Name,
                            "-",
                            vent.ServedPremises,
                            vent.VentilationSystemBrand,
                            vent.Power
                        });

                        ventilationExtractTable.Rows.Add(row);
                    }
                }

                return ventilationExtractTable;
            }
        }

        private TemplateTable buildingComponentsTable;

        public TemplateTable BuildingComponentsTable
        {
            get
            {
                if (buildingComponentsTable == null)
                {
                    buildingComponentsTable = new TemplateTable();

                    TemplateTableRow header = new TemplateTableRow(TemplateTableRowType.Header);
                    header.Cells.AddRange(new string[5] {
                        "Конструктивный элемент",
                        "Материал",
                        "Толщина, м",
                        "Техническое состояние (осадки, трещины, гниль и т.п.)",
                        "Физический износ элементов"
                    });

                    buildingComponentsTable.Rows.Add(header);

                    TemplateTableRow row;

                    row = new TemplateTableRow(TemplateTableRowType.Data);
                    row.Cells.AddRange(new string[5]
                    {
                        "Фундамент",
                        "",
                        "",
                        "",
                        ""
                    });

                    buildingComponentsTable.Rows.Add(row);

                    foreach (WallLayer wl in _building.WallingOfBuilding.Walls)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[5]
                        {
                            "Наружные и внутренние капитальные стены",
                            wl.Material.Caption,
                            wl.Thickness,
                            wl.TechnicalCondition,
                            wl.WearPercantage
                        });

                        buildingComponentsTable.Rows.Add(row);
                    }

                    foreach (WallLayer wl in _building.WallingOfBuilding.AtticFloor)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[5]
                        {
                            "Чердачное перекрытие",
                            wl.Material.Caption,
                            wl.Thickness,
                            wl.TechnicalCondition,
                            wl.WearPercantage
                        });

                        buildingComponentsTable.Rows.Add(row);
                    }

                    foreach (WallLayer wl in _building.WallingOfBuilding.BasementFloor)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[5]
                        {
                            "Подвальное перекрытие",
                            wl.Material.Caption,
                            wl.Thickness,
                            wl.TechnicalCondition,
                            wl.WearPercantage
                        });

                        buildingComponentsTable.Rows.Add(row);
                    }

                    foreach (Door d in _building.WallingOfBuilding.Doors)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[5]
                        {
                            "Двери",
                            d.DoorTypeCaption,
                            d.NumberOfDoors,
                            "",
                            ""
                        });

                        buildingComponentsTable.Rows.Add(row);
                    }

                    foreach (Window w in _building.WallingOfBuilding.Windows)
                    {
                        row = new TemplateTableRow(TemplateTableRowType.Data);
                        row.Cells.AddRange(new object[5]
                        {
                            "Окна",
                            w.WindowTypeCaption,
                            w.NumberOfWindows,
                            "",
                            ""
                        });

                        buildingComponentsTable.Rows.Add(row);
                    }
                }

                return buildingComponentsTable;
            }
        }

        /// <summary>
        /// Градусосутки отопительного периода
        /// </summary>
        public int GSOP
        {
            get
            {
                try
                {
                    return Convert.ToInt32((_building.WallingOfBuilding.BuildingThermoCoeffs.Tin - _building.WallingOfBuilding.BuildingThermoCoeffs.Tout_fact) * _building.WallingOfBuilding.BuildingThermoCoeffs.zh_fact);
                }
                catch
                {
                    return 0;
                }
            }
        }

        public double RKsten
        {
            get
            {
                double res = 0;

                try
                {
                    foreach (WallLayer wl in _building.WallingOfBuilding.Walls)
                    {
                        double pr;

                        try
                        {
                            pr = (wl.Thickness / wl.Material.ThermalResistant);
                        }
                        catch
                        {
                            pr = 0;
                        }

                        res = res + pr;
                    }
                }
                catch
                {
                    res= 0;
                }

                return res;
            }
        }

        public double R0sten
        {
            get
            {
                double res = 0;

                res = 1.0 / 8.7 + 1.0 / 23 + RKsten;

                return res;
            }
        }

        public double RKcherd
        {
            get
            {
                double res = 0;

                try
                {
                    foreach (WallLayer wl in _building.WallingOfBuilding.AtticFloor)
                    {
                        double pr;

                        try
                        {
                            pr = (wl.Thickness / wl.Material.ThermalResistant);
                            
                        }
                        catch
                        {
                            pr = 0;
                        }

                        res = res + pr;
                    }
                }
                catch
                {
                    res = 0;
                }

                return res;
            }
        }

        public double R0cherd
        {
            get
            {
                double res = 0;

                res = 1.0 / 8.7 + 1.0 / 12 + RKcherd;

                return res;
            }
        }

        public double RKpol
        {
            get
            {
                double res = 0;

                try
                {
                    foreach (WallLayer wl in _building.WallingOfBuilding.BasementFloor)
                    {
                        double pr;

                        try
                        {
                            pr = (wl.Thickness / wl.Material.ThermalResistant);
                        }
                        catch
                        {
                            pr = 0;
                        }

                        res = res + pr;
                    }
                }
                catch
                {
                    res = 0;
                }

                return res;
            }
        }

        public double R0pol
        {
            
            get
            {
                if (_building.WallingOfBuilding.Basement == BasementType.Warm)
                {

                    double res = 0;

                    res = 1.0 / 8.7 + 1.0 / 12 + RKcherd;

                    return res;
                }
                else
                {
                    //Нужно описать тёплый подвал
                    return 0;
                }
            }
        }
    }
}












