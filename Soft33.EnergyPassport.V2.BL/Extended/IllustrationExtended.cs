﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL.Extended
{
    public class IllustrationExtended
    {
        public int IllustrationID { get; set; }
        public int InspectionID { get; set; }
        public int? BuildingID { get; set; }
        public string DivisionName { get; set; }
        public string UserTitle { get; set; }
        public string FileName { get; set; }

        //public string TextId { get; set; }
        //public string Url { get; set; }

        public IllustrationExtended(Illustration illustration)
        {
            IllustrationID = illustration.IllustrationID;
            InspectionID = illustration.InspectionID;
            BuildingID = illustration.BuildingID;
            DivisionName = illustration.DivisionName;
            UserTitle = illustration.UserTitle;
            FileName = illustration.FileName;

            //Url = FileName;

        }
    }
}
