﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL;
using Soft33.EnergyPassport.V2.BL.Calculations;
using Soft33.EnergyPassport.V2.BL.Charts;
using Soft33.Navitas.Common;
using Soft33.Navitas.Common.Collections;
using Soft33.Navitas.Common.Enums;

namespace Soft33.EnergyPassport.V2.BL.Extended
{
    /// <summary>
    /// Имеет дополнительные свойства для построения отчета по обследованию.
    /// </summary>
    public partial class BuildingExtended
    {
        private Building _building;

        private DictionaryWithDefault<string, List<IllustrationExtended>> _illustration = 
            new DictionaryWithDefault<string, List<IllustrationExtended>>(
                    () => new List<IllustrationExtended>() // Функция - генератор нового значения
                );

        public int BuildingID { get; set; }
        public string Name { get; set; }
        public string PhysicalAddress { get; set; }
        public string Purpose { get; set; }
        public string TotalElectricEnergyConsumption { get; set; }
        public string BuildingYardage { get; set; }
        /// <summary>
        /// что расход электроэнергии в 2011г. превысил\ не превысил?
        /// </summary>
        public string MoreOrLessConsumption { get; set; }
        /// <summary>
        /// среднее превышение нормативного над фактическим потреблением
        /// </summary>
        public string AverageSpreadBetweenNormAndFactConsumption { get; set; }

        ///// <summary>
        ///// Максимальное отклонение от номинального напряжения
        ///// </summary>
        //public string MaxDeviationUnom { get; set; }
        ///// <summary>
        ///// Максимальное отклонение от номинального напряжения в процентах
        ///// </summary>
        //public string MaxDeviationUnomInPercents { get; set; }

        ///// <summary>
        ///// Максимальное допустимое / не допустимое
        ///// </summary>
        //public string MaxDeviationUnomComplies { get; set; }

        /// <summary>
        /// Свойство, через которое по имени раздела, можно получить иллюстрацию.
        /// </summary>
        public DictionaryWithDefault<string, List<IllustrationExtended>> Illustration
        {
            get { return _illustration; }
        }

        /// <summary>
        /// Удельный расход электроэнергии
        /// </summary>
        public CalculationSpecificEnergyConsumption CSEC
        {
            get; private set;
        }

        public CalculationGeneralInfo GeneralInfoTable
        {
            get; private set;
        }

        public CalculationElelctricEnergyMeterList CEEML
        {
            get;
            private set;
        }

        public CalculationElelctricEnergyMetersDescription CEEMD
        {
            get;
            private set;
        }

        public IList<TemplateChartData> Charts
        {
            get;
            private set;
        }

        public CalculationHeatingMeterList CHeatML
        {
            get;
            private set;
        }

        public CalculationHotWaterMeterList CHotWaterML
        {
            get;
            private set;
        }

        /// <summary>
        /// Фактический и нормативный удельные расходы электроэнергии
        /// </summary>
        public ChartSpecificEnergyConsumption ChartSpecificEnergyConsumption
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 4.8 - Фактическая и нормативная тепловые нагрузки на отопление за 2011г.
        /// </summary>
        public CalculationHeatingLoadFactAndNorm CHLFAN
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 4.9 - Фактическая и нормативная тепловые нагрузки на ГВС за 2011г.
        /// </summary>
        public CalculationDHWLoadFactAndNorm CDHWLFAN
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 4.10 - Фактическая и нормативная тепловые нагрузки на приточную вентиляцию за 2011г.
        /// </summary>
        public CalculationVentinLoadFactAndNorm CVILFAN
        {
            get;
            private set;
        }

        /// <summary>
        /// Рисунок 4.13- График фактического и нормативного потребления тепловой энергии на отопление за 2011 г.
        /// </summary>
        public ChartNormAndFactThermoConsumption ChartNormAndFactHeatingConsumption
        {
            get;
            private set;
        }

        /// <summary>
        /// Рисунок 4.14- График фактического и нормативного потребления тепловой энергии на ГВС за 2011 г.
        /// </summary>
        public ChartNormAndFactThermoConsumption ChartNormAndFactDHWConsumption
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 4.10 - Фактическая и нормативная тепловые нагрузки на приточную вентиляцию за 2011г.
        /// </summary>
        public ChartNormAndFactThermoConsumption ChartNormAndFactVentinConsumption
        {
            get;
            private set;
        }

        /// <summary>
        /// Таблица 8.1 – Сведения об эксплуатационной среде
        /// </summary>
        public CalculationEnvironment CE
        {
            get;
            private set;
        }

        /// <summary>
        /// поправочный коэффициент, учитывающий отличие расчетной температуры наружного воздуха для проектирования отопления
        /// </summary>
        public double? Alpha { get; set; }

        /// <summary>
        /// Объем здания
        /// </summary>
        public double? V { get; set; }

        /// <summary>
        /// Высота здания
        /// </summary>
        public double? L { get; set; }

        /// <summary>
        /// удельная отопительная характеристика здания при от tо = -30С
        /// </summary>
        public double? q0 { get; set; }

        /// <summary>
        /// расчетная температура наружного воздуха для проектирования отопления
        /// </summary>
        public double? t0 { get; set; }

        /// <summary>
        /// скорость ветра
        /// </summary>
        public double? w0 { get; set; }

        /// <summary>
        /// средний суточный расход горячей воды л/сутки одним пользователем
        /// </summary>
        public double? gDHW { get; set; }

        /// <summary>
        /// количество пользователей ГВС
        /// </summary>
        public double? m { get; set; }

        /// <summary>
        /// удельная тепловая вентиляционная характеристика здания, зависящая от назначения и 
        /// строительного объема вентилируемого здания, ккал/м3ч°С, принимается по МДС 41-2.2000 (табл.3). 
        /// </summary>
        public double? qv { get; set; }

        /// <summary>
        /// расчетная температура наружного воздуха, при которой определена тепловая нагрузка приточной вентиляции в проекте
        /// </summary>
        public double? Tnprv { get; set; }

        /// <summary>
        /// средняя расчетная температура наружного воздуха для проектирования приточной вентиляции в местности, где расположено здание
        /// </summary>
        public double? Tnrv { get; set; }

        /// <summary>
        /// фактическая продолжительность отопительного периода в отчетном (базовом) году, сут 
        /// </summary>
        public int? Zh_fact = null;

        /// <summary>
        /// фактическая средняя температура внутреннего воздуха за отопительный период в отчетном (базовом) году
        /// </summary>
        public double? Tint_fact = null;

        /// <summary>
        /// фактическая средняя температура наружного воздуха за отопительный период в 2011 году
        /// </summary>
        public double? Text_fact = null;

        /// <summary>
        /// Расчетная часовая  тепловая нагрузка на отопление здания, по укрупненным показателям 
        /// </summary>
        public double? Qomax { get; set; }

        /// <summary>
        /// Коэф-т инфильтрации
        /// </summary>
        public double? KHP { get; set; }

        /// <summary>
        /// Средний расчетный суточный объем потребления горячей воды
        /// </summary>
        public double? Vhw { get; set; }

        /// <summary>
        /// Среднечасовой за отопительный период расход тепловой энергии на горячее водоснабжение
        /// </summary>
        public double? Qohw { get; set; }

        /// <summary>
        /// Среднечасовой за летний период расход тепловой энергии на горячее водоснабжение
        /// </summary>
        public double? Qlhw { get; set; }

        /// <summary>
        /// Расчетная часовая тепловая нагрузка приточной вентиляции по укрупненным показателям
        /// </summary>
        public double? QvrBase { get; set; }

        /// <summary>
        /// Расчетная часовая тепловая нагрузка на приточную вентиляцию
        /// </summary>
        public double? Qvr { get; set; }

        /// <summary>
        /// Фактическая удельная тепловая характеристика на отопление 
        /// </summary>
        public double? SpecificThermalCharacteristics { get; set; }

        /// <summary>
        /// Нормативная удельная тепловая характеристика на отопление 
        /// </summary>
        public double? NormativeThermalCharacteristics { get; set; }

        /// <summary>
        /// Фактическое потребление тепловой энергии
        /// </summary>
        public double ThermalEnergyConsumptionFact { get; set; }

        /// <summary>
        /// Нормативное потребление тепловой энергии
        /// </summary>
        public double ThermalEnergyConsumptionNorm { get; set; }

        public double? Tin { get; set; }
        public double? Tout_fact { get; set; }
        public double? zh_fact { get; set; }

        public double? ThermoResistanceWalls { get; set; }
        public double? ThermoResistanceAtticFloor { get; set; }
        public double? ThermoResistanceDoorsWindows { get; set; }

        public BuildingExtended(Building building, CalculationThermoCoeffs thermoCoeffs)
        {
            _building = building;

            BuildingID = building.BuildingID;
            Name = building.GeneralInformation.Name;
            PhysicalAddress = building.GeneralInformation.PhysicalAddress;
            Purpose = building.GeneralInformation.Purpose.GetAttributeOfType<DisplayNameAttribute>().DisplayName;
            BuildingYardage = NumberUtils.FormatDouble(building.WallingOfBuilding.BuildingYardage);

            // Инициализаця списка иллюстрация
            foreach (BL.Illustration ill in building.Illustrations)
            {
                _illustration[ill.DivisionName].Add(new IllustrationExtended(ill));
            }

            // Инициализация расчетных значений
            CSEC = new CalculationSpecificEnergyConsumption(building);
            GeneralInfoTable = new CalculationGeneralInfo(building);
            CEEML = new CalculationElelctricEnergyMeterList(building);
            CEEMD = new CalculationElelctricEnergyMetersDescription(building);

            TotalElectricEnergyConsumption = NumberUtils.FormatDouble(CEEMD.TotalElectricEnergyConsumption);

            ChartSpecificEnergyConsumption = new Charts.ChartSpecificEnergyConsumption(building, CSEC.IntakeRealSpecificConsumptionList, CSEC.IntakeNormativeSpecificConsumption);

            AverageSpreadBetweenNormAndFactConsumption = NumberUtils.FormatDouble(Math.Abs(CSEC.AverageSpread));
            MoreOrLessConsumption = CSEC.AverageSpread > 0 ? "превысил" : "не превысил";


            Charts = new List<TemplateChartData>();

            // Для каждого ввода построить графики напряжения и тока
            foreach (var item in building.PowerQualitys.Powers)
            {
                ChartPowerQuality chart = new ChartPowerQuality(item.Simple, BL.Charts.PowerQualityType.Voltage, "U ввода " + item.PowerInput);
                Charts.Add(chart.Chart);
                chart = new ChartPowerQuality(item.Simple, BL.Charts.PowerQualityType.Current, "I ввода " + item.PowerInput);
                Charts.Add(chart.Chart);
            }

            CHeatML = new CalculationHeatingMeterList(building);
            CHotWaterML = new CalculationHotWaterMeterList(building);

            #region "Расчеты по теплоснабжению на отопление"

            KHP = FormulaInfiltrationCoef.Value(building, building.AdditionalInformation.CityCoeffs);
            Qomax = FormulaHeatingLoadPerHour.Value(building, building.AdditionalInformation.CityCoeffs, KHP);

            FormulaHeatingLoadAverage formulaHeating = new FormulaHeatingLoadAverage(building, building.AdditionalInformation.CityCoeffs, Qomax);

            // Средняя часовая тепловая нагрузка
            double?[] heatingLoadAverage = Enumerable.Range(1, 12).Select(
                i => building.AdditionalInformation.CityCoeffs != null ? (double?)formulaHeating.Value(building.AdditionalInformation.CityCoeffs.Values.Tout_cp[i - 1]) : null).ToArray();

            // Фактическое потребление на отопление берем из формы для здания
            double[] heatingEnergyConsumptionFact = Enumerable.Range(1, 12).Select(i => building.TwelveMonth.IntakeOfMonths[i].TERHeating.Value).ToArray();

            CHLFAN = new CalculationHeatingLoadFactAndNorm(building, thermoCoeffs, heatingLoadAverage, heatingEnergyConsumptionFact);

            #endregion

            #region "Расчеты по теплоснабжению на ГВС"

            Vhw = FormulaDhwConsumptionPerDay.Value(building);
            Qohw = FormulaEnergyOnDhwPerHour.Value(Vhw, thermoCoeffs.tox, thermoCoeffs);
            Qlhw = FormulaEnergyOnDhwPerHour.Value(Vhw, thermoCoeffs.toxl, thermoCoeffs);


            // Фактическое потребление энергии на ГВС берем из формы для здания
            double[] dhwConsumptionFact = Enumerable.Range(1, 12).Select(i => building.TwelveMonth.IntakeOfMonths[i].TERDhw.Value).ToArray();

            CDHWLFAN = new CalculationDHWLoadFactAndNorm(building, thermoCoeffs, Qohw, Qlhw, dhwConsumptionFact);

            #endregion

            #region "Расчеты по теплоснабжению на приточную вентиляцию"

            QvrBase = FormulaVentinLoadPerHourBase.Value(building, building.AdditionalInformation.CityCoeffs);
            Qvr = FormulaVentinLoadPerHour.Value(building, QvrBase, building.AdditionalInformation.CityCoeffs);

            // Фактическое потребление энергии на приточную вентиляцию берем из формы для здания
            double[] ventinConsumptionFact = Enumerable.Range(1, 12).Select(i => building.TwelveMonth.IntakeOfMonths[i].TERVentilationInlet.Value).ToArray();

            CVILFAN = new CalculationVentinLoadFactAndNorm(building, thermoCoeffs, Qvr, ventinConsumptionFact);

            #endregion

            #region "Расчеты по удельной тепловой характеристике объекта"

            int? zh_fact;
            double? tint_fact;
            double? text_fact;

            SpecificThermalCharacteristics = FormulaSpecificThermalCharacteristics.Value(building, building.AdditionalInformation.CityCoeffs, thermoCoeffs, out zh_fact, out tint_fact, out text_fact);
            //NormativeThermalCharacteristics = building.WallingOfBuilding.BuildingThermoCoeffs.NormativeThermalCharacteristics;

            Zh_fact = zh_fact;
            Tint_fact = tint_fact;
            Text_fact = text_fact;

            #endregion

            #region "Для оценки потенциала экономии тепловой энергии"

            // Сложить фактич. потребление тепловой энергии за год
            ThermalEnergyConsumptionFact = Enumerable.Range(1, 12).Select(i => building.TwelveMonth.IntakeOfMonths[i].ThermalEnergy.Value).Sum();

            // Сложить нормативное потребление из трех таблиц
            ThermalEnergyConsumptionNorm =
                (CHLFAN.NormativeTotal.HasValue ? CHLFAN.NormativeTotal.Value : 0)
                + (CDHWLFAN.NormativeTotal.HasValue ? CDHWLFAN.NormativeTotal.Value : 0)
                + (CVILFAN.NormativeTotal.HasValue ? CVILFAN.NormativeTotal.Value : 0);

            #endregion


            ChartNormAndFactHeatingConsumption = new ChartNormAndFactThermoConsumption(building, heatingEnergyConsumptionFact, CHLFAN.NormativeConsumption);
            ChartNormAndFactDHWConsumption = new ChartNormAndFactThermoConsumption(building, dhwConsumptionFact, CDHWLFAN.NormativeConsumption);
            ChartNormAndFactVentinConsumption = new ChartNormAndFactThermoConsumption(building, ventinConsumptionFact, CVILFAN.NormativeConsumption);

            Alpha = building.AdditionalInformation.CityCoeffs != null ? (double?)building.AdditionalInformation.CityCoeffs.Values.a : null;
            V = building.WallingOfBuilding.BuildingVolume;
            L = building.WallingOfBuilding.BuildingHeight;
            q0 = building.WallingOfBuilding.BuildingThermoCoeffs.q0;
            t0 = building.AdditionalInformation.CityCoeffs != null ? (double?)building.AdditionalInformation.CityCoeffs.Values.t0 : null;
            w0 = building.AdditionalInformation.CityCoeffs != null ? (double?)building.AdditionalInformation.CityCoeffs.Values.w0 : null;
            gDHW = building.WallingOfBuilding.BuildingThermoCoeffs.gDHW;
            m = building.WallingOfBuilding.BuildingThermoCoeffs.m;
            qv = building.WallingOfBuilding.BuildingThermoCoeffs.qv;
            Tnprv = building.AdditionalInformation.CityCoeffs != null ? (double?)building.AdditionalInformation.CityCoeffs.Values.Tout_ventin : null;
            Tnrv = building.AdditionalInformation.CityCoeffs != null ? (double?)building.AdditionalInformation.CityCoeffs.Values.Tout_ventincalc : null;

            CE = new CalculationEnvironment(building);

            Tin = building.WallingOfBuilding.BuildingThermoCoeffs.Tin;
            Tout_fact = building.WallingOfBuilding.BuildingThermoCoeffs.Tout_fact;
            //zh_fact

            ThermoResistanceWalls = building.WallingOfBuilding.BuildingWallingCoeffs.ThermoResistanceWalls;
            ThermoResistanceAtticFloor = building.WallingOfBuilding.BuildingWallingCoeffs.ThermoResistanceAtticFloor;
            ThermoResistanceDoorsWindows = building.WallingOfBuilding.BuildingWallingCoeffs.ThermoResistanceDoorsWindows;
        }
    }
}
