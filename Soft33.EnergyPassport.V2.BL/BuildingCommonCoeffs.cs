﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Разные коэф-ты для здания
    /// </summary>
    public class BuildingCommonCoeffs
    {
        /// <summary>
        /// Количество рабочих дней в год 
        /// </summary>
        public int? WorkDaysPerYear { get; set; }

        /// <summary>
        /// Количество рабочих часов в день
        /// </summary>
        public int? WorkHoursPerDay { get; set; }

        /// <summary>
        /// Режим работы
        /// </summary>
        public string WorkRegime { get; set; }
    }
}
