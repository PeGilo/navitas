﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Door
    {
        public Guid DoorTypeUID { get; set; }

        public string DoorTypeCaption { get; set; }

        public double ThermalResistance { get; set; }

        public int NumberOfDoors { get; set; }

        public double AverageYardage { get; set; }

        public bool IsEmpty()
        {
            return (DoorTypeUID == Guid.Empty
                && ThermalResistance == 0.0
                && NumberOfDoors == 0
                && AverageYardage == 0.0
                );
        }
    }
}
