﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class CalculationFinanceCoeffs
    {
        /// <summary>
        /// Стоимость теплоотражающей пленки
        /// </summary>
        public double HeatReflectingLayerCost { get; set; }
    }
}
