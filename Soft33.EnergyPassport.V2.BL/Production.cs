﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Production
    {
        public Production()
        {
            ProductionOfYearBaseMinus0 = new ProductionOfYear();
            ProductionOfYearBaseMinus1 = new ProductionOfYear();
            ProductionOfYearBaseMinus2 = new ProductionOfYear();
            ProductionOfYearBaseMinus3 = new ProductionOfYear();
            ProductionOfYearBaseMinus4 = new ProductionOfYear();
        }

        public string PhysicalTerms
        {
            get;
            set;
        }

        public string MainProductionPhysicalTerms
        {
            get;
            set;
        }

        public ProductionOfYear ProductionOfYearBaseMinus0
        {
            get;
            set;
        }

        public ProductionOfYear ProductionOfYearBaseMinus1
        {
            get;
            set;
        }

        public ProductionOfYear ProductionOfYearBaseMinus2
        {
            get;
            set;
        }

        public ProductionOfYear ProductionOfYearBaseMinus3
        {
            get;
            set;
        }

        public ProductionOfYear ProductionOfYearBaseMinus4
        {
            get;
            set;
        }

        [XmlIgnore]
        public Dictionary<int, ProductionOfYear> ProductionOfYears
        {
            get
            {
                Dictionary<int, ProductionOfYear> res = new Dictionary<int, ProductionOfYear>();
                res.Add(0, ProductionOfYearBaseMinus0);
                res.Add(-1, ProductionOfYearBaseMinus1);
                res.Add(-2, ProductionOfYearBaseMinus2);
                res.Add(-3, ProductionOfYearBaseMinus3);
                res.Add(-4, ProductionOfYearBaseMinus4);

                return res;
            }
        }
    }
}
