﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Приборы учета
    /// </summary>
    public class MeterLists
    {
        public MeterLists()
        {
            ElectricEnergy = new List<MeterElectricEnergy>();
            ColdWater = new List<MeterColdWater>();
            HotWater = new List<MeterHotWater>();
            Heating = new List<MeterHeating>();
            Gas = new List<MeterGas>();
        }

        /// <summary>
        /// Приборы учета электроэнергии
        /// </summary>
        public List<MeterElectricEnergy> ElectricEnergy { get; set; }

        /// <summary>
        /// Приборы учета холодной воды
        /// </summary>
        public List<MeterColdWater> ColdWater { get; set; }

        /// <summary>
        /// Приборы учёта горячей воды
        /// </summary>
        public List<MeterHotWater> HotWater { get; set; }

        /// <summary>
        /// Приборы учета тепла
        /// </summary>
        public List<MeterHeating> Heating { get; set; }

        /// <summary>
        /// Приборы учета газа
        /// </summary>
        public List<MeterGas> Gas { get; set; }
    }
}
