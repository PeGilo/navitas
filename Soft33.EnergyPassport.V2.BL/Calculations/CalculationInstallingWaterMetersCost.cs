﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.Extended;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 10.6 – Затраты на установку водосчетчика
    /// </summary>
    public class CalculationInstallingWaterMetersCost
    {
        public CalculationInstallingWaterMetersCost()
        {
             InitTable();
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }

        private void InitTable()
        {
            _table = new TemplateTable();

            const int COLUMNS_COUNT = 2;

            _table.AddRow(new string[COLUMNS_COUNT] { "Наименование", "Стоимость" }, TemplateTableRowType.Header)
                .AddRow(new string[COLUMNS_COUNT] { "Комплекс работ по установке водосчетчика на пластиковые трубы (установка водосчетчика, шарового вентиля, фильтра при стандартной схеме подключения)", "1200 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Комплекс работ по установке водосчетчика на металлические трубы (установка водосчетчика, шарового вентиля, фильтра при стандартной схеме подключения)", "1500 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Демонтаж водосчетчика", "200 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Установка водосчетчика", "400 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Демонтаж фильтра грубой очистки", "100 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Установка фильтра грубой очистки", "150 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Демонтаж проточного фильтра", "200 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Установка проточного фильтра", "700 руб." })
                .AddRow(new string[COLUMNS_COUNT] { "Мелкий ремонт сантехнических приборов", "400 руб." });
        }

    }
}
