﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Объемы энергопотребления зданиями и суммы платежей
    /// </summary>
    public class CalculationEnergyConsumptionAndPayments
    {
        private Inspection _inspection;

        public CalculationEnergyConsumptionAndPayments(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 4;

                    //Заголовок
                    TemplateTableRow row = new TemplateTableRow(TemplateTableRowType.Header);
                    row.Cells.AddRange(new string[COLUMNS_COUNT] {"Вид ТЭР", 
                        "Потребление в физических величинах", 
                        "Потребление в условном топливе, т.у.т.",
                        "Платежи, руб."});
                    _table.Rows.Add(row);

                    // Берем последний год из данных по организации
                    BL.IntakeOfPeriod intake = _inspection.Intake.FiveYear.YearMinus0;

                    _table
                        .AddRow(new object[COLUMNS_COUNT] { "Электроэнергия, кВт*ч", intake.ElectricEnergy.Value, intake.ElectricEnergy.ValueTUT, intake.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Тепловая энергия, в том числе, Гкал:", intake.ThermalEnergy.Value, intake.ThermalEnergy.ValueTUT, intake.ThermalEnergy.ValueRUR })
                            .AddRow(new object[COLUMNS_COUNT] { "- Отопление", intake.TERHeating.Value, intake.TERHeating.ValueTUT, intake.TERHeating.ValueRUR })
                            .AddRow(new object[COLUMNS_COUNT] { "- ГВС", intake.TERDhw.Value, intake.TERDhw.ValueTUT, intake.TERDhw.ValueRUR })
                            .AddRow(new object[COLUMNS_COUNT] { "- Вентиляция (приточная)", intake.TERVentilationInlet.Value, intake.TERVentilationInlet.ValueTUT, intake.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Водоснабжение, в том числе, м.куб.:", 
                            intake.HotWater.Value + intake.ColdWater.Value, 
                            intake.HotWater.ValueTUT + intake.ColdWater.ValueTUT, 
                            intake.HotWater.ValueRUR + intake.ColdWater.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "- ХВС", intake.ColdWater.Value, intake.ColdWater.ValueTUT, intake.ColdWater.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { " -ГВС", intake.HotWater.Value, intake.HotWater.ValueTUT, intake.HotWater.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Водоотведение, м.куб.", intake.Wastewater.Value, intake.Wastewater.ValueTUT, intake.Wastewater.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Природный газ (кроме моторного топлива), м.куб.", intake.NaturalGas.Value, intake.NaturalGas.ValueTUT, intake.NaturalGas.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Дрова, м.куб.", intake.Firewood.Value, intake.Firewood.ValueTUT, intake.Firewood.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Уголь, т.", intake.Coal.Value, intake.Coal.ValueTUT, intake.Coal.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Моторное топливо, в том числе т.:", intake.FuelOil.Value, intake.FuelOil.ValueTUT, intake.FuelOil.ValueRUR })
                            .AddRow(new object[COLUMNS_COUNT] { "- Бензин, т.", intake.TERGasoline.Value, intake.TERGasoline.ValueTUT, intake.TERGasoline.ValueRUR })
                            .AddRow(new object[COLUMNS_COUNT] { "- Керосин, т.", intake.TERKerosene.Value, intake.TERKerosene.ValueTUT, intake.TERKerosene.ValueRUR })
                            .AddRow(new object[COLUMNS_COUNT] { "- Дизельное топливо, т.", intake.TERDieselFuel.Value, intake.TERDieselFuel.ValueTUT, intake.TERDieselFuel.ValueRUR })
                            .AddRow(new object[COLUMNS_COUNT] { "- Газ, м.куб.", intake.TERGas.Value, intake.TERGas.ValueTUT, intake.TERGas.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "ИТОГО:", null, null, intake.TotalValueRUR });
                }

                return _table;
            }
        }
    }
}
