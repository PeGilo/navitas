﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 3.3.1 – Потребление электроэнергии на объекте
    /// </summary>
    public class CalculationElectricEnergyConsumption
    {
        private Inspection _inspection;

        public CalculationElectricEnergyConsumption(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 6;

                    TemplateTableRow row;
 
                    //Заголовок
                    row = new TemplateTableRow(TemplateTableRowType.Header);

                    row.Cells.AddRange(new string[COLUMNS_COUNT] {
                        "Наименование", 
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 4).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 3).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 2).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 1).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? _inspection.BaseYear.Value.ToString() + "г" : String.Empty
                    });

                    _table.Rows.Add(row);

                    _table
                        .AddRow(new object[COLUMNS_COUNT] { 
                            "Потребление, кВтч",
                            _inspection.Intake.FiveYear.YearMinus4.ElectricEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus3.ElectricEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus2.ElectricEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus1.ElectricEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus0.ElectricEnergy.Value
                        })
                        .AddRow(new object[COLUMNS_COUNT] { 
                            "Затраты, руб.",
                            _inspection.Intake.FiveYear.YearMinus4.ElectricEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus3.ElectricEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus2.ElectricEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus1.ElectricEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus0.ElectricEnergy.ValueRUR
                        });
                }

                return _table;
            }
        }
    }
}
