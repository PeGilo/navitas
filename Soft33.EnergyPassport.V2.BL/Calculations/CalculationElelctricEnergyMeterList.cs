﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица счетчиков здания
    /// </summary>
    public class CalculationElelctricEnergyMeterList
    {
        private Building _building;

        public CalculationElelctricEnergyMeterList(Building building)
        {
            _building = building;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 4;

                    TemplateTableRow row;
 
                    //Заголовок
                    row = new TemplateTableRow(TemplateTableRowType.Header);

                    row.Cells.AddRange(new string[COLUMNS_COUNT] {
                        "Наименование (тип, марка)", 
                        "Класс точности",
                        "Соответствие требованиям",
                        "Место установки"
                        //"Рисунок"
                    });

                    _table.Rows.Add(row);

                    foreach (MeterElectricEnergy meter in _building.Meters.ElectricEnergy)
                    {
                        if (!meter.IsEmpty())
                        {
                            _table.AddRow(new string[COLUMNS_COUNT] { 
                                meter.Caption,
                                meter.AccuracyClass,
                                meter.CompliesWithRequirements.HasValue ? (meter.CompliesWithRequirements.Value ? "Да": "Нет") : String.Empty,
                                meter.InstallationSite
                                //String.Empty
                            });
                        }
                    }
                }

                return _table;
            }
        }
    }
}
