﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.Extended;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица с коэф-тами по приточной вентиляции для каждого здания
    /// </summary>
    public class CalculationBuildingVentinCoeffs
    {
        IEnumerable<BuildingExtended> _buildings;

        public CalculationBuildingVentinCoeffs(IEnumerable<BuildingExtended> buildings)
        {
            _buildings = buildings;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 6;

                    foreach (var building in _buildings)
                    {
                        _table
                            .AddRow(new object[COLUMNS_COUNT] { 
                            building.Name,
                            building.Tnprv,
                            building.Tnrv,
                            building.qv,
                            building.QvrBase,
                            building.Qvr,
                        });
                    }
                }

                return _table;
            }
        }

    }
}
