﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 9.1 - Характеристика транспортных средств
    /// </summary>
    public class CalculationTransport
    {
        private Inspection _inspection;

        /// <summary>
        /// Общее кол-во ТС
        /// </summary>
        public int Total { get; set; }

        public CalculationTransport(Inspection inspection)
        {
            _inspection = inspection;

            InitTable();
        }

        private void InitTable()
        {
            _table = new TemplateTable();

            const int COLUMNS_COUNT = 10;

            _table.AddRow(new string[]{
                        "Марка транспортного средства (год выпуска)",
                        "Кол-во транспортных средств, шт.",
                        "Грузоподъемность, т; пассажировместимость, чел.",
                        "Вид используемого топлива",
                        "Удельный расход топлива по паспортным данным, л/100км",
                        "Пробег, км.",
                        "Отработанно, маш.-час",
                        "Тип спидометра",
                        "Спидометр исправен",
                        "Спидометр опломбирован"}, TemplateTableRowType.Header);

            Total = 0;

            foreach (VehicleUnit veh in _inspection.Vehicle.Vehicles)
            {
                if (!veh.IsEmpty())
                {
                    _table.AddRow(new object[COLUMNS_COUNT] { 
                                veh.Brand + " " + veh.Model + " (" + veh.Year + ")",
                                veh.Count.ToString(),
                                veh.VolumeOfCargo + " / " + veh.VolumeOfPassengerTraffic,
                                veh.TypeOfFuel,
                                veh.FuelConsumption,
                                veh.MileageLastYear,
                                veh.OperatingTimeLastYear,
                                veh.TypeOfOdometer,
                                veh.OdometerIsOK ? "да" : "нет",
                                veh.OdometerSealed ? "да" : "нет"
                            });
                    Total += veh.Count;
                }
            }
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }
    }
}
