﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 4.5.2 - Помесячное потребление тепловой энергии на объекте за 2011 г.
    /// </summary>
    public class CalculationHeatEnergyConsumptionByMonth
    {
        private Inspection _inspection;

        public CalculationHeatEnergyConsumptionByMonth(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 7;

                    _table.AddRow(new string[COLUMNS_COUNT] {
                        "Месяц", 
                        "Отопление",
                        "Отопление",
                        "ГВС",
                        "ГВС",
                        "Вентиляция",
                        "Вентиляция"
                    }, TemplateTableRowType.Header)
                    .AddRow(new string[COLUMNS_COUNT] {
                        "Месяц",
                        "Потребление, Гкал",
                        "Затраты, руб.",
                        "Потребление, Гкал",
                        "Затраты, руб.",
                        "Потребление, Гкал",
                        "Затраты, руб."
                    }, TemplateTableRowType.Header);

                    _table
                        .AddRow(new object[COLUMNS_COUNT] { "Январь",
                            _inspection.Intake.TwelveMonth.Month1.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month1.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month1.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month1.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month1.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month1.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Февраль",
                            _inspection.Intake.TwelveMonth.Month2.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month2.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month2.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month2.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month2.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month2.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Март",
                            _inspection.Intake.TwelveMonth.Month3.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month3.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month3.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month3.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month3.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month3.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Апрель",
                            _inspection.Intake.TwelveMonth.Month4.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month4.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month4.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month4.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month4.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month4.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Май",
                            _inspection.Intake.TwelveMonth.Month5.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month5.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month5.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month5.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month5.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month5.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Июнь",
                            _inspection.Intake.TwelveMonth.Month6.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month6.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month6.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month6.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month6.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month6.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Июль",
                            _inspection.Intake.TwelveMonth.Month7.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month7.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month7.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month7.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month7.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month7.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Август",
                            _inspection.Intake.TwelveMonth.Month8.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month8.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month8.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month8.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month8.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month8.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Сентябрь",
                            _inspection.Intake.TwelveMonth.Month9.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month9.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month9.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month9.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month9.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month9.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Октябрь",
                            _inspection.Intake.TwelveMonth.Month10.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month10.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month10.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month10.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month10.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month10.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Ноябрь",
                            _inspection.Intake.TwelveMonth.Month11.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month11.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month11.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month11.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month11.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month11.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Декабрь",
                            _inspection.Intake.TwelveMonth.Month12.TERHeating.Value,
                            _inspection.Intake.TwelveMonth.Month12.TERHeating.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month12.TERDhw.Value,
                            _inspection.Intake.TwelveMonth.Month12.TERDhw.ValueRUR,
                            _inspection.Intake.TwelveMonth.Month12.TERVentilationInlet.Value,
                            _inspection.Intake.TwelveMonth.Month12.TERVentilationInlet.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "ИТОГО",
                            _inspection.Intake.TwelveMonth.IntakeOfMonths.Select(pair => pair.Value.TERHeating.Value).Aggregate((d1, d2) => d1 + d2),
                            _inspection.Intake.TwelveMonth.IntakeOfMonths.Select(pair => pair.Value.TERHeating.ValueRUR).Aggregate((d1, d2) => d1 + d2),
                            _inspection.Intake.TwelveMonth.IntakeOfMonths.Select(pair => pair.Value.TERDhw.Value).Aggregate((d1, d2) => d1 + d2),
                            _inspection.Intake.TwelveMonth.IntakeOfMonths.Select(pair => pair.Value.TERDhw.ValueRUR).Aggregate((d1, d2) => d1 + d2),
                            _inspection.Intake.TwelveMonth.IntakeOfMonths.Select(pair => pair.Value.TERVentilationInlet.Value).Aggregate((d1, d2) => d1 + d2),
                            _inspection.Intake.TwelveMonth.IntakeOfMonths.Select(pair => pair.Value.TERVentilationInlet.ValueRUR).Aggregate((d1, d2) => d1 + d2)
                        });
                }

                return _table;
            }
        }

    }
}
