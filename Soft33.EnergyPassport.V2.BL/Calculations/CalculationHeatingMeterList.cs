﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 4.1 – Характеристики прибора учета тепловой энергии и его комплектующих
    /// </summary>
    public class CalculationHeatingMeterList
    {
        private Building _building;

        public CalculationHeatingMeterList(Building building)
        {
            _building = building;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 9;

                    _table.AddRow(new string[COLUMNS_COUNT]{
                        "№п.п",
                        "Место установки",
                        "Наименование прибора (тип, марка)",
                        //"Диапазон измерения",
                        "Опломбировка",
                        "Класс точности",
                        "Дата выпуска",
                        "Дата последней поверки",
                        "Заводской номер",
                        "Комментарий"
                    }, TemplateTableRowType.Header)
                    .AddRow(new string[COLUMNS_COUNT]{
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8",
                        "9"
                    }, TemplateTableRowType.Header);

                    for(int i = 0; i < _building.Meters.Heating.Count; i++)
                    {
                        MeterHeating meter = _building.Meters.Heating[i];
                        if (!meter.IsEmpty())
                        {
                            _table.AddRow(new string[COLUMNS_COUNT] { 
                                (i + 1).ToString(), // порядковый номер
                                meter.InstallationSite,
                                meter.Caption,
                                meter.Sealing ? "Да": "Нет",
                                meter.AccuracyClass,
                                meter.Year.HasValue ? meter.Year.ToString() : "",
                                meter.DateOfVerification.HasValue ? meter.DateOfVerification.Value.ToShortDateString() : "",
                                meter.SerialNumber,
                                meter.Comment
                            });
                        }
                    }
                }

                return _table;
            }
        }
    }
}
