﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица№3.7.1 – Фактический и нормативный удельные расходы электроэнергии здани-ем…за 2011 г.
    /// </summary>
    public class CalculationSpecificEnergyConsumption
    {
        //Для расчета удельного фактического расхода мы делим фактический расход на полезную площадь здания
        //Для расчета удельного нормативного расхода мы делим установленную мощность с учетом коэфициента использования
        //на полезную площадь здания
        private Building building;
        private TemplateTable specificEnergyConsumptionTable;
        private List<double> _intakeRealSpecificConsumptionList;
        private List<double> _intakeNormativeSpecificConsumption;
        private double _averageSpread;

        public CalculationSpecificEnergyConsumption(Building b)
        {
            building = b;
            _intakeRealSpecificConsumptionList = new List<double>();
            _intakeNormativeSpecificConsumption = new List<double>();

            InitTable();

            _averageSpread = _intakeRealSpecificConsumptionList.Zip(_intakeNormativeSpecificConsumption, (d1, d2) => d1 - d2).Average();
        }

        /// <summary>
        /// Фактический удельный расход эл.энергии по месяцам
        /// </summary>
        public IList<double> IntakeRealSpecificConsumptionList { get { return _intakeRealSpecificConsumptionList; } }

        /// <summary>
        /// Нормативный удельный рас-ход эл.энергии
        /// </summary>
        public IList<double> IntakeNormativeSpecificConsumption { get { return _intakeNormativeSpecificConsumption; } }

        /// <summary>
        /// Средняя разница между нормативным и факт. удельным расходом
        /// </summary>
        public double AverageSpread { get { return Double.IsNaN(_averageSpread) ? 0D : _averageSpread; } }

        public TemplateTable SpecificEnergyConsumptionTable
        {
            get
            {
                return specificEnergyConsumptionTable;
            }
        }

        private void InitTable()
        {
            specificEnergyConsumptionTable = new TemplateTable();

            TemplateTableRow row;

            //Заголовок
            row = new TemplateTableRow(TemplateTableRowType.Header);

            row.Cells.AddRange(new string[4] {"Месяц", 
                        "Потребление эл.энергии, кВт*ч", 
                        "Фактический удельный расход эл.энергии, кВт*ч/кв.м",
                        "Нормативный удельный рас-ход эл.энергии, кВт*ч/кв.м"});

            specificEnergyConsumptionTable.Rows.Add(row);

            //Строки данных
            double SummaryIntake = 0;
            double SummaryIntakeRealSpecificConsumption = 0;
            double SummaryIntakeNormativeSpecificConsumption = 0;

            //Площадь
            double S = building.WallingOfBuilding.BuildingYardage;

            //Нормативный удельный расход
            SummaryIntakeNormativeSpecificConsumption = (building.HouseholdEquipment.SummaryPowerCoefficient +
                building.ProcessEquipment.SummaryPowerCoefficient +
                building.OfficeEquipment.SummaryPowerCoefficient +
                building.HeatingEquipment.SummaryPowerCoefficient +
                building.InternalLightingEquipment.SummaryPowerCoefficient +
                building.ExternalLightingEquipment.SummaryPowerCoefficient) / S;

            double IntakeNormativeSpecificConsumption = SummaryIntakeNormativeSpecificConsumption / 12.0;

            double IntakeRealSpecificConsumption = 0;

            double Intake = 0;

            //Январь
            Intake = building.TwelveMonth.Month1.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Январь", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Февраль
            Intake = building.TwelveMonth.Month2.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Февраль", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Март
            Intake = building.TwelveMonth.Month3.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Март", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Апрель
            Intake = building.TwelveMonth.Month4.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Апрель", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Май
            Intake = building.TwelveMonth.Month5.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Май", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Июнь
            Intake = building.TwelveMonth.Month6.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Июнь", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Июль
            Intake = building.TwelveMonth.Month7.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Июль", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Август
            Intake = building.TwelveMonth.Month8.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Август", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Сентябрь
            Intake = building.TwelveMonth.Month9.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Сентябрь", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Октябрь
            Intake = building.TwelveMonth.Month10.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Октябрь", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Ноябрь
            Intake = building.TwelveMonth.Month11.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Ноябрь", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //Декабрь
            Intake = building.TwelveMonth.Month12.ElectricEnergy.Value;
            IntakeRealSpecificConsumption = Intake / S;

            _intakeRealSpecificConsumptionList.Add(IntakeRealSpecificConsumption);
            _intakeNormativeSpecificConsumption.Add(IntakeNormativeSpecificConsumption);
            row = new TemplateTableRow(TemplateTableRowType.Data);
            row.Cells.AddRange(new object[4] {"Декабрь", 
                        Intake, 
                        IntakeRealSpecificConsumption,
                        IntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);

            SummaryIntake = SummaryIntake + Intake;
            SummaryIntakeRealSpecificConsumption = SummaryIntakeRealSpecificConsumption + IntakeRealSpecificConsumption;

            //ИТОГО
            row = new TemplateTableRow(TemplateTableRowType.Footer);

            row.Cells.AddRange(new object[4] {"ИТОГО", 
                        SummaryIntake, 
                        SummaryIntakeRealSpecificConsumption,
                        SummaryIntakeNormativeSpecificConsumption});

            specificEnergyConsumptionTable.Rows.Add(row);
        }


    }
}
