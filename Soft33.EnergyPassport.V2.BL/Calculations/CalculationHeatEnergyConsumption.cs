﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 4.5.1 – Потребление тепловой энергии на объекте за 2007 – 2011 гг
    /// </summary>
    public class CalculationHeatEnergyConsumption
    {
        private Inspection _inspection;

        public CalculationHeatEnergyConsumption(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 6;

                    TemplateTableRow row;
 
                    //Заголовок
                    row = new TemplateTableRow(TemplateTableRowType.Header);

                    row.Cells.AddRange(new string[COLUMNS_COUNT] {
                        "Наименование", 
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 4).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 3).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 2).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 1).ToString() + "г" : String.Empty,
                        _inspection.BaseYear.HasValue ? _inspection.BaseYear.Value.ToString() + "г" : String.Empty
                    });

                    _table.Rows.Add(row);

                    _table
                        .AddRow(new object[COLUMNS_COUNT] { 
                            "Потребление, Гкал",
                            _inspection.Intake.FiveYear.YearMinus4.ThermalEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus3.ThermalEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus2.ThermalEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus1.ThermalEnergy.Value,
                            _inspection.Intake.FiveYear.YearMinus0.ThermalEnergy.Value
                        })
                        .AddRow(new object[COLUMNS_COUNT] { 
                            "Затраты, руб.",
                            _inspection.Intake.FiveYear.YearMinus4.ThermalEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus3.ThermalEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus2.ThermalEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus1.ThermalEnergy.ValueRUR,
                            _inspection.Intake.FiveYear.YearMinus0.ThermalEnergy.ValueRUR
                        });
                }

                return _table;
            }
        }
    }
}
