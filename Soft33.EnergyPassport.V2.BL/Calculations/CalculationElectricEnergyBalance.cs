﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 3.6.1 - Баланс электроэнергии
    /// </summary>
    public class CalculationElectricEnergyBalance
    {
        private Inspection _inspection;

        public CalculationElectricEnergyBalance(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 2;

                    TemplateTableRow row = new TemplateTableRow(TemplateTableRowType.Header);

                    row.Cells.AddRange(new string[COLUMNS_COUNT] {
                        "Статьи прихода и расхода", 
                        "Годовое потребление, кВт*ч"
                    });
                    _table.Rows.Add(row);

                    double processEqSummary = 0D,
                           householdEqSummary = 0D,
                           officeEqSummary = 0D,
                           heatingEqSummary = 0D,
                           inLightEqSummary = 0D,
                           exLightEqSummary = 0D;

                    // Сложить расчетный расход по всем зданиям
                    foreach(Building b in _inspection.Buildings)
                    {
                        processEqSummary += b.ProcessEquipment.SummaryWork / 1000; // Перевод в кВт
                        householdEqSummary += b.HouseholdEquipment.SummaryWork / 1000;
                        officeEqSummary += b.OfficeEquipment.SummaryWork / 1000;
                        heatingEqSummary += b.HeatingEquipment.SummaryWork / 1000;
                        inLightEqSummary += b.InternalLightingEquipment.SummaryWork / 1000;
                        exLightEqSummary += b.ExternalLightingEquipment.SummaryWork / 1000;
                    }

                    _table
                        .AddRow(new object[COLUMNS_COUNT] { 
                            "Приход (фактическая мощность)",
                            _inspection.Intake.FiveYear.YearMinus0.ElectricEnergy.Value
                        })
                        .AddRow(new object[COLUMNS_COUNT] { 
                            "Расход (расчетная мощность), в том числе на:",
                            processEqSummary + householdEqSummary + officeEqSummary + heatingEqSummary + inLightEqSummary + exLightEqSummary
                        })
                        .AddRow(new object[COLUMNS_COUNT] { "- технологическое оборудование", processEqSummary })
                        .AddRow(new object[COLUMNS_COUNT] { "- бытовое оборудование", householdEqSummary })
                        .AddRow(new object[COLUMNS_COUNT] { "- оргтехника", officeEqSummary })
                        .AddRow(new object[COLUMNS_COUNT] { "- электронагревательные приборы", heatingEqSummary })
                        .AddRow(new object[COLUMNS_COUNT] { "- освещение внутреннее", inLightEqSummary })
                        .AddRow(new object[COLUMNS_COUNT] { "- освещение наружное", exLightEqSummary });
                }

                return _table;
            }
        }

    }
}
