﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Объемы энергопотребления зданиями и суммы платежей
    /// </summary>
    public class CalculationEnergyConsumptionDynamic
    {
        private const int COLUMNS_COUNT = 7;
        private Inspection _inspection;

        public CalculationEnergyConsumptionDynamic(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();



                    //TemplateTableRow row;

                    //Заголовок
                    //_table
                    //    .AddRow(new object[COLUMNS_COUNT] { "", 0.0, 0.0, 0.0 })

                    IntakeOfPeriod intake = new IntakeOfPeriod();

                    //CreateRowValues("Электроэнергия", "т у.т.", () => intake.TERGasoline);

                    _table
                        .AddRow(CreateRowValuesTUT("Электроэнергия", "т у.т.", () => intake.ElectricEnergy))
                        .AddRow(CreateRowValues("Электроэнергия ", "кВт*ч", () => intake.ElectricEnergy))
                        .AddRow(CreateRowValuesTUT("Тепловая энергия, в том числе:", "т у.т.", () => intake.ThermalEnergy))
                        .AddRow(CreateRowValues("Тепловая энергия, в том числе:", "Гкал", () => intake.ThermalEnergy))
                        .AddRow(CreateRowValuesTUT("Отопление", "т у.т.", () => intake.TERHeating))
                        .AddRow(CreateRowValues("Отопление", "Гкал", () => intake.TERHeating))
                        .AddRow(CreateRowValuesTUT("ГВС", "т у.т.", () => intake.TERDhw))
                        .AddRow(CreateRowValues("ГВС", "Гкал", () => intake.TERDhw))
                        .AddRow(CreateRowValuesTUT("Вентиляция (приточная)", "т у.т.", () => intake.TERVentilationInlet))
                        .AddRow(CreateRowValues("Вентиляция (приточная)", "Гкал", () => intake.TERVentilationInlet))
                        .AddRow(CreateRowValuesTUT("Водоснабжение горячее", "т у.т.", () => intake.HotWater))
                        .AddRow(CreateRowValues("Водоснабжение горячее", "м.куб.", () => intake.HotWater))
                        .AddRow(CreateRowValuesTUT("Водоснабжение холодное", "т у.т.", () => intake.ColdWater))
                        .AddRow(CreateRowValues("Водоснабжение холодное", "м.куб.", () => intake.ColdWater))
                        .AddRow(CreateRowValuesTUT("Водоотведение общее", "т у.т.", () => intake.Wastewater))
                        .AddRow(CreateRowValues("Водоотведение общее", "м.куб.", () => intake.Wastewater))
                        .AddRow(CreateRowValuesTUT("Дрова", "т у.т.", () => intake.Firewood))
                        .AddRow(CreateRowValues("Дрова", "м.куб.", () => intake.Firewood))
                        .AddRow(CreateRowValuesTUT("Уголь", "т у.т.", () => intake.Coal))
                        .AddRow(CreateRowValues("Уголь", "т.", () => intake.Coal))
                        .AddRow(CreateRowValuesTUT("Природный газ (кроме моторного топлива)", "т у.т.", () => intake.NaturalGas))
                        .AddRow(CreateRowValues("Природный газ (кроме моторного топлива)", "м.куб.", () => intake.NaturalGas))
                        .AddRow(CreateRowValuesTUT("Моторное топливо, в том числе:", "т у.т.", () => intake.FuelOil))
                        .AddRow(CreateRowValues("Моторное топливо, в том числе:", "л.", () => intake.FuelOil))
                        .AddRow(CreateRowValuesTUT("Бензин", "т у.т.", () => intake.TERGasoline))
                        .AddRow(CreateRowValues("Бензин", "л.", () => intake.TERGasoline))
                        .AddRow(CreateRowValuesTUT("Керосин", "т у.т.", () => intake.TERKerosene))
                        .AddRow(CreateRowValues("Керосин", "л.", () => intake.TERKerosene))
                        .AddRow(CreateRowValuesTUT("Дизельное топливо", "т у.т.", () => intake.TERDieselFuel))
                        .AddRow(CreateRowValues("Дизельное топливо", "л.", () => intake.TERDieselFuel))
                        .AddRow(CreateRowValuesTUT("Газ", "т у.т.", () => intake.TERGas))
                        .AddRow(CreateRowValues("Газ", "м.куб.", () => intake.TERGas));

                }

                return _table;
            }
        }

        private object[] CreateRowValues(string s1, string s2, Expression<Func<TER>> expression)
        {
            string propertyName = ReflectionHelper.GetPropertyName<TER>(expression);

            PropertyInfo monthProperty = typeof(BL.IntakeOfPeriod).GetProperty(propertyName);

            TER ter_0 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus0, null);
            TER ter_1 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus1, null);
            TER ter_2 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus2, null);
            TER ter_3 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus3, null);
            TER ter_4 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus4, null);
            
            //BL.IntakeOfPeriod intake = _inspection.Intake.FiveYear.YearMinus0;

            return
                new object[COLUMNS_COUNT] { s1, s2, ter_4.Value, ter_3.Value, ter_2.Value, ter_1.Value, ter_0.Value };
        }

        private object[] CreateRowValuesTUT(string s1, string s2, Expression<Func<TER>> expression)
        {
            string propertyName = ReflectionHelper.GetPropertyName<TER>(expression);

            PropertyInfo monthProperty = typeof(BL.IntakeOfPeriod).GetProperty(propertyName);

            TER ter_0 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus0, null);
            TER ter_1 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus1, null);
            TER ter_2 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus2, null);
            TER ter_3 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus3, null);
            TER ter_4 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus4, null);

            //BL.IntakeOfPeriod intake = _inspection.Intake.FiveYear.YearMinus0;

            return
                new object[COLUMNS_COUNT] { s1, s2, ter_4.ValueTUT, ter_3.ValueTUT, ter_2.ValueTUT, ter_1.ValueTUT, ter_0.ValueTUT };
        }
    }
}
