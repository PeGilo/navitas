﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.Extended;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица потэнциалов экономии тепловой энергии для зданий
    /// </summary>
    public class CalculationBuildingThermalEnergyEconomyCoeffs
    {
        IEnumerable<BuildingExtended> _buildings;

        /// <summary>
        /// Суммарное потребление по объекту фактическое
        /// </summary>
        public double ConsumptionFact { get; set; }

        /// <summary>
        /// Суммарное потребление по объекту нормируемое
        /// </summary>
        public double ConsumptionNorm { get; set; }

        /// <summary>
        /// Потенциал экономии в процентах
        /// </summary>
        public double PotentialPercents { get; set; }

        public CalculationBuildingThermalEnergyEconomyCoeffs(IEnumerable<BuildingExtended> buildings)
        {
            _buildings = buildings;

            InitTable();
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }

        private void InitTable()
        {
            _table = new TemplateTable();

            const int COLUMNS_COUNT = 5;

            _table.AddRow(new string[] { 
                        "Здание",
                        "Нормативное потребление тепловой энергии, Гкал",
                        "Фактическое потребление тепловой энергии, Гкал",
                        "Потенциал экономии тепловой энергии, Гкал",
                        "Потенциал экономии тепловой энергии, %",
                    }, TemplateTableRowType.Header);

            ConsumptionFact = 0;
            ConsumptionNorm = 0;
            

            foreach (var building in _buildings)
            {
                double potential = Math.Abs(building.ThermalEnergyConsumptionFact - building.ThermalEnergyConsumptionNorm);

                _table
                    .AddRow(new object[COLUMNS_COUNT] { 
                            building.Name,
                            building.ThermalEnergyConsumptionNorm,
                            building.ThermalEnergyConsumptionFact,
                            potential,
                            (potential / building.ThermalEnergyConsumptionFact) * 100 // В процентах
                        });

                ConsumptionFact += building.ThermalEnergyConsumptionFact;
                ConsumptionNorm += building.ThermalEnergyConsumptionNorm;
            }

            PotentialPercents = ((ConsumptionFact - ConsumptionNorm) / ConsumptionFact) * 100;
        }

    }
}
