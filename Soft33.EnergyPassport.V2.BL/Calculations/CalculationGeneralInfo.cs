﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Удельный расход электроэнергии
    /// </summary>
    public class CalculationGeneralInfo
    {
        private Building _building;

        public CalculationGeneralInfo(Building b)
        {
            _building = b;
        }

        private TemplateTable _generalInfoTable;

        public TemplateTable GeneralInfoTable
        {
            get
            {
                if (_generalInfoTable == null)
                {
                    _generalInfoTable = new TemplateTable();

                    const int COLUMNS_COUNT = 4;

                    TemplateTableRow row;
 
                    //Заголовок
                    row = new TemplateTableRow(TemplateTableRowType.Header);
                                        
                    row.Cells.AddRange(new string[COLUMNS_COUNT] {"№ п/п", 
                        "Характеристика", 
                        "Единица измерения",
                        "Величина"});

                    _generalInfoTable.Rows.Add(row);

                    // площадь окон
                    double windowsYardage = _building.WallingOfBuilding.Windows.Sum(w => w.NumberOfWindows * w.AverageYardage);

                    // площадь ограждающих конструкций складывается из площади окон и площади стен
                    double? YardageBarrierConstruction = _building.WallingOfBuilding.WallsYardage + windowsYardage;

                    // всего персонала
                    int totalStaff = _building.GeneralInformation.SupportStaff
                        + _building.GeneralInformation.RegularStaff
                        + _building.GeneralInformation.Residents
                        + _building.GeneralInformation.Visitors
                        + _building.GeneralInformation.Patients
                        + _building.GeneralInformation.Students;


                    _generalInfoTable
                        .AddRow(new string[COLUMNS_COUNT] { "1", "Тип здания", "-", "" })
                        .AddRow(new string[COLUMNS_COUNT] { "2", "Этажность", "шт.", _building.AdditionalInformation.NumberOfFloor.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "3", "Строительный объем", "м.куб.", NumberUtils.FormatDouble(_building.WallingOfBuilding.BuildingVolume) })
                        .AddRow(new string[COLUMNS_COUNT] { "4", "Площадь объекта", "м.кв.", NumberUtils.FormatDouble(_building.WallingOfBuilding.BuildingYardage) })
                        .AddRow(new string[COLUMNS_COUNT] { "5", "Площадь застройки", "м.кв.", NumberUtils.FormatDouble(_building.WallingOfBuilding.ConstructionYardage) })
                        .AddRow(new string[COLUMNS_COUNT] { "6", "Год постройки", "г.", _building.AdditionalInformation.BuildingYear.HasValue ? _building.AdditionalInformation.BuildingYear.Value.ToString() : ""})
                        .AddRow(new string[COLUMNS_COUNT] { "7", "Количество подъездов", "шт.", _building.AdditionalInformation.NumberOfEntrances.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "8", "Электроснабжение", "-", 
                            (_building.AdditionalInformation.Resources.ElectricityResource != null) ? _building.AdditionalInformation.Resources.ElectricityResource.Name : "" })
                        .AddRow(new string[COLUMNS_COUNT] { "9", "Теплоснабжение", "-", 
                            (_building.AdditionalInformation.Resources.HeatResource != null) ? _building.AdditionalInformation.Resources.HeatResource.Name : ""})
                        .AddRow(new string[COLUMNS_COUNT] { "10", "Водоснабжение", "-", 
                            (_building.AdditionalInformation.Resources.WaterResource != null) ? _building.AdditionalInformation.Resources.WaterResource.Name : ""})
                        .AddRow(new string[COLUMNS_COUNT] { "11", "Водотведение", "-",
                            (_building.AdditionalInformation.Resources.SewerageResource != null) ? _building.AdditionalInformation.Resources.SewerageResource.Name : ""})
                            .AddRow(new string[COLUMNS_COUNT] { "12", "Площадь ораждающих конструкций, в т.ч.:", "м.кв.", YardageBarrierConstruction.HasValue ? NumberUtils.FormatDouble(YardageBarrierConstruction.Value) : "" })
                        .AddRow(new string[COLUMNS_COUNT] { "12.1", "-стен", "м.кв.", NumberUtils.FormatDouble(_building.WallingOfBuilding.WallsYardage) })
                        .AddRow(new string[COLUMNS_COUNT] { "12.2", "-окон", "м.кв.", NumberUtils.FormatDouble(windowsYardage) })
                        .AddRow(new string[COLUMNS_COUNT] { "13", "Режим работы", "-", _building.WallingOfBuilding.BuildingCommonCoeffs.WorkRegime })
                        .AddRow(new string[COLUMNS_COUNT] { "14", "Количество рабочих дней в году", "шт.", _building.WallingOfBuilding.BuildingCommonCoeffs.WorkDaysPerYear.HasValue ? _building.WallingOfBuilding.BuildingCommonCoeffs.WorkDaysPerYear.Value.ToString() : "" })
                        .AddRow(new string[COLUMNS_COUNT] { "15", "Численность персонала", "чел.", totalStaff.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "15.1", "Вспомогательный", "чел.", _building.GeneralInformation.SupportStaff.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "15.2", "Штатный персонал", "чел.", _building.GeneralInformation.RegularStaff.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "15.3", "Проживающие", "чел.", _building.GeneralInformation.Residents.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "15.4", "Посетители", "чел.", _building.GeneralInformation.Visitors.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "15.5", "Пациенты", "чел.", _building.GeneralInformation.Patients.ToString() })
                        .AddRow(new string[COLUMNS_COUNT] { "15.6", "Учащиеся", "чел.", _building.GeneralInformation.Students.ToString() });
                }

                return _generalInfoTable;
            }
        }
    }
}
