﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 3.3.2 - Сведения о помесячном электропотреблении на объекте в 2011 г.
    /// </summary>
    public class CalculationElectricEnergyConsumptionByMonth
    {
        private Inspection _inspection;

        public CalculationElectricEnergyConsumptionByMonth(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 3;

                    TemplateTableRow row;

                    //Заголовок
                    row = new TemplateTableRow(TemplateTableRowType.Header);

                    row.Cells.AddRange(new string[COLUMNS_COUNT] {
                        "Наименование", 
                        "Потребление, кВт*ч",
                        "Затраты, руб.",
                    });

                    _table.Rows.Add(row);

                    _table
                        .AddRow(new object[COLUMNS_COUNT] { "Январь",
                            _inspection.Intake.TwelveMonth.Month1.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month1.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Февраль",
                            _inspection.Intake.TwelveMonth.Month2.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month2.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Март",
                            _inspection.Intake.TwelveMonth.Month3.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month3.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Апрель",
                            _inspection.Intake.TwelveMonth.Month4.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month4.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Май",
                            _inspection.Intake.TwelveMonth.Month5.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month5.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Июнь",
                            _inspection.Intake.TwelveMonth.Month6.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month6.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Июль",
                            _inspection.Intake.TwelveMonth.Month7.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month7.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Август",
                            _inspection.Intake.TwelveMonth.Month8.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month8.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Сентябрь",
                            _inspection.Intake.TwelveMonth.Month9.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month9.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Октябрь",
                            _inspection.Intake.TwelveMonth.Month10.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month10.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Ноябрь",
                            _inspection.Intake.TwelveMonth.Month11.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month11.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "Декабрь",
                            _inspection.Intake.TwelveMonth.Month12.ElectricEnergy.Value,
                            _inspection.Intake.TwelveMonth.Month12.ElectricEnergy.ValueRUR })
                        .AddRow(new object[COLUMNS_COUNT] { "ИТОГО",
                            TotalEnergyValue(_inspection.Intake.TwelveMonth),
                            TotalEnergyValueRUR(_inspection.Intake.TwelveMonth) });
                }

                return _table;
            }
        }

        private double TotalEnergyValue(TwelveMonthIntake intake)
        {
            double sum = 0;
            foreach (var item in intake.IntakeOfMonths)
            {
                sum += item.Value.ElectricEnergy.Value;
            }
            return sum;
        }

        private double TotalEnergyValueRUR(TwelveMonthIntake intake)
        {
            double sum = 0;
            foreach (var item in intake.IntakeOfMonths)
            {
                sum += item.Value.ElectricEnergy.ValueRUR;
            }
            return sum;
        }
    }
}
