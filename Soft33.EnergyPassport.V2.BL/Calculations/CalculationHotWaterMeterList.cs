﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 4.1 (II) – Характеристики приборов учета ГВС
    /// </summary>
    public class CalculationHotWaterMeterList
    {
        private Building _building;

        public CalculationHotWaterMeterList(Building building)
        {
            _building = building;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 10;

                    _table.AddRow(new string[COLUMNS_COUNT]{
                        "№п.п",
                        "Место установки",
                        "Наименование прибора (тип, марка)",
                        "Диапазон измерения",
                        "Опломбировка",
                        "Класс точности",
                        "Дата выпуска",
                        "Дата последней поверки",
                        "Заводской номер",
                        "Комментарий"
                    }, TemplateTableRowType.Header)
                    .AddRow(new string[COLUMNS_COUNT]{
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8",
                        "9",
                        "10"
                    }, TemplateTableRowType.Header);

                    for (int i = 0; i < _building.Meters.HotWater.Count; i++)
                    {
                        MeterHotWater meter = _building.Meters.HotWater[i];
                        if (!meter.IsEmpty())
                        {
                            _table.AddRow(new string[COLUMNS_COUNT] { 
                                (i + 1).ToString(), // порядковый номер
                                meter.InstallationSite,
                                meter.Caption,
                                meter.MeasurementRange,
                                meter.Sealing ? "Да": "Нет",
                                meter.AccuracyClass,
                                meter.Year.HasValue ? meter.Year.ToString() : "",
                                meter.DateOfVerification.HasValue ? meter.DateOfVerification.Value.ToShortDateString() : "",
                                meter.SerialNumber,
                                meter.Comment
                            });
                        }
                    }
                }

                return _table;
            }
        }
    }
}
