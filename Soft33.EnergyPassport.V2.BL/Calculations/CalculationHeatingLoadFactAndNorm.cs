﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица 4.8 - Фактическая и нормативная тепловые нагрузки на отопление  за 2011г.
    /// </summary>
    public class CalculationHeatingLoadFactAndNorm
    {
        private Building _building;
        private TemplateTable _table;
        private CalculationThermoCoeffs _thermoCoeffs;
        private List<double?> _normativeConsumption;
        private double?[] _heatingLoadAverage;
        private double[] _heatingEnergyConsumptionFact;

        /// <summary>
        /// Сумма столбца нормативного потребления
        /// </summary>
        public double? NormativeTotal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="thermoCoeffs"></param>
        /// <param name="heatingLoadAverage">Средняя часовая тепловая нагрузка на отопление</param>
        /// <param name="heatingEnergyConsumptionFact">Фактическое потребление тепловой энергии </param>
        public CalculationHeatingLoadFactAndNorm(Building b,
            CalculationThermoCoeffs thermoCoeffs,
            double?[] heatingLoadAverage,
            double[] heatingEnergyConsumptionFact)
        {
            _building = b;
            _heatingLoadAverage = heatingLoadAverage;
            _heatingEnergyConsumptionFact = heatingEnergyConsumptionFact;
            _thermoCoeffs = thermoCoeffs;
            _normativeConsumption = new List<double?>();

            InitTable();
        }

        /// <summary>
        /// Нормативное потребление тепловой энергии на отопление
        /// </summary>
        public IList<double?> NormativeConsumption { get { return _normativeConsumption; } }

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }

        private void InitTable()
        {
            _table = new TemplateTable();

            double? norm;
            _normativeConsumption.Clear();

            LoadRow(0, "Январь", out norm); _normativeConsumption.Add(norm);
            LoadRow(1, "Февраль", out norm); _normativeConsumption.Add(norm);
            LoadRow(2, "Март", out norm); _normativeConsumption.Add(norm);
            LoadRow(3, "Апрель", out norm); _normativeConsumption.Add(norm);
            LoadRow(4, "Май", out norm); _normativeConsumption.Add(norm);

            // Для летних месяцев не должно быть данных
            _table.AddRow(new object[] { "Июнь", null, null, null, null, _heatingEnergyConsumptionFact.Length > 5 ? (double?)_heatingEnergyConsumptionFact[5] : null });
            _normativeConsumption.Add(null);

            _table.AddRow(new object[] { "Июль", null, null, null, null, _heatingEnergyConsumptionFact.Length > 6 ? (double?)_heatingEnergyConsumptionFact[6] : null });
            _normativeConsumption.Add(null);

            _table.AddRow(new object[] { "Август", null, null, null, null, _heatingEnergyConsumptionFact.Length > 7 ? (double?)_heatingEnergyConsumptionFact[7] : null });
            _normativeConsumption.Add(null);
            
            LoadRow(8, "Сентябрь", out norm); _normativeConsumption.Add(norm);
            LoadRow(9, "Октябрь", out norm); _normativeConsumption.Add(norm);
            LoadRow(10, "Ноябрь", out norm); _normativeConsumption.Add(norm);
            LoadRow(11, "Декабрь", out norm); _normativeConsumption.Add(norm);

            NormativeTotal = _normativeConsumption.Sum();

            _table.AddRow(new object[]{
                "ИТОГО:",
                "",
                "",
                "",
                NormativeTotal,
                _heatingEnergyConsumptionFact.Sum()
            }, TemplateTableRowType.Footer);
        }

        private void LoadRow(int index, string monthName, out double? normativeConsumption)
        {
            double? col2 = _building.AdditionalInformation.CityCoeffs != null ? 
                (double?)_building.AdditionalInformation.CityCoeffs.Values.Tout_cp[index] : null;

            double? col3 = _heatingLoadAverage.Length > index ? _heatingLoadAverage[index] : null;
            int? col4 = _building.AdditionalInformation.CityCoeffs != null ?
                (int?)_building.AdditionalInformation.CityCoeffs.Values.HeatingDaysByMonth[index] : null;
            normativeConsumption = (col4.HasValue && col3.HasValue) ? (double?)(col4.Value * col3.Value * 24 /* Кол-во часов в сутках */) : null;
            double? col6 = _heatingEnergyConsumptionFact.Length > index ? (double?)_heatingEnergyConsumptionFact[index] : null;

            _table.AddRow(new object[] {
                monthName,
                col2,
                col3, 
                col4,
                normativeConsumption,
                col6 
            });
        }


    }
}
