﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.Extended;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица с коэф-тами по отоплению для каждого здания
    /// </summary>
    public class CalculationBuildingHeatingCoeffs
    {
        //private Inspection _inspection;
        IEnumerable<BuildingExtended> _buildings;

        public CalculationBuildingHeatingCoeffs(IEnumerable<BuildingExtended> buildings)
        {
            //_inspection = inspection;
            _buildings = buildings;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 9;

                    foreach (var building in _buildings)
                    {
                        _table
                            .AddRow(new object[COLUMNS_COUNT] { 
                            building.Name,

                            building.Alpha,
                            building.V,
                            building.L,
                            building.q0,
                            building.t0,
                            building.w0,

                            building.KHP,
                            building.Qomax

                        });
                    }
                }

                return _table;
            }
        }

    }
}
