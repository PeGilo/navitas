﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Таблица счетчиков здания
    /// </summary>
    public class CalculationElelctricEnergyMetersDescription
    {
        private const int COLUMNS_COUNT = 6;
        private Building _building;
        private TemplateTable _table;
        private double _totalElectricEnergyConsumption;

        public CalculationElelctricEnergyMetersDescription(Building building)
        {
            _building = building;

            _totalElectricEnergyConsumption = InitTable();
        }


        public double TotalElectricEnergyConsumption
        {
            get
            {
                return _totalElectricEnergyConsumption;
            }
        }

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }

        private double InitTable()
        {
            _table = new TemplateTable();

            TemplateTableRow row = new TemplateTableRow(TemplateTableRowType.Header);

            row.Cells.AddRange(new string[COLUMNS_COUNT] {
                        "Наименование электроприемника", 
                        "Количество, шт.",
                        "Мощность, Вт",
                        "Итого установленная мощность, Вт",
                        "Время работы в году, час",
                        "Итого потребляемая мощность в год, Wn, кВт*ч/год"
                    });

            _table.Rows.Add(row);

            double subtotal = 0;
            double total = 0;

            _table.AddRow(new string[COLUMNS_COUNT] { "Технологическое оборудование", "", "", "", "", "" }, TemplateTableRowType.DataBold);
            subtotal = FillTableWith(_building.ProcessEquipment);
            _table.AddRow(new object[COLUMNS_COUNT] { "Итого:", null, null, null, null, subtotal });
            total += subtotal;

            _table.AddRow(new string[COLUMNS_COUNT] { "Бытовое оборудование", "", "", "", "", "" }, TemplateTableRowType.DataBold);
            subtotal = FillTableWith(_building.HouseholdEquipment);
            _table.AddRow(new object[COLUMNS_COUNT] { "Итого:", null, null, null, null, subtotal });
            total += subtotal;

            _table.AddRow(new string[COLUMNS_COUNT] { "Оргтехника", "", "", "", "", "" }, TemplateTableRowType.DataBold);
            subtotal = FillTableWith(_building.OfficeEquipment);
            _table.AddRow(new object[COLUMNS_COUNT] { "Итого:", null, null, null, null, subtotal });
            total += subtotal;

            _table.AddRow(new string[COLUMNS_COUNT] { "Электронагревательные приборы", "", "", "", "", "" }, TemplateTableRowType.DataBold);
            subtotal = FillTableWith(_building.HeatingEquipment);
            _table.AddRow(new object[COLUMNS_COUNT] { "Итого:", null, null, null, null, subtotal });
            total += subtotal;

            _table.AddRow(new string[COLUMNS_COUNT] { "Внутреннее освещение", "", "", "", "", "" }, TemplateTableRowType.DataBold);
            subtotal = FillTableWith(_building.InternalLightingEquipment);
            _table.AddRow(new object[COLUMNS_COUNT] { "Итого:", null, null, null, null, subtotal });
            total += subtotal;

            _table.AddRow(new string[COLUMNS_COUNT] { "Наружное освещение", "", "", "", "", "" }, TemplateTableRowType.DataBold);
            subtotal = FillTableWith(_building.ExternalLightingEquipment);
            _table.AddRow(new object[COLUMNS_COUNT] { "Итого:", null, null, null, null, subtotal });
            total += subtotal;

            _table.AddRow(new object[COLUMNS_COUNT] { "Итого, по объекту:", null, null, null, null, total });

            return total;
        }


        /// <summary>
        /// Возвращает значение итого.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private double FillTableWith(ElectroloadList list)
        {
            double total = 0D;
            foreach (Electroload eload in list.Electroloads)
            {
                if (!eload.IsEmpty())
                {
                    double Wn = (eload.SummaryPower * eload.TimeHoursPerYear) / 1000; // Потребляемая мощность в год (переводим в КилоВт)
                    _table.AddRow(new object[COLUMNS_COUNT] { 
                                eload.Name,
                                eload.Count.ToString(),
                                eload.Power,
                                eload.SummaryPower,
                                eload.TimeHoursPerYear,
                                Wn
                            });
                    total += Wn;
                }
            }
            return total;
        }
    }
}
