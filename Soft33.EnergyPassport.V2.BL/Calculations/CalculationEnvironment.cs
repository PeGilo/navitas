﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Сведения об эксплуатационной среде 
    /// </summary>
    public class CalculationEnvironment
    {
        private Building _building;

        public CalculationEnvironment(Building building)
        {
            _building = building;
        }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new TemplateTable();

                    const int COLUMNS_COUNT = 11;

                    foreach (EnvironmentQuality qty in _building.EnvironmentQualitys.Environments)
                    {
                        if (!qty.IsEmpty())
                        {
                            _table.AddRow(new object[COLUMNS_COUNT] { 
                                qty.RoomTypeCaption,
                                null,
                                "9:30",
                                qty.Temperature,
                                _building.WallingOfBuilding.BuildingThermoCoeffs.Tin,
                                qty.Illumination,
                                null,
                                qty.Humidity,
                                null,
                                qty.WindSpeed,
                                null
                            });
                        }
                    }
                }

                return _table;
            }
        }
    }
}
