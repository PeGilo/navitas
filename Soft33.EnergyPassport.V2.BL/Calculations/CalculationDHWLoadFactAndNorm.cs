﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Фактическая и нормативная тепловые нагрузки на ГВС за 2011г.
    /// </summary>
    public class CalculationDHWLoadFactAndNorm
    {
        private Building _building;
        private TemplateTable _table;
        private CalculationThermoCoeffs _thermoCoeffs;
        private List<double?> _normativeConsumption;
        private double? _Qohw;
        private double? _Qlhw;
        private double[] _dhwConsumptionFact;

        /// <summary>
        /// Сумма столбца нормативного потребления
        /// </summary>
        public double? NormativeTotal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="thermoCoeffs"></param>
        /// <param name="Qohw">Среднечасовой за отопительный период расход тепловой энергии на горячее водоснабжение</param>
        /// <param name="Qlwh">Среднечасовой за летний период расход тепловой энергии на горячее водоснабжение</param>
        /// <param name="dhwConsumptionFact">Фактическое потребление тепловой энергии на ГВС</param>
        public CalculationDHWLoadFactAndNorm(Building b,
            CalculationThermoCoeffs thermoCoeffs,
            double? Qohw, double? Qlhw,
            double[] dhwConsumptionFact)
        {
            _building = b;
            _Qohw = Qohw;
            _Qlhw = Qlhw;
            _dhwConsumptionFact = dhwConsumptionFact;
            _thermoCoeffs = thermoCoeffs;
            _normativeConsumption = new List<double?>();

            InitTable();
        }

        /// <summary>
        /// Нормативное потребление тепловой энергии на отопление
        /// </summary>
        public IList<double?> NormativeConsumption { get { return _normativeConsumption; } }

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }

        private void InitTable()
        {
            _table = new TemplateTable();

            _table.AddRow(new string[] { 
                "Месяц",
                "Средняя часовая тепловая нагрузка на ГВС, Гкал/ч",
                "Количество часов в месяце, час",
                "Нормативное потребление тепловой энергии на ГВС, Гкал",
                "Фактическое потребление тепловой энергии на ГВС, Гкал"
            }, TemplateTableRowType.Header);

            double? norm;
            _normativeConsumption.Clear();

            LoadRow(0, "Январь", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(1, "Февраль", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(2, "Март", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(3, "Апрель", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(4, "Май", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(5, "Июнь", _Qlhw, out norm); _normativeConsumption.Add(norm);
            LoadRow(6, "Июль", _Qlhw, out norm); _normativeConsumption.Add(norm);
            LoadRow(7, "Август", _Qlhw, out norm); _normativeConsumption.Add(norm);
            LoadRow(8, "Сентябрь", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(9, "Октябрь", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(10, "Ноябрь", _Qohw, out norm); _normativeConsumption.Add(norm);
            LoadRow(11, "Декабрь", _Qohw, out norm); _normativeConsumption.Add(norm);

            NormativeTotal = _normativeConsumption.Sum();

            _table.AddRow(new object[]{
                "ИТОГО:",
                "",
                "",
                NormativeTotal,
                _dhwConsumptionFact.Sum()
            }, TemplateTableRowType.Footer);
        }

        private void LoadRow(int index, string monthName, double? Q, out double? normativeConsumption)
        {
            double? col2 = Q;
            int? col3 = (int?)_thermoCoeffs.HoursPerMonth[index];
            normativeConsumption = (col3.HasValue && col2.HasValue) ? (double?)(col3.Value * col2.Value) : null;
            double? col5 = _dhwConsumptionFact.Length > index ? (double?)_dhwConsumptionFact[index] : null;

            _table.AddRow(new object[] {
                monthName,
                col2,
                col3, 
                normativeConsumption,
                col5
            });
        }


    }
}
