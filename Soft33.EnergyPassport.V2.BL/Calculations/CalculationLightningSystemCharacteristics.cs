﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.EnergyPassport.V2.BL.Extended;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Характеристика системы освещения
    /// </summary>
    public class CalculatioLightningSystemCharacteristics
    {
        private IEnumerable<Building> _buildings;

        public CalculatioLightningSystemCharacteristics(IEnumerable<Building> buildings)
        {
            _buildings = buildings;

            InitTable();
        }

        public double LightsEnergyConsumptionByYear { get; private set; }

        public double LightsPower { get; private set; }

        private TemplateTable _table;

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }


        private void InitTable()
        {
            _table = new TemplateTable();

            const int COLUMNS_COUNT = 8;

            LightsPower = 0D;
            LightsEnergyConsumptionByYear = 0D;

            foreach (var b in _buildings)
            {
                double sumPower = b.InternalLightingEquipment.SummaryPower;
                _table.AddRow(new object[COLUMNS_COUNT] { 
                            b.GeneralInformation.Name,
                            b.WallingOfBuilding.BuildingYardage,
                            null,
                            null,
                            null,
                            null,
                            sumPower / 1000,
                            sumPower / b.WallingOfBuilding.BuildingYardage
                        });

                LightsPower += sumPower / 1000;
                LightsEnergyConsumptionByYear += b.InternalLightingEquipment.SummaryWork / 1000;
            }
        }
    }
}
