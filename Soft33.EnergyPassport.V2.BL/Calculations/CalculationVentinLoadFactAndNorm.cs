﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;

namespace Soft33.EnergyPassport.V2.BL.Calculations
{
    /// <summary>
    /// Фактическая и нормативная тепловые нагрузки на приточную вентиляцию за 2011г.
    /// </summary>
    public class CalculationVentinLoadFactAndNorm
    {
        private Building _building;
        private TemplateTable _table;
        private CalculationThermoCoeffs _thermoCoeffs;
        private List<double?> _normativeConsumption;
        private double? _Qvr;
        private double[] _ventinConsumptionFact;

        /// <summary>
        /// Сумма столбца нормативного потребления
        /// </summary>
        public double? NormativeTotal { get; set; }

        public CalculationVentinLoadFactAndNorm(Building b,
            CalculationThermoCoeffs thermoCoeffs,
            double? Qvr,
            double[] ventinConsumptionFact)
        {
            _building = b;
            _Qvr = Qvr;
            _ventinConsumptionFact = ventinConsumptionFact;
            _thermoCoeffs = thermoCoeffs;
            _normativeConsumption = new List<double?>();

            InitTable();
        }

        /// <summary>
        /// Нормативное потребление тепловой энергии на отопление
        /// </summary>
        public IList<double?> NormativeConsumption { get { return _normativeConsumption; } }

        public TemplateTable Table
        {
            get
            {
                return _table;
            }
        }

        private void InitTable()
        {
            _table = new TemplateTable();

            _table.AddRow(new string[] { 
                "Месяц",
                "Средняя часовая тепловая нагрузка на вентиляцию, Гкал/ч",
                "Количество часов в месяце, час",
                "Нормативное потребление тепловой энергии на вентиляцию, Гкал",
                "Фактическое потребление тепловой энергии на вентиляцию, Гкал"
            }, TemplateTableRowType.Header);

            double? norm;
            _normativeConsumption.Clear();

            LoadRow(0, "Январь", out norm); _normativeConsumption.Add(norm);
            LoadRow(1, "Февраль", out norm); _normativeConsumption.Add(norm);
            LoadRow(2, "Март", out norm); _normativeConsumption.Add(norm);
            LoadRow(3, "Апрель", out norm); _normativeConsumption.Add(norm);
            LoadRow(4, "Май", out norm); _normativeConsumption.Add(norm);
            LoadRow(5, "Июнь", out norm); _normativeConsumption.Add(norm);
            LoadRow(6, "Июль", out norm); _normativeConsumption.Add(norm);
            LoadRow(7, "Август", out norm); _normativeConsumption.Add(norm);
            LoadRow(8, "Сентябрь", out norm); _normativeConsumption.Add(norm);
            LoadRow(9, "Октябрь", out norm); _normativeConsumption.Add(norm);
            LoadRow(10, "Ноябрь", out norm); _normativeConsumption.Add(norm);
            LoadRow(11, "Декабрь", out norm); _normativeConsumption.Add(norm);

            NormativeTotal = _normativeConsumption.Sum();

            _table.AddRow(new object[]{
                "ИТОГО:",
                "",
                "",
                NormativeTotal,
                _ventinConsumptionFact.Sum()
            }, TemplateTableRowType.Footer);
        }

        private void LoadRow(int index, string monthName, out double? normativeConsumption)
        {
            double? col2 = _Qvr;
            int? col3 = (int?)_thermoCoeffs.HoursPerMonth[index];
            normativeConsumption = (col3.HasValue && col2.HasValue) ? (double?)(col3.Value * col2.Value) : null;
            double? col5 = _ventinConsumptionFact.Length > index ? (double?)_ventinConsumptionFact[index] : null;

            _table.AddRow(new object[] {
                monthName,
                col2,
                col3, 
                normativeConsumption,
                col5
            });
        }


    }
}
