﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class WallLayer
    {
        public WallLayer()
        {
            Material = new WallMaterial();
        }

        public WallMaterial Material { get; set; }

        /// <summary>
        /// Толщина слоя в метрах
        /// </summary>
        public double Thickness { get; set; }

        /// <summary>
        /// Процент износа
        /// </summary>
        public double WearPercantage { get; set; }

        public string TechnicalCondition { get; set; }


        /// <summary>
        /// Содержит ли объект полезную информацию, которую следует сохранить или нет.
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            return ((Material == null || Material.WallMaterialUID == Guid.Empty)
                && Thickness == 0D
                && WearPercantage == 0D
                && String.IsNullOrEmpty(TechnicalCondition)
                );
        }
    }
}
