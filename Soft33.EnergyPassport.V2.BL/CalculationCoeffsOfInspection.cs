﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Собраны коэф-ты которые влияют на расчет по обследованию
    /// </summary>
    public class CalculationCoeffsOfInspection
    {
        public CalculationCoeffsOfInspection()
        {

        }

        /// <summary>
        /// Тариф на тепловую энергию
        /// </summary>
        public double? ThermoEnergyTariff { get; set; }

        /// <summary>
        /// Тариф на хол. воду
        /// </summary>
        public double? ColdWaterTariff { get; set; }

        /// <summary>
        /// Тариф на гор. воду
        /// </summary>
        public double? HotWaterTariff { get; set; }
    }
}
