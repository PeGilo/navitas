﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Staff
    {
        public Staff()
        {
            Units = new List<StaffUnit>();
        }

        public List<StaffUnit> Units { get; set; }
    }
}
