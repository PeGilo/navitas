﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Электроприёмники
    /// </summary>
    public class Electroload
    {
        /// <summary>
        /// Наименование оборудования
        /// </summary>
        //[DisplayName("Наименование оборудования")]
        //[TableColumn("Наименование оборудования")]
        public string Name { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        //[DisplayName("Количество")]
        //[TableColumn("Количество")]
        public int Count { get; set; }

        /// <summary>
        /// Мощность
        /// </summary>
        //[DisplayName("Мощность")]
        //[TableColumn("Мощность")]
        public double Power { get; set; }

        /// <summary>
        /// Суммарная мощность
        /// </summary>
        //[DisplayName("Суммарная мощность")]
        //[TableColumn("Суммарная мощность")]
        public double SummaryPower
        {
            get { return Power * Count; }
        }

        public double TimeHoursPerYear { get; set; }

        public double Coefficient
        {
            get
            {
                if (TimeHoursPerYear != 0)
                {
                    return TimeHoursPerYear / 8760;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Суммарная мощность на кол-во часов в год
        /// </summary>
        [XmlIgnore]
        public double SummaryWork { get { return SummaryPower * TimeHoursPerYear; } }

        public bool IsEmpty()
        {
            return (SummaryPower == default(double)
                && Power == default(double)
                && Count == default(int)
                && TimeHoursPerYear == default(double)
                && String.IsNullOrEmpty(Name)
                );
        }
    }
}
