﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class VentilationExtractList
    {
        public VentilationExtractList()
        {
            VentilationExtracts = new List<VentilationExtract>();
        }

        public List<VentilationExtract> VentilationExtracts;
    }
}
