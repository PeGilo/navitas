﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public enum BasementType
    {
        Undefined = 0,

        // Тёплый
        Warm = 1,

        // Холодный
        Cold = 2,

        // Отсутствует
        None = 3
    }
}
