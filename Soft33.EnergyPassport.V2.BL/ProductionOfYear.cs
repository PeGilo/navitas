﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class ProductionOfYear
    {
        public string NomenclatureOfMainProducts
        {
            get;
            set;
        }

        public string CodeOfMainProducts
        {
            get;
            set;
        }

        public float VolumeOfProduction
        {
            get;
            set;
        }

        public float VolumeOfMainProduction
        {
            get;
            set;
        }

        public float VolumeOfAdditionalProduction
        {
            get;
            set;
        }

        public float VolumeOfConsumptionOfEnergyResourcesOnRangeMainProducts
        {
            get;
            set;
        }

        public float ProductionInPhysicalTerms
        {
            get;
            set;
        }

        public float MainProductionInPhysicalTerms
        {
            get;
            set;
        }
    }
}
