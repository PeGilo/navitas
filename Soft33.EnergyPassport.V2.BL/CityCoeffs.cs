﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class CityCoeffs
    {
        public int CityCoeffsId { get; set; }

        public string CityName { get; set; }

        public CityCoeffsValues Values { get; set; }

        public CityCoeffs()
        {
            Values = new CityCoeffsValues();
        }

        public CityCoeffs(int id, string name)
            : this()
        {
            CityCoeffsId = id;
            CityName = name;
        }
    }
}
