﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Window
    {
        public Guid WindowTypeUID { get; set; }

        public string WindowTypeCaption { get; set; }

        public double ThermalResistance { get; set; }

        public int NumberOfWindows { get; set; }

        public double AverageYardage { get; set; }

        public bool IsEmpty()
        {
            return (WindowTypeUID == Guid.Empty
                && ThermalResistance == 0.0
                && NumberOfWindows == 0
                && AverageYardage == 0.0
                );
        }
    }
}
