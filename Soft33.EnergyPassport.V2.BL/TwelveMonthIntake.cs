﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Soft33.EnergyPassport.V2.BL
{
    public class TwelveMonthIntake
    {
        public TwelveMonthIntake()
        {
            Month1 = new IntakeOfPeriod();
            Month2 = new IntakeOfPeriod();
            Month3 = new IntakeOfPeriod();
            Month4 = new IntakeOfPeriod();
            Month5 = new IntakeOfPeriod();
            Month6 = new IntakeOfPeriod();
            Month7 = new IntakeOfPeriod();
            Month8 = new IntakeOfPeriod();
            Month9 = new IntakeOfPeriod();
            Month10 = new IntakeOfPeriod();
            Month11 = new IntakeOfPeriod();
            Month12 = new IntakeOfPeriod();
        }

        public IntakeOfPeriod Month1;
        public IntakeOfPeriod Month2;
        public IntakeOfPeriod Month3;
        public IntakeOfPeriod Month4;
        public IntakeOfPeriod Month5;
        public IntakeOfPeriod Month6;
        public IntakeOfPeriod Month7;
        public IntakeOfPeriod Month8;
        public IntakeOfPeriod Month9;
        public IntakeOfPeriod Month10;
        public IntakeOfPeriod Month11;
        public IntakeOfPeriod Month12;

        [XmlIgnore]
        public Dictionary<int, IntakeOfPeriod> IntakeOfMonths
        {
            get
            {
                Dictionary<int, IntakeOfPeriod> res = new Dictionary<int, IntakeOfPeriod>();
                res.Add(1, Month1);
                res.Add(2, Month2);
                res.Add(3, Month3);
                res.Add(4, Month4);
                res.Add(5, Month5);
                res.Add(6, Month6);
                res.Add(7, Month7);
                res.Add(8, Month8);
                res.Add(9, Month9);
                res.Add(10, Month10);
                res.Add(11, Month11);
                res.Add(12, Month12);

                return res;
            }
        }
    }
}
