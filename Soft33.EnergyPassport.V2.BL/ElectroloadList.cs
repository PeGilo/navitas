﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class ElectroloadList
    {
        public ElectroloadList()
        {
            Electroloads = new List<Electroload>();
        }

        public List<Electroload> Electroloads;

        public double SummaryPower
        {
            get
            {
                double res = 0;

                foreach (Electroload el in Electroloads)
                {
                    res = res + el.SummaryPower;
                }

                return res;
            }
        }

        public double SummaryPowerCoefficient
        {
            get
            {
                double res = 0;

                foreach (Electroload el in Electroloads)
                {
                    res = res + el.SummaryPower * el.Coefficient;
                }

                return res;
            }
        }

        /// <summary>
        /// Суммарное по всем списку Мощность * Кол-во часов в год
        /// </summary>
        public double SummaryWork
        {
            get
            {
                double res = 0;

                foreach (Electroload el in Electroloads)
                {
                    res = res + el.SummaryWork;
                }

                return res;
            }
        }

        public bool IsEmpty()
        {
            return Electroloads.All(eload => eload.IsEmpty());
        }
    }
}
