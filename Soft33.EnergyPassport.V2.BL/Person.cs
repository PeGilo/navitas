﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Должностное лицо
    /// </summary>
    //[TypeConverter(typeof(ExpandableObjectConverter33))]
    public class Person
    {
        public override string ToString()
        {
            return FullName + ", " + Position + ", тел.:" + Phone;
        }

        public Person()
        {
            FullName = "";
            Position = "";
            Phone = "";
            Fax = "";
        }

        public Person(Person other)
        {
            FullName = other.FullName;
            Position = other.Position;
            Phone = other.Phone;
            Fax = other.Fax;
        }

        /// <summary>
        /// Должность
        /// </summary>
        //[DisplayName("1. Должность")]
        //[Description("Должность")]
        //[DocVariable("Должность")]
        public string Position { get; set; }

        /// <summary>
        /// ФИО 
        /// </summary>
        //[DisplayName("2. ФИО")]
        //[Description("ФИО")]
        //[DocVariable("ФИО")]
        public string FullName { get; set; }

        /// <summary>
        /// Телефон 
        /// </summary>
        //[DisplayName("3. Телефон")]
        //[Description("Телефон")]
        //[DocVariable("Телефон")]
        public string Phone { get; set; }

        /// <summary>
        /// Факс 
        /// </summary>
        //[DisplayName("4. Факс")]
        //[Description("Факс")]
        //[DocVariable("Факс")]
        public string Fax { get; set; }
    }
}
