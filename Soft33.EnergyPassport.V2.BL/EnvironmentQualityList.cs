﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class EnvironmentQualityList
    {
        public EnvironmentQualityList()
        {
            Environments = new List<EnvironmentQuality>();
        }

        public List<EnvironmentQuality> Environments { get; set; }
    }
}
