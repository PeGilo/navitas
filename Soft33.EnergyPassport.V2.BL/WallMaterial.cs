﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class WallMaterial
    {
        public string Caption { get; set; }

        public double ThermalResistant { get; set; }

        public Guid WallMaterialUID { get; set; }
    }
}
