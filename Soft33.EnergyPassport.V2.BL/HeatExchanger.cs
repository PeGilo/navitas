﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Теплообменник
    /// </summary>
    public class HeatExchanger
    {
        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// Тип, марка
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Кол-во секций, шт.
        /// </summary>
        public int? SectionsCount { get; set; }

        /// <summary>
        /// Часы работы в год, час
        /// </summary>
        public double? HoursPerYear { get; set; }

        /// <summary>
        /// Примечание (где установлен, работает или нет, в каком состоянии)
        /// </summary>
        public string Comment { get; set; }

        public bool IsEmpty()
        {
            return !(
                !String.IsNullOrEmpty(Purpose)
                || !String.IsNullOrEmpty(Name)
                || SectionsCount.HasValue
                || HoursPerYear.HasValue
                || !String.IsNullOrEmpty(Comment)
                );
        }
    }
}
