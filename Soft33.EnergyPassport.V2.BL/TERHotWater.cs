﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Горячее водоснабжение
    /// </summary>
    public class TERHotWater: TER
    {
        public override string Unit
        {
            get { return "Куб.М."; }
        }

        public override double CoefficientTUT
        {
            get { return 0; }
        }
    }
}
