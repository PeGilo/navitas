﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Коэф-ты для здания связанные с расчетами по воде
    /// </summary>
    public class BuildingWaterCoeffs
    {
        /// <summary>
        /// норма расхода горчей воды в час наибольшего водопотребления на человека
        /// </summary>
        public double? gHW_Max { get; set; }

        /// <summary>
        /// норма расхода холодной воды в час наибольшего водопотребления на человека
        /// </summary>
        public double? gCW_Max { get; set; }
    }
}
