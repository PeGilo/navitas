﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class PowerQualityList
    {
        public PowerQualityList()
        {
            Powers = new List<PowerQuality>();
        }

        public List<PowerQuality> Powers { get; set; }

    }
}
