﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class EnvironmentQuality
    {
        public Guid RoomTypeUID { get; set; }

        public string RoomTypeCaption { get; set; }

        public double Humidity { get; set; }

        public double Illumination { get; set; }

        public double Temperature { get; set; }

        public double WindSpeed { get; set; }

        public bool IsEmpty()
        {
            return (RoomTypeUID == Guid.Empty
                && Humidity == 0.0
                && Illumination == 0.0
                && Temperature == 0.0
                && WindSpeed == 0.0
                );
        }
    }
}
