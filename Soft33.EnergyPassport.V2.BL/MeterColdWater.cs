﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class MeterColdWater: Meter
    {
        /// <summary>
        /// Диапазон измерений
        /// </summary>
        public string MeasurementRange { get; set; }
    }
}
