﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Intake
    {
        public Intake()
        {
            TwelveMonth = new TwelveMonthIntake();

            FiveYear = new FiveYearIntake();
        }

        public TwelveMonthIntake TwelveMonth { get; set; }

        public FiveYearIntake FiveYear { get; set; }
    }
}
