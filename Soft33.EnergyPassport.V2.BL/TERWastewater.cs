﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Водоотведение
    /// </summary>
    public class TERWastewater: TER
    {
        public override string Unit
        {
            get { return "куб.м."; }
        }

        public override double CoefficientTUT
        {
            get { return 0; }
        }
    }
}
