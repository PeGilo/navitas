﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public abstract class TER
    {
        public double Value { get; set; }

        public double ValueTUT 
        {
            get
            {
                return Value * CoefficientTUT;
            }
        }

        public abstract string Unit { get; }

        public abstract double CoefficientTUT { get; }

        public double ValueRUR { get; set; }

        public double Tariff
        {
            get
            {
                if (Value != 0)
                {
                    return ValueRUR / Value;
                }
                else
                {
                    return 0;
                }

            }
        }

        public bool IsZero()
        {
            return Value == default(double);
        }
    }
}
