﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Мазут
    /// </summary>
    public class TERGasoline: TER
    {

        public override string Unit
        {
            get { return "т"; }
        }

        public override double CoefficientTUT
        {
            get { return 1.31; }
        }
    }
}
