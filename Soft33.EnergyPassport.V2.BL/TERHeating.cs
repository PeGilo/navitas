﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Теплоснабжение
    /// </summary>
    public class TERHeating: TER
    {
        public override string Unit
        {
            get { return "Гкал"; }
        }

        public override double CoefficientTUT
        {
            get { return 0.143; }
        }
    }
}
