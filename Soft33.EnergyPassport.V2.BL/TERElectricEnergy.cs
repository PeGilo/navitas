﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Электроэнергия
    /// </summary>
    public class TERElectricEnergy: TER
    {
        public override string Unit
        {
            get { return "кВт*ч"; }
        }

        public override double CoefficientTUT
        {
            get { return 0.000123; }
        }
    }
}
