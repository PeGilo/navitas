﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class SimplePowerQualityItem
    {
        /// <summary>
        /// Время замера (без даты)
        /// </summary>
        public DateTime? Time { get; set; }

        public double? UPhaseA { get; set; }
        public double? UPhaseB { get; set; }
        public double? UPhaseC { get; set; }

        public double? IPhaseA { get; set; }
        public double? IPhaseB { get; set; }
        public double? IPhaseC { get; set; }
    }
}
