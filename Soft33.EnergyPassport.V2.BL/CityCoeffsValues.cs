﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class CityCoeffsValues
    {

        /// <summary>
        /// поправочный коэффициент, учитывающий отличие расчетной температуры наружного воздуха для проектирования отопления
        /// </summary>
        public double a { get; set; }

        /// <summary>
        /// расчетная температура наружного воздуха для проектирования отопления
        /// </summary>
        public double t0 { get; set; }

        /// <summary>
        /// скорость ветра
        /// </summary>
        public double w0 { get; set; }

        /// <summary>
        /// средняя месячная расчетная температура наружного воздуха
        /// </summary>
        public double[] Tout_cp { get; set; }

        /// <summary>
        /// расчетная температура наружного воздуха, при которой определена тепловая нагрузка приточной вентиляции в проекте
        /// </summary>
        public double Tout_ventin { get; set; }

        /// <summary>
        /// средняя расчетная температура наружного воздуха для проектирования приточной вентиляции в местности, где расположено здание
        /// </summary>
        public double Tout_ventincalc { get; set; }

        /// <summary>
        /// фактическая продолжительность отопительного периода в отчетном (базовом) году
        /// </summary>
        public int Zh_default { get; set; }

        /// <summary>
        /// фактическая средняя температура внутреннего воздуха за отопительный период в отчетном (базовом) году
        /// </summary>
        public double Tint_default { get; set; }

        /// <summary>
        /// фактическая средняя температура наружного воздуха за отопительный период 
        /// </summary>
        public double Tout_default { get; set; }

        /// <summary>
        /// Кол-во отопительных дней по месяцам
        /// </summary>
        public int[] HeatingDaysByMonth { get; set; }

        public CityCoeffsValues()
        {
            //ThermoParameters = new CityThermoParameters();
            Tout_cp = new double[12];
            HeatingDaysByMonth = new int[12];
        }
    }
}
