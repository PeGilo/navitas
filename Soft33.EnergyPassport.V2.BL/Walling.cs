﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Walling
    {
        public Walling()
        {
            Walls = new List<WallLayer>();
            AtticFloor = new List<WallLayer>();
            BasementFloor = new List<WallLayer>();
            Windows = new List<Window>();
            Doors = new List<Door>();
            BuildingThermoCoeffs = new BuildingThermoCoeffs();
            BuildingWaterCoeffs = new BuildingWaterCoeffs();
            BuildingCommonCoeffs = new BuildingCommonCoeffs();
            BuildingWallingCoeffs = new BuildingWallingCoeffs();
        }

        /// <summary>
        /// Стены
        /// </summary>
        public List<WallLayer> Walls { get; set; }

        /// <summary>
        /// Чердачное перекрытие
        /// </summary>
        public List<WallLayer> AtticFloor { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public List<WallLayer> BasementFloor { get; set; }

        /// <summary>
        /// Тип подвала
        /// </summary>
        public BasementType Basement { get; set; }

        /// <summary>
        /// Коэф-ты для расчетов
        /// </summary>
        public BuildingThermoCoeffs BuildingThermoCoeffs { get; set; }

        /// <summary>
        /// Коэф-ты для расчетов
        /// </summary>
        public BuildingWaterCoeffs BuildingWaterCoeffs { get; set; }

        /// <summary>
        /// разные коэф-ты для расчетов
        /// </summary>
        public BuildingCommonCoeffs BuildingCommonCoeffs { get; set; }

        /// <summary>
        /// Коэф-ты по перекрытиям
        /// </summary>
        public BuildingWallingCoeffs BuildingWallingCoeffs { get; set; }

        /// <summary>
        /// Процент износа ограждающих конструкций
        /// </summary>
        public double WearPercantage
        {
            get
            {
                // Среднеарифметическое ото всех

                double total = Walls.Select(w => w.WearPercantage).Sum()
                    + AtticFloor.Select(w => w.WearPercantage).Sum()
                    + BasementFloor.Select(w => w.WearPercantage).Sum();
                int count = Walls.Count + AtticFloor.Count + BasementFloor.Count;
                if (count != 0)
                {
                    return total / count;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Площадь здания
        /// </summary>
        public double BuildingYardage { get; set; }

        /// <summary>
        /// Площадь застройки
        /// </summary>
        public double ConstructionYardage { get; set; }

        /// <summary>
        /// объем здания по наружному обмеру
        /// </summary>
        public double BuildingVolume { get; set; }

        /// <summary>
        /// Высота здания (L)
        /// </summary>
        public double BuildingHeight { get; set; }

        /// <summary>
        /// Площадь пола
        /// </summary>
        public double BasementFloorYardage { get; set; }

        /// <summary>
        /// Площадь потолка
        /// </summary>
        public double AtticFloorYardage { get; set; }

        /// <summary>
        /// Площадь стен
        /// </summary>
        public double WallsYardage { get; set; }

        /// <summary>
        /// Окна
        /// </summary>
        public List<Window> Windows { get; set; }

        /// <summary>
        /// Двери
        /// </summary>
        public List<Door> Doors { get; set; }
    }
}
