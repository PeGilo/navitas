﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class RadiatorList
    {
        public RadiatorList()
        {
            Radiators = new List<Radiator>();
        }

        public List<Radiator> Radiators;
    }
}
