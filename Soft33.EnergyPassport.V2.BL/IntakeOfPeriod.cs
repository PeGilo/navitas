﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Soft33.EnergyPassport.V2.BL
{
    public class IntakeOfPeriod
    {
        private TERThermalEnergy _thermalEnergy;
        private TERFuelOil _fuelOil;

        public IntakeOfPeriod()
        {
            ElectricEnergy = new TERElectricEnergy();
            ThermalEnergy = new TERThermalEnergy();
            TERHeating = new TERHeating();
            TERDhw = new TERDhw();
            TERVentilationInlet = new TERVentilationInlet();
            ColdWater = new TERColdWater();
            HotWater = new TERHotWater();
            Wastewater = new TERWastewater();
            NaturalGas = new TERNaturalGas();
            Firewood = new TERFirewood();
            Coal = new TERCoal();
            FuelOil = new TERFuelOil();
            TERGasoline = new TERGasoline();
            TERKerosene = new TERKerosene();
            TERDieselFuel = new TERDieselFuel();
            TERGas = new TERGas();
        }

        public TERElectricEnergy ElectricEnergy { get; set; }

        /// <summary>
        /// Суммарная тепловая энергия (Отопление, ГВС, Вентиляция теплая)
        /// </summary>
        public TERThermalEnergy ThermalEnergy {
            get
            {
                _thermalEnergy.Value = TERHeating.Value + TERDhw.Value + TERVentilationInlet.Value;
                _thermalEnergy.ValueRUR = TERHeating.ValueRUR + TERDhw.ValueRUR + TERVentilationInlet.ValueRUR;
                return _thermalEnergy;
            }
            set
            {
                _thermalEnergy = value;
            }
        }
		
        /// <summary>
        /// Отопление
        /// </summary>
		public TERHeating TERHeating { get; set; }

        /// <summary>
        /// ГВС
        /// </summary>
		public TERDhw TERDhw { get; set; }

        /// <summary>
        /// Вентиляция (приточная)
        /// </summary>
		public TERVentilationInlet TERVentilationInlet { get; set; }
		
        /// <summary>
        /// Водоснабжение холодное
        /// </summary>
        public TERColdWater ColdWater { get; set; }

        /// <summary>
        /// Водоснабжение горячее
        /// </summary>
        public TERHotWater HotWater { get; set; }
        public TERWastewater Wastewater { get; set; }
        public TERNaturalGas NaturalGas { get; set; }
        public TERFirewood Firewood { get; set; }
        public TERCoal Coal { get; set; }

        /// <summary>
        /// Суммарное по топливу
        /// </summary>
        public TERFuelOil FuelOil {
            get
            {
                _fuelOil.Value = TERGasoline.Value + TERKerosene.Value + TERDieselFuel.Value + TERGas.Value;
                _fuelOil.ValueRUR = TERGasoline.ValueRUR + TERKerosene.ValueRUR + TERDieselFuel.ValueRUR + TERGas.ValueRUR;
                return _fuelOil;
            }
            set { _fuelOil = value; }
        }

        public TERGasoline TERGasoline { get; set; }
		public TERKerosene TERKerosene { get; set; }
		public TERDieselFuel TERDieselFuel { get; set; }
        public TERGas TERGas { get; set; }

        /// <summary>
        /// ИТОГО платежей
        /// </summary>
        [XmlIgnore]
        public double TotalValueRUR
        {
            get
            {
                return ElectricEnergy.ValueRUR
                    + ThermalEnergy.ValueRUR
                    + TERHeating.ValueRUR
                    + TERDhw.ValueRUR
                    + TERVentilationInlet.ValueRUR
                    + ColdWater.ValueRUR
                    + HotWater.ValueRUR
                    + Wastewater.ValueRUR
                    + NaturalGas.ValueRUR
                    + Firewood.ValueRUR
                    + Coal.ValueRUR
                    + FuelOil.ValueRUR
                    + TERGasoline.ValueRUR
                    + TERKerosene.ValueRUR
                    + TERDieselFuel.ValueRUR
                    + TERGas.ValueRUR;
            }
        }
    }
}
