﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Разные коэф-ты для здания по перекрытиям
    /// </summary>
    public class BuildingWallingCoeffs
    {
        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: Стен
        /// </summary>
        public double? ThermoResistanceWalls { get;set;}

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: Покрытий и перекрытий над проездами
        /// </summary>
        public double? ThermoResistanceDriveway { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: Перекрытий чердачных, над неотапли- ваемыми подпольями и подвалами
        /// </summary>
        public double? ThermoResistanceAtticFloor { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций:Окон и балконных дверей, витрин и витражей
        /// </summary>
        public double? ThermoResistanceDoorsWindows { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций:Фонарей с вертикальным остеклением
        /// </summary>
        public double? ThermoResistanceVerticalLight { get; set; }

        /// <summary>
        /// Нормируемые значения сопротивления теплопередаче ограждающих конструкций: остеклений
        /// </summary>
        public double? ThermoResistanceVerticalGlass { get; set; }

    }
}
