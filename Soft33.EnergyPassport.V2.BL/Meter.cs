﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Прибор учета
    /// </summary>
    public abstract class Meter
    {
        public Guid InstallationSiteUID { get; set; }

        /// <summary>
        /// Место установки
        /// </summary>
        public string InstallationSite { get; set; }

        public Guid CaptionUID { get; set; }

        /// <summary>
        /// Наименование прибора
        /// </summary>
        public string Caption { get; set; }

        public Guid AccuracyClassUID { get; set; }

        /// <summary>
        /// Класс точности
        /// </summary>
        public string AccuracyClass { get; set; }

        /// <summary>
        /// Наличие опломбировки
        /// </summary>
        public bool Sealing { get; set; }

        /// <summary>
        /// Год выпуска
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Дата последней поверки
        /// по-хорошему год и месяц без учета дней
        /// </summary>
        public DateTime? DateOfVerification { get; set; }

        /// <summary>
        /// Заводской номер
        /// </summary>
        public String SerialNumber { get; set; }

        /// <summary>
        /// Комментарий и доп.информация
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Фотография прибора учёта (пока не используется)
        /// </summary>
        public Picture Photo { get; set; }

        /// <summary>
        /// Соответствует требованиям
        /// </summary>
        /// <remarks>Выводится из класса точности. Возможно неопределенное состояние.</remarks>
        [XmlIgnore]
        public bool? CompliesWithRequirements
        {
            get
            {
                double? accuracyClassValue = NumberUtils.ExtractDouble(AccuracyClass);
                return (accuracyClassValue.HasValue && accuracyClassValue.Value >= 1D);
            }
        }

        public bool IsEmpty()
        {
            return (String.IsNullOrEmpty(InstallationSite)
                && String.IsNullOrEmpty(Caption)
                && String.IsNullOrEmpty(AccuracyClass)
                && !Year.HasValue
                && !DateOfVerification.HasValue
                && String.IsNullOrEmpty(SerialNumber)
                && String.IsNullOrEmpty(Comment));
        }
    }
}
