﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Radiator
    {
        //Этаж установки
        public string Location { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public int CountThermostats { get; set; }
    }
}
