﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class StaffUnit
    {
        //ФИО
        public string FIO { get; set; }

        //Должность
        public string Position { get; set; }

        //Наименование образовательного учреждения
        public string EducationalOrganizationName { get; set; }

        //Адрес образовательного учреждения
        public string EducationalOrganizationAddress { get; set; }

        //Лицензия образовательного учреждения
        public string EducationalOrganizationLicense { get; set; }

        //Наименование курса
        public string CourseTitle { get; set; }

        //Тип курса
        public string CourseType { get; set; }

        //Дата начала курса
        public DateTime? StartDate { get; set; }

        //Дата окончания курса
        public DateTime? ExpirationDate { get; set; }

        //Диплом об обучении
        public string EducationalDiplom { get; set; }

        //Квалификация
        public string Qualification { get; set; }
    }
}
