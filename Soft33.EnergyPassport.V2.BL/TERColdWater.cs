﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Холодное водоснабжение
    /// </summary>
    public class TERColdWater: TER
    {
        public override string Unit
        {
            get { return "Куб.М."; }
        }

        public override double CoefficientTUT
        {
            get { return 0; }
        }
    }
}
