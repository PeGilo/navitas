﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Soft33.EnergyPassport.V2.BL
{
    public class FiveYearIntake
    {
        public FiveYearIntake()
        {
            YearMinus0 = new IntakeOfPeriod();
            YearMinus1 = new IntakeOfPeriod();
            YearMinus2 = new IntakeOfPeriod();
            YearMinus3 = new IntakeOfPeriod();
            YearMinus4 = new IntakeOfPeriod();
        }

        public IntakeOfPeriod YearMinus0;
        public IntakeOfPeriod YearMinus1;
        public IntakeOfPeriod YearMinus2;
        public IntakeOfPeriod YearMinus3;
        public IntakeOfPeriod YearMinus4;

        [XmlIgnore]
        public Dictionary<int, IntakeOfPeriod> IntakeOfYears
        {
            get
            {
                Dictionary<int, IntakeOfPeriod> res = new Dictionary<int, IntakeOfPeriod>();
                res.Add(0, YearMinus0);
                res.Add(-1, YearMinus1);
                res.Add(-2, YearMinus2);
                res.Add(-3, YearMinus3);
                res.Add(-4, YearMinus4);

                return res;
            }
        }
    }
}
