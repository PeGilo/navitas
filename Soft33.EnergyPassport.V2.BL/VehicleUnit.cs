﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    //Транспортное средство
    public class VehicleUnit
    {
        //Марка
        public string Brand { get; set; }

        //Модель
        public string Model { get; set; }

        //Год выпуска
        public int Year { get; set; }

        //Количество однообразных транспортных средств
        public int Count { get; set; }

        //Грузоподъемность, пассажировместимость
        public string Capacity { get; set; }

        //Тип топлива
        public string TypeOfFuel { get; set; }

        //Расход
        public string FuelConsumption { get; set; }

        //Пробег за последний год (в километрах)
        public int MileageLastYear { get; set; }

        //Наработка машиночасов за последний год
        public int OperatingTimeLastYear { get; set; }

        //Объем грузоперевозок (тонн)
        public int VolumeOfCargo { get; set; }

        //Объем пассажироперевозок (чел)
        public int VolumeOfPassengerTraffic { get; set; }

        //Тип одометра
        public string TypeOfOdometer { get; set; }

        //Исправность одометра
        public bool OdometerIsOK { get; set; }

        //Опломбированность одометра
        public bool OdometerSealed { get; set; }

        public VehicleUnit()
        {
            Brand = String.Empty;
            Model = String.Empty;
            Capacity = String.Empty;
            TypeOfFuel = String.Empty;
            FuelConsumption = String.Empty;
            TypeOfOdometer = String.Empty;
            OdometerIsOK = true;
            OdometerSealed = true;
        }

        public bool IsEmpty()
        {
            return !(
                   !String.IsNullOrEmpty(Brand)
                || !String.IsNullOrEmpty(Model)
                || Year != 0
                || Count != 0
                || !String.IsNullOrEmpty(Capacity)
                //|| !String.IsNullOrEmpty(TypeOfFuel)
                || !String.IsNullOrEmpty(FuelConsumption)
                || MileageLastYear != 0
                || OperatingTimeLastYear != 0
                || VolumeOfCargo != 0
                || VolumeOfPassengerTraffic != 0
                //|| !String.IsNullOrEmpty(TypeOfOdometer)
                );
        }

    }
}
