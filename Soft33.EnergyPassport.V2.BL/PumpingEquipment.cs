﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Насосное оборудование
    /// </summary>
    public class PumpingEquipment
    {
        /// <summary>
        /// Назначение насоса
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// Марка насоса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Кол-во, шт.
        /// </summary>
        public int? Count { get; set; }

        /// <summary>
        /// Электродвигатель Марка
        /// </summary>
        public string EngineName { get; set; }

        /// <summary>
        /// Электродвигатель Мощность, кВт	
        /// </summary>
        public double? EnginePower { get; set; }

        /// <summary>
        /// Часы работы в год, час
        /// </summary>
        public double? HoursPerYear { get; set; }

        /// <summary>
        /// Примечание (где установлен, работает или нет, в каком состоянии)
        /// </summary>
        public string Comment { get; set; }

        public bool IsEmpty()
        {
            return !(
                !String.IsNullOrEmpty(Purpose)
                || !String.IsNullOrEmpty(Name)
                || Count.HasValue
                || !String.IsNullOrEmpty(EngineName)
                || EnginePower.HasValue
                || HoursPerYear.HasValue
                || !String.IsNullOrEmpty(Comment)
                );
        }
    }
}
