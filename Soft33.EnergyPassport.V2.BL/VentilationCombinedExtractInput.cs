﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class VentilationCombinedExtractInput
    {
        public string Name { get; set; }

        public string ServedPremises { get; set; }

        public string VentilationSystemBrand { get; set; }

        public double AirFlow { get; set; }

        public double PowerInHeating { get; set; }

        public double PowerInCooling { get; set; }

        public int TimeHoursPerYear { get; set; }

        public int TimeHoursPerMonth { get; set; }

        public int TimeHoursPerYearInColdPeriod { get; set; }
        
        public int TimeHoursPerYearInWarmPeriod { get; set; }

        public string PumpBrand { get; set; }

        public double PumpPower { get; set; }

        public bool AvailabilityOfRecycling { get; set; }

        public bool IsEmpty()
        {
            return (
                String.IsNullOrEmpty(Name)
                && String.IsNullOrEmpty(ServedPremises)
                && String.IsNullOrEmpty(VentilationSystemBrand)
                && AirFlow == default(double)
                && PowerInHeating == default(double)
                && PowerInCooling == default(double)
                && TimeHoursPerYear == default(int)
                && TimeHoursPerMonth == default(int)
                && TimeHoursPerYearInColdPeriod == default(int)
                && TimeHoursPerYearInWarmPeriod == default(int)
                && String.IsNullOrEmpty(PumpBrand)
                && PumpPower == default(double)
                );
        }
    }
}
