﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class PointOfWater
    {
        public Guid PointOfWaterUID { get; set; }

        public string PointOfWaterCaption { get; set; }

        public Guid PointOfWaterDeviceTypeUID { get; set; }

        public string PointOfWaterDeviceTypeCaption { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public string Description { get; set; }

        public bool IsEmpty()
        {
            return (PointOfWaterUID == Guid.Empty 
                && PointOfWaterDeviceTypeUID == Guid.Empty
                && Number == 0
                && String.IsNullOrEmpty(Description)
                );
        }
    }
}
