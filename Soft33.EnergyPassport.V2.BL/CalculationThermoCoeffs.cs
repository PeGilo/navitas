﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Общие данные. Все, что относится к теплоснабжению
    /// </summary>
    public class CalculationThermoCoeffs
    {

        /// <summary>
        /// температура холодной водопроводной воды в отопительный период 
        /// </summary>
        public double tox { get; set; }

        /// <summary>
        /// температура холодной водопроводной воды в летний период 
        /// </summary>
        public double toxl { get; set; }

        /// <summary>
        /// Кол-во часов в месяце
        /// </summary>
        public int[] HoursPerMonth { get; set; }

        /// <summary>
        /// нормативная температура горячей воды
        /// </summary>
        public double tDHW { get; set; }

        /// <summary>
        /// плотность воды
        /// </summary>
        public double p { get; set; }

        /// <summary>
        /// удельная теплоемкость
        /// </summary>
        public double cw { get; set; }

        /// <summary>
        /// коэффициент, учитывающий потери теплоты трубопроводами системы горячего водоснабжения
        /// </summary>
        public double Khi { get; set; }

        /// <summary>
        /// коэффициент для учета потерь теплоты теплопроводами, проложенными в не отапливаемых помещениях
        /// </summary>
        public double Knm { get; set; }

        public CalculationThermoCoeffs()
        {
            //HeatingParameters = new CalculationHeatingParameters();
            HoursPerMonth = new int[12];
        }
    }

}
