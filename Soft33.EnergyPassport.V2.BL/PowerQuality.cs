﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class PowerQuality
    {
        public PowerQuality()
        {
            Simple = new SimplePowerQuality();
                        
            //По умолчанию упрощенный способ оценки качества электроэнергии
            QualityType = PowerQualityType.Simple;
        }

        /// <summary>
        /// Наименование электроввода
        /// </summary>
        public string PowerInput { get; set; }

        public PowerQualityType QualityType { get; set; }

        public SimplePowerQuality Simple { get; set; }

        public GOSTPowerQuality GOST { get; set; }
    }
}
