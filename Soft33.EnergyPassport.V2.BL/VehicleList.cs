﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class VehicleList
    {
        public VehicleList()
        {
            Vehicles = new List<VehicleUnit>();
        }

        public List<VehicleUnit> Vehicles { get; set; }
    }
}
