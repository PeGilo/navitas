﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Коэф-ты для здания связанные с расчетом отопления
    /// </summary>
    public class BuildingThermoCoeffs
    {
        #region "По отоплению"

        /// <summary>
        /// удельная отопительная характеристика здания при от tо = -30С
        /// </summary>
        public double? q0 { get; set; }

        ///// <summary>
        ///// усредненная расчетная температура внутреннего воздуха в отапливаемых помещениях здания
        ///// </summary>
        //public double? Tj { get; set; }

        /// <summary>
        /// усредненная расчетная температура внутреннего воздуха в отапливаемых помещениях здания
        /// </summary>
        public double? Tin { get; set; }

        #endregion

        #region "По ГВС"

        /// <summary>
        /// средний суточный расход горячей воды л/сутки одним пользователем
        /// </summary>
        public double? gDHW { get; set; }

        /// <summary>
        /// количество пользователей ГВС
        /// </summary>
        public double? m { get; set; }

        #endregion

        #region "По Вентиляции"

        /// <summary>
        /// удельная тепловая вентиляционная характеристика здания, зависящая от назначения и 
        /// строительного объема вентилируемого здания, ккал/м3ч°С, принимается по МДС 41-2.2000 (табл.3). 
        /// </summary>
        public double? qv { get; set; }

        #endregion

        #region "По Удельной тепловой характеристике"

        /// <summary>
        /// фактическая продолжительность отопительного периода в отчетном (базовом) году, сут 
        /// </summary>
        /// <remarks>Переопределяет значение из снипа (по-умолчанию)</remarks>
        public int? zh_fact { get; set; }

        /// <summary>
        /// фактическая средняя температура внутреннего воздуха за отопительный период в отчетном (базовом) году
        /// </summary>
        /// <remarks>Переопределяет значение из снипа (по-умолчанию)</remarks>
        public double? Tint_fact { get; set; }

        /// <summary>
        /// фактическая средняя температура наружного воздуха за отопительный период в 2011 году
        /// </summary>
        /// <remarks>Переопределяет значение из снипа (по-умолчанию)</remarks>
        public double? Tout_fact { get; set; }

        ///// <summary>
        ///// Нормативная удельная тепловая характеристика здания
        ///// </summary>
        //public double? NormativeThermalCharacteristics { get; set; }

        #endregion

        /// <summary>
        /// температуру воздуха в наиболее холодный пятидневки
        /// </summary>
        public double? Tout_min { get; set; }
    }
}
