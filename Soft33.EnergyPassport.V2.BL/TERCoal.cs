﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Уголь
    /// </summary>
    public class TERCoal: TER
    {
        public override string Unit
        {
            get { return "т"; }
        }

        public override double CoefficientTUT
        {
            get { return 0.852; }
        }
    }
}
