﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Inspection
    {
        /// <summary>
        /// Кол-во пустых строк для вновь созданного объекта
        /// </summary>
        protected const int EMPTY_ROWS_COUNT = 2;

        protected const int NEW_STAFF_COUNT = 2;
        protected const int NEW_VEHICLES_COUNT = 2;

        protected int? _baseYear;

        public virtual int InspectionID
        {
            get;
            set;
        }

        public virtual System.Guid InspectionGUID
        {
            get;
            set;
        }

        public virtual string FullName
        {
            get;
            set;
        }

        public virtual int? BaseYear
        {
            get
            {
                // Если год не определен, вернуть прошедший.
                return _baseYear.HasValue ? _baseYear : DateTime.Now.AddYears(-1).Year;
            }
            set
            {
                _baseYear = value;
            }
        }

        public virtual int AuditorID { get; set; }

        //public Auditor Auditor { get; set; }

        public virtual int? ExpertID { get; set; }

        public virtual int? InspectionTypeID { get; set; }

        //public InspectionType InspectionType { get; set; }

        public virtual Client Client { get; set; }

        public virtual Production Production { get; set; }

        public virtual Staff Staff { get; set; }

        public virtual VehicleList Vehicle { get; set; }

        public virtual Intake Intake { get; set; }

        //public Transport Transport { get; set; }

        public virtual IList<Building> Buildings { get; set; }

        public virtual CalculationCoeffsOfInspection CalculationCoeffs { get; set; }
        
        public Inspection()
        {
            Client = new Client();
            Production = new Production();
            Staff = new Staff();
            Intake = new Intake();
            Buildings = new List<Building>();
            CalculationCoeffs = new CalculationCoeffsOfInspection();

            // При создании нового обследования, по-умолчанию должно быть несколько пустых строк в Персонале.
            for (int i = 0; i < NEW_STAFF_COUNT; i++)
            {
                Staff.Units.Add(new StaffUnit());
            }
            // и в транспорте
            Vehicle = new VehicleList();
            for (int i = 0; i < NEW_VEHICLES_COUNT; i++)
            {
                Vehicle.Vehicles.Add(new VehicleUnit());
            }
        }

        /// <summary>
        /// Создает объект здания с проинициализированными списками, чтобы они содержали
        /// необходимое число пустых строк
        /// </summary>
        /// <returns></returns>
        public virtual Building CreateBuilding()
        {
            Building building = new Building();

            for (int i = 0; i < EMPTY_ROWS_COUNT; i++)
            {
                building.HouseholdEquipment.Electroloads.Add(new Electroload());
                building.ProcessEquipment.Electroloads.Add(new Electroload());
                building.OfficeEquipment.Electroloads.Add(new Electroload());
                building.HeatingEquipment.Electroloads.Add(new Electroload());
                building.InternalLightingEquipment.Electroloads.Add(new Electroload());
                building.ExternalLightingEquipment.Electroloads.Add(new Electroload());

                building.VentilationCombinedExtractInputs.VentilationCombinedExtractInputs.Add(new VentilationCombinedExtractInput());
                building.VentilationExtracts.VentilationExtracts.Add(new VentilationExtract());

                building.Radiators.Radiators.Add(new Radiator());
                building.WallingOfBuilding.Walls.Add(new WallLayer());
                building.WallingOfBuilding.Doors.Add(new Door());
                building.WallingOfBuilding.BasementFloor.Add(new WallLayer());
                building.WallingOfBuilding.AtticFloor.Add(new WallLayer());
                building.WallingOfBuilding.Windows.Add(new Window());

                building.PowerQualitys.Powers.Add(new PowerQuality());
                building.EnvironmentQualitys.Environments.Add(new EnvironmentQuality());

                building.PointOfWaters.Points.Add(new PointOfWater());

                building.Meters.ElectricEnergy.Add(new MeterElectricEnergy());
                building.Meters.ColdWater.Add(new MeterColdWater());
                building.Meters.HotWater.Add(new MeterHotWater());
                building.Meters.Heating.Add(new MeterHeating());
                building.Meters.Gas.Add(new MeterGas());
            }

            return building;
        }
    }
}