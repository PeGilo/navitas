﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Оборудование ИТП
    /// </summary>
    public class ItpEquipment
    {
        public ItpEquipment()
        {
            HeatExchangers = new List<HeatExchanger>();
            Boilers = new List<Boiler>();
            PumpingEquipment = new List<PumpingEquipment>();
        }

        public List<HeatExchanger> HeatExchangers { get; set; }

        public List<Boiler> Boilers { get; set; }

        public List<PumpingEquipment> PumpingEquipment { get; set; }

        /// <summary>
        /// Кол-во вводов в здании, шт. -
        /// </summary>
        public int? InputCount { get; set; }

        /// <summary>
        /// Кол-во ИТП в здании, шт -
        /// </summary>
        public int? ItpCount { get; set; }

        /// <summary>
        /// Общая протяженность трубопроводов в ИТП, м
        /// </summary>
        public double? PipelinesTotalLength { get; set; }

        /// <summary>
        /// Тип изоляции трубопроводов в ИТП
        /// </summary>
        public string ItpIsolationType { get; set; }
    }
}
