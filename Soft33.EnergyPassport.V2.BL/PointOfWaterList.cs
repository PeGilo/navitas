﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class PointOfWaterList
    {
        public PointOfWaterList()
        {
            Points = new List<PointOfWater>();
        }

        public List<PointOfWater> Points { get; set; }
    }
}
