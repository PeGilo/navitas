﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class ResourceSupplyType
    {
        public ResourceSupplyType(int id, string name)
        {
            ResourceSupplyTypeID = id;
            Name = name;
        }

        public int ResourceSupplyTypeID { get; private set; }

        public string Name { get; private set; }
    }
}
