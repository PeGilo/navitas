﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    public class Illustration
    {
        public int IllustrationID { get; set; }
        public int InspectionID { get; set; }
        public int? BuildingID { get; set; }
        public string DivisionName { get; set; }
        public string UserTitle { get; set; }
        public string FileName { get; set; }
    }

    public class Building
    {
        public Building()
        {
            GeneralInformation = new BuildingGeneralInformation();
            AdditionalInformation = new BuildingAdditionalInformation();

            TwelveMonth = new TwelveMonthIntake();

            HouseholdEquipment = new ElectroloadList();
            ProcessEquipment = new ElectroloadList();
            OfficeEquipment = new ElectroloadList();
            HeatingEquipment = new ElectroloadList();
            InternalLightingEquipment = new ElectroloadList();
            ExternalLightingEquipment = new ElectroloadList();

            Radiators = new RadiatorList();
            VentilationCombinedExtractInputs = new VentilationCombinedExtractInputList();
            VentilationExtracts = new VentilationExtractList();

            WallingOfBuilding = new Walling();

            PowerQualitys = new PowerQualityList();

            EnvironmentQualitys = new EnvironmentQualityList();

            PointOfWaters = new PointOfWaterList();

            Meters = new MeterLists();

            Illustrations = new List<Illustration>();

            ItpEquipment = new ItpEquipment();
        }

        public int BuildingID { get; set; }

        public System.Guid BuildingGUID { get; set; }

        public BuildingGeneralInformation GeneralInformation { get; set; }

        public BuildingAdditionalInformation AdditionalInformation { get; set; }
        
        public TwelveMonthIntake TwelveMonth { get; set; }

        public ElectroloadList HouseholdEquipment { get; set; }
        public ElectroloadList ProcessEquipment { get; set; }
        public ElectroloadList OfficeEquipment { get; set; }
        public ElectroloadList HeatingEquipment { get; set; }
        public ElectroloadList InternalLightingEquipment { get; set; }
        public ElectroloadList ExternalLightingEquipment { get; set; }

        public RadiatorList Radiators { get; set; }

        public VentilationCombinedExtractInputList VentilationCombinedExtractInputs { get; set; }

        public VentilationExtractList VentilationExtracts { get; set; }

        public Walling WallingOfBuilding { get; set; }

        public PowerQualityList PowerQualitys { get; set; }

        public EnvironmentQualityList EnvironmentQualitys { get; set; }

        public PointOfWaterList PointOfWaters { get; set; }

        public MeterLists Meters { get; set; }

        public IList<Illustration> Illustrations { get; set; }

        public ItpEquipment ItpEquipment { get; set; }
    }
}
