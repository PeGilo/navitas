﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL
{
    /// <summary>
    /// Заказчик
    /// </summary>
    //[TypeConverter(typeof(ExpandableObjectConverter33))]
    //Проверка проекта
    public class Client
    {
        public Client()
        {
            Okved = new OKVED();
            Head = new Person();
            TechnicalPerson = new Person();
            EnergyPerson = new Person();
        }

        //public override string ToString()
        //{
        //    return Name + ", " + JuridicalAddress;
        //}

        /// <summary>
        /// Наименование
        /// </summary>
        //[DisplayName("01. Краткое наименование юридического лица")]
        //[Description("Например, Тагильская средняя школа №1")]
        //[DocVariable("Краткое наименование юридического лица")]
        public string Name { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        //[DisplayName("03. Полное наименование юридического лица")]
        //[Description("Например, МБУЗ Тагильская средняя школа №1")]
        //[DocVariable("Полное наименование юридического лица")]
        public string FullName { get; set; }

        /// <summary>
        /// Организационно правовая форма
        /// </summary>
        //[DisplayName("02. Организационно правовая форма")]
        //[Description("Например, Общество с ограниченной ответственностью или Краевое государственное учреждение")]
        //[DocVariable("Организационно правовая форма")]
        public string OPF { get; set; }

        /// <summary>
        /// Фактический адрес
        /// </summary>
        //[DisplayName("04. Фактический адрес")]
        //[Description("Фактическое местонахождение юридического лица")]
        //[DocVariable("Фактический адрес")]
        public string PhysicalAddress { get; set; }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        //[DisplayName("05. Юридический адрес")]
        //[Description("Юридический адрес")]
        //[DocVariable("Юридический адрес")]
        public string JuridicalAddress { get; set; }

        /// <summary>
        /// Наименование основного общества
        /// </summary>
        //[DisplayName("06. Наименование основного общества")]
        //[Description("Для зависимых обществ")]
        //[DocVariable("Наименование основного общества")]
        public string NameOfParentCompany { get; set; }

        /// <summary>
        /// Доля муниципальной, государственной обственности
        /// </summary>
        //[DisplayName("07. Доля муниципальной, государственной обственности")]
        //[Description("с указанием единицы, например, 75%")]
        //[DocVariable("Доля муниципальной, государственной обственности")]
        public string ShareOfOwnership { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        //[DisplayName("08. ИНН")]
        //[Description("ИНН")]
        //[DocVariable("ИНН")]
        public string INN { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        //[DisplayName("09. КПП")]
        //[Description("КПП")]
        //[DocVariable("КПП")]
        public string KPP { get; set; }

        /// <summary>
        /// Расчётный счёт
        /// </summary>
        //[DisplayName("10. Расчётный счёт")]
        //[Description("Расчётный счёт, полная формулировка, например, 407883738684585 в Красноярском филиале ОАО \"ВТБ24\" корр.счёт 301234234223, БИК 040456768 г.Красноярск")]
        //[DocVariable("Расчётный счёт")]
        public string CurrentAccount { get; set; }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        //[DisplayName("11. ОКВЭД")]
        //[Description("ОКВЭД")]
        //[DocVariable("ОКВЭД")]
        public OKVED Okved { get; set; }

        /// <summary>
        /// Руководитель
        /// </summary>
        //[DisplayName("12. Руководитель")]
        //[Description("Руководитель юридического лица")]
        //[Report("Руководитель")]
        public Person Head { get; set; }

        /// <summary>
        /// Ответственный
        /// </summary>
        //[DisplayName("13. Ответственный за энергообследование")]
        //[Description("Ответственный за проведение энергетического обследования")]
        //[Report("Ответственный за энергообследование")]
        public Person EnergyPerson { get; set; }

        /// <summary>
        /// Энергетик
        /// </summary>
        //[DisplayName("14. Энергетик")]
        //[Description("Энергетик")]
        //[Report("Энергетик")]
        public Person TechnicalPerson { get; set; }
    }
}
