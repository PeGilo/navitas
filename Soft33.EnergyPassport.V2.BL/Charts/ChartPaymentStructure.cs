﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Рисунок 2.1 – Структура платежей в 2011 г.
    /// </summary>
    public class ChartPaymentStructure
    {
        private Inspection _inspection;

        public ChartPaymentStructure(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    // Инициализируем свойства графика
                    _chart.Type = ChartType.Pie;
                    _chart.XAxisTitle = "";
                    _chart.YAxisTitle = "";
                    TemplateSeries series1 = new TemplateSeries();
                    series1.Title = "Series 1";

                    // Загружаем данные

                    // Берем последний год из данных по организации
                    BL.IntakeOfPeriod intake = _inspection.Intake.FiveYear.YearMinus0;

                    double total = intake.ElectricEnergy.ValueRUR
                        + intake.ThermalEnergy.ValueRUR
                        + intake.ColdWater.ValueRUR
                        + intake.HotWater.ValueRUR
                        + intake.Wastewater.ValueRUR
                        + intake.NaturalGas.ValueRUR
                        + intake.Firewood.ValueRUR
                        + intake.Coal.ValueRUR
                        + intake.FuelOil.ValueRUR;

                    Dictionary<string, double> values = new Dictionary<string, double>()
                    {
                        { "Электроэнергия" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.ElectricEnergy.ValueRUR, total)) + "%)", 
                            intake.ElectricEnergy.ValueRUR },
                        { "Тепловая энергия" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.ThermalEnergy.ValueRUR, total)) + "%)", 
                            intake.ThermalEnergy.ValueRUR },
                        { "ХВС" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.ColdWater.ValueRUR, total)) + "%)", 
                            intake.ColdWater.ValueRUR },
                        { "ГВС" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.HotWater.ValueRUR, total)) + "%)", 
                            intake.HotWater.ValueRUR },
                        { "Водоотведение" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.Wastewater.ValueRUR, total)) + "%)", 
                            intake.Wastewater.ValueRUR },
                        { "Природный газ (кроме моторного топлива" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.NaturalGas.ValueRUR, total)) + "%)", 
                            intake.NaturalGas.ValueRUR },
                        { "Дрова" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.Firewood.ValueRUR, total)) + "%)", 
                            intake.Firewood.ValueRUR },
                        { "Уголь" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.Coal.ValueRUR, total)) + "%)", 
                            intake.Coal.ValueRUR },
                        { "Моторное топливо" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(intake.FuelOil.ValueRUR, total)) + "%)", 
                            intake.FuelOil.ValueRUR }
                    };

                    List<string> xvalues = new List<string>();
                    List<double> yvalues = new List<double>();

                    // Удаляем нулевые показатели
                    foreach(var el in values){
                        if(el.Value != 0.0){
                            xvalues.Add(el.Key);
                            yvalues.Add(el.Value);
                        }
                    }

                    series1.xValues = xvalues.ToArray(); // new string[] { "Электроэнергия", "Тепловая энергия", "Холодное водоснабжение", "Водоотведение" };
                    series1.yValues = yvalues.ToArray(); //new double[] { 10, 20, 30, 0 };

                    _chart.Series.Add(series1);
                }
                return _chart;
            }
        }
    }
}
