﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Рисунок 4.5.1 - Динамика потребления тепловой энергии за 2011 г.
    /// </summary>
    public class ChartHeatEnergyConsumptionDynamic
    {
        private Inspection _inspection;

        public ChartHeatEnergyConsumptionDynamic(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    _chart.Type = ChartType.Column;
                    _chart.XAxisTitle = "Месяц";
                    _chart.YAxisTitle = "Потребление энергоресурса (Гкал)";
                    _chart.LegendEnabled = true;

                    string[] xvalues = new string[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };

                    double[] yvalues = Enumerable.Range(1, 12)
                        .Select(i => _inspection.Intake.TwelveMonth.IntakeOfMonths[i].TERHeating.Value).ToArray();
                    _chart.AddSeries("Отопление", xvalues, yvalues);

                    yvalues = Enumerable.Range(1, 12)
                        .Select(i => _inspection.Intake.TwelveMonth.IntakeOfMonths[i].TERDhw.Value).ToArray();
                    _chart.AddSeries("ГВС", xvalues, yvalues);

                    yvalues = Enumerable.Range(1, 12)
                        .Select(i => _inspection.Intake.TwelveMonth.IntakeOfMonths[i].TERVentilationInlet.Value).ToArray();
                    _chart.AddSeries("Вентиляция", xvalues, yvalues);

                }
                return _chart;
            }
        }

    }
}
