﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    public class TemplateSeries
    {
        public string Title;

        public object[] xValues;
        public double[] yValues;

        public TemplateSeries()
        {
            Title = String.Empty;

            xValues = new object[0];
            yValues = new double[0];
        }

        public bool IsEmpty()
        {
            return xValues.Length == 0 || yValues.Length == 0;
        }
    }
}
