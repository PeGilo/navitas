﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soft33.EnergyPassport.V2.BL.Extended;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Нормативное и фактическое потребление воды предприятием
    /// </summary>
    public class WaterFlowRegulation
    {
        private Inspection _inspection;
        private InspectionExtended _inspectionExtended;

        public WaterFlowRegulation(Inspection inspection, InspectionExtended inspectionExtended)
        {
            _inspection = inspection;
            _inspectionExtended = inspectionExtended;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    _chart.Type = ChartType.Column;
                    _chart.XAxisTitle = "Потребление холодной воды (куб.м)";
                    _chart.YAxisTitle = "Месяц";
                    _chart.LegendEnabled = true;

                    string[] xvalues = new string[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };

                    double[] yvaluesFact = new double[] {
                        _inspection.Intake.TwelveMonth.Month1.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month2.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month3.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month4.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month5.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month6.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month7.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month8.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month9.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month10.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month11.ColdWater.Value,
                        _inspection.Intake.TwelveMonth.Month12.ColdWater.Value
                    };

                    double[] yvaluesNorm = new double[] {
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow,
                        _inspectionExtended.WaterFlow
                    };

                    TemplateSeries seriesFact = new TemplateSeries();
                    seriesFact.Title = "Фактически расход";
                    seriesFact.xValues = xvalues;
                    seriesFact.yValues = yvaluesFact;

                    TemplateSeries seriesNorm = new TemplateSeries();
                    seriesNorm.Title = "Нормативный расход";
                    seriesNorm.xValues = xvalues;
                    seriesNorm.yValues = yvaluesNorm;

                    _chart.Series.Add(seriesFact);
                    _chart.Series.Add(seriesNorm);
                }

                return _chart;
            }
        }
    }
}
