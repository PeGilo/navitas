﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    public enum PowerQualityType
    {
        Current,
        Voltage
    }

    /// <summary>
    /// Рисунок 3.8.1 - Качество электроэнергии в здании
    /// </summary>
    public class ChartPowerQuality
    {
        //private Building _building;
        private SimplePowerQuality _powerQuality;
        private PowerQualityType _type;
        private string _yAxisName;

        public ChartPowerQuality(SimplePowerQuality powerQuality, PowerQualityType type, string yAxisName)
        {
            _powerQuality = powerQuality;
            _type = type;
            _yAxisName = yAxisName;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    _chart.Type = ChartType.Line;
                    _chart.XAxisTitle = "Время";
                    _chart.YAxisTitle = _yAxisName;// (_type == PowerQualityType.Current) ? "I" : "U";

                    TemplateSeries[] series = new TemplateSeries[3]{
                        new TemplateSeries(),
                        new TemplateSeries(),
                        new TemplateSeries()
                    };
                    series[0].Title = "Фаза 1";
                    series[1].Title = "Фаза 2";
                    series[2].Title = "Фаза 3";

                    List<string>[] xvalues = new List<string>[3]{
                        new List<string>(),
                        new List<string>(),
                        new List<string>()
                    };
                    List<double>[] yvalues = new List<double>[3]{
                        new List<double>(),
                        new List<double>(),
                        new List<double>()
                    };

                    AddPhaseA(xvalues[0], yvalues[0]);
                    AddPhaseB(xvalues[1], yvalues[1]);
                    AddPhaseC(xvalues[2], yvalues[2]);

                    series[0].xValues = xvalues[0].ToArray();
                    series[1].xValues = xvalues[1].ToArray();
                    series[2].xValues = xvalues[2].ToArray();

                    series[0].yValues = yvalues[0].ToArray();
                    series[1].yValues = yvalues[1].ToArray();
                    series[2].yValues = yvalues[2].ToArray();

                    _chart.Series.AddRange(series);
                }

                return _chart;
            }
        }

        private void AddPhaseA(List<string> xvalues, List<double> yvalues)
        {
            for (int i = 0; i < _powerQuality.Count; i++)
            {
                if (_powerQuality[i].Time.HasValue 
                    && (_type == PowerQualityType.Current ? _powerQuality[i].IPhaseA.HasValue : _powerQuality[i].UPhaseA.HasValue) )
                {
                    xvalues.Add(_powerQuality[i].Time.Value.ToString("HH:mm"));

                    yvalues.Add(_type == PowerQualityType.Current ? _powerQuality[i].IPhaseA.Value : _powerQuality[i].UPhaseA.Value);
                }
            }
        }

        private void AddPhaseB(List<string> xvalues, List<double> yvalues)
        {
            for (int i = 0; i < _powerQuality.Count; i++)
            {
                if (_powerQuality[i].Time.HasValue 
                    && (_type == PowerQualityType.Current ? _powerQuality[i].IPhaseB.HasValue : _powerQuality[i].UPhaseB.HasValue) )
                {
                    xvalues.Add(_powerQuality[i].Time.Value.ToString("HH:mm"));

                    yvalues.Add(_type == PowerQualityType.Current ? _powerQuality[i].IPhaseB.Value : _powerQuality[i].UPhaseB.Value);
                }
            }
        }

        private void AddPhaseC(List<string> xvalues, List<double> yvalues)
        {
            for (int i = 0; i < _powerQuality.Count; i++)
            {
                if (_powerQuality[i].Time.HasValue 
                    && (_type == PowerQualityType.Current ? _powerQuality[i].IPhaseC.HasValue : _powerQuality[i].UPhaseC.HasValue) )
                {
                    xvalues.Add(_powerQuality[i].Time.Value.ToString("HH:mm"));

                    yvalues.Add(_type == PowerQualityType.Current ? _powerQuality[i].IPhaseC.Value : _powerQuality[i].UPhaseC.Value);
                }
            }
        }
    }
}
