﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Рисунок 3.7.1 – Фактический и нормативный удельные расходы электроэнергии зданием
    /// </summary>
    public class ChartSpecificEnergyConsumption
    {
        private Building _building;
        private IList<double> _intakeRealSpecificConsumptionList;
        private IList<double> _intakeNormativeSpecificConsumption;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inspection"></param>
        /// <param name="_intakeRealSpecificConsumptionList">Фактический удельный расход эл.энергии по месяцам</param>
        /// <param name="_intakeNormativeSpecificConsumption">Нормативный удельный расход эл.энергии</param>
        public ChartSpecificEnergyConsumption(Building building, IList<double> intakeRealSpecificConsumptionList, IList<double> intakeNormativeSpecificConsumption)
        {
            _building = building;
            _intakeRealSpecificConsumptionList = intakeRealSpecificConsumptionList;
            _intakeNormativeSpecificConsumption = intakeNormativeSpecificConsumption;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    _chart.Type = ChartType.Column;
                    _chart.XAxisTitle = "Месяц";
                    _chart.YAxisTitle = "Удельный расход электроэнергии, кВтч/м.кв.";

                    string[] xvalues = new string[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };

                    // Избавиться от ненормальных чисел
                    for (int i = 0; i < _intakeRealSpecificConsumptionList.Count; i++)
                    {
                        if (Double.IsInfinity(_intakeRealSpecificConsumptionList[i])
                             || Double.IsNaN(_intakeRealSpecificConsumptionList[i]))
                        {
                            _intakeRealSpecificConsumptionList[i] = 0D;
                        }
                    }
                    for (int i = 0; i < _intakeNormativeSpecificConsumption.Count; i++)
                    {
                        if(Double.IsInfinity(_intakeNormativeSpecificConsumption[i])
                            || Double.IsNaN(_intakeNormativeSpecificConsumption[i]))
                        {
                            _intakeNormativeSpecificConsumption[i] = 0D;
                        }
                    }

                    TemplateSeries seriesReal = new TemplateSeries();
                    seriesReal.Title = "Фактический удельный расход";
                    seriesReal.xValues = xvalues;
                    seriesReal.yValues = _intakeRealSpecificConsumptionList.ToArray();
                    _chart.Series.Add(seriesReal);

                    TemplateSeries seriesNormative = new TemplateSeries();
                    seriesNormative.Title = "Нормативный удельный расход";
                    seriesNormative.xValues = xvalues;
                    seriesNormative.yValues = _intakeNormativeSpecificConsumption.ToArray();
                    _chart.Series.Add(seriesNormative);
                }
                return _chart;
            }
        }


        private TemplateSeries CreateSeries(string name, string[] xvalues, double[] yvalues)
        {
            TemplateSeries series = new TemplateSeries();
            series.Title = name;
            series.xValues = xvalues;
            series.yValues = yvalues;
            return series;
        }
    }
}
