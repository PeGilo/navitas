﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Рисунок 3.6.1- Структура расходной части баланса электроэнергии
    /// </summary>
    public class ChartElectricEnergyConsumptionStructure
    {
        private Inspection _inspection;
        private TemplateChartData _chart;

        public ChartElectricEnergyConsumptionStructure(Inspection inspection)
        {
            _inspection = inspection;

            InitChart();
        }

        public string MostConsumingElEnergyEquipment { get; set; }
        public double MostConsumingElEnergyPercent { get; set; }

        public TemplateChartData Chart
        {
            get
            {
                return _chart;
            }
        }


        private void InitChart()
        {
            _chart = new TemplateChartData();

            // Инициализируем свойства графика
            _chart.Type = ChartType.Pie;
            _chart.XAxisTitle = "";
            _chart.YAxisTitle = "";
            TemplateSeries series1 = new TemplateSeries();
            series1.Title = "Series 1";

            // Загружаем данные

            double processEqSummary = 0D,
                   householdEqSummary = 0D,
                   officeEqSummary = 0D,
                   heatingEqSummary = 0D,
                   inLightEqSummary = 0D,
                   exLightEqSummary = 0D,
                   total = 0D;
            
            // Сложить расчетный расход по всем зданиям
            foreach (Building b in _inspection.Buildings)
            {
                processEqSummary += b.ProcessEquipment.SummaryWork;
                householdEqSummary += b.HouseholdEquipment.SummaryWork;
                officeEqSummary += b.OfficeEquipment.SummaryWork;
                heatingEqSummary += b.HeatingEquipment.SummaryWork;
                inLightEqSummary += b.InternalLightingEquipment.SummaryWork;
                exLightEqSummary += b.ExternalLightingEquipment.SummaryWork;
            }
            total = processEqSummary + householdEqSummary + officeEqSummary + heatingEqSummary + inLightEqSummary + exLightEqSummary;

            Dictionary<string, double> values = new Dictionary<string, double>()
                    {
                        { "Технологическое оборудование" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(processEqSummary, total)) + "%)", 
                            processEqSummary },
                        { "Бытовое оборудование" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(householdEqSummary, total)) + "%)", 
                            householdEqSummary },
                        { "Оргтехника" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(officeEqSummary, total)) + "%)", 
                            officeEqSummary },
                        { "Электронагревательные приборы" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(heatingEqSummary, total)) + "%)", 
                            heatingEqSummary },
                        { "Освещение внутреннее" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(inLightEqSummary, total)) + "%)", 
                            inLightEqSummary },
                        { "Освещение наружное" + " (" + NumberUtils.FormatPercents(NumberUtils.Percents(exLightEqSummary, total)) + "%)", 
                            exLightEqSummary }
                    };

            // попутно инициализируем свойства указывающие на максимальную часть и ее проценты
            var maxValue = values.Aggregate((pair1, pair2) => pair1.Value > pair2.Value ? pair1 : pair2);
            MostConsumingElEnergyEquipment = maxValue.Key;
            MostConsumingElEnergyPercent = ( maxValue.Value / total ) * 100;

            List<string> xvalues = new List<string>();
            List<double> yvalues = new List<double>();

            // Удаляем нулевые показатели
            foreach (var el in values)
            {
                if (el.Value != 0.0)
                {
                    xvalues.Add(el.Key);
                    yvalues.Add(el.Value);
                }
            }

            series1.xValues = xvalues.ToArray(); // new string[] { "Электроэнергия", "Тепловая энергия", "Холодное водоснабжение", "Водоотведение" };
            series1.yValues = yvalues.ToArray(); //new double[] { 10, 20, 30, 0 };

            _chart.Series.Add(series1);
        }
    }
}
