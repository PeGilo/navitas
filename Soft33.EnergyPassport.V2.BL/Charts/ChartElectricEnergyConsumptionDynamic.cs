﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Рисунок 2.2 – График динамика потребления энергетических ресурсов
    /// </summary>
    public class ChartElectricEnergyConsumptionDynamic
    {
        private Inspection _inspection;

        public ChartElectricEnergyConsumptionDynamic(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    _chart.Type = ChartType.Column;
                    _chart.XAxisTitle = "Потребление энергоресурса (кВтч)";
                    _chart.YAxisTitle = "Месяц";
                    _chart.LegendEnabled = false;

                    string[] xvalues = new string[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };

                    double[] yvalues = new double[] {
                        _inspection.Intake.TwelveMonth.Month1.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month2.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month3.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month4.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month5.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month6.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month7.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month8.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month9.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month10.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month11.ElectricEnergy.Value,
                        _inspection.Intake.TwelveMonth.Month12.ElectricEnergy.Value
                    };

                    TemplateSeries series = new TemplateSeries();
                    series.Title = "";
                    series.xValues = xvalues;
                    series.yValues = yvalues;

                    _chart.Series.Add(series);
                }
                return _chart;
            }
        }

    }
}
