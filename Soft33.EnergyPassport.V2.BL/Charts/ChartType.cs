﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Тип графика
    /// </summary>
    public enum ChartType
    {
        Column,
        Line,
        Pie
    }
}
