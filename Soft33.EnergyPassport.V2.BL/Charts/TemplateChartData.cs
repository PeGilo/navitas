﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    public class TemplateChartData
    {
        public ChartType Type { get; set; }

        public string XAxisTitle { get; set; }

        public string YAxisTitle { get; set; }

        public bool LegendEnabled { get; set; }

        public List<TemplateSeries> Series { get; set; }

        /// <summary>
        /// Используется только на этапе рендеринга. Можно не инициализировать.
        /// </summary>
        public string ImageFileName { get; set; }

        public TemplateChartData()
        {
            LegendEnabled = true; // by default
            Series = new List<TemplateSeries>();
        }

        public void AddSeries(string title, string[] xvalues, double[] yvalues)
        {
            TemplateSeries series = new TemplateSeries();
            series.Title = title;
            series.xValues = xvalues;
            series.yValues = yvalues;

            Series.Add(series);
        }
    }
}
