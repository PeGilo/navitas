﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Рисунок 2.2 – График динамика потребления энергетических ресурсов
    /// </summary>
    public class ChartEnergyConsumptionDynamic
    {
        private Inspection _inspection;

        public ChartEnergyConsumptionDynamic(Inspection inspection)
        {
            _inspection = inspection;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    _chart.Type = ChartType.Column;
                    _chart.XAxisTitle = "Год";
                    _chart.YAxisTitle = "Отчетное значение потребления энергоресурса (в физических величинах)";

                    string baseYear = _inspection.BaseYear.HasValue ? _inspection.BaseYear.Value.ToString() + " г." : String.Empty;
                    string baseYear_1 = _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 1).ToString() + " г." : String.Empty;
                    string baseYear_2 = _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 2).ToString() + " г." : String.Empty;
                    string baseYear_3 = _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 3).ToString() + " г." : String.Empty;
                    string baseYear_4 = _inspection.BaseYear.HasValue ? (_inspection.BaseYear.Value - 4).ToString() + " г." : String.Empty;

                    string[] xvalues = new string[] { baseYear_4, baseYear_3, baseYear_2, baseYear_1, baseYear };

                    IntakeOfPeriod intake = new IntakeOfPeriod();

                    Dictionary<string, double[]> values = new Dictionary<string, double[]>()
                    {
                        {"Электроэнергия ", CreateRowValues(() => intake.ElectricEnergy) },
                        {"Тепловая энергия",  CreateRowValues(() => intake.ThermalEnergy) },
                        {"Водоснабжение горячее", CreateRowValues(() => intake.HotWater) },
                        {"Водоснабжение холодное",  CreateRowValues(() => intake.ColdWater) },
                        {"Водоотведение общее",  CreateRowValues(() => intake.Wastewater) },
                        {"Дрова",  CreateRowValues(() => intake.Firewood) },
                        {"Уголь",  CreateRowValues(() => intake.Coal) },
                        {"Природный газ (кроме моторного топлива)", CreateRowValues(() => intake.NaturalGas) },
                        {"Моторное топливо", CreateRowValues(() => intake.FuelOil) }
                    };

                    // Удаляем нулевые показатели
                    foreach (var el in values)
                    {
                        if (el.Value.Any(d => d != 0.0))
                        {
                            _chart.Series.Add(CreateSeries(el.Key, xvalues, el.Value));
                        }
                    }
                }
                return _chart;
            }
        }

        private double[] CreateRowValues(Expression<Func<TER>> expression)
        {
            string propertyName = ReflectionHelper.GetPropertyName<TER>(expression);

            PropertyInfo monthProperty = typeof(BL.IntakeOfPeriod).GetProperty(propertyName);

            TER ter_0 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus0, null);
            TER ter_1 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus1, null);
            TER ter_2 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus2, null);
            TER ter_3 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus3, null);
            TER ter_4 = (TER)monthProperty.GetValue(_inspection.Intake.FiveYear.YearMinus4, null);

            return
                new double[] { ter_4.Value, ter_3.Value, ter_2.Value, ter_1.Value, ter_0.Value };
        }

        private TemplateSeries CreateSeries(string name, string[] xvalues, double[] yvalues)
        {
            TemplateSeries series = new TemplateSeries();
            series.Title = name;
            series.xValues = xvalues;
            series.yValues = yvalues;
            return series;
        }
    }
}
