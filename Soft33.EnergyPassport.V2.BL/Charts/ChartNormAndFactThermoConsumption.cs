﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using Soft33.EnergyPassport.V2.BL.TemplateCalculations;
using Soft33.EnergyPassport.V2.BL;
using Soft33.Navitas.Common;

namespace Soft33.EnergyPassport.V2.BL.Charts
{
    /// <summary>
    /// Рисунок 4.13- График фактического и нормативного потребления тепловой энергии за 2011 г.
    /// </summary>
    public class ChartNormAndFactThermoConsumption
    {
        private Building _building;
        private IList<double> _consumptionFact;
        private IList<double?> _consumptionNorm;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="building"></param>
        /// <param name="consumptionFact">Фактическое потребление</param>
        /// <param name="consumptionNorm">Нормативное потребление</param>
        public ChartNormAndFactThermoConsumption(Building building, IList<double> consumptionFact, IList<double?> consumptionNorm)
        {
            _building = building;
            _consumptionFact = consumptionFact;
            _consumptionNorm = consumptionNorm;
        }

        private TemplateChartData _chart;

        public TemplateChartData Chart
        {
            get
            {
                if (_chart == null)
                {
                    _chart = new TemplateChartData();

                    _chart.Type = ChartType.Column;
                    _chart.XAxisTitle = "Месяц";
                    _chart.YAxisTitle = "Потребление, Гкал";

                    string[] xvalues = new string[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };

                   
                    // Заменить ненормальные числа и null на 0

                    TemplateSeries seriesReal = new TemplateSeries();
                    seriesReal.Title = "Фактическое потребление";
                    seriesReal.xValues = xvalues;
                    seriesReal.yValues = _consumptionFact.Select(d => Double.IsInfinity(d) || Double.IsNaN(d) ? 0D : d).ToArray(); 
                    _chart.Series.Add(seriesReal);

                    TemplateSeries seriesNormative = new TemplateSeries();
                    seriesNormative.Title = "Нормируемое потребление";
                    seriesNormative.xValues = xvalues;
                    seriesNormative.yValues = _consumptionNorm.Select(d => (d.HasValue && (!Double.IsInfinity(d.Value) && !Double.IsNaN(d.Value))) ? d.Value : 0D).ToArray();
                    _chart.Series.Add(seriesNormative);
                }
                return _chart;
            }
        }
    }
}
